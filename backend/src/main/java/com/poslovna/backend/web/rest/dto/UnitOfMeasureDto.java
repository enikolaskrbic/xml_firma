package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;


public class UnitOfMeasureDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(max = 255)
    private String name;

    
    @Size(max = 255)
    private String shortName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final UnitOfMeasureDto other = (UnitOfMeasureDto) obj;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((shortName == null && other.shortName != null) || !shortName.equals(other.shortName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "UnitOfMeasureDto[" + "name=" + name + ", shortName=" + shortName + "]";
    }

}

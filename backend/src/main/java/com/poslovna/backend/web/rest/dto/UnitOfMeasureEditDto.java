package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;


public class UnitOfMeasureEditDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    @Size(max = 255)
    private String shortName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final UnitOfMeasureEditDto other = (UnitOfMeasureEditDto) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((shortName == null && other.shortName != null) || !shortName.equals(other.shortName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "UnitOfMeasureEditDto[" + "id=" + id + ", name=" + name + ", shortName=" + shortName + "]";
    }

}

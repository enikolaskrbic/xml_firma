package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import java.time.*;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class DocumentResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    private ZonedDateTime dateOfValue;

    private ZonedDateTime dateOfDelivery;

    private ZonedDateTime dateOfStatement;

    @NotNull
    private Long warehouseId;

    @Size(max = 255)
    private String name;


    private RoleBusinessPartner status;

    @NotNull
    @Size(max = 4)
    private String year;


    private Long warehouseIdOut;


    @Size(max = 255)
    private String warehouseNameOut;

    @NotNull
    @Size(max = 255)
    private String warehouseName;

    @NotNull
    private Long businessPartnerId;

    @NotNull
    @Size(max = 255)
    private String businessPartnerName;

    @NotNull
    private Long businessYearId;

    @NotNull
    @Size(max = 4)
    private String businessYearYear;

    private TypeDocument typeDocument;

    private StatusDocument documentStatus;

    public String getBusinessPartnerName() {
        return businessPartnerName;
    }

    public void setBusinessPartnerName(String businessPartnerName) {
        this.businessPartnerName = businessPartnerName;
    }

    public String getBusinessYearYear() {
        return businessYearYear;
    }

    public void setBusinessYearYear(String businessYearYear) {
        this.businessYearYear = businessYearYear;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateOfValue() {
        return dateOfValue;
    }

    public void setDateOfValue(ZonedDateTime dateOfValue) {
        this.dateOfValue = dateOfValue;
    }

    public ZonedDateTime getDateOfDelivery() {
        return dateOfDelivery;
    }

    public void setDateOfDelivery(ZonedDateTime dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }

    public ZonedDateTime getDateOfStatement() {
        return dateOfStatement;
    }

    public void setDateOfStatement(ZonedDateTime dateOfStatement) {
        this.dateOfStatement = dateOfStatement;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getBusinessPartnerId() {
        return businessPartnerId;
    }

    public void setBusinessPartnerId(Long businessPartnerId) {
        this.businessPartnerId = businessPartnerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleBusinessPartner getStatus() {
        return status;
    }

    public void setStatus(RoleBusinessPartner status) {
        this.status = status;
    }

    public Long getBusinessYearId() {
        return businessYearId;
    }

    public void setBusinessYearId(Long businessYearId) {
        this.businessYearId = businessYearId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Long getWarehouseIdOut() {
        return warehouseIdOut;
    }

    public void setWarehouseIdOut(Long warehouseIdOut) {
        this.warehouseIdOut = warehouseIdOut;
    }

    public String getWarehouseNameOut() {
        return warehouseNameOut;
    }

    public void setWarehouseNameOut(String warehouseNameOut) {
        this.warehouseNameOut = warehouseNameOut;
    }

    public TypeDocument getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(TypeDocument typeDocument) {
        this.typeDocument = typeDocument;
    }

    public StatusDocument getDocumentStatus() {
        return documentStatus;
    }

    public void setDocumentStatus(StatusDocument documentStatus) {
        this.documentStatus = documentStatus;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final DocumentResponseDto other = (DocumentResponseDto) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((dateOfValue == null && other.dateOfValue != null) || !dateOfValue.equals(other.dateOfValue))
            return false;
        if ((dateOfDelivery == null && other.dateOfDelivery != null) || !dateOfDelivery.equals(other.dateOfDelivery))
            return false;
        if ((dateOfStatement == null && other.dateOfStatement != null) || !dateOfStatement.equals(other.dateOfStatement))
            return false;
        if ((warehouseId == null && other.warehouseId != null) || !warehouseId.equals(other.warehouseId))
            return false;
        if ((warehouseName == null && other.warehouseName != null) || !warehouseName.equals(other.warehouseName))
            return false;
        if ((businessPartnerId == null && other.businessPartnerId != null) || !businessPartnerId.equals(other.businessPartnerId))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((status == null && other.status != null) || !status.equals(other.status))
            return false;
        if ((businessYearId == null && other.businessYearId != null) || !businessYearId.equals(other.businessYearId))
            return false;
        if ((year == null && other.year != null) || !year.equals(other.year))
            return false;
        if ((warehouseIdOut == null && other.warehouseIdOut != null) || !warehouseIdOut.equals(other.warehouseIdOut))
            return false;
        if ((warehouseNameOut == null && other.warehouseNameOut != null) || !warehouseNameOut.equals(other.warehouseNameOut))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((dateOfValue == null) ? 0 : dateOfValue.hashCode());
        result = prime * result + ((dateOfDelivery == null) ? 0 : dateOfDelivery.hashCode());
        result = prime * result + ((dateOfStatement == null) ? 0 : dateOfStatement.hashCode());
        result = prime * result + ((warehouseId == null) ? 0 : warehouseId.hashCode());
        result = prime * result + ((warehouseName == null) ? 0 : warehouseName.hashCode());
        result = prime * result + ((businessPartnerId == null) ? 0 : businessPartnerId.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((businessYearId == null) ? 0 : businessYearId.hashCode());
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        result = prime * result + ((warehouseIdOut == null) ? 0 : warehouseIdOut.hashCode());
        result = prime * result + ((warehouseNameOut == null) ? 0 : warehouseNameOut.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "DocumentResponseDto[" + "id=" + id + ", dateOfValue=" + dateOfValue + ", dateOfDelivery=" + dateOfDelivery + ", dateOfStatement=" + dateOfStatement + ", warehouseId=" + warehouseId
                + ", warehouseName=" + warehouseName + ", businessPartnerId=" + businessPartnerId + ", name=" + name + ", status=" + status + ", businessYearId=" + businessYearId + ", year=" + year
                + ", warehouseIdOut=" + warehouseIdOut + ", warehouseNameOut=" + warehouseNameOut + "]";
    }

}

package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class AnalyticsResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    private TypeAnalytics type;

    @NotNull
    private BigDecimal quantity;

    @NotNull
    private BigDecimal balance;

    @NotNull
    private BigDecimal price;

    @NotNull
    private BigDecimal value;

    @NotNull
    private BigDecimal totalValue;

    @NotNull
    private TypeDirection direction;

    public TypeDirection getDirection() {
        return direction;
    }

    public void setDirection(TypeDirection direction) {
        this.direction = direction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeAnalytics getType() {
        return type;
    }

    public void setType(TypeAnalytics type) {
        this.type = type;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final AnalyticsResponse other = (AnalyticsResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((type == null && other.type != null) || !type.equals(other.type))
            return false;
        if ((quantity == null && other.quantity != null) || !quantity.equals(other.quantity))
            return false;
        if ((balance == null && other.balance != null) || !balance.equals(other.balance))
            return false;
        if ((price == null && other.price != null) || !price.equals(other.price))
            return false;
        if ((value == null && other.value != null) || !value.equals(other.value))
            return false;
        if ((totalValue == null && other.totalValue != null) || !totalValue.equals(other.totalValue))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        result = prime * result + ((totalValue == null) ? 0 : totalValue.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "AnalyticsResponse[" + "id=" + id + ", type=" + type + ", quantity=" + quantity + ", balance=" + balance + ", price=" + price + ", value=" + value + ", totalValue=" + totalValue + "]";
    }

}

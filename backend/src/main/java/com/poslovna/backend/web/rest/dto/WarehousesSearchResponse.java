package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;


public class WarehousesSearchResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    @Size(max = 255)
    private String code;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    @Size(max = 255)
    private String address;

    @NotNull
    @Size(max = 255)
    private String city;

    @NotNull
    @Size(max = 255)
    private String country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final WarehousesSearchResponse other = (WarehousesSearchResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((code == null && other.code != null) || !code.equals(other.code))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((address == null && other.address != null) || !address.equals(other.address))
            return false;
        if ((city == null && other.city != null) || !city.equals(other.city))
            return false;
        if ((country == null && other.country != null) || !country.equals(other.country))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "WarehousesSearchResponse[" + "id=" + id + ", code=" + code + ", name=" + name + ", address=" + address + ", city=" + city + ", country=" + country + "]";
    }

}

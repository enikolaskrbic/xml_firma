package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class BusinessPartnersResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    @Size(max = 255)
    private String address;

    @NotNull
    @Size(max = 255)
    private String city;

    @NotNull
    @Size(max = 255)
    private String country;

    @Size(max = 255)
    private String web;

    @Size(max = 255)
    private String email;

    @Size(max = 255)
    private String phone;

    @NotNull
    private RoleBusinessPartner status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public RoleBusinessPartner getStatus() {
        return status;
    }

    public void setStatus(RoleBusinessPartner status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final BusinessPartnersResponse other = (BusinessPartnersResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((address == null && other.address != null) || !address.equals(other.address))
            return false;
        if ((city == null && other.city != null) || !city.equals(other.city))
            return false;
        if ((country == null && other.country != null) || !country.equals(other.country))
            return false;
        if ((web == null && other.web != null) || !web.equals(other.web))
            return false;
        if ((email == null && other.email != null) || !email.equals(other.email))
            return false;
        if ((phone == null && other.phone != null) || !phone.equals(other.phone))
            return false;
        if ((status == null && other.status != null) || !status.equals(other.status))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + ((web == null) ? 0 : web.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "BusinessPartnersResponse[" + "id=" + id + ", name=" + name + ", address=" + address + ", city=" + city + ", country=" + country + ", web=" + web + ", email=" + email + ", phone="
                + phone + ", status=" + status + "]";
    }

}

package com.poslovna.backend.web.rest;

import javax.inject.Inject;

import com.poslovna.backend.config.CustomProperties;
import com.poslovna.backend.database.DatabaseQuery;
import com.poslovna.backend.generates.Config;
import com.poslovna.backend.model.util.ExceptionUtil;
import com.poslovna.backend.repository.tuple.DocumentItemArticleDocumentTuple;
import com.poslovna.backend.service.XMLService;
import com.poslovna.backend.util.MyValidationEventHandler;
import com.poslovna.backend.util.SoapUnil;
import com.poslovna.backend.web.rest.exception.CustomException;
import com.poslovna.backend.xml_model.faktura.Faktura;
import com.poslovna.backend.xml_model.faktura.StavkaFakture;
import com.poslovna.backend.xml_model.faktura.ZaglavljeFakture;
import com.poslovna.backend.xml_model.firm.Firma;
import com.poslovna.backend.xml_model.izvod.IzvodRequest;
import com.poslovna.backend.xml_model.izvod.IzvodResponse;
import com.poslovna.backend.xml_model.nalogzaplacanje.NalogZaPlacanjeRequest;
import com.poslovna.backend.xml_model.nalogzaplacanje.NalogZaPlacanjeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;
import javax.validation.Valid;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import com.poslovna.backend.model.*;
import com.poslovna.backend.web.rest.dto.*;
import org.xml.sax.SAXException;
import springfox.documentation.annotations.ApiIgnore;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/")
public class XmlApi {

    private final Logger log = LoggerFactory.getLogger(XmlApi.class);

    @Inject
    XMLService xmlService;

    @Inject
    DatabaseQuery databaseQuery;

    @Inject
    ExceptionUtil exceptionUtil;

    @Inject
    CustomProperties customProperties;

    @RequestMapping(value = "/xml/firma/{pib}/faktura/input", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @Timed
    @Transactional
    public ResponseEntity<Void> createInputFaktura(@PathVariable int pib, @RequestBody Faktura faktura) {

        try{
            String xml = xmlService.validateSchemaMarshall("com.poslovna.backend.xml_model.faktura", Config.schema_fakturaIn, faktura, "faktura");
            if(xml == null){
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
            databaseQuery.createInputFaktura(faktura.getZaglavljeFakture().getPibKupca(), xml);
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/xml/firma/{pib}/faktura/output", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @Timed
    @Transactional
    public ResponseEntity<Void> createOutputFaktura(@PathVariable int pib, @RequestBody Faktura faktura) {
        faktura.setIdPoruke(generateCode());
        try{
            String xml = xmlService.validateSchemaMarshall("com.poslovna.backend.xml_model.faktura", Config.schema_faktura, faktura, "faktura");
            if(xml == null){
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
            databaseQuery.createOutputFaktura(String.valueOf(pib), xml);
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/xml/firma/{pib}/faktura/{idFakture}/stavka/output", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @Timed
    @Transactional
    public ResponseEntity<Void> createOutputStavkaFaktura(@PathVariable int pib,@PathVariable String idFakture, @RequestBody StavkaFakture stavkaFakture) throws JAXBException, IOException {

        stavkaFakture.setIznosRabata((stavkaFakture.getJedinicnaCena().multiply(stavkaFakture.getKolicina())).multiply(stavkaFakture.getProcenatRabata().divide(new BigDecimal(100))).setScale(2, BigDecimal.ROUND_HALF_UP));

        stavkaFakture.setUmanjenoZaRabat((stavkaFakture.getJedinicnaCena().multiply(stavkaFakture.getKolicina())).subtract(stavkaFakture.getIznosRabata()).setScale(2, BigDecimal.ROUND_HALF_UP));

        stavkaFakture.setVrednost(stavkaFakture.getUmanjenoZaRabat().setScale(2, BigDecimal.ROUND_HALF_UP).add(stavkaFakture.getUmanjenoZaRabat().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(0.2))).setScale(2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));

        stavkaFakture.setUkupanPorez(stavkaFakture.getUmanjenoZaRabat().multiply(new BigDecimal(0.2)).setScale(2, BigDecimal.ROUND_HALF_UP));

        try{
            String xml = xmlService.validateSchemaMarshall("com.poslovna.backend.xml_model.faktura", Config.schema_stavkaFaktura, stavkaFakture, "stavkaFakture");
            if(xml == null){
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
            databaseQuery.createOutputStavkaFaktura(String.valueOf(pib), idFakture, xml);
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        ZaglavljeFakture zaglavljeFakture;
        try {
            zaglavljeFakture = databaseQuery.getZaglavljeFaktureObject(String.valueOf(pib), idFakture);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        zaglavljeFakture.setUkupanPorez(zaglavljeFakture.getUkupanPorez().add(stavkaFakture.getUkupanPorez()));
        zaglavljeFakture.setUkupanRabat(zaglavljeFakture.getUkupanRabat().add(stavkaFakture.getIznosRabata()));
        zaglavljeFakture.setIznosZaUplatu(zaglavljeFakture.getIznosZaUplatu().add(stavkaFakture.getVrednost()));
        zaglavljeFakture.setUpunoRobeIUsluga(zaglavljeFakture.getUpunoRobeIUsluga().add(stavkaFakture.getVrednost()));
        zaglavljeFakture.setVrednostRobe(zaglavljeFakture.getVrednostRobe().add(stavkaFakture.getVrednost()));
        zaglavljeFakture.setVrednostUsluga(zaglavljeFakture.getVrednostUsluga().add(stavkaFakture.getVrednost()));

        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance("com.poslovna.backend.xml_model.faktura");
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        File file = new File("test.xml");

        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();

        marshaller.marshal(zaglavljeFakture, file);

        BufferedReader br = new BufferedReader(new FileReader("test.xml"));
        String line = null;
        String xml = "";
        boolean first = true;
        while ((line = br.readLine()) != null) {
            if(first){
                first = false;
            }else{
                xml += line;
            }
        }

        try {
            DatabaseQuery.updateZaglavljeFakture(String.valueOf(pib), idFakture, xml);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/xml/firma/{pib}/ulazne-fakture", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<Faktura>> getUlazneFakture(@PathVariable("pib") int pib) {
        List<Faktura> fakturaLista;
        try{
            fakturaLista = DatabaseQuery.getListaUlaznihFaktura(String.valueOf(pib));
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(fakturaLista);
    }

    @RequestMapping(value = "/xml/firma/{pib}/izlazne-fakture", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<Faktura>> getIzlazneFakture(@PathVariable("pib") int pib) {
        List<Faktura> fakturaLista;
        try{
            fakturaLista = DatabaseQuery.getListaIzlaznihFaktura(String.valueOf(pib));
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(fakturaLista);
    }

    @RequestMapping(value = "/xml/firma/{pib}/faktura-izlazna/{idFakture}/stavka", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<StavkaFakture>> getStavkeIzlazneFakture(@PathVariable("pib") int pib, @PathVariable("idFakture") String idFakture) {
        List<StavkaFakture> fakturaLista;
        try{
            fakturaLista = DatabaseQuery.getStavkeIzlazneFakture(String.valueOf(pib), idFakture);
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(fakturaLista);
    }

    @RequestMapping(value = "/xml/firma/{pib}/faktura-ulazna/{idFakture}/stavka", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<StavkaFakture>> getStavkeUlazneFakture(@PathVariable("pib") int pib, @PathVariable("idFakture") String idFakture) {
        List<StavkaFakture> fakturaLista;
        try{
            fakturaLista = DatabaseQuery.getStavkeUlazneFakture(String.valueOf(pib), idFakture);
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(fakturaLista);
    }

    @RequestMapping(value = "/xml/firma/{pib}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<Firma> getFirma(@PathVariable("pib") int pib) {
        Firma firma;
        try{
            firma = DatabaseQuery.getFirma(String.valueOf(pib));
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(firma);
    }

    @RequestMapping(value = "/xml/firme/bez/{pib}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<Firma>> getStavkeUlazneFakture(@PathVariable("pib") int pib) {
        List<Firma> firmaList;
        try{
            firmaList = DatabaseQuery.getOstaleFirme(String.valueOf(pib));
        }catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok().body(firmaList);
    }

    @RequestMapping(value = "/xml/firme/izvod/racun", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<IzvodResponse> getIzvodRacuna(@RequestParam(value = "apiBanke", required = true) String apiBanke, @RequestBody IzvodRequest izvodRequest) throws JAXBException, SAXException, FileNotFoundException {
        try {
            xmlService.validateSchemaMarshall("com.poslovna.backend.xml_model.izvod","/Users/ivansavic/Workspace/xml_firma/backend/src/main/resources/schemas/izvod.xsd",izvodRequest,"izvodRequest");
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        String soapMessage;
        try {
            soapMessage = SoapUnil.createIzvodSoapMessge(izvodRequest);
        } catch (SOAPException e) {
            return exceptionUtil.catchCustomException(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (IOException e) {
            return exceptionUtil.catchCustomException(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        String response2="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        String response="";
        try {
            response+=SoapUnil.callSoapService(soapMessage,apiBanke);
        } catch (IOException e) {
            return exceptionUtil.catchCustomException(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        response=response.split("<SOAP-ENV:Body>")[1].split("</SOAP-ENV:Body>")[0];
        response=response.replaceAll("ns2:","");
        response=response.replaceAll(":ns2","");
        //response=response.replaceAll("Z<","<");
        IzvodResponse izvodResponse = new IzvodResponse();
        response2+=response;
        JAXBContext context = JAXBContext.newInstance("com.poslovna.backend.xml_model.izvod");
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new File("/Users/ivansavic/Workspace/xml_firma/backend/src/main/resources/schemas/izvod.xsd"));

        Unmarshaller unmarshaller = context.createUnmarshaller();

        // Podešavanje unmarshaller-a za XML schema validaciju
        unmarshaller.setSchema(schema);
        unmarshaller.setEventHandler(new MyValidationEventHandler());

        //InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.xml");
        File file = new File("test.xml");

        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.print(response2);
        writer.close();

        try {
            izvodResponse = (IzvodResponse) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            return exceptionUtil.catchCustomException(e.getMessage(), HttpStatus.NOT_FOUND);
        }


        return ResponseEntity.ok().body(izvodResponse);
    }


    @RequestMapping(value = "/xml/firme/nalog", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<NalogZaPlacanjeResponse> posaljiNalog(@RequestParam(value = "apiBanke", required = true) String apiBanke, @RequestBody NalogZaPlacanjeRequest nalogZaPlacanjeRequest) throws JAXBException, SAXException, FileNotFoundException {
        try {
            xmlService.validateSchemaMarshall("com.poslovna.backend.xml_model.nalogzaplacanje","/Users/ivansavic/Workspace/xml_firma/backend/src/main/resources/schemas/nalogZaPlacanje.xsd",nalogZaPlacanjeRequest,"NalogZaPlacanjeRequest");
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }

        String soapMessage;
        try {
            soapMessage = SoapUnil.createNalogMessage(nalogZaPlacanjeRequest);
        } catch (SOAPException e) {
            return exceptionUtil.catchCustomException(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (IOException e) {
            return exceptionUtil.catchCustomException(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        String response2="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        String response="";
        try {
            response+=SoapUnil.callSoapService(soapMessage,apiBanke);
        } catch (IOException e) {
            return exceptionUtil.catchCustomException(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        response=response.split("<ns2:odgovor>")[1].split("</ns2:odgovor>")[0];

        NalogZaPlacanjeResponse nalogZaPlacanjeResponse = new NalogZaPlacanjeResponse();
        nalogZaPlacanjeResponse.setOdgovor(response);

        if(response.contains("OK")){
            return ResponseEntity.ok().body(nalogZaPlacanjeResponse);
        }else{
            return exceptionUtil.catchCustomException(response, HttpStatus.CONFLICT);
        }

    }



    public String generateCode(){
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

        Random rnd = new Random();
        StringBuilder code = new StringBuilder((100000 + rnd.nextInt(900000)));

        for (int i = 0; i < 16; i++) {
            code.append(chars[rnd.nextInt(chars.length)]);
        }

        return code.toString();
    }

}

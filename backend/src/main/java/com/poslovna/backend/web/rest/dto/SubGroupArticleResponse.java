package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class SubGroupArticleResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    @Size(max = 255)
    private String code;

    @NotNull
    private RoleArticle status;

    @NotNull
    private Long subGroupId;

    @NotNull
    @Size(max = 255)
    private String subGroupName;

    @NotNull
    private Long unitOfMeasureId;

    @NotNull
    @Size(max = 255)
    private String unitOfMeasureName;

    @Size(max = 255)
    private String unitOfMeasureShortName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RoleArticle getStatus() {
        return status;
    }

    public void setStatus(RoleArticle status) {
        this.status = status;
    }

    public Long getSubGroupId() {
        return subGroupId;
    }

    public void setSubGroupId(Long subGroupId) {
        this.subGroupId = subGroupId;
    }

    public String getSubGroupName() {
        return subGroupName;
    }

    public void setSubGroupName(String subGroupName) {
        this.subGroupName = subGroupName;
    }

    public Long getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    public void setUnitOfMeasureId(Long unitOfMeasureId) {
        this.unitOfMeasureId = unitOfMeasureId;
    }

    public String getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        this.unitOfMeasureName = unitOfMeasureName;
    }

    public String getUnitOfMeasureShortName() {
        return unitOfMeasureShortName;
    }

    public void setUnitOfMeasureShortName(String unitOfMeasureShortName) {
        this.unitOfMeasureShortName = unitOfMeasureShortName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final SubGroupArticleResponse other = (SubGroupArticleResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((code == null && other.code != null) || !code.equals(other.code))
            return false;
        if ((status == null && other.status != null) || !status.equals(other.status))
            return false;
        if ((subGroupId == null && other.subGroupId != null) || !subGroupId.equals(other.subGroupId))
            return false;
        if ((subGroupName == null && other.subGroupName != null) || !subGroupName.equals(other.subGroupName))
            return false;
        if ((unitOfMeasureId == null && other.unitOfMeasureId != null) || !unitOfMeasureId.equals(other.unitOfMeasureId))
            return false;
        if ((unitOfMeasureName == null && other.unitOfMeasureName != null) || !unitOfMeasureName.equals(other.unitOfMeasureName))
            return false;
        if ((unitOfMeasureShortName == null && other.unitOfMeasureShortName != null) || !unitOfMeasureShortName.equals(other.unitOfMeasureShortName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((subGroupId == null) ? 0 : subGroupId.hashCode());
        result = prime * result + ((subGroupName == null) ? 0 : subGroupName.hashCode());
        result = prime * result + ((unitOfMeasureId == null) ? 0 : unitOfMeasureId.hashCode());
        result = prime * result + ((unitOfMeasureName == null) ? 0 : unitOfMeasureName.hashCode());
        result = prime * result + ((unitOfMeasureShortName == null) ? 0 : unitOfMeasureShortName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "SubGroupArticleResponse[" + "id=" + id + ", name=" + name + ", code=" + code + ", status=" + status + ", subGroupId=" + subGroupId + ", subGroupName=" + subGroupName
                + ", unitOfMeasureId=" + unitOfMeasureId + ", unitOfMeasureName=" + unitOfMeasureName + ", unitOfMeasureShortName=" + unitOfMeasureShortName + "]";
    }

}

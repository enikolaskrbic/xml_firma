package com.poslovna.backend.web.rest.exception;

import java.util.HashMap;
import java.util.Map;


public final class ConstraintMapping {

    private static final Map<String, String> mappings = new HashMap<>();

    static {
        mappings.put("UNQ_USER_E_7E1D17", "User.email.unique");
        mappings.put("UNQ_USER_EVC_4E3FFA", "User.emailVerificationCode.unique");
        mappings.put("UNQ_USER_RPC_74E561", "User.resetPasswordCode.unique");
    }

    private ConstraintMapping() {
    };

    public static String getErrorCodeForConstraint(String constraint) {
        return mappings.get(constraint);
    }

}

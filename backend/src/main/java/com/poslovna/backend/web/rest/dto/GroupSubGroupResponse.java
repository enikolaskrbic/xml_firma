package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;


public class GroupSubGroupResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    private Long groupId;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    @Size(max = 255)
    private String groupFirmName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupFirmName() {
        return groupFirmName;
    }

    public void setGroupFirmName(String groupFirmName) {
        this.groupFirmName = groupFirmName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final GroupSubGroupResponse other = (GroupSubGroupResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((groupId == null && other.groupId != null) || !groupId.equals(other.groupId))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((groupFirmName == null && other.groupFirmName != null) || !groupFirmName.equals(other.groupFirmName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((groupFirmName == null) ? 0 : groupFirmName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "GroupSubGroupResponse[" + "id=" + id + ", groupId=" + groupId + ", name=" + name + ", groupFirmName=" + groupFirmName + "]";
    }

}

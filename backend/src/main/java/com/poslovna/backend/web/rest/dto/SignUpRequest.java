package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import java.time.*;

import javax.validation.constraints.*;


public class SignUpRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(max = 255)
    private String firstName;

    @NotNull
    @Size(max = 255)
    private String lastName;

    @NotNull
    private Boolean isDeleted;


    private ZonedDateTime timetamp;

    private Long firmId;

    @NotNull
    @Size(min = 6, max = 128)
    @Pattern(regexp = "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$")
    private String email;

    @NotNull
    @Size(min = 6, max = 32)
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$")
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public ZonedDateTime getTimetamp() {
        return timetamp;
    }

    public void setTimetamp(ZonedDateTime timetamp) {
        this.timetamp = timetamp;
    }

    public Long getFirmId() {
        return firmId;
    }

    public void setFirmId(Long firmId) {
        this.firmId = firmId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final SignUpRequest other = (SignUpRequest) obj;
        if ((firstName == null && other.firstName != null) || !firstName.equals(other.firstName))
            return false;
        if ((lastName == null && other.lastName != null) || !lastName.equals(other.lastName))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        if ((timetamp == null && other.timetamp != null) || !timetamp.equals(other.timetamp))
            return false;
        if ((firmId == null && other.firmId != null) || !firmId.equals(other.firmId))
            return false;
        if ((email == null && other.email != null) || !email.equals(other.email))
            return false;
        if ((password == null && other.password != null) || !password.equals(other.password))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        result = prime * result + ((timetamp == null) ? 0 : timetamp.hashCode());
        result = prime * result + ((firmId == null) ? 0 : firmId.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "SignUpRequest[" + "firstName=" + firstName + ", lastName=" + lastName + ", isDeleted=" + isDeleted + ", timetamp=" + timetamp + ", firmId=" + firmId + ", email=" + email + "]";
    }

}

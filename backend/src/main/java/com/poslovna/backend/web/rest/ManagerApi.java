package com.poslovna.backend.web.rest;

import java.io.*;
import java.sql.SQLException;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.poslovna.backend.model.util.ExceptionUtil;
import com.poslovna.backend.service.ManagerService;
import com.poslovna.backend.util.JasperUtil;
import com.poslovna.backend.web.rest.exception.CustomException;
import net.sf.jasperreports.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;
import javax.validation.Valid;
import com.poslovna.backend.model.*;
import com.poslovna.backend.web.rest.dto.*;

import com.poslovna.backend.model.enumeration.*;
import java.math.BigDecimal;
import com.poslovna.backend.repository.*;
import com.poslovna.backend.repository.tuple.*;

import org.springframework.format.annotation.DateTimeFormat;


@RestController
@RequestMapping("/api/")
public class ManagerApi {

    private final Logger log = LoggerFactory.getLogger(ManagerApi.class);

    @Inject
    private AnalyticsRepository analyticsRepository;

    @Inject
    private ArticleRepository articleRepository;

    @Inject
    private BusinessPartnerRepository businessPartnerRepository;

    @Inject
    private BusinessYearRepository businessYearRepository;

    @Inject
    private DocumentRepository documentRepository;

    @Inject
    private DocumentItemRepository documentItemRepository;

    @Inject
    private GroupFirmRepository groupFirmRepository;

    @Inject
    ManagerService managerService;

    @Inject
    ExceptionUtil exceptionUtil;
	
	@Inject
    private SubGroupRepository subGroupRepository;

    @Inject
    private UnitOfMeasureRepository unitOfMeasureRepository;

    @Inject
    private WarehouseRepository warehouseRepository;

    @Inject
    private WarehouseCardRepository warehouseCardRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private JasperUtil jasperUtil;


    @RequestMapping(value = "/manager/add-group", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addGroup(@Valid @RequestBody GroupDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-group {}", request);
        try {
            managerService.addGroup(request, principalId);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-group", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editGroup(@Valid @RequestBody GroupIdDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-group {}", request);
        try {
            managerService.editGroup(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/delete-group", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteGroup(@Valid @RequestBody GroupIdDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/delete-group {}", request);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(value = "/manager/firm/groups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<GroupsResponse>> groups( @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /manager/firm/groups");
        User user = userRepository.findOne(principalId);
        final List<GroupFirmFirmTuple> result = groupFirmRepository.groups(user.getFirm().get().getId());
        return ResponseEntity.ok().body(result.stream().map(this::convertToGroupsResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/groups/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<SearchGroupsResponse>> searchGroups(@RequestParam(value = "groupFirmId", required = false) Optional<Long> groupFirmId,
            @RequestParam(value = "groupFirmName", required = false) Optional<String> groupFirmName) {
        log.debug("GET /manager/firm/groups/search");
        final List<GroupFirmFirmTuple> result = groupFirmRepository.searchGroups(groupFirmId, groupFirmName);
        return ResponseEntity.ok().body(result.stream().map(this::convertToSearchGroupsResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/add-subgroup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addSubGroup(@Valid @RequestBody SubGroupDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-subgroup {}", request);
        try {
            managerService.addSubGroup(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-subgroup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editSubGroup(@Valid @RequestBody SubGroupEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-subgroup {}", request);
        try {
            managerService.editSubGroup(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/delete-subgroup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteSubGroup(@Valid @RequestBody SubGroupDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/delete-subgroup {}", request);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(value = "/manager/firm/subgroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<SubgroupsResponse>> subgroups( @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /manager/firm/subgroups");
        User user = userRepository.findOne(principalId);
        final List<SubGroupGroupFirmTuple> result = subGroupRepository.subgroups(user.getFirm().get().getId());
        return ResponseEntity.ok().body(result.stream().map(this::convertToSubgroupsResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/group/{groupId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteGroupHard(@PathVariable Long groupId)  {
        log.debug("DELETE /manager/group/{}", groupId);
        groupFirmRepository.delete(groupId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/manager/firm/subgroups/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<SearchSubGroupsResponse>> searchSubGroups(@RequestParam(value = "subGroupId", required = false) Optional<Long> subGroupId,
            @RequestParam(value = "subGroupGroupId", required = false) Optional<Long> subGroupGroupId, @RequestParam(value = "subGroupName", required = false) Optional<String> subGroupName,
            @RequestParam(value = "groupFirmName", required = false) Optional<String> groupFirmName) {
        log.debug("GET /manager/firm/subgroups/search");
        final List<SubGroupGroupFirmTuple> result = subGroupRepository.searchSubGroups(subGroupId, subGroupGroupId, subGroupName, groupFirmName);
        return ResponseEntity.ok().body(result.stream().map(this::convertToSearchSubGroupsResponse).collect(Collectors.toList()));

    }

    @RequestMapping(value = "/manager/add-article", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addArticle(@Valid @RequestBody ArticleDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-article {}", request);
        try {
            managerService.addArticle(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-article", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editArticle(@Valid @RequestBody ArticleEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-article {}", request);
        try {
            managerService.editArticle(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/manager/subgroup/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteSubGroupHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/subgroup/{}", id);
        subGroupRepository.delete(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/manager/firm/articles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<ArticlesResponse>> articles(@ApiIgnore @AuthenticationPrincipal Long principalId)  {
	log.debug("GET /manager/firm/articles");
	final List<ArticleUnitOfMeasureSubGroupTuple> result = articleRepository.articles(userRepository.findOne(principalId).getFirm().get().getId());
    return ResponseEntity.ok().body(result.stream().map(this::convertToArticlesResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/articles/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<SearchArticlesResponse>> searchArticles(@RequestParam(value = "articleId", required = false) Optional<Long> articleId,
            @RequestParam(value = "ArticleName", required = false) Optional<String> ArticleName, @RequestParam(value = "articleCode", required = false) Optional<String> articleCode,
            @RequestParam(value = "articleStatus", required = false) Optional<RoleArticle> articleStatus, @RequestParam(value = "subGroupId", required = false) Optional<Long> subGroupId,
            @RequestParam(value = "subGroupName", required = false) Optional<String> subGroupName, @RequestParam(value = "unitOfMeasureId", required = false) Optional<Long> unitOfMeasureId,
            @RequestParam(value = "unitOfMeasureName", required = false) Optional<String> unitOfMeasureName,
            @RequestParam(value = "unitOfMeasureShortName", required = false) Optional<String> unitOfMeasureShortName) {
        log.debug("GET /manager/firm/articles/search");
        final List<ArticleUnitOfMeasureSubGroupTuple> result = articleRepository.searchArticles(articleId, ArticleName, articleCode, articleStatus, subGroupId, subGroupName, unitOfMeasureId,
                unitOfMeasureName, unitOfMeasureShortName);
        return ResponseEntity.ok().body(result.stream().map(this::convertToSearchArticlesResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/add-BussinesYear", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addBussinesYear(@Valid @RequestBody BusinessYearDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-BussinesYear {}", request);
        try {
            managerService.addBussinesYear(request,principalId);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-BussinesYear", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editBussinesYear(@Valid @RequestBody BusinessYearIdDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-BussinesYear {}", request);
        try {
            managerService.editBussinesYear(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/firm/businessYears", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<BusinessYearsResponse>> businessYears(@ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /manager/firm/businessYears");
        User user = userRepository.findOne(principalId);
        final List<BusinessYearFirmTuple> result = businessYearRepository.businessYears(user.getFirm().get().getId());
        return ResponseEntity.ok().body(result.stream().map(this::convertToBusinessYearsResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/businessYear/active", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<ActiveBusinessYearResponse>> activeBusinessYear(@ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /manager/firm/businessYear/active");
        final List<BusinessYearFirmTuple> result = businessYearRepository.activeBusinessYearByFirm(userRepository.findOne(principalId).getFirm().get().getId());
        return ResponseEntity.ok().body(result.stream().map(this::convertToActiveBusinessYearResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/businessYears/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<BusinessYearsSearchResponse>> businessYearsSearch(@RequestParam(value = "businessYearId", required = false) Optional<Long> businessYearId,
            @RequestParam(value = "businessYearYear", required = false) Optional<String> businessYearYear,
            @RequestParam(value = "businessYearIsFinished", required = false) Optional<Boolean> businessYearIsFinished) {
        log.debug("GET /manager/firm/businessYears/search");
        final List<BusinessYearFirmTuple> result = businessYearRepository.businessYearsSearch(businessYearId, businessYearYear, businessYearIsFinished);
        return ResponseEntity.ok().body(result.stream().map(this::convertToBusinessYearsSearchResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/finish-BussinesYear", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> finishYear(@Valid @RequestBody YearIdDTO request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/finish-BussinesYear {}", request);
        try {
            managerService.finishBussinesYear(request.getId());
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/add-warehouse", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addWarehouse(@Valid @RequestBody WarehouseDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-warehouse {}", request);
        try {
            managerService.addWarehouse(request,principalId);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-warehouse", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editWarehouse(@Valid @RequestBody WarehouseEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-warehouse {}", request);
        try {
            managerService.editWarehouse(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/firm/warehouses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<WarehousesResponse>> warehouses(@ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /manager/firm/warehouses");
        User user = userRepository.getOne(principalId);
        final List<WarehouseFirmTuple> result = warehouseRepository.warehouses(user.getFirm().get().getId());
        return ResponseEntity.ok().body(result.stream().map(this::convertToWarehousesResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/warehouses/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<WarehousesSearchResponse>> warehousesSearch(@RequestParam(value = "warehouseId", required = false) Optional<Long> warehouseId,
            @RequestParam(value = "warehouseCode", required = false) Optional<String> warehouseCode, @RequestParam(value = "warehouseName", required = false) Optional<String> warehouseName,
            @RequestParam(value = "warehouseAddress", required = false) Optional<String> warehouseAddress, @RequestParam(value = "warehouseCity", required = false) Optional<String> warehouseCity,
            @RequestParam(value = "warehouseCountry", required = false) Optional<String> warehouseCountry) {
        log.debug("GET /manager/firm/warehouses/search");
        final List<WarehouseFirmTuple> result = warehouseRepository.warehousesSearch(warehouseId, warehouseCode, warehouseName, warehouseAddress, warehouseCity, warehouseCountry);
        return ResponseEntity.ok().body(result.stream().map(this::convertToWarehousesSearchResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/add-warehouse-card", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addWarehouseCard(@Valid @RequestBody WarehouseCardDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-warehouse-card {}", request);
        try {
            managerService.addWarehouseCard(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-warehouse-card", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editWarehouseCard(@Valid @RequestBody WarehouseCardEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-warehouse-card {}", request);
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(value = "/manager/firm/warehousecards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<WarehousesCardResponse>> warehousesCard(@ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /manager/firm/warehousecards");

        User user = userRepository.findOne(principalId);
        final List<WarehouseCardArticleWarehouseBusinessYearTuple> result = warehouseCardRepository.warehousesCard(user.getFirm().get().getId());
        return ResponseEntity.ok().body(result.stream().map(this::convertToWarehousesCardResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/warehousecards/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<WarehousesCardSearchResponse>> warehousesCardSearch(@RequestParam(value = "warehouseCardId", required = false) Optional<Long> warehouseCardId,
            @RequestParam(value = "warehouseCardBusinessYearId", required = false) Optional<Long> warehouseCardBusinessYearId,
            @RequestParam(value = "businessYearYear", required = false) Optional<String> businessYearYear, @RequestParam(value = "articleId", required = false) Optional<Long> articleId,
            @RequestParam(value = "articleName", required = false) Optional<String> articleName, @RequestParam(value = "articleCode", required = false) Optional<String> articleCode,
            @RequestParam(value = "warehouseCardInput", required = false) Optional<BigDecimal> warehouseCardInput,
            @RequestParam(value = "warehouseCardOutput", required = false) Optional<BigDecimal> warehouseCardOutput,
            @RequestParam(value = "warehouseCardBalance", required = false) Optional<BigDecimal> warehouseCardBalance,
            @RequestParam(value = "warehouseCardInValue", required = false) Optional<BigDecimal> warehouseCardInValue,
            @RequestParam(value = "warehouseCardOutValue", required = false) Optional<BigDecimal> warehouseCardOutValue,
            @RequestParam(value = "warehouseCardTotalValue", required = false) Optional<BigDecimal> warehouseCardTotalValue,
            @RequestParam(value = "warehouseId", required = false) Optional<Long> warehouseId, @RequestParam(value = "warehouseName", required = false) Optional<String> warehouseName) {
        log.debug("GET /manager/firm/warehousecards/search");
        final List<WarehouseCardArticleWarehouseBusinessYearTuple> result = warehouseCardRepository.warehousesCardSearch(warehouseCardId, warehouseCardBusinessYearId, businessYearYear, articleId,
                articleName, articleCode, warehouseCardInput, warehouseCardOutput, warehouseCardBalance, warehouseCardInValue, warehouseCardOutValue, warehouseCardTotalValue, warehouseId,
                warehouseName);
        return ResponseEntity.ok().body(result.stream().map(this::convertToWarehousesCardSearchResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/add-bussines-partner", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addBussinesPartner(@Valid @RequestBody BussinesPartnerDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-bussines-partner {}", request);
        try {
            managerService.addBussinesPartner(request,principalId);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-bussines-partner", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editBussinesPartner(@Valid @RequestBody BussinesPartnerEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-bussines-partner {}", request);
        try {
            managerService.editBussinesPartner(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/firm/businessPartners", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<BusinessPartnersResponse>> businessPartners(@ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /manager/firm/businessPartners");
        User user = userRepository.findOne(principalId);
        final List<BusinessPartnerFirmTuple> result = businessPartnerRepository.businessPartners(user.getFirm().get().getId());
        return ResponseEntity.ok().body(result.stream().map(this::convertToBusinessPartnersResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/businessPartners/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<BusinessPartnersSearchResponse>> businessPartnersSearch(@RequestParam(value = "businessPartnerId", required = false) Optional<Long> businessPartnerId,
            @RequestParam(value = "businessPartnerName", required = false) Optional<String> businessPartnerName,
            @RequestParam(value = "businessPartnerAddress", required = false) Optional<String> businessPartnerAddress,
            @RequestParam(value = "businessPartnerCity", required = false) Optional<String> businessPartnerCity,
            @RequestParam(value = "businessPartnerCountry", required = false) Optional<String> businessPartnerCountry,
            @RequestParam(value = "businessPartnerWeb", required = false) Optional<String> businessPartnerWeb,
            @RequestParam(value = "businessPartnerEmail", required = false) Optional<String> businessPartnerEmail,
            @RequestParam(value = "businessPartnerPhone", required = false) Optional<String> businessPartnerPhone,
            @RequestParam(value = "businessPartnerStatus", required = false) Optional<RoleBusinessPartner> businessPartnerStatus) {
        log.debug("GET /manager/firm/businessPartners/search");
        final List<BusinessPartnerFirmTuple> result = businessPartnerRepository.businessPartnersSearch(businessPartnerId, businessPartnerName, businessPartnerAddress, businessPartnerCity,
                businessPartnerCountry, businessPartnerWeb, businessPartnerEmail, businessPartnerPhone, businessPartnerStatus);
        return ResponseEntity.ok().body(result.stream().map(this::convertToBusinessPartnersSearchResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/add-unitOfMeasure", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addUnitOfMeasure(@Valid @RequestBody UnitOfMeasureDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        try {
            managerService.addUnitOfMeasure(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-unitOfMeasure", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editUnitOfMeasure(@Valid @RequestBody UnitOfMeasureEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-unitOfMeasure {}", request);
        try {
            managerService.editUnitOfMeasure(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/firm/unitOfMeasures", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<UnitOfMeasuresResponse>> unitOfMeasures() {
        log.debug("GET /manager/firm/unitOfMeasures");
        final List<UnitOfMeasure> result = unitOfMeasureRepository.unitOfMeasures();
        return ResponseEntity.ok().body(result.stream().map(this::convertToUnitOfMeasuresResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/unitOfMeasures/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<UnitOfMeasuresSearchResponse>> unitOfMeasuresSearch(@RequestParam(value = "unitOfMeasureId", required = false) Optional<Long> unitOfMeasureId,
            @RequestParam(value = "unitOfMeasureName", required = false) Optional<String> unitOfMeasureName,
            @RequestParam(value = "unitOfMeasureShortName", required = false) Optional<String> unitOfMeasureShortName) {
        log.debug("GET /manager/firm/unitOfMeasures/search");
        final List<UnitOfMeasure> result = unitOfMeasureRepository.unitOfMeasuresSearch(unitOfMeasureId, unitOfMeasureName, unitOfMeasureShortName);
        return ResponseEntity.ok().body(result.stream().map(this::convertToUnitOfMeasuresSearchResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/add-document", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addDocument(@Valid @RequestBody DocumentDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-document {}", request);
        try {
            managerService.addDocument(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-document", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editDocument(@Valid @RequestBody DocumentEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-document {}", request);
        try {
            managerService.editDocument(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/book-document", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> bookDocument(@Valid @RequestBody DocumentIdDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/book-document {}", request);
        try {
            managerService.bookDocument(request.getId());
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/reverse-document", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> reverseDocument(@Valid @RequestBody DocumentIdDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/reverse-document {}", request);
        try {
            managerService.reverseDocument(request.getId());
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/firm/document", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<DocumentResponseDto>> documents(@ApiIgnore @AuthenticationPrincipal Long principalId)  {
        log.debug("GET /manager/firm/document","");
        List<DocumentResponseDto> documentResponseDtos;
        try {
            documentResponseDtos= managerService.getDocuments(principalId);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<DocumentResponseDto>>(documentResponseDtos,HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/firm/document/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<DocumentsSearchResponse>> documentsSearch(@RequestParam(value = "documentId", required = false) Optional<Long> documentId,
            @RequestParam(value = "documentDateOfValue", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> documentDateOfValue,
            @RequestParam(value = "documentDateOfDelivery", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<ZonedDateTime> documentDateOfDelivery,
            @RequestParam(value = "warehuseId", required = false) Optional<Long> warehuseId, @RequestParam(value = "warehouseName", required = false) Optional<String> warehouseName,
            @RequestParam(value = "businessPartnerId", required = false) Optional<Long> businessPartnerId,
            @RequestParam(value = "bussinesPartnerName", required = false) Optional<String> bussinesPartnerName,
            @RequestParam(value = "businessYearId", required = false) Optional<Long> businessYearId, @RequestParam(value = "businessYearYear", required = false) Optional<String> businessYearYear) {
        log.debug("GET /manager/firm/document/search");
        final List<DocumentBusinessPartnerWarehouseBusinessYearTuple> result = documentRepository.documentsSearch(documentId, documentDateOfValue, documentDateOfDelivery, warehuseId, warehouseName,
                businessPartnerId, bussinesPartnerName, businessYearId, businessYearYear);
        return ResponseEntity.ok().body(result.stream().map(this::convertToDocumentsSearchResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/add-document-item", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> addDocumentItem(@Valid @RequestBody DocumentItemDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/add-document-item {}", request);
        try {
            managerService.addDocumentItem(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/edit-document-item", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> editDocumentItem(@Valid @RequestBody DocumentItemEditDto request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /manager/edit-document-item {}", request);
        try {
            managerService.editDocumentItem(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/firm/documentItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<DocumentItemsResponse>> documentItems(@RequestParam("documentId") Long documentId) {
        log.debug("GET /manager/firm/documentItems");
        final List<DocumentItemArticleDocumentUnitOfMeasureTuple> result = documentItemRepository.documentItems(documentId);
        return ResponseEntity.ok().body(result.stream().map(this::convertToDocumentItemsResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/documentItems/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<DocumentItemsSearchResponse>> documentItemsSearch(@RequestParam(value = "documentItemId", required = false) Optional<Long> documentItemId,
            @RequestParam(value = "documentItemType", required = false) Optional<TypeDocument> documentItemType, @RequestParam(value = "documentId", required = false) Optional<Long> documentId,
            @RequestParam(value = "articleName", required = false) Optional<String> articleName,
            @RequestParam(value = "articleUnitOfMeasureId", required = false) Optional<Long> articleUnitOfMeasureId) {
        log.debug("GET /manager/firm/documentItems/search");
        final List<DocumentItemArticleDocumentUnitOfMeasureTuple> result = documentItemRepository.documentItemsSearch(documentItemId, documentItemType, documentId, articleName, articleUnitOfMeasureId);
        return ResponseEntity.ok().body(result.stream().map(this::convertToDocumentItemsSearchResponse).collect(Collectors.toList()));
    }


    @RequestMapping(value = "/manager/firm/analytics", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<AnalyticsResponse>> analytics(@RequestParam("warehouseCardId") Long warehouseCardId) {
        log.debug("GET /manager/firm/analytics");
        final List<AnalyticsWarehouseCardArticleTuple> result = analyticsRepository.analytics(warehouseCardId);
        return ResponseEntity.ok().body(result.stream().map(this::convertToAnalyticsResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/firm/analytics/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<List<AnalyticsSearchResponse>> analyticsSearch(@RequestParam("warehouseCardId") Long warehouseCardId,
            @RequestParam(value = "analyticsId", required = false) Optional<Long> analyticsId, @RequestParam(value = "analyticsType", required = false) Optional<TypeAnalytics> analyticsType,
            @RequestParam(value = "analyticsQuantity", required = false) Optional<BigDecimal> analyticsQuantity,
            @RequestParam(value = "analyticsBalance", required = false) Optional<BigDecimal> analyticsBalance,
            @RequestParam(value = "analyticsPrice", required = false) Optional<BigDecimal> analyticsPrice,
            @RequestParam(value = "analyticsValue", required = false) Optional<BigDecimal> analyticsValue,
            @RequestParam(value = "analyticsTotalValue", required = false) Optional<BigDecimal> analyticsTotalValue) {
        log.debug("GET /manager/firm/analytics/search");
        final List<AnalyticsWarehouseCardArticleTuple> result = analyticsRepository.analyticsSearch(warehouseCardId, analyticsId, analyticsType, analyticsQuantity, analyticsBalance, analyticsPrice,
                analyticsValue, analyticsTotalValue);
        return ResponseEntity.ok().body(result.stream().map(this::convertToAnalyticsSearchResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/manager/group/subgroup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<PagedDTO<GroupSubGroupResponse>> groupSubGroup(@RequestParam("groupFirmId") Long groupFirmId, @RequestParam("drop") Integer drop, @RequestParam("take") Integer take) {
        log.debug("GET /manager/group/subgroup");
        final List<SubGroupGroupFirmTuple> result = subGroupRepository.findGroupSubGroup(groupFirmId, drop, take);
        final Long totalCount = subGroupRepository.countGroupSubGroup(groupFirmId);
        return ResponseEntity.ok().body(new PagedDTO<GroupSubGroupResponse>(result.stream().map(this::convertToGroupSubGroupResponse).collect(Collectors.toList()), totalCount));
    }

    @RequestMapping(value = "/manager/subgroup/article", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<PagedDTO<SubGroupArticleResponse>> subGroupArticle(@RequestParam("subGroupId") Long subGroupId, @RequestParam("drop") Integer drop, @RequestParam("take") Integer take) {
        log.debug("GET /manager/subgroup/article");
        final List<ArticleUnitOfMeasureSubGroupTuple> result = articleRepository.findSubGroupArticle(subGroupId, drop, take);
        final Long totalCount = articleRepository.countSubGroupArticle(subGroupId);
        return ResponseEntity.ok().body(new PagedDTO<SubGroupArticleResponse>(result.stream().map(this::convertToSubGroupArticleResponse).collect(Collectors.toList()), totalCount));
    }

    @RequestMapping(value = "/manager/warehouse/warehousecard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<PagedDTO<WarehouseWarehouseCardResponse>> warehouseWarehouseCard(@RequestParam("warehouseId") Long warehouseId, @RequestParam("drop") Integer drop,
            @RequestParam("take") Integer take) {
        log.debug("GET /manager/warehouse/warehousecard");
        final List<WarehouseCardArticleWarehouseBusinessYearTuple> result = warehouseCardRepository.findWarehouseWarehouseCard(warehouseId, drop, take);
        final Long totalCount = warehouseCardRepository.countWarehouseWarehouseCard(warehouseId);
        return ResponseEntity.ok().body(new PagedDTO<WarehouseWarehouseCardResponse>(result.stream().map(this::convertToWarehouseWarehouseCardResponse).collect(Collectors.toList()), totalCount));
    }

    @RequestMapping(value = "/manager/document/documentItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<PagedDTO<DocumentDocumentItemResponse>> documentDocumentItem(@RequestParam("documentId") Long documentId, @RequestParam("drop") Integer drop,
            @RequestParam("take") Integer take) {
        log.debug("GET /manager/document/documentItems");
        final List<DocumentItemArticleDocumentTuple> result = documentItemRepository.findDocumentDocumentItem(documentId, drop, take);
        final Long totalCount = documentItemRepository.countDocumentDocumentItem(documentId);
        return ResponseEntity.ok().body(new PagedDTO<DocumentDocumentItemResponse>(result.stream().map(this::convertToDocumentDocumentItemResponse).collect(Collectors.toList()), totalCount));
    }

    @RequestMapping(value = "/manager/article/documentItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<PagedDTO<ArticleDocumentItemResponse>> articleDocumentItem(@RequestParam("articleId") Long articleId, @RequestParam("drop") Integer drop,
            @RequestParam("take") Integer take) {
        log.debug("GET /manager/article/documentItems");
        final List<DocumentItemArticleDocumentTuple> result = documentItemRepository.findArticleDocumentItem(articleId, drop, take);
        final Long totalCount = documentItemRepository.countArticleDocumentItem(articleId);
        return ResponseEntity.ok().body(new PagedDTO<ArticleDocumentItemResponse>(result.stream().map(this::convertToArticleDocumentItemResponse).collect(Collectors.toList()), totalCount));
    }

    @RequestMapping(value = "/manager/article/warehousecard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<PagedDTO<ArticleWarehouseCardResponse>> articleWarehouseCard(@RequestParam("articleId") Long articleId, @RequestParam("drop") Integer drop,
            @RequestParam("take") Integer take) {
        log.debug("GET /manager/article/warehousecard");
        final List<WarehouseCardArticleWarehouseBusinessYearTuple> result = warehouseCardRepository.findArticleWarehouseCard(articleId, drop, take);
        final Long totalCount = warehouseCardRepository.countArticleWarehouseCard(articleId);
        return ResponseEntity.ok().body(new PagedDTO<ArticleWarehouseCardResponse>(result.stream().map(this::convertToArticleWarehouseCardResponse).collect(Collectors.toList()), totalCount));

    }
    @RequestMapping(value = "/manager/article/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteArticleHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/article/{}", id);
        articleRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/manager/businessYear/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteBusinessYearHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/businessYear/{}", id);
        businessYearRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/manager/warehouse/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteWarehouseHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/warehouse/{}", id);
        warehouseRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/manager/businessPartner/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteBusinessPartnerHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/businessPartner/{}", id);
        businessPartnerRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/manager/document/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteDocumentHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/document/{}", id);
        documentRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/manager/documentItem/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteDocumentItemHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/documentItem/{}", id);
        documentItemRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/manager/unitOfMeasure/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> deleteUnitOfMeasureHard(@PathVariable Long id)  {
        log.debug("DELETE /manager/unitOfMeasure/{}", id);
        unitOfMeasureRepository.delete(id);
        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/manager/reports/lager-list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<byte[]> getLagerListReport(@RequestParam("warehouseId") Long warehouseId, @ApiIgnore @AuthenticationPrincipal Long principalId) throws JRException, IOException, CustomException {

        Warehouse warehouse = warehouseRepository.findOne(warehouseId);
        List<WarehouseCard> warehouseCards = warehouseCardRepository.findByWarehouseAndBusinessYear(warehouseId);
        if(warehouseCards.isEmpty()){
            throw new CustomException(ExceptionMessage.PDF_IS_EMPTY);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application.properties/pdf"));
        String filename = "lagerList.pdf";
        headers.setContentDispositionFormData(filename, filename);
        Map<String,Object> params = new HashMap<>();
        params.put("idWarehouse", warehouseId);
        byte [] pdfByte = jasperUtil.createReport("temp/lager.jasper",params);
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdfByte, headers, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/manager/reports/article-code", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<byte[]> getArticleCodeReport( @ApiIgnore @AuthenticationPrincipal Long principalId) throws JRException, IOException,CustomException {

        User user = userRepository.findOne(principalId);
        if(user==null){
            try {
                throw new CustomException(ExceptionMessage.USER_DOESNT_EXIST);
            } catch (CustomException e) {
                e.printStackTrace();
            }
        }


        List<Article> articles = articleRepository.findAll();
        List<Article> tempList = new ArrayList<>();
        for (Article article: articles) {
            if(article.getSubGroup().getGroup().getFirm().getId()==user.getFirm().get().getId()){
                tempList.add(article);
                break;
            }
        }
        if(tempList.isEmpty()){
            throw new CustomException(ExceptionMessage.PDF_IS_EMPTY);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application.properties/pdf"));
        String filename = "articleCodes.pdf";
        headers.setContentDispositionFormData(filename, filename);
        Map<String,Object> params = new HashMap<>();
        params.put("idFirm", user.getFirm().get().getId());
        byte [] pdfByte = jasperUtil.createReport("temp/sifarnik.jasper",params);
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdfByte, headers, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/manager/reports/analytics", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<byte[]> getAnalyticsReport(@RequestParam("warehouseCardId") Long warehouseCardId, @ApiIgnore @AuthenticationPrincipal Long principalId) throws JRException, IOException, CustomException {

        List<Analytics> analytics = analyticsRepository.findByWarehouseCard(warehouseCardId);
        if(analytics.isEmpty()){
            throw new CustomException(ExceptionMessage.PDF_IS_EMPTY);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application.properties/pdf"));
        String filename = "analytics.pdf";
        headers.setContentDispositionFormData(filename, filename);
        Map<String,Object> params = new HashMap<>();
        params.put("idWarehouseCard", warehouseCardId);
        byte [] pdfByte = jasperUtil.createReport("temp/warehouseCard.jasper",params);
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdfByte, headers, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/manager/article/price", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<BigDecimal> getArticlePrice(@RequestParam("articleId") Long articleId,@RequestParam("documentId") Long documentId, @ApiIgnore @AuthenticationPrincipal Long principalId) throws CustomException {
        Document document = documentRepository.findOne(documentId);
        List<WarehouseCard> warehouseCards = warehouseCardRepository.findByArticle(articleId);
        for (WarehouseCard warehouseCard: warehouseCards) {
            if(!warehouseCard.getIsDeleted() && document.getWarehouse().getId() == warehouseCard.getWarehouse().getId()){
                return ResponseEntity.ok().body(warehouseCard.getAveragePrice());
            }
        }
        throw new CustomException(ExceptionMessage.ARTICLE_DOESNT_HAVE_PRICE);
    }

    @RequestMapping(value = "/manager/warehouse/leveling", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public ResponseEntity<Void> levelingWarehouse(@RequestParam("warehouseId") Long warehouseId, @ApiIgnore @AuthenticationPrincipal Long principalId) throws CustomException {
        List<BusinessYear> businessYears = businessYearRepository.findByIsFinished(false);
        if(businessYears.isEmpty()){
            throw new CustomException(ExceptionMessage.NO_ACTIVE_YEAR);
        }
        try {
            //managerService.calculateLeveling (warehouseId);
            managerService.calculateLevelingWithProcedure(warehouseId);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        } catch (SQLException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private GroupsResponse convertToGroupsResponse(GroupFirmFirmTuple model) {
        final GroupsResponse dto = new GroupsResponse();
        dto.setId(model.getGroupFirm().getId());
        dto.setName(model.getGroupFirm().getName());
        return dto;
    }

    private SearchGroupsResponse convertToSearchGroupsResponse(GroupFirmFirmTuple model) {
        final SearchGroupsResponse dto = new SearchGroupsResponse();
        dto.setId(model.getGroupFirm().getId());
        dto.setName(model.getGroupFirm().getName());
        dto.setFirmId(model.getGroupFirm().getFirm().getId());
        dto.setIsDeleted(model.getGroupFirm().getIsDeleted());
        dto.setFirmName(model.getFirm().getName());
        dto.setFirmAddress(model.getFirm().getAddress());
        dto.setFirmCity(model.getFirm().getCity());
        dto.setFirmCountry(model.getFirm().getCountry());
        dto.setFirmEmail(model.getFirm().getEmail().orElse(null));
        dto.setFirmPhone(model.getFirm().getPhone().orElse(null));
        dto.setFirmWeb(model.getFirm().getWeb().orElse(null));
        dto.setFirmPib(model.getFirm().getPib());
        dto.setFirmIsDeleted(model.getFirm().getIsDeleted());
        return dto;
    }

    private SubgroupsResponse convertToSubgroupsResponse(SubGroupGroupFirmTuple model) {
        final SubgroupsResponse dto = new SubgroupsResponse();
        dto.setId(model.getSubGroup().getId());
        dto.setGroupId(model.getSubGroup().getGroup().getId());
        dto.setName(model.getSubGroup().getName());
        dto.setGroupFirmName(model.getGroupFirm().getName());
        return dto;
    }

    private SearchSubGroupsResponse convertToSearchSubGroupsResponse(SubGroupGroupFirmTuple model) {
        final SearchSubGroupsResponse dto = new SearchSubGroupsResponse();
        dto.setId(model.getSubGroup().getId());
        dto.setGroupId(model.getSubGroup().getGroup().getId());
        dto.setName(model.getSubGroup().getName());
        dto.setGroupFirmName(model.getGroupFirm().getName());
        return dto;
    }

    private ArticlesResponse convertToArticlesResponse(ArticleUnitOfMeasureSubGroupTuple model) {
        final ArticlesResponse dto = new ArticlesResponse();
        dto.setId(model.getArticle().getId());
        dto.setName(model.getArticle().getName());
        dto.setCode(model.getArticle().getCode());
        dto.setStatus(model.getArticle().getStatus());
        dto.setSubGroupId(model.getSubGroup().getId());
        dto.setSubGroupName(model.getSubGroup().getName());
        dto.setUnitOfMeasureId(model.getUnitOfMeasure().getId());
        dto.setUnitOfMeasureName(model.getUnitOfMeasure().getName());
        dto.setUnitOfMeasureShortName(model.getUnitOfMeasure().getShortName().orElse(null));
        return dto;
    }

    private SearchArticlesResponse convertToSearchArticlesResponse(ArticleUnitOfMeasureSubGroupTuple model) {
        final SearchArticlesResponse dto = new SearchArticlesResponse();
        dto.setId(model.getArticle().getId());
        dto.setName(model.getArticle().getName());
        dto.setCode(model.getArticle().getCode());
        dto.setStatus(model.getArticle().getStatus());
        dto.setSubGroupId(model.getSubGroup().getId());
        dto.setSubGroupName(model.getSubGroup().getName());
        dto.setUnitOfMeasureId(model.getUnitOfMeasure().getId());
        dto.setUnitOfMeasureName(model.getUnitOfMeasure().getName());
        dto.setUnitOfMeasureShortName(model.getUnitOfMeasure().getShortName().orElse(null));
        return dto;
    }

    private BusinessYearsResponse convertToBusinessYearsResponse(BusinessYearFirmTuple model) {
        final BusinessYearsResponse dto = new BusinessYearsResponse();
        dto.setId(model.getBusinessYear().getId());
        dto.setYear(model.getBusinessYear().getYear());
        dto.setIsFinished(model.getBusinessYear().getIsFinished());
        return dto;
    }

    private ActiveBusinessYearResponse convertToActiveBusinessYearResponse(BusinessYearFirmTuple model) {
        final ActiveBusinessYearResponse dto = new ActiveBusinessYearResponse();
        dto.setId(model.getBusinessYear().getId());
        dto.setYear(model.getBusinessYear().getYear());
        dto.setIsFinished(model.getBusinessYear().getIsFinished());
        return dto;
    }

    private BusinessYearsSearchResponse convertToBusinessYearsSearchResponse(BusinessYearFirmTuple model) {
        final BusinessYearsSearchResponse dto = new BusinessYearsSearchResponse();
        dto.setId(model.getBusinessYear().getId());
        dto.setYear(model.getBusinessYear().getYear());
        dto.setIsFinished(model.getBusinessYear().getIsFinished());
        return dto;
    }

    private WarehousesResponse convertToWarehousesResponse(WarehouseFirmTuple model) {
        final WarehousesResponse dto = new WarehousesResponse();
        dto.setId(model.getWarehouse().getId());
        dto.setCode(model.getWarehouse().getCode());
        dto.setName(model.getWarehouse().getName());
        dto.setAddress(model.getWarehouse().getAddress());
        dto.setCity(model.getWarehouse().getCity());
        dto.setCountry(model.getWarehouse().getCountry());
        return dto;
    }

    private WarehousesSearchResponse convertToWarehousesSearchResponse(WarehouseFirmTuple model) {
        final WarehousesSearchResponse dto = new WarehousesSearchResponse();
        dto.setId(model.getWarehouse().getId());
        dto.setCode(model.getWarehouse().getCode());
        dto.setName(model.getWarehouse().getName());
        dto.setAddress(model.getWarehouse().getAddress());
        dto.setCity(model.getWarehouse().getCity());
        dto.setCountry(model.getWarehouse().getCountry());
        return dto;
    }

    private WarehousesCardResponse convertToWarehousesCardResponse(WarehouseCardArticleWarehouseBusinessYearTuple model) {
        final WarehousesCardResponse dto = new WarehousesCardResponse();
        dto.setId(model.getWarehouseCard().getId());
        dto.setBusinessYearId(model.getWarehouseCard().getBusinessYear().getId());
        dto.setBusinessYearYear(model.getBusinessYear().getYear());
        dto.setArticleId(model.getArticle().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleCode(model.getArticle().getCode());
        dto.setInput(model.getWarehouseCard().getInput());
        dto.setOutput(model.getWarehouseCard().getOutput());
        dto.setBalance(model.getWarehouseCard().getBalance());
        dto.setAveragePrice(model.getWarehouseCard().getAveragePrice());
        dto.setInValue(model.getWarehouseCard().getInValue());
        dto.setOutValue(model.getWarehouseCard().getOutValue());
        dto.setTotalValue(model.getWarehouseCard().getTotalValue());
        dto.setWarehouseId(model.getWarehouse().getId());
        dto.setWarehouseName(model.getWarehouse().getName());
        dto.setStartState(model.getWarehouseCard().getStartState());
        return dto;
    }

    private WarehousesCardSearchResponse convertToWarehousesCardSearchResponse(WarehouseCardArticleWarehouseBusinessYearTuple model) {
		final WarehousesCardSearchResponse dto = new WarehousesCardSearchResponse();
		dto.setId(model.getWarehouseCard().getId());
        dto.setBusinessYearId(model.getWarehouseCard().getBusinessYear().getId());
        dto.setBusinessYearYear(model.getBusinessYear().getYear());
        dto.setArticleId(model.getArticle().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleCode(model.getArticle().getCode());
        dto.setInput(model.getWarehouseCard().getInput());
        dto.setOutput(model.getWarehouseCard().getOutput());
        dto.setBalance(model.getWarehouseCard().getBalance());
        dto.setAveragePrice(model.getWarehouseCard().getAveragePrice());
        dto.setInValue(model.getWarehouseCard().getInValue());
        dto.setOutValue(model.getWarehouseCard().getOutValue());
        dto.setTotalValue(model.getWarehouseCard().getTotalValue());
        dto.setWarehouseId(model.getWarehouse().getId());
        dto.setWarehouseName(model.getWarehouse().getName());
        dto.setStartState(model.getWarehouseCard().getStartState());
        return dto;
}

    private BusinessPartnersResponse convertToBusinessPartnersResponse(BusinessPartnerFirmTuple model) {
        final BusinessPartnersResponse dto = new BusinessPartnersResponse();
        dto.setId(model.getBusinessPartner().getId());
        dto.setName(model.getBusinessPartner().getName());
        dto.setAddress(model.getBusinessPartner().getAddress());
        dto.setCity(model.getBusinessPartner().getCity());
        dto.setCountry(model.getBusinessPartner().getCountry());
        dto.setWeb(model.getBusinessPartner().getWeb().orElse(null));
        dto.setEmail(model.getBusinessPartner().getEmail().orElse(null));
        dto.setPhone(model.getBusinessPartner().getPhone().orElse(null));
        dto.setStatus(model.getBusinessPartner().getStatus());
        return dto;
    }

    private BusinessPartnersSearchResponse convertToBusinessPartnersSearchResponse(BusinessPartnerFirmTuple model) {
        final BusinessPartnersSearchResponse dto = new BusinessPartnersSearchResponse();
        dto.setId(model.getBusinessPartner().getId());
        dto.setName(model.getBusinessPartner().getName());
        dto.setAddress(model.getBusinessPartner().getAddress());
        dto.setCity(model.getBusinessPartner().getCity());
        dto.setCountry(model.getBusinessPartner().getCountry());
        dto.setWeb(model.getBusinessPartner().getWeb().orElse(null));
        dto.setEmail(model.getBusinessPartner().getEmail().orElse(null));
        dto.setPhone(model.getBusinessPartner().getPhone().orElse(null));
        dto.setStatus(model.getBusinessPartner().getStatus());
        return dto;
    }

    private UnitOfMeasuresResponse convertToUnitOfMeasuresResponse(UnitOfMeasure model) {
        final UnitOfMeasuresResponse dto = new UnitOfMeasuresResponse();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setShortName(model.getShortName().orElse(null));
        return dto;
    }

    private UnitOfMeasuresSearchResponse convertToUnitOfMeasuresSearchResponse(UnitOfMeasure model) {
        final UnitOfMeasuresSearchResponse dto = new UnitOfMeasuresSearchResponse();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setShortName(model.getShortName().orElse(null));
        return dto;
    }



    private DocumentsSearchResponse convertToDocumentsSearchResponse(DocumentBusinessPartnerWarehouseBusinessYearTuple model) {
        final DocumentsSearchResponse dto = new DocumentsSearchResponse();
        dto.setId(model.getDocument().getId());
        dto.setDateOfValue(model.getDocument().getDateOfValue().orElse(null));
        dto.setDateOfDelivery(model.getDocument().getDateOfDelivery().orElse(null));
        dto.setWarehouseId(model.getWarehouse().getId());
        dto.setWarehouseName(model.getWarehouse().getName());
        dto.setBusinessPartnerId(model.getBusinessPartner().getId());
        dto.setName(model.getBusinessPartner().getName());
        dto.setBusinessYearId(model.getBusinessYear().getId());
        dto.setYear(model.getBusinessYear().getYear());
        dto.setWarehouseIdOut(model.getDocument().getWarehouseOut().isPresent() ? model.getDocument().getWarehouseOut().get().getId() : null);
        dto.setWarehouseNameOut(model.getDocument().getWarehouseOut().isPresent() ? model.getDocument().getWarehouseOut().get().getName() : "");
        dto.setTypeDocument(model.getDocument().getType());
        dto.setStatus(model.getBusinessPartner().getStatus());

        if(model.getDocument().getIsReversed()){
            dto.setDocumentStatus(StatusDocument.REVERSED);
        }else if(model.getDocument().getIsBooked()){
            dto.setDocumentStatus(StatusDocument.BOOKED);
        }else{
            dto.setDocumentStatus(StatusDocument.PENDING);
        }

        return dto;
    }

    private DocumentItemsResponse convertToDocumentItemsResponse(DocumentItemArticleDocumentUnitOfMeasureTuple model) {
		final DocumentItemsResponse dto = new DocumentItemsResponse();
		dto.setId(model.getDocumentItem().getId());
        dto.setQuantity(model.getDocumentItem().getQuantity());
        dto.setPrice(model.getDocumentItem().getPrice());
        dto.setDocumentId(model.getDocument().getId());
        dto.setArticleId(model.getArticle().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleUnitOfMeasureId(model.getArticle().getUnitOfMeasure().getId());
        dto.setUnitOfMeasureName(model.getUnitOfMeasure().getName());
		return dto;
}

    private DocumentItemsSearchResponse convertToDocumentItemsSearchResponse(DocumentItemArticleDocumentUnitOfMeasureTuple model) {
		final DocumentItemsSearchResponse dto = new DocumentItemsSearchResponse();
		dto.setId(model.getDocumentItem().getId());
        dto.setQuantity(model.getDocumentItem().getQuantity());
        dto.setPrice(model.getDocumentItem().getPrice());
        dto.setDocumentId(model.getDocument().getId());
        dto.setArticleId(model.getArticle().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleUnitOfMeasureId(model.getArticle().getUnitOfMeasure().getId());
        dto.setUnitOfMeasureName(model.getUnitOfMeasure().getName());
		return dto;
}

    private AnalyticsResponse convertToAnalyticsResponse(AnalyticsWarehouseCardArticleTuple model) {
        final AnalyticsResponse dto = new AnalyticsResponse();
        dto.setId(model.getAnalytics().getId());
        dto.setType(model.getAnalytics().getType());
        dto.setQuantity(model.getAnalytics().getQuantity());
        dto.setBalance(model.getAnalytics().getBalance());
        dto.setPrice(model.getAnalytics().getPrice());
        dto.setValue(model.getAnalytics().getValue());
        dto.setTotalValue(model.getAnalytics().getTotalValue());
        dto.setDirection(model.getAnalytics().getDirection());
        return dto;
    }

    private AnalyticsSearchResponse convertToAnalyticsSearchResponse(AnalyticsWarehouseCardArticleTuple model) {
        final AnalyticsSearchResponse dto = new AnalyticsSearchResponse();
        dto.setId(model.getAnalytics().getId());
        dto.setType(model.getAnalytics().getType());
        dto.setQuantity(model.getAnalytics().getQuantity());
        dto.setBalance(model.getAnalytics().getBalance());
        dto.setPrice(model.getAnalytics().getPrice());
        dto.setValue(model.getAnalytics().getValue());
        dto.setTotalValue(model.getAnalytics().getTotalValue());
        dto.setDirection(model.getAnalytics().getDirection());
        return dto;
    }

    private GroupSubGroupResponse convertToGroupSubGroupResponse(SubGroupGroupFirmTuple model) {
        final GroupSubGroupResponse dto = new GroupSubGroupResponse();
        dto.setId(model.getSubGroup().getId());
        dto.setGroupId(model.getSubGroup().getGroup().getId());
        dto.setName(model.getSubGroup().getName());
        dto.setGroupFirmName(model.getGroupFirm().getName());
        return dto;
    }

    private SubGroupArticleResponse convertToSubGroupArticleResponse(ArticleUnitOfMeasureSubGroupTuple model) {
        final SubGroupArticleResponse dto = new SubGroupArticleResponse();
        dto.setId(model.getArticle().getId());
        dto.setName(model.getArticle().getName());
        dto.setCode(model.getArticle().getCode());
        dto.setStatus(model.getArticle().getStatus());
        dto.setSubGroupId(model.getSubGroup().getId());
        dto.setSubGroupName(model.getSubGroup().getName());
        dto.setUnitOfMeasureId(model.getUnitOfMeasure().getId());
        dto.setUnitOfMeasureName(model.getUnitOfMeasure().getName());
        dto.setUnitOfMeasureShortName(model.getUnitOfMeasure().getShortName().orElse(null));
        return dto;
    }

    private WarehouseWarehouseCardResponse convertToWarehouseWarehouseCardResponse(WarehouseCardArticleWarehouseBusinessYearTuple model) {
        final WarehouseWarehouseCardResponse dto = new WarehouseWarehouseCardResponse();
        dto.setId(model.getWarehouseCard().getId());
        dto.setBusinessYearId(model.getWarehouseCard().getBusinessYear().getId());
        dto.setBusinessYearYear(model.getBusinessYear().getYear());
        dto.setArticleId(model.getArticle().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleCode(model.getArticle().getCode());
        dto.setInput(model.getWarehouseCard().getInput());
        dto.setOutput(model.getWarehouseCard().getOutput());
        dto.setBalance(model.getWarehouseCard().getBalance());
        dto.setInValue(model.getWarehouseCard().getInValue());
        dto.setOutValue(model.getWarehouseCard().getOutValue());
        dto.setTotalValue(model.getWarehouseCard().getTotalValue());
        dto.setWarehouseId(model.getWarehouse().getId());
        dto.setWarehouseName(model.getWarehouse().getName());
        return dto;
    }

    private DocumentDocumentItemResponse convertToDocumentDocumentItemResponse(DocumentItemArticleDocumentTuple model) {
        final DocumentDocumentItemResponse dto = new DocumentDocumentItemResponse();
        dto.setId(model.getDocumentItem().getId());
        dto.setQuantity(model.getDocumentItem().getQuantity());
        dto.setPrice(model.getDocumentItem().getPrice());
        dto.setDocumentId(model.getDocument().getId());
        dto.setArticleId(model.getArticle().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleUnitOfMeasureId(model.getArticle().getUnitOfMeasure().getId());
        return dto;
    }

    private ArticleDocumentItemResponse convertToArticleDocumentItemResponse(DocumentItemArticleDocumentTuple model) {
        final ArticleDocumentItemResponse dto = new ArticleDocumentItemResponse();
        dto.setId(model.getDocumentItem().getId());
        dto.setType(model.getDocumentItem().getType());
        dto.setDocumentId(model.getDocument().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleUnitOfMeasureId(model.getArticle().getUnitOfMeasure().getId());
        return dto;
    }

    private ArticleWarehouseCardResponse convertToArticleWarehouseCardResponse(WarehouseCardArticleWarehouseBusinessYearTuple model) {
        final ArticleWarehouseCardResponse dto = new ArticleWarehouseCardResponse();
        dto.setId(model.getWarehouseCard().getId());
        dto.setBusinessYearId(model.getWarehouseCard().getBusinessYear().getId());
        dto.setBusinessYearYear(model.getBusinessYear().getYear());
        dto.setArticleId(model.getArticle().getId());
        dto.setArticleName(model.getArticle().getName());
        dto.setArticleCode(model.getArticle().getCode());
        dto.setInput(model.getWarehouseCard().getInput());
        dto.setOutput(model.getWarehouseCard().getOutput());
        dto.setBalance(model.getWarehouseCard().getBalance());
        dto.setInValue(model.getWarehouseCard().getInValue());
        dto.setOutValue(model.getWarehouseCard().getOutValue());
        dto.setTotalValue(model.getWarehouseCard().getTotalValue());
        dto.setWarehouseId(model.getWarehouse().getId());
        dto.setWarehouseName(model.getWarehouse().getName());
        return dto;
    }
}

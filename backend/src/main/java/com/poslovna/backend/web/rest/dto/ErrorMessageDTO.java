package com.poslovna.backend.web.rest.dto;

/**
 * Created by Skrbic on 3/16/2017.
 */
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by ivansavic on 11/28/16.
 */
public class ErrorMessageDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ErrorMessageDTO other = (ErrorMessageDTO) obj;
        if ((message == null && other.message != null) || !message.equals(other.message))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ErrorMessageDTO[" + "message=" + message + "]";
    }

}


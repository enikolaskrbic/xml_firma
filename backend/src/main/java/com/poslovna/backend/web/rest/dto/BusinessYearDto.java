package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;


public class BusinessYearDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(max = 255)
    private String year;

    @NotNull
    private Boolean isFinished;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(Boolean isFinished) {
        this.isFinished = isFinished;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final BusinessYearDto other = (BusinessYearDto) obj;
        if ((year == null && other.year != null) || !year.equals(other.year))
            return false;
        if ((isFinished == null && other.isFinished != null) || !isFinished.equals(other.isFinished))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        result = prime * result + ((isFinished == null) ? 0 : isFinished.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "BusinessYearDto[" + "year=" + year + ", isFinished=" + isFinished + "]";
    }

}

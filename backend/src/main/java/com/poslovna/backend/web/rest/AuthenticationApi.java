package com.poslovna.backend.web.rest;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;
import javax.validation.Valid;
import com.poslovna.backend.model.*;
import com.poslovna.backend.web.rest.dto.*;

import com.poslovna.backend.repository.*;
import com.poslovna.backend.service.*;


@RestController
@RequestMapping("/api/")
public class AuthenticationApi {

    private final Logger log = LoggerFactory.getLogger(AuthenticationApi.class);

    @Inject
    private FirmRepository firmRepository;
    @Inject
    private UserService userService;

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<SignUpResponse> signUp(@Valid @RequestBody SignUpRequest request) {
        log.debug("POST /sign-up {}", request);
        final User user = userService.emailSignUp(request.getFirstName(), request.getLastName(), request.getIsDeleted(), request.getTimetamp(), request.getFirmId(), request.getEmail(),
                request.getPassword());
        return ResponseEntity.ok().body(convertToSignUpResponse(user));
    }

    @RequestMapping(value = "/sign-in", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<SignInResponse> signIn(@RequestBody SignInRequest request) {
        log.debug("POST /sign-in {}", request);
        final SignInResponse response = userService.signInXml(request.getEmail(), request.getPassword());
        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(value = "/forgot-password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<Void> forgotPassword(@Valid @RequestBody ForgotPasswordRequest request) {
        log.debug("POST /forgot-password {}", request);
        userService.forgotPassword(request.getEmail());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/reset-password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<Void> resetPassword(@Valid @RequestBody ResetPasswordRequest request) {
        log.debug("POST /reset-password {}", request);
        userService.resetPassword(request.getResetPasswordCode(), request.getNewPassword());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/verify-email", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<VerifyEmailResponse> verifyEmail(@Valid @RequestBody VerifyEmailRequest request) {
        log.debug("POST /verify-email {}", request);
        final User user = userService.verifyEmail(request.getEmailVerificationCode());
        return ResponseEntity.ok().body(convertToVerifyEmailResponse(user));
    }

    @RequestMapping(value = "/change-password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ChangePasswordResponse> changePassword(@Valid @RequestBody ChangePasswordRequest request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /change-password {}", request);
        final User user = userService.changePassword(principalId, request.getOldPassword(), request.getNewPassword());
        return ResponseEntity.ok().body(convertToChangePasswordResponse(user));
    }

    private SignUpResponse convertToSignUpResponse(User model) {
        final SignUpResponse dto = new SignUpResponse();
        dto.setId(model.getId());
        dto.setFirstName(model.getFirstName());
        dto.setLastName(model.getLastName());
        dto.setIsDeleted(model.getIsDeleted());
        dto.setTimetamp(model.getTimetamp());
        dto.setFirmId(model.getFirm().map(f -> f.getId()).orElse(null));
        dto.setRole(model.getRole());
        dto.setEmail(model.getEmail());
        return dto;
    }

    private VerifyEmailResponse convertToVerifyEmailResponse(User model) {
        final VerifyEmailResponse dto = new VerifyEmailResponse();
        dto.setId(model.getId());
        dto.setFirstName(model.getFirstName());
        dto.setLastName(model.getLastName());
        dto.setIsDeleted(model.getIsDeleted());
        dto.setTimetamp(model.getTimetamp());
        dto.setFirmId(model.getFirm().map(f -> f.getId()).orElse(null));
        dto.setRole(model.getRole());
        dto.setEmail(model.getEmail());
        return dto;
    }

    private ChangePasswordResponse convertToChangePasswordResponse(User model) {
        final ChangePasswordResponse dto = new ChangePasswordResponse();
        dto.setId(model.getId());
        dto.setFirstName(model.getFirstName());
        dto.setLastName(model.getLastName());
        dto.setIsDeleted(model.getIsDeleted());
        dto.setTimetamp(model.getTimetamp());
        dto.setFirmId(model.getFirm().map(f -> f.getId()).orElse(null));
        dto.setRole(model.getRole());
        dto.setEmail(model.getEmail());
        return dto;
    }
}

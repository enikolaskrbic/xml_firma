package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.*;


public class WarehouseCardDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal balance;

    @NotNull
    private Long warehouse;

    @NotNull
    private Long businessYear;

    @NotNull
    private Long article;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Long getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Long warehouse) {
        this.warehouse = warehouse;
    }

    public Long getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(Long businessYear) {
        this.businessYear = businessYear;
    }

    public Long getArticle() {
        return article;
    }

    public void setArticle(Long article) {
        this.article = article;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final WarehouseCardDto other = (WarehouseCardDto) obj;
        if ((balance == null && other.balance != null) || !balance.equals(other.balance))
            return false;
        if ((warehouse == null && other.warehouse != null) || !warehouse.equals(other.warehouse))
            return false;
        if ((businessYear == null && other.businessYear != null) || !businessYear.equals(other.businessYear))
            return false;
        if ((article == null && other.article != null) || !article.equals(other.article))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
        result = prime * result + ((businessYear == null) ? 0 : businessYear.hashCode());
        result = prime * result + ((article == null) ? 0 : article.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "WarehouseCardDto[" + "balance=" + balance + ", warehouse=" + warehouse + ", businessYear=" + businessYear + ", article=" + article + "]";
    }

}

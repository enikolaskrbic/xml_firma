package com.poslovna.backend.web.rest;

import javax.inject.Inject;

import com.poslovna.backend.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;
import javax.validation.Valid;
import com.poslovna.backend.model.*;
import com.poslovna.backend.web.rest.dto.*;


@RestController
@RequestMapping("/api/")
public class SecurityApi {

    private final Logger log = LoggerFactory.getLogger(SecurityApi.class);

    @Inject
    SecurityService securityService;

}

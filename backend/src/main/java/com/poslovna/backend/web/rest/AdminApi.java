package com.poslovna.backend.web.rest;

import java.util.Optional;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.poslovna.backend.model.util.ExceptionUtil;
import com.poslovna.backend.service.AdminService;
import com.poslovna.backend.web.rest.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;
import javax.validation.Valid;
import com.poslovna.backend.model.*;
import com.poslovna.backend.web.rest.dto.*;

import com.poslovna.backend.repository.*;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/api/")
public class AdminApi {

    private final Logger log = LoggerFactory.getLogger(AdminApi.class);

    @Inject
    private FirmRepository firmRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AdminService adminService;

    @Inject
    ExceptionUtil exceptionUtil;

    @RequestMapping(value = "/admin/firms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<FirmsResponse>> firms(@ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /admin/firms");
        final List<Firm> result = firmRepository.firms();
        return ResponseEntity.ok().body(result.stream().map(this::convertToFirmsResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/admin/add-firm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Void> addFirm(@Valid @RequestBody FirmDTO request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /admin/add-firm {}", request);
        try {
            adminService.addFirm(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<UsersResponse>> users(@ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("GET /admin/users");
        final List<User> result = userRepository.users();
        return ResponseEntity.ok().body(result.stream().map(this::convertToUsersResponse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/admin/add-user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Void> addUser(@Valid @RequestBody UserDTO request, @ApiIgnore @AuthenticationPrincipal Long principalId) {
        log.debug("POST /admin/add-user {}", request);
        try {
            adminService.addUser(request);
        } catch (CustomException e) {
            return exceptionUtil.catchCustomException(e, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private FirmsResponse convertToFirmsResponse(Firm model) {
        final FirmsResponse dto = new FirmsResponse();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setPib(model.getPib());
        dto.setAddress(model.getAddress());
        dto.setCity(model.getCity());
        dto.setCountry(model.getCountry());
        dto.setPhone(model.getPhone().orElse(null));
        dto.setEmail(model.getEmail().orElse(null));
        dto.setWeb(model.getWeb().orElse(null));
        return dto;
    }

    private UsersResponse convertToUsersResponse(User model) {
        final UsersResponse dto = new UsersResponse();
        dto.setFirstName(model.getFirstName());
        dto.setLastName(model.getLastName());
        dto.setFirmId(model.getFirm().map(f -> f.getId()).orElse(null));
        return dto;
    }
}

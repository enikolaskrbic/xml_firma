package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;


public class GroupIdDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @Size(max = 255)
    private String group;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final GroupIdDto other = (GroupIdDto) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((group == null && other.group != null) || !group.equals(other.group))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((group == null) ? 0 : group.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "GroupIdDto[" + "id=" + id + ", group=" + group + "]";
    }

}

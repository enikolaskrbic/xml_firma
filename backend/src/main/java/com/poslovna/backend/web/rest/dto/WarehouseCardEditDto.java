package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.*;


public class WarehouseCardEditDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final WarehouseCardEditDto other = (WarehouseCardEditDto) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((balance == null && other.balance != null) || !balance.equals(other.balance))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "WarehouseCardEditDto[" + "id=" + id + ", balance=" + balance + "]";
    }

}

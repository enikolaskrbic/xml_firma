package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;


public class SearchGroupsResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    private Long firmId;

    @NotNull
    private Boolean isDeleted;

    @NotNull
    @Size(max = 255)
    private String firmName;

    @NotNull
    @Size(max = 255)
    private String firmAddress;

    @NotNull
    @Size(max = 255)
    private String firmCity;

    @NotNull
    @Size(max = 255)
    private String firmCountry;

    @Size(max = 255)
    private String firmEmail;

    @Size(max = 255)
    private String firmPhone;

    @Size(max = 255)
    private String firmWeb;

    @NotNull
    @Size(max = 8)
    private String firmPib;

    @NotNull
    private Boolean firmIsDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFirmId() {
        return firmId;
    }

    public void setFirmId(Long firmId) {
        this.firmId = firmId;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getFirmAddress() {
        return firmAddress;
    }

    public void setFirmAddress(String firmAddress) {
        this.firmAddress = firmAddress;
    }

    public String getFirmCity() {
        return firmCity;
    }

    public void setFirmCity(String firmCity) {
        this.firmCity = firmCity;
    }

    public String getFirmCountry() {
        return firmCountry;
    }

    public void setFirmCountry(String firmCountry) {
        this.firmCountry = firmCountry;
    }

    public String getFirmEmail() {
        return firmEmail;
    }

    public void setFirmEmail(String firmEmail) {
        this.firmEmail = firmEmail;
    }

    public String getFirmPhone() {
        return firmPhone;
    }

    public void setFirmPhone(String firmPhone) {
        this.firmPhone = firmPhone;
    }

    public String getFirmWeb() {
        return firmWeb;
    }

    public void setFirmWeb(String firmWeb) {
        this.firmWeb = firmWeb;
    }

    public String getFirmPib() {
        return firmPib;
    }

    public void setFirmPib(String firmPib) {
        this.firmPib = firmPib;
    }

    public Boolean getFirmIsDeleted() {
        return firmIsDeleted;
    }

    public void setFirmIsDeleted(Boolean firmIsDeleted) {
        this.firmIsDeleted = firmIsDeleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final SearchGroupsResponse other = (SearchGroupsResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((firmId == null && other.firmId != null) || !firmId.equals(other.firmId))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        if ((firmName == null && other.firmName != null) || !firmName.equals(other.firmName))
            return false;
        if ((firmAddress == null && other.firmAddress != null) || !firmAddress.equals(other.firmAddress))
            return false;
        if ((firmCity == null && other.firmCity != null) || !firmCity.equals(other.firmCity))
            return false;
        if ((firmCountry == null && other.firmCountry != null) || !firmCountry.equals(other.firmCountry))
            return false;
        if ((firmEmail == null && other.firmEmail != null) || !firmEmail.equals(other.firmEmail))
            return false;
        if ((firmPhone == null && other.firmPhone != null) || !firmPhone.equals(other.firmPhone))
            return false;
        if ((firmWeb == null && other.firmWeb != null) || !firmWeb.equals(other.firmWeb))
            return false;
        if ((firmPib == null && other.firmPib != null) || !firmPib.equals(other.firmPib))
            return false;
        if ((firmIsDeleted == null && other.firmIsDeleted != null) || !firmIsDeleted.equals(other.firmIsDeleted))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((firmId == null) ? 0 : firmId.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        result = prime * result + ((firmName == null) ? 0 : firmName.hashCode());
        result = prime * result + ((firmAddress == null) ? 0 : firmAddress.hashCode());
        result = prime * result + ((firmCity == null) ? 0 : firmCity.hashCode());
        result = prime * result + ((firmCountry == null) ? 0 : firmCountry.hashCode());
        result = prime * result + ((firmEmail == null) ? 0 : firmEmail.hashCode());
        result = prime * result + ((firmPhone == null) ? 0 : firmPhone.hashCode());
        result = prime * result + ((firmWeb == null) ? 0 : firmWeb.hashCode());
        result = prime * result + ((firmPib == null) ? 0 : firmPib.hashCode());
        result = prime * result + ((firmIsDeleted == null) ? 0 : firmIsDeleted.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "SearchGroupsResponse[" + "id=" + id + ", name=" + name + ", firmId=" + firmId + ", isDeleted=" + isDeleted + ", firmName=" + firmName + ", firmAddress=" + firmAddress + ", firmCity="
                + firmCity + ", firmCountry=" + firmCountry + ", firmEmail=" + firmEmail + ", firmPhone=" + firmPhone + ", firmWeb=" + firmWeb + ", firmPib=" + firmPib + ", firmIsDeleted="
                + firmIsDeleted + "]";
    }

}

package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class ArticleEditDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    @Size(max = 255)
    private String code;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    private RoleArticle status;

    @NotNull
    private Long subGroup;

    @NotNull
    private Long unitOfMeasure;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleArticle getStatus() {
        return status;
    }

    public void setStatus(RoleArticle status) {
        this.status = status;
    }

    public Long getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(Long subGroup) {
        this.subGroup = subGroup;
    }

    public Long getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(Long unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ArticleEditDto other = (ArticleEditDto) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((code == null && other.code != null) || !code.equals(other.code))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((status == null && other.status != null) || !status.equals(other.status))
            return false;
        if ((subGroup == null && other.subGroup != null) || !subGroup.equals(other.subGroup))
            return false;
        if ((unitOfMeasure == null && other.unitOfMeasure != null) || !unitOfMeasure.equals(other.unitOfMeasure))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((subGroup == null) ? 0 : subGroup.hashCode());
        result = prime * result + ((unitOfMeasure == null) ? 0 : unitOfMeasure.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ArticleEditDto[" + "id=" + id + ", code=" + code + ", name=" + name + ", status=" + status + ", subGroup=" + subGroup + ", unitOfMeasure=" + unitOfMeasure + "]";
    }

}

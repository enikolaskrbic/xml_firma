package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.*;


public class DocumentItemsResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    private BigDecimal quantity;

    @NotNull
    private BigDecimal price;

    @NotNull
    private Long documentId;

    @NotNull
    private Long articleId;

    @NotNull
    @Size(max = 255)
    private String articleName;

    @NotNull
    private Long articleUnitOfMeasureId;

    @NotNull
    @Size(max = 255)
    private String unitOfMeasureName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public Long getArticleUnitOfMeasureId() {
        return articleUnitOfMeasureId;
    }

    public void setArticleUnitOfMeasureId(Long articleUnitOfMeasureId) {
        this.articleUnitOfMeasureId = articleUnitOfMeasureId;
    }

    public String getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        this.unitOfMeasureName = unitOfMeasureName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final DocumentItemsResponse other = (DocumentItemsResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((quantity == null && other.quantity != null) || !quantity.equals(other.quantity))
            return false;
        if ((price == null && other.price != null) || !price.equals(other.price))
            return false;
        if ((documentId == null && other.documentId != null) || !documentId.equals(other.documentId))
            return false;
        if ((articleId == null && other.articleId != null) || !articleId.equals(other.articleId))
            return false;
        if ((articleName == null && other.articleName != null) || !articleName.equals(other.articleName))
            return false;
        if ((articleUnitOfMeasureId == null && other.articleUnitOfMeasureId != null) || !articleUnitOfMeasureId.equals(other.articleUnitOfMeasureId))
            return false;
        if ((unitOfMeasureName == null && other.unitOfMeasureName != null) || !unitOfMeasureName.equals(other.unitOfMeasureName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((documentId == null) ? 0 : documentId.hashCode());
        result = prime * result + ((articleId == null) ? 0 : articleId.hashCode());
        result = prime * result + ((articleName == null) ? 0 : articleName.hashCode());
        result = prime * result + ((articleUnitOfMeasureId == null) ? 0 : articleUnitOfMeasureId.hashCode());
        result = prime * result + ((unitOfMeasureName == null) ? 0 : unitOfMeasureName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "DocumentItemsResponse[" + "id=" + id + ", quantity=" + quantity + ", price=" + price + ", documentId=" + documentId + ", articleId=" + articleId + ", articleName=" + articleName
                + ", articleUnitOfMeasureId=" + articleUnitOfMeasureId + ", unitOfMeasureName=" + unitOfMeasureName + "]";
    }

}

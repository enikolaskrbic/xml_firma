package com.poslovna.backend.web.rest.exception;

import com.poslovna.backend.model.enumeration.ExceptionMessage;

/**
 * Created by Skrbic on 3/16/2017.
 */
public class CustomException extends Exception{
    public CustomException(ExceptionMessage msg) {
        super(String.valueOf(msg));
    }

    public CustomException(String msg){
        super(msg);
    }

}

package com.poslovna.backend.web.rest.exception;

public class UserWithEmailAlreadyExists extends RuntimeException {

    public UserWithEmailAlreadyExists(String message) {
        super(message);
    }
}
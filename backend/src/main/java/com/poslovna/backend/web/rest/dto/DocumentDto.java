package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import java.time.*;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class DocumentDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private TypeDocument type;

    private ZonedDateTime dateOfDelivery;

    private ZonedDateTime dateOfValue;

    private ZonedDateTime dateOfStatement;

    @NotNull
    private Long businessYear;

    private Long businessPartner;

    @NotNull
    private Long warehouse;

    private Long warehouseOut;

    public TypeDocument getType() {
        return type;
    }

    public void setType(TypeDocument type) {
        this.type = type;
    }

    public ZonedDateTime getDateOfDelivery() {
        return dateOfDelivery;
    }

    public void setDateOfDelivery(ZonedDateTime dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }

    public ZonedDateTime getDateOfValue() {
        return dateOfValue;
    }

    public void setDateOfValue(ZonedDateTime dateOfValue) {
        this.dateOfValue = dateOfValue;
    }

    public ZonedDateTime getDateOfStatement() {
        return dateOfStatement;
    }

    public void setDateOfStatement(ZonedDateTime dateOfStatement) {
        this.dateOfStatement = dateOfStatement;
    }

    public Long getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(Long businessYear) {
        this.businessYear = businessYear;
    }

    public Long getBusinessPartner() {
        return businessPartner;
    }

    public void setBusinessPartner(Long businessPartner) {
        this.businessPartner = businessPartner;
    }

    public Long getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Long warehouse) {
        this.warehouse = warehouse;
    }

    public Long getWarehouseOut() {
        return warehouseOut;
    }

    public void setWarehouseOut(Long warehouseOut) {
        this.warehouseOut = warehouseOut;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final DocumentDto other = (DocumentDto) obj;
        if ((type == null && other.type != null) || !type.equals(other.type))
            return false;
        if ((dateOfDelivery == null && other.dateOfDelivery != null) || !dateOfDelivery.equals(other.dateOfDelivery))
            return false;
        if ((dateOfValue == null && other.dateOfValue != null) || !dateOfValue.equals(other.dateOfValue))
            return false;
        if ((dateOfStatement == null && other.dateOfStatement != null) || !dateOfStatement.equals(other.dateOfStatement))
            return false;
        if ((businessYear == null && other.businessYear != null) || !businessYear.equals(other.businessYear))
            return false;
        if ((businessPartner == null && other.businessPartner != null) || !businessPartner.equals(other.businessPartner))
            return false;
        if ((warehouse == null && other.warehouse != null) || !warehouse.equals(other.warehouse))
            return false;
        if ((warehouseOut == null && other.warehouseOut != null) || !warehouseOut.equals(other.warehouseOut))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((dateOfDelivery == null) ? 0 : dateOfDelivery.hashCode());
        result = prime * result + ((dateOfValue == null) ? 0 : dateOfValue.hashCode());
        result = prime * result + ((dateOfStatement == null) ? 0 : dateOfStatement.hashCode());
        result = prime * result + ((businessYear == null) ? 0 : businessYear.hashCode());
        result = prime * result + ((businessPartner == null) ? 0 : businessPartner.hashCode());
        result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
        result = prime * result + ((warehouseOut == null) ? 0 : warehouseOut.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "DocumentDto[" + "type=" + type + ", dateOfDelivery=" + dateOfDelivery + ", dateOfValue=" + dateOfValue + ", dateOfStatement=" + dateOfStatement + ", businessYear=" + businessYear
                + ", businessPartner=" + businessPartner + ", warehouse=" + warehouse + ", warehouseOut=" + warehouseOut + "]";
    }

}

package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.*;


public class DocumentItemDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long document;

    @NotNull
    private BigDecimal quantity;

    @NotNull
    private BigDecimal price;

    @NotNull
    private Long article;

    public Long getDocument() {
        return document;
    }

    public void setDocument(Long document) {
        this.document = document;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getArticle() {
        return article;
    }

    public void setArticle(Long article) {
        this.article = article;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final DocumentItemDto other = (DocumentItemDto) obj;
        if ((document == null && other.document != null) || !document.equals(other.document))
            return false;
        if ((quantity == null && other.quantity != null) || !quantity.equals(other.quantity))
            return false;
        if ((price == null && other.price != null) || !price.equals(other.price))
            return false;
        if ((article == null && other.article != null) || !article.equals(other.article))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((document == null) ? 0 : document.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((article == null) ? 0 : article.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "DocumentItemDto[" + "document=" + document + ", quantity=" + quantity + ", price=" + price + ", article=" + article + "]";
    }

}

package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class ArticleDocumentItemResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    private TypeDocument type;

    @NotNull
    private Long documentId;

    @NotNull
    @Size(max = 255)
    private String articleName;

    @NotNull
    private Long articleUnitOfMeasureId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeDocument getType() {
        return type;
    }

    public void setType(TypeDocument type) {
        this.type = type;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public Long getArticleUnitOfMeasureId() {
        return articleUnitOfMeasureId;
    }

    public void setArticleUnitOfMeasureId(Long articleUnitOfMeasureId) {
        this.articleUnitOfMeasureId = articleUnitOfMeasureId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ArticleDocumentItemResponse other = (ArticleDocumentItemResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((type == null && other.type != null) || !type.equals(other.type))
            return false;
        if ((documentId == null && other.documentId != null) || !documentId.equals(other.documentId))
            return false;
        if ((articleName == null && other.articleName != null) || !articleName.equals(other.articleName))
            return false;
        if ((articleUnitOfMeasureId == null && other.articleUnitOfMeasureId != null) || !articleUnitOfMeasureId.equals(other.articleUnitOfMeasureId))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((documentId == null) ? 0 : documentId.hashCode());
        result = prime * result + ((articleName == null) ? 0 : articleName.hashCode());
        result = prime * result + ((articleUnitOfMeasureId == null) ? 0 : articleUnitOfMeasureId.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ArticleDocumentItemResponse[" + "id=" + id + ", type=" + type + ", documentId=" + documentId + ", articleName=" + articleName + ", articleUnitOfMeasureId=" + articleUnitOfMeasureId
                + "]";
    }

}

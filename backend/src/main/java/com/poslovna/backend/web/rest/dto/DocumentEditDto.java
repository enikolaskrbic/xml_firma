package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import java.time.*;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class DocumentEditDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    private TypeDocument type;

    private ZonedDateTime dateOfDelivery;

    @NotNull
    private ZonedDateTime dateOfValue;

    private ZonedDateTime dateOfStatement;

    @Size(max = 255)
    private String paymentOfAccount;

    @Size(max = 255)
    private String referenceNumber;

    @NotNull
    private Long businessYear;


    private Long businessPartner;

    @NotNull
    private Long warehouse;

    private Long warehouseOut;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeDocument getType() {
        return type;
    }

    public void setType(TypeDocument type) {
        this.type = type;
    }

    public ZonedDateTime getDateOfDelivery() {
        return dateOfDelivery;
    }

    public void setDateOfDelivery(ZonedDateTime dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery;
    }

    public ZonedDateTime getDateOfValue() {
        return dateOfValue;
    }

    public void setDateOfValue(ZonedDateTime dateOfValue) {
        this.dateOfValue = dateOfValue;
    }

    public ZonedDateTime getDateOfStatement() {
        return dateOfStatement;
    }

    public void setDateOfStatement(ZonedDateTime dateOfStatement) {
        this.dateOfStatement = dateOfStatement;
    }

    public String getPaymentOfAccount() {
        return paymentOfAccount;
    }

    public void setPaymentOfAccount(String paymentOfAccount) {
        this.paymentOfAccount = paymentOfAccount;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Long getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(Long businessYear) {
        this.businessYear = businessYear;
    }

    public Long getBusinessPartner() {
        return businessPartner;
    }

    public void setBusinessPartner(Long businessPartner) {
        this.businessPartner = businessPartner;
    }

    public Long getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Long warehouse) {
        this.warehouse = warehouse;
    }

    public Long getWarehouseOut() {
        return warehouseOut;
    }

    public void setWarehouseOut(Long warehouseOut) {
        this.warehouseOut = warehouseOut;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final DocumentEditDto other = (DocumentEditDto) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((type == null && other.type != null) || !type.equals(other.type))
            return false;
        if ((dateOfDelivery == null && other.dateOfDelivery != null) || !dateOfDelivery.equals(other.dateOfDelivery))
            return false;
        if ((dateOfValue == null && other.dateOfValue != null) || !dateOfValue.equals(other.dateOfValue))
            return false;
        if ((dateOfStatement == null && other.dateOfStatement != null) || !dateOfStatement.equals(other.dateOfStatement))
            return false;
        if ((paymentOfAccount == null && other.paymentOfAccount != null) || !paymentOfAccount.equals(other.paymentOfAccount))
            return false;
        if ((referenceNumber == null && other.referenceNumber != null) || !referenceNumber.equals(other.referenceNumber))
            return false;
        if ((businessYear == null && other.businessYear != null) || !businessYear.equals(other.businessYear))
            return false;
        if ((businessPartner == null && other.businessPartner != null) || !businessPartner.equals(other.businessPartner))
            return false;
        if ((warehouse == null && other.warehouse != null) || !warehouse.equals(other.warehouse))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((dateOfDelivery == null) ? 0 : dateOfDelivery.hashCode());
        result = prime * result + ((dateOfValue == null) ? 0 : dateOfValue.hashCode());
        result = prime * result + ((dateOfStatement == null) ? 0 : dateOfStatement.hashCode());
        result = prime * result + ((paymentOfAccount == null) ? 0 : paymentOfAccount.hashCode());
        result = prime * result + ((referenceNumber == null) ? 0 : referenceNumber.hashCode());
        result = prime * result + ((businessYear == null) ? 0 : businessYear.hashCode());
        result = prime * result + ((businessPartner == null) ? 0 : businessPartner.hashCode());
        result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "DocumentEditDto[" + "id=" + id + ", type=" + type + ", dateOfDelivery=" + dateOfDelivery + ", dateOfValue=" + dateOfValue + ", dateOfStatement=" + dateOfStatement
                + ", paymentOfAccount=" + paymentOfAccount + ", referenceNumber=" + referenceNumber + ", businessYear=" + businessYear + ", businessPartner=" + businessPartner + ", warehouse="
                + warehouse + "]";
    }

}

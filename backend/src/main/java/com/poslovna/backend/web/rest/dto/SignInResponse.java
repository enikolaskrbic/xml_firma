package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;

import java.time.*;

import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


public class SignInResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 64, max = 64)
    private String accessToken;


    private Long id;


    @Size(max = 255)
    private String firstName;

    @Size(max = 255)
    private String lastName;


    private Boolean isDeleted;


    private ZonedDateTime timetamp;

    private Long firmId;


    private Role role;


    @Size(min = 6, max = 128)
    @Pattern(regexp = "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$")
    private String email;

    private String korisnickoIme;

    private String rola;

    private int pibFirme;

    private String apiFirme;


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public ZonedDateTime getTimetamp() {
        return timetamp;
    }

    public void setTimetamp(ZonedDateTime timetamp) {
        this.timetamp = timetamp;
    }

    public Long getFirmId() {
        return firmId;
    }

    public void setFirmId(Long firmId) {
        this.firmId = firmId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getRola() {
        return rola;
    }

    public void setRola(String rola) {
        this.rola = rola;
    }

    public int getPibFirme() {
        return pibFirme;
    }

    public void setPibFirme(int pibFirme) {
        this.pibFirme = pibFirme;
    }

    public String getApiFirme() {
        return apiFirme;
    }

    public void setApiFirme(String apiFirme) {
        this.apiFirme = apiFirme;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final SignInResponse other = (SignInResponse) obj;
        if ((accessToken == null && other.accessToken != null) || !accessToken.equals(other.accessToken))
            return false;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((firstName == null && other.firstName != null) || !firstName.equals(other.firstName))
            return false;
        if ((lastName == null && other.lastName != null) || !lastName.equals(other.lastName))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        if ((timetamp == null && other.timetamp != null) || !timetamp.equals(other.timetamp))
            return false;
        if ((firmId == null && other.firmId != null) || !firmId.equals(other.firmId))
            return false;
        if ((role == null && other.role != null) || !role.equals(other.role))
            return false;
        if ((email == null && other.email != null) || !email.equals(other.email))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accessToken == null) ? 0 : accessToken.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        result = prime * result + ((timetamp == null) ? 0 : timetamp.hashCode());
        result = prime * result + ((firmId == null) ? 0 : firmId.hashCode());
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "SignInResponse[" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", isDeleted=" + isDeleted + ", timetamp=" + timetamp + ", firmId=" + firmId + ", role=" + role
                + ", email=" + email + "]";
    }

}

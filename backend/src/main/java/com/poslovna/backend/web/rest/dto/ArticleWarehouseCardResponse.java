package com.poslovna.backend.web.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.*;


public class ArticleWarehouseCardResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @NotNull
    private Long businessYearId;

    @NotNull
    @Size(max = 4)
    private String businessYearYear;

    @NotNull
    private Long articleId;

    @NotNull
    @Size(max = 255)
    private String articleName;

    @NotNull
    @Size(max = 255)
    private String articleCode;

    @NotNull
    private BigDecimal input;

    @NotNull
    private BigDecimal output;

    @NotNull
    private BigDecimal balance;

    @NotNull
    private BigDecimal inValue;

    @NotNull
    private BigDecimal outValue;

    @NotNull
    private BigDecimal totalValue;

    @NotNull
    private Long warehouseId;

    @NotNull
    @Size(max = 255)
    private String warehouseName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBusinessYearId() {
        return businessYearId;
    }

    public void setBusinessYearId(Long businessYearId) {
        this.businessYearId = businessYearId;
    }

    public String getBusinessYearYear() {
        return businessYearYear;
    }

    public void setBusinessYearYear(String businessYearYear) {
        this.businessYearYear = businessYearYear;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public BigDecimal getInput() {
        return input;
    }

    public void setInput(BigDecimal input) {
        this.input = input;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getInValue() {
        return inValue;
    }

    public void setInValue(BigDecimal inValue) {
        this.inValue = inValue;
    }

    public BigDecimal getOutValue() {
        return outValue;
    }

    public void setOutValue(BigDecimal outValue) {
        this.outValue = outValue;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ArticleWarehouseCardResponse other = (ArticleWarehouseCardResponse) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((businessYearId == null && other.businessYearId != null) || !businessYearId.equals(other.businessYearId))
            return false;
        if ((businessYearYear == null && other.businessYearYear != null) || !businessYearYear.equals(other.businessYearYear))
            return false;
        if ((articleId == null && other.articleId != null) || !articleId.equals(other.articleId))
            return false;
        if ((articleName == null && other.articleName != null) || !articleName.equals(other.articleName))
            return false;
        if ((articleCode == null && other.articleCode != null) || !articleCode.equals(other.articleCode))
            return false;
        if ((input == null && other.input != null) || !input.equals(other.input))
            return false;
        if ((output == null && other.output != null) || !output.equals(other.output))
            return false;
        if ((balance == null && other.balance != null) || !balance.equals(other.balance))
            return false;
        if ((inValue == null && other.inValue != null) || !inValue.equals(other.inValue))
            return false;
        if ((outValue == null && other.outValue != null) || !outValue.equals(other.outValue))
            return false;
        if ((totalValue == null && other.totalValue != null) || !totalValue.equals(other.totalValue))
            return false;
        if ((warehouseId == null && other.warehouseId != null) || !warehouseId.equals(other.warehouseId))
            return false;
        if ((warehouseName == null && other.warehouseName != null) || !warehouseName.equals(other.warehouseName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((businessYearId == null) ? 0 : businessYearId.hashCode());
        result = prime * result + ((businessYearYear == null) ? 0 : businessYearYear.hashCode());
        result = prime * result + ((articleId == null) ? 0 : articleId.hashCode());
        result = prime * result + ((articleName == null) ? 0 : articleName.hashCode());
        result = prime * result + ((articleCode == null) ? 0 : articleCode.hashCode());
        result = prime * result + ((input == null) ? 0 : input.hashCode());
        result = prime * result + ((output == null) ? 0 : output.hashCode());
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + ((inValue == null) ? 0 : inValue.hashCode());
        result = prime * result + ((outValue == null) ? 0 : outValue.hashCode());
        result = prime * result + ((totalValue == null) ? 0 : totalValue.hashCode());
        result = prime * result + ((warehouseId == null) ? 0 : warehouseId.hashCode());
        result = prime * result + ((warehouseName == null) ? 0 : warehouseName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ArticleWarehouseCardResponse[" + "id=" + id + ", businessYearId=" + businessYearId + ", businessYearYear=" + businessYearYear + ", articleId=" + articleId + ", articleName="
                + articleName + ", articleCode=" + articleCode + ", input=" + input + ", output=" + output + ", balance=" + balance + ", inValue=" + inValue + ", outValue=" + outValue
                + ", totalValue=" + totalValue + ", warehouseId=" + warehouseId + ", warehouseName=" + warehouseName + "]";
    }

}

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.05.11 at 07:31:09 PM CEST 
//


package com.poslovna.backend.xml_model.nalogzaplacanje;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the security.services.web.xml.nalogzaplacanje package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: security.services.web.xml.nalogzaplacanje
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NalogZaPlacanjeResponse }
     * 
     */
    public NalogZaPlacanjeResponse createNalogZaPlacanjeResponse() {
        return new NalogZaPlacanjeResponse();
    }

    /**
     * Create an instance of {@link NalogZaPlacanjeRequest }
     * 
     */
    public NalogZaPlacanjeRequest createNalogZaPlacanjeRequest() {
        return new NalogZaPlacanjeRequest();
    }

    /**
     * Create an instance of {@link Podaci }
     * 
     */
    public Podaci createPodaci() {
        return new Podaci();
    }

}

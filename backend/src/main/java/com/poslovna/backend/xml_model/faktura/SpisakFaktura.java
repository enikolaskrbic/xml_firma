//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5.1 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.05.06 at 12:39:06 PM CEST 
//


package com.poslovna.backend.xml_model.faktura;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listaUlaznihFaktura" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://xml.web.services.security/faktura}faktura" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="pibFirme" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *                       &lt;minExclusive value="9999999"/>
 *                       &lt;maxExclusive value="100000000"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="listaIzlaznihFaktura" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://xml.web.services.security/faktura}faktura" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="pibFirme" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *                       &lt;minExclusive value="9999999"/>
 *                       &lt;maxExclusive value="100000000"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listaUlaznihFaktura",
    "listaIzlaznihFaktura"
})
@XmlRootElement(name = "spisakFaktura")
public class SpisakFaktura {

    @XmlElement(required = true)
    protected List<SpisakFaktura.ListaUlaznihFaktura> listaUlaznihFaktura;
    @XmlElement(required = true)
    protected List<SpisakFaktura.ListaIzlaznihFaktura> listaIzlaznihFaktura;

    /**
     * Gets the value of the listaUlaznihFaktura property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaUlaznihFaktura property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaUlaznihFaktura().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpisakFaktura.ListaUlaznihFaktura }
     * 
     * 
     */
    public List<SpisakFaktura.ListaUlaznihFaktura> getListaUlaznihFaktura() {
        if (listaUlaznihFaktura == null) {
            listaUlaznihFaktura = new ArrayList<SpisakFaktura.ListaUlaznihFaktura>();
        }
        return this.listaUlaznihFaktura;
    }

    /**
     * Gets the value of the listaIzlaznihFaktura property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaIzlaznihFaktura property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaIzlaznihFaktura().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpisakFaktura.ListaIzlaznihFaktura }
     * 
     * 
     */
    public List<SpisakFaktura.ListaIzlaznihFaktura> getListaIzlaznihFaktura() {
        if (listaIzlaznihFaktura == null) {
            listaIzlaznihFaktura = new ArrayList<SpisakFaktura.ListaIzlaznihFaktura>();
        }
        return this.listaIzlaznihFaktura;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://xml.web.services.security/faktura}faktura" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="pibFirme" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
     *             &lt;minExclusive value="9999999"/>
     *             &lt;maxExclusive value="100000000"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "faktura"
    })
    public static class ListaIzlaznihFaktura {

        protected List<Faktura> faktura;
        @XmlAttribute(name = "pibFirme", required = true)
        protected int pibFirme;

        /**
         * Gets the value of the faktura property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the faktura property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFaktura().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Faktura }
         * 
         * 
         */
        public List<Faktura> getFaktura() {
            if (faktura == null) {
                faktura = new ArrayList<Faktura>();
            }
            return this.faktura;
        }

        /**
         * Gets the value of the pibFirme property.
         * 
         */
        public int getPibFirme() {
            return pibFirme;
        }

        /**
         * Sets the value of the pibFirme property.
         * 
         */
        public void setPibFirme(int value) {
            this.pibFirme = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://xml.web.services.security/faktura}faktura" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="pibFirme" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
     *             &lt;minExclusive value="9999999"/>
     *             &lt;maxExclusive value="100000000"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "faktura"
    })
    public static class ListaUlaznihFaktura {

        protected List<Faktura> faktura;
        @XmlAttribute(name = "pibFirme", required = true)
        protected int pibFirme;

        /**
         * Gets the value of the faktura property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the faktura property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFaktura().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Faktura }
         * 
         * 
         */
        public List<Faktura> getFaktura() {
            if (faktura == null) {
                faktura = new ArrayList<Faktura>();
            }
            return this.faktura;
        }

        /**
         * Gets the value of the pibFirme property.
         * 
         */
        public int getPibFirme() {
            return pibFirme;
        }

        /**
         * Sets the value of the pibFirme property.
         * 
         */
        public void setPibFirme(int value) {
            this.pibFirme = value;
        }

    }

}

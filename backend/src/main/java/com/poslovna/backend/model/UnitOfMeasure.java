package com.poslovna.backend.model;

import java.io.Serializable;

import java.util.Optional;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "UnitOfMeasure")
public class UnitOfMeasure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @Size(max = 255)
    @Column(name = "shortName")
    private String shortName;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<String> getShortName() {
        return Optional.ofNullable(shortName);
    }

    public void setShortName(Optional<String> shortName) {
        this.shortName = shortName.orElse(null);
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final UnitOfMeasure other = (UnitOfMeasure) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((shortName == null && other.shortName != null) || !shortName.equals(other.shortName))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "UnitOfMeasure[" + "id=" + id + ", name=" + name + ", shortName=" + shortName + ", isDeleted=" + isDeleted + "]";
    }

}

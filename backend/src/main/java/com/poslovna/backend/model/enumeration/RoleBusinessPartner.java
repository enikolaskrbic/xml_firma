package com.poslovna.backend.model.enumeration;

public enum RoleBusinessPartner {

    BUYER, SUPPLIER, BUYER_AND_SUPPLIER

}

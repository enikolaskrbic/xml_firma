package com.poslovna.backend.model;

import java.io.Serializable;

import java.util.Optional;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "Firm")
public class Firm implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @NotNull
    @Size(max = 255)
    @Column(name = "address")
    private String address;

    @NotNull
    @Size(max = 255)
    @Column(name = "city")
    private String city;

    @NotNull
    @Size(max = 255)
    @Column(name = "country")
    private String country;

    @Size(max = 255)
    @Column(name = "email")
    private String email;

    @Size(max = 255)
    @Column(name = "phone")
    private String phone;

    @Size(max = 255)
    @Column(name = "web")
    private String web;

    @NotNull
    @Size(max = 8)
    @Column(name = "pib")
    private String pib;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }

    public void setEmail(Optional<String> email) {
        this.email = email.orElse(null);
    }

    public Optional<String> getPhone() {
        return Optional.ofNullable(phone);
    }

    public void setPhone(Optional<String> phone) {
        this.phone = phone.orElse(null);
    }

    public Optional<String> getWeb() {
        return Optional.ofNullable(web);
    }

    public void setWeb(Optional<String> web) {
        this.web = web.orElse(null);
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Firm other = (Firm) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((name == null && other.name != null) || !name.equals(other.name))
            return false;
        if ((address == null && other.address != null) || !address.equals(other.address))
            return false;
        if ((city == null && other.city != null) || !city.equals(other.city))
            return false;
        if ((country == null && other.country != null) || !country.equals(other.country))
            return false;
        if ((email == null && other.email != null) || !email.equals(other.email))
            return false;
        if ((phone == null && other.phone != null) || !phone.equals(other.phone))
            return false;
        if ((web == null && other.web != null) || !web.equals(other.web))
            return false;
        if ((pib == null && other.pib != null) || !pib.equals(other.pib))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((web == null) ? 0 : web.hashCode());
        result = prime * result + ((pib == null) ? 0 : pib.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Firm[" + "id=" + id + ", name=" + name + ", address=" + address + ", city=" + city + ", country=" + country + ", email=" + email + ", phone=" + phone + ", web=" + web + ", pib=" + pib
                + ", isDeleted=" + isDeleted + "]";
    }

}

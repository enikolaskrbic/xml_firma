package com.poslovna.backend.model.enumeration;

/**
 * Created by ivansavic on 4/22/17.
 */
public enum StatusDocument {
    PENDING, BOOKED, REVERSED
}

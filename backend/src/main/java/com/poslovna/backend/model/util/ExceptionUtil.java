package com.poslovna.backend.model.util;

/**
 * Created by Skrbic on 3/16/2017.
 */
import com.poslovna.backend.web.rest.dto.ErrorMessageDTO;
import com.poslovna.backend.web.rest.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.sql.SQLException;

/**
 * Created by ivansavic on 12/19/16.
 */
@Component
public class ExceptionUtil extends  Throwable{

    public ResponseEntity catchCustomException(CustomException e, HttpStatus httpStatus){
        ErrorMessageDTO errorMessageDTO = new ErrorMessageDTO();
        errorMessageDTO.setMessage(e.getMessage());

        return new ResponseEntity(errorMessageDTO, httpStatus);
    }

    public ResponseEntity catchCustomException(SQLException e, HttpStatus httpStatus){
        ErrorMessageDTO errorMessageDTO = new ErrorMessageDTO();
        errorMessageDTO.setMessage(e.getMessage());

        return new ResponseEntity(errorMessageDTO, httpStatus);
    }

    public ResponseEntity catchCustomException(String message, HttpStatus httpStatus){
        ErrorMessageDTO errorMessageDTO = new ErrorMessageDTO();
        errorMessageDTO.setMessage(message);

        return new ResponseEntity(errorMessageDTO, httpStatus);
    }
}

package com.poslovna.backend.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.*;

import java.util.Optional;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


@Entity
@Table(name = "Analytics")
public class Analytics implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "date")
    private ZonedDateTime date;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeAnalytics type;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "direction")
    private TypeDirection direction;

    @NotNull
    @Column(name = "quantity")
    private BigDecimal quantity;

    @NotNull
    @Column(name = "balance")
    private BigDecimal balance;

    @NotNull
    @Column(name = "price")
    private BigDecimal price;

    @NotNull
    @Column(name = "value")
    private BigDecimal value;

    @NotNull
    @Column(name = "totalValue")
    private BigDecimal totalValue;

    @ManyToOne
    @JoinColumn(name = "documentItemId")
    private DocumentItem documentItem;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "warehouseCardId")
    private WarehouseCard warehouseCard;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public TypeAnalytics getType() {
        return type;
    }

    public void setType(TypeAnalytics type) {
        this.type = type;
    }

    public TypeDirection getDirection() {
        return direction;
    }

    public void setDirection(TypeDirection direction) {
        this.direction = direction;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public Optional<DocumentItem> getDocumentItem() {
        return Optional.ofNullable(documentItem);
    }

    public void setDocumentItem(Optional<DocumentItem> documentItem) {
        this.documentItem = documentItem.orElse(null);
    }

    public WarehouseCard getWarehouseCard() {
        return warehouseCard;
    }

    public void setWarehouseCard(WarehouseCard warehouseCard) {
        this.warehouseCard = warehouseCard;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Analytics other = (Analytics) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((date == null && other.date != null) || !date.equals(other.date))
            return false;
        if ((type == null && other.type != null) || !type.equals(other.type))
            return false;
        if ((direction == null && other.direction != null) || !direction.equals(other.direction))
            return false;
        if ((quantity == null && other.quantity != null) || !quantity.equals(other.quantity))
            return false;
        if ((balance == null && other.balance != null) || !balance.equals(other.balance))
            return false;
        if ((price == null && other.price != null) || !price.equals(other.price))
            return false;
        if ((value == null && other.value != null) || !value.equals(other.value))
            return false;
        if ((totalValue == null && other.totalValue != null) || !totalValue.equals(other.totalValue))
            return false;
        if ((documentItem == null && other.documentItem != null) || !documentItem.equals(other.documentItem))
            return false;
        if ((warehouseCard == null && other.warehouseCard != null) || !warehouseCard.equals(other.warehouseCard))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((direction == null) ? 0 : direction.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        result = prime * result + ((totalValue == null) ? 0 : totalValue.hashCode());
        result = prime * result + ((documentItem == null) ? 0 : documentItem.hashCode());
        result = prime * result + ((warehouseCard == null) ? 0 : warehouseCard.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Analytics[" + "id=" + id + ", date=" + date + ", type=" + type + ", direction=" + direction + ", quantity=" + quantity + ", balance=" + balance + ", price=" + price + ", value="
                + value + ", totalValue=" + totalValue + ", documentItem=" + documentItem + ", warehouseCard=" + warehouseCard + ", isDeleted=" + isDeleted + "]";
    }

}

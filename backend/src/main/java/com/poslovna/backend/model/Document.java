package com.poslovna.backend.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.*;

import java.util.Optional;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


@Entity
@Table(name = "Document")
public class Document implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeDocument type;

    @Column(name = "dateOfDelivery")
    private ZonedDateTime dateOfDelivery;

    @Column(name = "dateOfValue")
    private ZonedDateTime dateOfValue;

    @Column(name = "dateOfStatement")
    private ZonedDateTime dateOfStatement;

    @Column(name = "amount")
    private BigDecimal amount;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "businessYearId")
    private BusinessYear businessYear;

    @ManyToOne
    @JoinColumn(name = "businessPartnerId")
    private BusinessPartner businessPartner;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "warehouseId")
    private Warehouse warehouse;

    @ManyToOne
    @JoinColumn(name = "warehouseOutId")
    private Warehouse warehouseOut;

    @NotNull
    @Column(name = "isReversed")
    private Boolean isReversed;

    @NotNull
    @Column(name = "isBooked")
    private Boolean isBooked;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeDocument getType() {
        return type;
    }

    public void setType(TypeDocument type) {
        this.type = type;
    }

    public Optional<ZonedDateTime> getDateOfDelivery() {
        return Optional.ofNullable(dateOfDelivery);
    }

    public void setDateOfDelivery(Optional<ZonedDateTime> dateOfDelivery) {
        this.dateOfDelivery = dateOfDelivery.orElse(null);
    }

    public Optional<ZonedDateTime> getDateOfValue() {
        return Optional.ofNullable(dateOfValue);
    }

    public void setDateOfValue(Optional<ZonedDateTime> dateOfValue) {
        this.dateOfValue = dateOfValue.orElse(null);
    }

    public Optional<ZonedDateTime> getDateOfStatement() {
        return Optional.ofNullable(dateOfStatement);
    }

    public void setDateOfStatement(Optional<ZonedDateTime> dateOfStatement) {
        this.dateOfStatement = dateOfStatement.orElse(null);
    }

    public Optional<BigDecimal> getAmount() {
        return Optional.ofNullable(amount);
    }

    public void setAmount(Optional<BigDecimal> amount) {
        this.amount = amount.orElse(null);
    }

    public BusinessYear getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(BusinessYear businessYear) {
        this.businessYear = businessYear;
    }

    public Optional<BusinessPartner> getBusinessPartner() {
        return Optional.ofNullable(businessPartner);
    }

    public void setBusinessPartner(Optional<BusinessPartner> businessPartner) {
        this.businessPartner = businessPartner.orElse(null);
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Optional<Warehouse> getWarehouseOut() {
        return Optional.ofNullable(warehouseOut);
    }

    public void setWarehouseOut(Optional<Warehouse> warehouseOut) {
        this.warehouseOut = warehouseOut.orElse(null);
    }

    public Boolean getIsReversed() {
        return isReversed;
    }

    public void setIsReversed(Boolean isReversed) {
        this.isReversed = isReversed;
    }

    public Boolean getIsBooked() {
        return isBooked;
    }

    public void setIsBooked(Boolean isBooked) {
        this.isBooked = isBooked;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Document other = (Document) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((type == null && other.type != null) || !type.equals(other.type))
            return false;
        if ((dateOfDelivery == null && other.dateOfDelivery != null) || !dateOfDelivery.equals(other.dateOfDelivery))
            return false;
        if ((dateOfValue == null && other.dateOfValue != null) || !dateOfValue.equals(other.dateOfValue))
            return false;
        if ((dateOfStatement == null && other.dateOfStatement != null) || !dateOfStatement.equals(other.dateOfStatement))
            return false;
        if ((amount == null && other.amount != null) || !amount.equals(other.amount))
            return false;
        if ((businessYear == null && other.businessYear != null) || !businessYear.equals(other.businessYear))
            return false;
        if ((businessPartner == null && other.businessPartner != null) || !businessPartner.equals(other.businessPartner))
            return false;
        if ((warehouse == null && other.warehouse != null) || !warehouse.equals(other.warehouse))
            return false;
        if ((warehouseOut == null && other.warehouseOut != null) || !warehouseOut.equals(other.warehouseOut))
            return false;
        if ((isReversed == null && other.isReversed != null) || !isReversed.equals(other.isReversed))
            return false;
        if ((isBooked == null && other.isBooked != null) || !isBooked.equals(other.isBooked))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((dateOfDelivery == null) ? 0 : dateOfDelivery.hashCode());
        result = prime * result + ((dateOfValue == null) ? 0 : dateOfValue.hashCode());
        result = prime * result + ((dateOfStatement == null) ? 0 : dateOfStatement.hashCode());
        result = prime * result + ((amount == null) ? 0 : amount.hashCode());
        result = prime * result + ((businessYear == null) ? 0 : businessYear.hashCode());
        result = prime * result + ((businessPartner == null) ? 0 : businessPartner.hashCode());
        result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
        result = prime * result + ((warehouseOut == null) ? 0 : warehouseOut.hashCode());
        result = prime * result + ((isReversed == null) ? 0 : isReversed.hashCode());
        result = prime * result + ((isBooked == null) ? 0 : isBooked.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Document[" + "id=" + id + ", type=" + type + ", dateOfDelivery=" + dateOfDelivery + ", dateOfValue=" + dateOfValue + ", dateOfStatement=" + dateOfStatement + ", amount=" + amount
                + ", businessYear=" + businessYear + ", businessPartner=" + businessPartner + ", warehouse=" + warehouse + ", warehouseOut=" + warehouseOut + ", isReversed=" + isReversed
                + ", isBooked=" + isBooked + ", isDeleted=" + isDeleted + "]";
    }

}

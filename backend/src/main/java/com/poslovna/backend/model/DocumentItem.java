package com.poslovna.backend.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


@Entity
@Table(name = "DocumentItem")
public class DocumentItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "documentId")
    private Document document;

    @NotNull
    @Column(name = "quantity")
    private BigDecimal quantity;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeDocument type;

    @NotNull
    @Column(name = "price")
    private BigDecimal price;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "articleId")
    private Article article;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public TypeDocument getType() {
        return type;
    }

    public void setType(TypeDocument type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final DocumentItem other = (DocumentItem) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((document == null && other.document != null) || !document.equals(other.document))
            return false;
        if ((quantity == null && other.quantity != null) || !quantity.equals(other.quantity))
            return false;
        if ((type == null && other.type != null) || !type.equals(other.type))
            return false;
        if ((price == null && other.price != null) || !price.equals(other.price))
            return false;
        if ((article == null && other.article != null) || !article.equals(other.article))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((document == null) ? 0 : document.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((article == null) ? 0 : article.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "DocumentItem[" + "id=" + id + ", document=" + document + ", quantity=" + quantity + ", type=" + type + ", price=" + price + ", article=" + article + ", isDeleted=" + isDeleted + "]";
    }

}

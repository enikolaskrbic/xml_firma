package com.poslovna.backend.model.enumeration;

/**
 * Created by Skrbic on 3/16/2017.
 */
public enum ExceptionMessage {

    PERMISSION_DENIED("Permission denied."),
    USER_DOESNT_EXIST("User does not exist."),
    WRONG_PASSWORD("Wrong password."),
    EMAIL_NOT_VERIFIED("Email not verified."),
    USER_ALREADY_EXISTS("User already exists. Please try with other email address."),
    REQUEST_ALREADY_SENT("Request already sent."),
    REQUEST_ON_PENDING("Request on pending."),
    PIB_INCORECT("PIB is incorect. PIB contains 8 characters!"),
    PIB_ALREADY_EXISTS("PIB already exists. Please choose other PIB."),
    UNAUTHORIZED("Unauthorized."),
    OBJECT_DOESNT_EXIST("Object doesnt exist in database! Please try again."),
    DOCUMENT_ITEM_ALREADY_EXSIST("You already add document item with that article. Choose other document for this article."),
    CANT_CHANGE_THIS_DOCUMENT("You can not change this document because this document is already booked or reversed!"),
    DOCUMENT_ITEM_DOESNT_EXIST("Document does not have document items!"),
    ACTIVE_YEAR_EXISTS("There is an active year. Please finish active year first, than open new active year!"),
    BOOK_NOT_ALLOWED("Booking is not allowed, beacuse business year is finished or already booked/reversed ."),
    CANT_EDIT_YEAR("Can not edit finshed business year. Only open year is editable."),
    CANT_SAME_WAREHOUSE("You can't create document for same warehouse. Please choose other warehouse."),
    DOCUMENT_CANT_BE_BOOKED("Document can not be booked. Document does not have items or year is finished! Please choose other document."),
    DOCUMENT_CANT_BE_REVERSE("Document can not be reversed. Document is not booked or is already reveres!"),
    REVERSE_NOT_ALLOWED("Reverse is not allowed. Business year is finished."),
    REVERSE_NOT_ALLOWED_NOT_ACTIVE_CARD("Reverse is not allowed. There are not active card."),
    DATE_CAN_NOT_BE_BEFORE_NOW("Date can not be in future. Please choose date for today or days before."),
    PDF_IS_EMPTY("PDF is empty."),
    ARTICLE_DOESNT_HAVE_PRICE("Article does not have price. Please insert article with price first."),
    NOT_ENOUGHT_QUANTITY_FOR_REVERSE("You try to reverse more quantity than balance has."),
    NO_ACTIVE_YEAR("There are not active business year."),
    MORE_THAN_HAVE("There are not available quantity of item. Please choose other input for quantity!");

    private final String text;

    private ExceptionMessage(final String text) {
        this.text = text;
    }
    @Override
    public String toString() {
        return text;
    }
}

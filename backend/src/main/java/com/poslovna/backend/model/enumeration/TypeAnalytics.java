package com.poslovna.backend.model.enumeration;

public enum TypeAnalytics {

    INPUT, OUTPUT, LEVELING, CORRELATION, BETWEEN, START_STATE

}

package com.poslovna.backend.model;

import java.io.Serializable;
import java.math.BigDecimal;

import java.util.Optional;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "WarehouseCard")
public class WarehouseCard implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "startState")
    private BigDecimal startState;

    @NotNull
    @Column(name = "input")
    private BigDecimal input;

    @NotNull
    @Column(name = "output")
    private BigDecimal output;

    @NotNull
    @Column(name = "balance")
    private BigDecimal balance;

    @NotNull
    @Column(name = "inValue")
    private BigDecimal inValue;

    @NotNull
    @Column(name = "outValue")
    private BigDecimal outValue;

    @NotNull
    @Column(name = "totalValue")
    private BigDecimal totalValue;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "warehouseId")
    private Warehouse warehouse;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "businessYearId")
    private BusinessYear businessYear;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "articleId")
    private Article article;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    @NotNull
    @Column(name = "averagePrice")
    private BigDecimal averagePrice;

    @Column(name = "lastPrice")
    private BigDecimal lastPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getStartState() {
        return startState;
    }

    public void setStartState(BigDecimal startState) {
        this.startState = startState;
    }

    public BigDecimal getInput() {
        return input;
    }

    public void setInput(BigDecimal input) {
        this.input = input;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getInValue() {
        return inValue;
    }

    public void setInValue(BigDecimal inValue) {
        this.inValue = inValue;
    }

    public BigDecimal getOutValue() {
        return outValue;
    }

    public void setOutValue(BigDecimal outValue) {
        this.outValue = outValue;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public BusinessYear getBusinessYear() {
        return businessYear;
    }

    public void setBusinessYear(BusinessYear businessYear) {
        this.businessYear = businessYear;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public Optional<BigDecimal> getLastPrice() {
        return Optional.ofNullable(lastPrice);
    }

    public void setLastPrice(Optional<BigDecimal> lastPrice) {
        this.lastPrice = lastPrice.orElse(null);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final WarehouseCard other = (WarehouseCard) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((startState == null && other.startState != null) || !startState.equals(other.startState))
            return false;
        if ((input == null && other.input != null) || !input.equals(other.input))
            return false;
        if ((output == null && other.output != null) || !output.equals(other.output))
            return false;
        if ((balance == null && other.balance != null) || !balance.equals(other.balance))
            return false;
        if ((inValue == null && other.inValue != null) || !inValue.equals(other.inValue))
            return false;
        if ((outValue == null && other.outValue != null) || !outValue.equals(other.outValue))
            return false;
        if ((totalValue == null && other.totalValue != null) || !totalValue.equals(other.totalValue))
            return false;
        if ((warehouse == null && other.warehouse != null) || !warehouse.equals(other.warehouse))
            return false;
        if ((businessYear == null && other.businessYear != null) || !businessYear.equals(other.businessYear))
            return false;
        if ((article == null && other.article != null) || !article.equals(other.article))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        if ((averagePrice == null && other.averagePrice != null) || !averagePrice.equals(other.averagePrice))
            return false;
        if ((lastPrice == null && other.lastPrice != null) || !lastPrice.equals(other.lastPrice))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((startState == null) ? 0 : startState.hashCode());
        result = prime * result + ((input == null) ? 0 : input.hashCode());
        result = prime * result + ((output == null) ? 0 : output.hashCode());
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + ((inValue == null) ? 0 : inValue.hashCode());
        result = prime * result + ((outValue == null) ? 0 : outValue.hashCode());
        result = prime * result + ((totalValue == null) ? 0 : totalValue.hashCode());
        result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
        result = prime * result + ((businessYear == null) ? 0 : businessYear.hashCode());
        result = prime * result + ((article == null) ? 0 : article.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        result = prime * result + ((averagePrice == null) ? 0 : averagePrice.hashCode());
        result = prime * result + ((lastPrice == null) ? 0 : lastPrice.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "WarehouseCard[" + "id=" + id + ", startState=" + startState + ", input=" + input + ", output=" + output + ", balance=" + balance + ", inValue=" + inValue + ", outValue=" + outValue
                + ", totalValue=" + totalValue + ", warehouse=" + warehouse + ", businessYear=" + businessYear + ", article=" + article + ", isDeleted=" + isDeleted + ", averagePrice=" + averagePrice
                + ", lastPrice=" + lastPrice + "]";
    }

}

package com.poslovna.backend.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "BusinessYear")
public class BusinessYear implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 4)
    @Column(name = "year")
    private String year;

    @NotNull
    @Column(name = "isFinished")
    private Boolean isFinished;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "firmId")
    private Firm firm;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(Boolean isFinished) {
        this.isFinished = isFinished;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final BusinessYear other = (BusinessYear) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((year == null && other.year != null) || !year.equals(other.year))
            return false;
        if ((isFinished == null && other.isFinished != null) || !isFinished.equals(other.isFinished))
            return false;
        if ((firm == null && other.firm != null) || !firm.equals(other.firm))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        result = prime * result + ((isFinished == null) ? 0 : isFinished.hashCode());
        result = prime * result + ((firm == null) ? 0 : firm.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "BusinessYear[" + "id=" + id + ", year=" + year + ", isFinished=" + isFinished + ", firm=" + firm + ", isDeleted=" + isDeleted + "]";
    }

}

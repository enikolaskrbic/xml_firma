package com.poslovna.backend.model;

import java.io.Serializable;

import java.time.*;

import java.util.Optional;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.poslovna.backend.model.enumeration.*;


@Entity
@Table(name = "User")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "firstName")
    private String firstName;

    @NotNull
    @Size(max = 255)
    @Column(name = "lastName")
    private String lastName;

    @NotNull
    @Column(name = "isDeleted")
    private Boolean isDeleted;

    @NotNull
    @Column(name = "timetamp")
    private ZonedDateTime timetamp;

    @ManyToOne
    @JoinColumn(name = "firmId")
    private Firm firm;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @NotNull
    @Size(min = 6, max = 128)
    @Pattern(regexp = "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$")
    @Column(name = "email")
    private String email;

    @NotNull
    @Size(min = 6, max = 128)
    @Column(name = "passwordHash")
    private String passwordHash;

    @Size(min = 64, max = 64)
    @Column(name = "emailVerificationCode")
    private String emailVerificationCode;

    @Column(name = "emailVerificationCodeTimestamp")
    private ZonedDateTime emailVerificationCodeTimestamp;

    @NotNull
    @Column(name = "emailVerified")
    private Boolean emailVerified;

    @Size(min = 64, max = 64)
    @Column(name = "resetPasswordCode")
    private String resetPasswordCode;

    @Column(name = "resetPasswordCodeTimestamp")
    private ZonedDateTime resetPasswordCodeTimestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public ZonedDateTime getTimetamp() {
        return timetamp;
    }

    public void setTimetamp(ZonedDateTime timetamp) {
        this.timetamp = timetamp;
    }

    public Optional<Firm> getFirm() {
        return Optional.ofNullable(firm);
    }

    public void setFirm(Optional<Firm> firm) {
        this.firm = firm.orElse(null);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Optional<String> getEmailVerificationCode() {
        return Optional.ofNullable(emailVerificationCode);
    }

    public void setEmailVerificationCode(Optional<String> emailVerificationCode) {
        this.emailVerificationCode = emailVerificationCode.orElse(null);
    }

    public Optional<ZonedDateTime> getEmailVerificationCodeTimestamp() {
        return Optional.ofNullable(emailVerificationCodeTimestamp);
    }

    public void setEmailVerificationCodeTimestamp(Optional<ZonedDateTime> emailVerificationCodeTimestamp) {
        this.emailVerificationCodeTimestamp = emailVerificationCodeTimestamp.orElse(null);
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public Optional<String> getResetPasswordCode() {
        return Optional.ofNullable(resetPasswordCode);
    }

    public void setResetPasswordCode(Optional<String> resetPasswordCode) {
        this.resetPasswordCode = resetPasswordCode.orElse(null);
    }

    public Optional<ZonedDateTime> getResetPasswordCodeTimestamp() {
        return Optional.ofNullable(resetPasswordCodeTimestamp);
    }

    public void setResetPasswordCodeTimestamp(Optional<ZonedDateTime> resetPasswordCodeTimestamp) {
        this.resetPasswordCodeTimestamp = resetPasswordCodeTimestamp.orElse(null);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final User other = (User) obj;
        if ((id == null && other.id != null) || !id.equals(other.id))
            return false;
        if ((firstName == null && other.firstName != null) || !firstName.equals(other.firstName))
            return false;
        if ((lastName == null && other.lastName != null) || !lastName.equals(other.lastName))
            return false;
        if ((isDeleted == null && other.isDeleted != null) || !isDeleted.equals(other.isDeleted))
            return false;
        if ((timetamp == null && other.timetamp != null) || !timetamp.equals(other.timetamp))
            return false;
        if ((firm == null && other.firm != null) || !firm.equals(other.firm))
            return false;
        if ((role == null && other.role != null) || !role.equals(other.role))
            return false;
        if ((email == null && other.email != null) || !email.equals(other.email))
            return false;
        if ((passwordHash == null && other.passwordHash != null) || !passwordHash.equals(other.passwordHash))
            return false;
        if ((emailVerificationCode == null && other.emailVerificationCode != null) || !emailVerificationCode.equals(other.emailVerificationCode))
            return false;
        if ((emailVerificationCodeTimestamp == null && other.emailVerificationCodeTimestamp != null) || !emailVerificationCodeTimestamp.equals(other.emailVerificationCodeTimestamp))
            return false;
        if ((emailVerified == null && other.emailVerified != null) || !emailVerified.equals(other.emailVerified))
            return false;
        if ((resetPasswordCode == null && other.resetPasswordCode != null) || !resetPasswordCode.equals(other.resetPasswordCode))
            return false;
        if ((resetPasswordCodeTimestamp == null && other.resetPasswordCodeTimestamp != null) || !resetPasswordCodeTimestamp.equals(other.resetPasswordCodeTimestamp))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
        result = prime * result + ((timetamp == null) ? 0 : timetamp.hashCode());
        result = prime * result + ((firm == null) ? 0 : firm.hashCode());
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((passwordHash == null) ? 0 : passwordHash.hashCode());
        result = prime * result + ((emailVerificationCode == null) ? 0 : emailVerificationCode.hashCode());
        result = prime * result + ((emailVerificationCodeTimestamp == null) ? 0 : emailVerificationCodeTimestamp.hashCode());
        result = prime * result + ((emailVerified == null) ? 0 : emailVerified.hashCode());
        result = prime * result + ((resetPasswordCode == null) ? 0 : resetPasswordCode.hashCode());
        result = prime * result + ((resetPasswordCodeTimestamp == null) ? 0 : resetPasswordCodeTimestamp.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "User[" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", isDeleted=" + isDeleted + ", timetamp=" + timetamp + ", firm=" + firm + ", role=" + role + ", email="
                + email + ", emailVerificationCodeTimestamp=" + emailVerificationCodeTimestamp + ", emailVerified=" + emailVerified + ", resetPasswordCodeTimestamp=" + resetPasswordCodeTimestamp
                + "]";
    }

}

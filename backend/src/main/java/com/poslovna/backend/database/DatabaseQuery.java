package com.poslovna.backend.database;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.transform.TransformerFactory;

import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.poslovna.backend.config.CustomProperties;
import com.poslovna.backend.converter.Converter;
import com.poslovna.backend.converter.types.UnmarshallType;
import com.poslovna.backend.model.Firm;
import com.poslovna.backend.model.enumeration.ExceptionMessage;
import com.poslovna.backend.web.rest.exception.CustomException;
import com.poslovna.backend.xml_model.faktura.Faktura;
import com.poslovna.backend.xml_model.faktura.StavkaFakture;
import com.poslovna.backend.xml_model.faktura.ZaglavljeFakture;
import com.poslovna.backend.xml_model.firm.Firma;
import com.poslovna.backend.xml_model.firm.SpisakFirmi;
import com.poslovna.backend.xml_model.korisnik.Korisnik;
import org.springframework.stereotype.Service;

@Service
public class DatabaseQuery {

	private static TransformerFactory transformerFactory;

	static {
		transformerFactory = TransformerFactory.newInstance();
	}
	
	public static String readXmlFromDatabase(String docId) {
		DOMHandle content = new DOMHandle();
		DatabaseAccessor.getInstance();
		DocumentMetadataHandle metadata = new DocumentMetadataHandle();
		System.out.println("[INFO] Retrieving \"" + docId + "\" from "
				+ (DatabaseAccessor.props.database.equals("") ? "default" : DatabaseAccessor.props.database.equals(""))
				+ " database.");

		DatabaseAccessor.xmlManager.read(docId, metadata, content);

		return content.toString();
	}
	
	public static SpisakFirmi readSpisakFirmiFromDatabase(String spisakFirmi){
        String xmlDocString = DatabaseQuery.readXmlFromDatabase(spisakFirmi);
		return (SpisakFirmi) Converter.unmarshall(UnmarshallType.FROM_STRING, xmlDocString, SpisakFirmi.class);
	}
	
	public static Firma readFirmaFromDatabase(String firma){
        String xmlDocString = DatabaseQuery.readXmlFromDatabase(firma);
		return (Firma) Converter.unmarshall(UnmarshallType.FROM_STRING, xmlDocString, Firma.class);
	}
	
	public static List<Object> searchFirmByName(String nazivFirme) {
		StringBuilder query = new StringBuilder();
		List<Object> results = new ArrayList();
		
		query.append("declare namespace pp = \"http://xml.web.services.security/firma\";\n"
				+ "for $x in doc(\"instanceSpisakFirmi.xml\")//pp:spisakFirmi//pp:firma\n"
				+ "where $x//pp:naziv='" + nazivFirme+"'\n"
				+ "return $x;");

		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				Firma firma = (Firma) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, Firma.class);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return results;
	}
	
	public static Korisnik searchKorisnik(String korisnicko, String lozinka){

		StringBuilder query = new StringBuilder();
		Korisnik korisnik = null;

		query.append("declare namespace pp = \"http://xml.web.services.security/korisnik\";\n"
				+ "for $x in doc(\"instanceSpisakKorisnika.xml\")//pp:korisnici//pp:korisnik\n"
				+ "where $x//@korisnickoIme='" + korisnicko+"' and \n"
				+ "$x//pp:lozinkaHash='"+lozinka+"'\n"
				+ "return $x;");

		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				korisnik = (Korisnik) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, Korisnik.class);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return korisnik;
	}

	public static void createInputFaktura(String firmaPib, String xmlFaktura) throws CustomException {
		StringBuilder query = new StringBuilder();

		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"xdmp:node-insert-child(doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaUlaznihFaktura[@pibFirme='" + firmaPib +"'], " + xmlFaktura + ");");

		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}
	}

	public static void createOutputFaktura(String firmaPib, String xmlFaktura) throws CustomException {
		StringBuilder query = new StringBuilder();

		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"xdmp:node-insert-child(doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaIzlaznihFaktura[@pibFirme='" + firmaPib +"'], " + xmlFaktura + ");");

		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}
	}

	public static void createOutputStavkaFaktura(String pib,String idFakture,String xmlStavkaFaktura) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"xdmp:node-insert-child(doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaIzlaznihFaktura[@pibFirme='" + pib +"']//pp:faktura[@idPoruke='" +idFakture+"'],"+xmlStavkaFaktura+");");

		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}
	}

	public static List<Faktura> getListaUlaznihFaktura(String firmaPib) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaUlaznihFaktura[@pibFirme='" + firmaPib +"']//pp:faktura;");

		List<Faktura> fakturaLista = new ArrayList<>();
		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				Faktura faktura = (Faktura) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, Faktura.class);
				fakturaLista.add(faktura);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}

		return fakturaLista;
	}

	public static List<Faktura> getListaIzlaznihFaktura(String firmaPib) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaIzlaznihFaktura[@pibFirme='" + firmaPib +"']//pp:faktura;");

		List<Faktura> fakturaLista = new ArrayList<>();
		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				Faktura faktura = (Faktura) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, Faktura.class);
				fakturaLista.add(faktura);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}

		return fakturaLista;
	}

	public static List<StavkaFakture> getStavkeIzlazneFakture(String firmaPib, String fakturaId) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaIzlaznihFaktura[@pibFirme='" + firmaPib +"']//pp:faktura[@idPoruke='" + fakturaId +"']//pp:stavkaFakture;");

		List<StavkaFakture> fakturaLista = new ArrayList<>();
		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				StavkaFakture faktura = (StavkaFakture) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, StavkaFakture.class);
				fakturaLista.add(faktura);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}

		return fakturaLista;
	}

	public static List<StavkaFakture> getStavkeUlazneFakture(String firmaPib, String fakturaId) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaUlaznihFaktura[@pibFirme='" + firmaPib +"']//pp:faktura[@idPoruke='" + fakturaId +"']//pp:stavkaFakture;");

		List<StavkaFakture> fakturaLista = new ArrayList<>();
		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				StavkaFakture faktura = (StavkaFakture) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, StavkaFakture.class);
				fakturaLista.add(faktura);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}

		return fakturaLista;
	}

	public static Firma getFirma(String firmaPib) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/firma\";\n" +
					"doc(\"instanceSpisakFirmi.xml\")//pp:spisakFirmi//pp:firma[@pib='" + firmaPib + "'];");

		Firma firma = null;
		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				 firma = (Firma) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, Firma.class);
				 break;
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}

		return firma;
	}

	public static List<Firma> getOstaleFirme(String firmaPib) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/firma\";\n" +
				"doc(\"instanceSpisakFirmi.xml\")//pp:spisakFirmi//pp:firma[@pib!='" + firmaPib + "'];");

		List<Firma> fakturaLista = new ArrayList<>();
		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				Firma faktura = (Firma) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, Firma.class);
				fakturaLista.add(faktura);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}

		return fakturaLista;
	}

	public static void updateZaglavljeFakture(String pibFirme ,String idFakture ,String xmlZaglavljeFakture) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				"  xdmp:node-replace(doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaIzlaznihFaktura[@pibFirme='" +pibFirme+ "']//pp:faktura[@idPoruke='"+idFakture+"']//pp:zaglavljeFakture," + xmlZaglavljeFakture + ");");

		Vector<String> searchResults;
		try {
			searchResults = XQueryInvoker.execute(query.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}
	}

	public static ZaglavljeFakture getZaglavljeFaktureObject(String pibFirme ,String idFakture ) throws CustomException {
		StringBuilder query = new StringBuilder();
		query.append("declare namespace pp = \"http://xml.web.services.security/faktura\";\n" +
				" doc(\"instanceSpisakFaktura.xml\")//pp:spisakFaktura//pp:listaIzlaznihFaktura[@pibFirme='" +pibFirme+ "']//pp:faktura[@idPoruke='"+idFakture+"']//pp:zaglavljeFakture;");

		Vector<String> searchResults;
		ZaglavljeFakture zaglavljeFakture = null;

		try {
			searchResults = XQueryInvoker.execute(query.toString());
			for (String docStr : searchResults) {
				zaglavljeFakture = (ZaglavljeFakture) Converter.unmarshall(UnmarshallType.FROM_STRING, docStr, ZaglavljeFakture.class);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}

		return zaglavljeFakture;
	}

}

package com.poslovna.backend.service;

import com.mysql.jdbc.CallableStatement;
import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.*;
import com.poslovna.backend.web.rest.dto.*;
import com.poslovna.backend.web.rest.exception.CustomException;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.w3c.dom.DocumentType;

import javax.inject.Inject;
import java.io.Console;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Skrbic on 3/19/2017.
 */
@Service
public class ManagerService {

    @Inject
    UserRepository userRepository;

    @Inject
    GroupFirmRepository groupFirmRepository;

    @Inject
    SubGroupRepository subGroupRepository;

    @Inject
    UnitOfMeasureRepository unitOfMeasureRepository;

    @Inject
    ArticleRepository articleRepository;

    @Inject
    BusinessYearRepository businessYearRepository;

    @Inject
    WarehouseRepository warehouseRepository;

    @Inject
    WarehouseCardRepository warehouseCardRepository;

    @Inject
    BusinessPartnerRepository businessPartnerRepository;

    @Inject
    DocumentRepository documentRepository;

    @Inject
    DocumentItemRepository documentItemRepository;

    @Inject
    AnalyticsRepository analyticsRepository;

    @Inject
    DataSourceProperties dataSourceProperties;

    public void addGroup(GroupDto groupDto, Long principalId) throws CustomException {
        User user = userRepository.findOne(principalId);
        if (user == null) {
            throw new CustomException(ExceptionMessage.USER_DOESNT_EXIST);
        }
        if (!user.getRole().equals(Role.ROLE_MANAGER)) {
            throw new CustomException(ExceptionMessage.UNAUTHORIZED);
        }
        Optional<Firm> firm = user.getFirm();

        if (firm == null) {
            throw new CustomException(ExceptionMessage.UNAUTHORIZED);
        }
        GroupFirm group = new GroupFirm();
        group.setFirm(firm.get());
        group.setName(groupDto.getName());
        group.setIsDeleted(false);
        groupFirmRepository.save(group);
    }

    public void editGroup(GroupIdDto groupIdDto) throws CustomException {
        GroupFirm group = groupFirmRepository.findOne(groupIdDto.getId());
        group.setName(groupIdDto.getGroup());
        groupFirmRepository.save(group);
    }

    public void addSubGroup(SubGroupDto subGroupDto) throws CustomException {
        SubGroup subGroup = new SubGroup();
        subGroup.setName(subGroupDto.getName());
        subGroup.setGroup(groupFirmRepository.findOne(subGroupDto.getId()));
        subGroup.setIsDeleted(false);
        subGroupRepository.save(subGroup);
    }

    public void editSubGroup(SubGroupEditDto subGroupDto) throws CustomException {
        SubGroup subGroup = subGroupRepository.findOne(subGroupDto.getId());
        if (subGroup == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }
        subGroup.setGroup(groupFirmRepository.findOne(subGroupDto.getGroup()));
        subGroup.setName(subGroupDto.getName());
        subGroupRepository.save(subGroup);
    }

    public void addArticle(ArticleDto articleDto) throws CustomException {
        Article article = new Article();
        article.setName(articleDto.getName());
        article.setCode(articleDto.getCode());
        article.setStatus(articleDto.getStatus());
        article.setSubGroup(subGroupRepository.findOne(articleDto.getSubGroup()));
        article.setUnitOfMeasure(unitOfMeasureRepository.findOne(articleDto.getUnitOfMeasure()));
        article.setIsDeleted(false);

        articleRepository.save(article);
    }

    public void editArticle(ArticleEditDto articleDto) throws CustomException {
        Article article = articleRepository.findOne(articleDto.getId());
        if (article == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }
        article.setName(articleDto.getName());
        article.setCode(articleDto.getCode());
        article.setStatus(articleDto.getStatus());
        article.setSubGroup(subGroupRepository.findOne(articleDto.getSubGroup()));
        article.setUnitOfMeasure(unitOfMeasureRepository.findOne(articleDto.getUnitOfMeasure()));


        articleRepository.save(article);
    }

    public void addBussinesYear(BusinessYearDto businessYearDto, Long principalId) throws CustomException {
        User user = userRepository.findOne(principalId);
        if (user == null) {
            throw new CustomException(ExceptionMessage.USER_DOESNT_EXIST);
        }

        List<BusinessYear> businessYears = businessYearRepository.findByFirmAndIsFinished(user.getFirm().get().getId(),false);
        if (!businessYears.isEmpty()) {
            throw new CustomException(ExceptionMessage.ACTIVE_YEAR_EXISTS);
        }

        List<BusinessYear> businessYearsLast = businessYearRepository.findByFirmAndIsDeleted(user.getFirm().get().getId(),false);   //nije obradjena poslednja aktivna - nije zakljucena

        BusinessYear businessYear = new BusinessYear();
        businessYear.setFirm(user.getFirm().get());
        businessYear.setYear(businessYearDto.getYear());
        businessYear.setIsFinished(false);
        businessYear.setIsDeleted(false);
        businessYearRepository.save(businessYear);


        if (!businessYearsLast.isEmpty()) {
            BusinessYear businessYearLast = businessYearsLast.get(0);
            businessYearLast.setIsDeleted(true);
            businessYearRepository.save(businessYearLast);

            List<WarehouseCard> warehouseCards = warehouseCardRepository.findByBusinessYear(businessYearLast.getId());
            if (!warehouseCards.isEmpty()) {
                for (WarehouseCard warehouseCard : warehouseCards) {
                    //if(warehouseCard.getBalance().compareTo(BigDecimal.ZERO) == 1){
                    WarehouseCard newWarehaouseCard = new WarehouseCard();
                    newWarehaouseCard.setStartState(warehouseCard.getBalance());
                    newWarehaouseCard.setArticle(articleRepository.findOne(warehouseCard.getArticle().getId()));
                    newWarehaouseCard.setWarehouse(warehouseRepository.findOne(warehouseCard.getWarehouse().getId()));
                    newWarehaouseCard.setBusinessYear(businessYear);
                    newWarehaouseCard.setBalance(warehouseCard.getBalance());
                    newWarehaouseCard.setInput(BigDecimal.ZERO);
                    newWarehaouseCard.setOutput(BigDecimal.ZERO);
                    newWarehaouseCard.setInValue(BigDecimal.ZERO);
                    newWarehaouseCard.setOutValue(BigDecimal.ZERO);
                    newWarehaouseCard.setTotalValue(warehouseCard.getTotalValue());
                    newWarehaouseCard.setIsDeleted(false);
                    newWarehaouseCard.setAveragePrice(warehouseCard.getAveragePrice());
                    warehouseCardRepository.save(newWarehaouseCard);

                    warehouseCard.setIsDeleted(true);
                    warehouseCardRepository.save(warehouseCard);

                    Analytics analytics = new Analytics();
                    analytics.setType(TypeAnalytics.START_STATE);
                    analytics.setDirection(TypeDirection.INPUT);
                    analytics.setPrice(newWarehaouseCard.getAveragePrice());
                    analytics.setQuantity(warehouseCard.getBalance());
                    analytics.setBalance(warehouseCard.getBalance());
                    analytics.setValue(analytics.getPrice().multiply(analytics.getQuantity()));
                    analytics.setTotalValue(analytics.getPrice().multiply(analytics.getQuantity()));
                    analytics.setWarehouseCard(newWarehaouseCard);
                    analytics.setDate(ZonedDateTime.now());
                    analytics.setIsDeleted(false);
                    analyticsRepository.save(analytics);
                    //}
                }
            }
        }
    }

    public void finishBussinesYear(Long yearId) throws CustomException {
        BusinessYear businessYear = businessYearRepository.findOne(yearId);
        if (businessYear == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }
        //calculateLeveling(businessYear);

        businessYear.setIsFinished(true);
        businessYearRepository.save(businessYear);
    }

    public void calculateLeveling(Long warehouseId) throws CustomException{
        List<WarehouseCard> warehouseCards = warehouseCardRepository.findByWarehouseAndBusinessYear(warehouseId);
        for (WarehouseCard warehouseCard: warehouseCards) {
            BigDecimal leveling = warehouseCard.getAveragePrice().multiply(warehouseCard.getBalance()).setScale(4, BigDecimal.ROUND_HALF_UP);
            leveling = (leveling.subtract(warehouseCard.getTotalValue()).setScale(4, BigDecimal.ROUND_HALF_UP));

            Analytics analytics = new Analytics();
            analytics.setType(TypeAnalytics.LEVELING);
            analytics.setDirection(TypeDirection.INPUT);
            analytics.setPrice(warehouseCard.getAveragePrice());
            analytics.setQuantity(BigDecimal.ZERO.setScale(4, BigDecimal.ROUND_HALF_UP));
            analytics.setBalance(warehouseCard.getBalance());
            analytics.setValue(leveling);
            analytics.setTotalValue(warehouseCard.getTotalValue().add(leveling));
            analytics.setWarehouseCard(warehouseCard);
            analytics.setDate(ZonedDateTime.now());
            analytics.setIsDeleted(false);
            analytics.setDocumentItem(Optional.empty());
            analyticsRepository.save(analytics);

            warehouseCard.setTotalValue(analytics.getTotalValue());
            warehouseCardRepository.save(warehouseCard);
        }
    }

    public void calculateLevelingWithProcedure(Long warehouseId) throws SQLException, CustomException {
        Connection connection = (Connection) DriverManager.getConnection(dataSourceProperties.getUrl() , dataSourceProperties.getUsername(), dataSourceProperties.getPassword());

        try {
            CallableStatement proc = (CallableStatement) connection.prepareCall("{ call levelingWarehouse(?)}");
            proc.setLong("warehouseId", warehouseId);
            proc.execute();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            throw new CustomException(e.getMessage());
        }

        connection.close();
    }

    public void editBussinesYear(BusinessYearIdDto businessYearDto) throws CustomException {
        BusinessYear businessYear = businessYearRepository.findOne(businessYearDto.getId());
        if (businessYear == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }
        if(businessYear.getIsFinished()){
            throw new CustomException(ExceptionMessage.CANT_EDIT_YEAR);
        }
        businessYear.setYear(businessYearDto.getYear());
        businessYearRepository.save(businessYear);
    }

    public void addWarehouse(WarehouseDto warehouseDto, Long principalId) throws CustomException {
        Warehouse warehouse = new Warehouse();
        User user = userRepository.getOne(principalId);
        if (user == null) {
            throw new CustomException(ExceptionMessage.USER_DOESNT_EXIST);
        }
        warehouse.setFirm(user.getFirm().get());
        warehouse.setName(warehouseDto.getName());
        warehouse.setAddress(warehouseDto.getAddress());
        warehouse.setCity(warehouseDto.getCity());
        warehouse.setCountry(warehouseDto.getCountry());
        warehouse.setCode(warehouseDto.getCode());
        warehouse.setIsDeleted(false);

        warehouseRepository.save(warehouse);
    }

    public void editWarehouse(WarehouseEditDto warehouseEditDto) throws CustomException {
        Warehouse warehouse = warehouseRepository.getOne(warehouseEditDto.getId());
        if (warehouse == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }
        warehouse.setName(warehouseEditDto.getName());
        warehouse.setCountry(warehouseEditDto.getCountry());
        warehouse.setCity(warehouseEditDto.getCity());
        warehouse.setAddress(warehouseEditDto.getAddress());
        warehouse.setCode(warehouseEditDto.getCode());

        warehouseRepository.save(warehouse);
    }

    public void addWarehouseCard(WarehouseCardDto warehouseCardDto) throws CustomException {
        WarehouseCard warehouseCard = new WarehouseCard();

        warehouseCard.setArticle(articleRepository.findOne(warehouseCardDto.getArticle()));
        warehouseCard.setWarehouse(warehouseRepository.findOne(warehouseCardDto.getWarehouse()));
        warehouseCard.setBusinessYear(businessYearRepository.findOne(warehouseCardDto.getBusinessYear()));
        warehouseCard.setBalance(BigDecimal.ZERO);
        warehouseCard.setInput(BigDecimal.ZERO);
        warehouseCard.setOutput(BigDecimal.ZERO);
        warehouseCard.setInValue(BigDecimal.ZERO);
        warehouseCard.setOutValue(BigDecimal.ZERO);
        warehouseCard.setTotalValue(BigDecimal.ZERO);
        warehouseCard.setIsDeleted(false);

        warehouseCardRepository.save(warehouseCard);
    }

    public void addBussinesPartner(BussinesPartnerDto bussinesPartnerDto, Long principalId) throws CustomException {
        BusinessPartner businessPartner = new BusinessPartner();
        User user = userRepository.findOne(principalId);
        if (user == null) {
            throw new CustomException(ExceptionMessage.USER_DOESNT_EXIST);
        }
        businessPartner.setAddress(bussinesPartnerDto.getAddress());
        businessPartner.setCity(bussinesPartnerDto.getCity());
        businessPartner.setCountry(bussinesPartnerDto.getCountry());
        businessPartner.setEmail(Optional.ofNullable(bussinesPartnerDto.getEmail()));
        businessPartner.setFirm(user.getFirm().get());
        businessPartner.setName(bussinesPartnerDto.getName());
        businessPartner.setPhone(Optional.ofNullable(bussinesPartnerDto.getPhone()));
        businessPartner.setStatus(bussinesPartnerDto.getStatus());
        businessPartner.setWeb(Optional.ofNullable(bussinesPartnerDto.getWeb()));
        businessPartner.setIsDeleted(false);

        businessPartnerRepository.save(businessPartner);
    }

    public void editBussinesPartner(BussinesPartnerEditDto bussinesPartnerEditDto) throws CustomException {
        BusinessPartner businessPartner = businessPartnerRepository.findOne(bussinesPartnerEditDto.getIdPartner());
        if (businessPartner == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }
        businessPartner.setAddress(bussinesPartnerEditDto.getAddress());
        businessPartner.setCity(bussinesPartnerEditDto.getCity());
        businessPartner.setCountry(bussinesPartnerEditDto.getCountry());
        businessPartner.setEmail(Optional.ofNullable(bussinesPartnerEditDto.getEmail()));
        businessPartner.setName(bussinesPartnerEditDto.getName());
        businessPartner.setPhone(Optional.ofNullable(bussinesPartnerEditDto.getPhone()));
        businessPartner.setStatus(bussinesPartnerEditDto.getStatus());
        businessPartner.setWeb(Optional.ofNullable(bussinesPartnerEditDto.getWeb()));

        businessPartnerRepository.save(businessPartner);
    }

    public void addUnitOfMeasure(UnitOfMeasureDto unitOfMeasureDto) throws CustomException {
        UnitOfMeasure unitOfMeasure = new UnitOfMeasure();
        unitOfMeasure.setIsDeleted(false);
        unitOfMeasure.setName(unitOfMeasureDto.getName());
        unitOfMeasure.setShortName(Optional.ofNullable(unitOfMeasureDto.getShortName()));
        unitOfMeasureRepository.save(unitOfMeasure);
    }

    public void editUnitOfMeasure(UnitOfMeasureEditDto unitOfMeasureEditDto) throws CustomException {
        UnitOfMeasure unitOfMeasure = unitOfMeasureRepository.findOne(unitOfMeasureEditDto.getId());
        unitOfMeasure.setName(unitOfMeasureEditDto.getName());
        unitOfMeasure.setShortName(Optional.of(unitOfMeasureEditDto.getShortName()));
        unitOfMeasureRepository.save(unitOfMeasure);
    }

    public void addDocument(DocumentDto documentDto) throws CustomException {
        Document document = new Document();

        if(documentDto.getDateOfValue().isAfter(ZonedDateTime.now())){
            throw new CustomException(ExceptionMessage.DATE_CAN_NOT_BE_BEFORE_NOW);

        }

        if (documentDto.getBusinessPartner() != null) {
            document.setBusinessPartner(Optional.ofNullable(businessPartnerRepository.findOne(documentDto.getBusinessPartner())));
        } else {
            if(documentDto.getWarehouse()==documentDto.getWarehouseOut()){
                throw new CustomException(ExceptionMessage.CANT_SAME_WAREHOUSE);
            }
            document.setWarehouseOut(Optional.ofNullable(warehouseRepository.findOne(documentDto.getWarehouseOut())));
        }
        document.setBusinessYear(businessYearRepository.findOne(documentDto.getBusinessYear()));
        document.setDateOfDelivery(Optional.ofNullable(documentDto.getDateOfDelivery()));
        document.setDateOfStatement(Optional.ofNullable(documentDto.getDateOfStatement()));
        document.setDateOfValue(Optional.ofNullable(documentDto.getDateOfValue()));
        document.setIsBooked(false);
        document.setIsDeleted(false);
        document.setIsReversed(false);
        document.setAmount(Optional.ofNullable(BigDecimal.ZERO));
        document.setType(documentDto.getType());
        document.setWarehouse(warehouseRepository.findOne(documentDto.getWarehouse()));

        documentRepository.save(document);
    }

    public void editDocument(DocumentEditDto documentEditDto) throws CustomException {
        Document document = documentRepository.findOne(documentEditDto.getId());
        if(document==null){
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }
        if(document.getIsBooked() || document.getIsReversed()){
            throw new CustomException(ExceptionMessage.CANT_CHANGE_THIS_DOCUMENT);
        }
        document.setType(documentEditDto.getType());
        document.setDateOfValue(Optional.ofNullable(documentEditDto.getDateOfValue()));
        document.setBusinessYear(businessYearRepository.findOne(documentEditDto.getBusinessYear()));
        document.setWarehouse(warehouseRepository.findOne(documentEditDto.getWarehouse()));
        if(documentEditDto.getBusinessPartner() !=null)
            document.setBusinessPartner(Optional.ofNullable(businessPartnerRepository.findOne(documentEditDto.getBusinessPartner())));
        if(documentEditDto.getWarehouseOut() != null)
            document.setWarehouseOut(Optional.ofNullable(warehouseRepository.findOne(documentEditDto.getWarehouseOut())));
        documentRepository.save(document);
    }

    public void addDocumentItem(DocumentItemDto documentItemDto) throws CustomException {
        List<DocumentItem> documentItems = documentItemRepository.findByArticleAndDocument(documentItemDto.getArticle(), documentItemDto.getDocument());

        if (!documentItems.isEmpty()) {
            throw new CustomException(ExceptionMessage.DOCUMENT_ITEM_ALREADY_EXSIST);
        }
        Document document = documentRepository.findOne(documentItemDto.getDocument());

        if (document.getIsBooked() || document.getIsReversed() || document.getIsDeleted()) {
            throw new CustomException(ExceptionMessage.CANT_CHANGE_THIS_DOCUMENT);

        }

        DocumentItem documentItem = new DocumentItem();
        documentItem.setArticle(articleRepository.findOne(documentItemDto.getArticle()));
        documentItem.setDocument(document);
        documentItem.setIsDeleted(false);
        documentItem.setPrice(documentItemDto.getPrice());
        documentItem.setQuantity(documentItemDto.getQuantity());
        documentItem.setType(document.getType());
        documentItemRepository.save(documentItem);

    }

    public List<DocumentResponseDto> getDocuments(Long principalId) throws CustomException {
        Long firmId = userRepository.findOne(principalId).getFirm().get().getId();
        List<Document> documents = documentRepository.findAll();
        List<DocumentResponseDto> documentsResultSet = new ArrayList<>();
        for (Document document: documents) {
            if(document.getBusinessPartner().isPresent()){
                if(document.getBusinessPartner().get().getFirm().getId()==firmId){
                    DocumentResponseDto documentResponseDto = new DocumentResponseDto();
                    documentResponseDto.setBusinessPartnerId(document.getBusinessPartner().get().getId());
                    documentResponseDto.setBusinessYearId(document.getBusinessYear().getId());
                    documentResponseDto.setDateOfDelivery(document.getDateOfDelivery().orElse(null));
                    documentResponseDto.setDateOfStatement(document.getDateOfStatement().orElse(null));
                    documentResponseDto.setDateOfValue(document.getDateOfValue().orElse(null));
                    documentResponseDto.setName(document.getBusinessPartner().get().getName());
                    documentResponseDto.setStatus(document.getBusinessPartner().get().getStatus());
                    documentResponseDto.setWarehouseId(document.getWarehouse().getId());
                    documentResponseDto.setWarehouseName(document.getWarehouse().getName());
//                    documentResponseDto.setWarehouseIdOut(document.getWarehouseOut().get().getId());
//                    documentResponseDto.setWarehouseNameOut(document.getWarehouseOut().get().getName());
                    documentResponseDto.setYear(document.getBusinessYear().getYear());
                    documentResponseDto.setId(document.getId());
                    documentResponseDto.setTypeDocument(document.getType());
                    if(document.getIsReversed()){
                        documentResponseDto.setDocumentStatus(StatusDocument.REVERSED);
                    }else if(document.getIsBooked()){
                        documentResponseDto.setDocumentStatus(StatusDocument.BOOKED);
                    }else{
                        documentResponseDto.setDocumentStatus(StatusDocument.PENDING);
                    }
                    documentsResultSet.add(documentResponseDto);
                }

            }else if(document.getWarehouseOut().isPresent()){
                if(document.getWarehouseOut().get().getFirm().getId()==firmId){
                    DocumentResponseDto documentResponseDto = new DocumentResponseDto();
                    //documentResponseDto.setBusinessPartnerId(document.getBusinessPartner().get().getId());
                    documentResponseDto.setBusinessYearId(document.getBusinessYear().getId());
                    documentResponseDto.setDateOfDelivery(document.getDateOfDelivery().orElse(null));
                    documentResponseDto.setDateOfStatement(document.getDateOfStatement().orElse(null));
                    documentResponseDto.setDateOfValue(document.getDateOfValue().orElse(null));
                    //documentResponseDto.setName(document.getBusinessPartner().get().getName());
                   // documentResponseDto.setStatus(document.getBusinessPartner().get().getStatus());
                    documentResponseDto.setWarehouseId(document.getWarehouse().getId());
                    documentResponseDto.setWarehouseName(document.getWarehouse().getName());
                    documentResponseDto.setWarehouseIdOut(document.getWarehouseOut().get().getId());
                    documentResponseDto.setWarehouseNameOut(document.getWarehouseOut().get().getName());
                    documentResponseDto.setYear(document.getBusinessYear().getYear());
                    documentResponseDto.setId(document.getId());
                    documentResponseDto.setTypeDocument(document.getType());
                    if(document.getIsReversed()){
                        documentResponseDto.setDocumentStatus(StatusDocument.REVERSED);
                    }else if(document.getIsBooked()){
                        documentResponseDto.setDocumentStatus(StatusDocument.BOOKED);
                    }else{
                        documentResponseDto.setDocumentStatus(StatusDocument.PENDING);
                    }
                    documentsResultSet.add(documentResponseDto);
                }
            }

        }

        return documentsResultSet;
    }

    public void editDocumentItem(DocumentItemEditDto documentItemEditDto) throws CustomException {
        Document document = documentRepository.findOne(documentItemEditDto.getDocument());

        if (document.getIsBooked() || document.getIsReversed() || document.getIsDeleted()) {
            throw new CustomException(ExceptionMessage.CANT_CHANGE_THIS_DOCUMENT);

        }
        DocumentItem documentItem = documentItemRepository.findOne(documentItemEditDto.getId());
        documentItem.setQuantity(documentItemEditDto.getQuantity());
        documentItem.setPrice(documentItemEditDto.getPrice());
        documentItem.setArticle(articleRepository.findOne(documentItemEditDto.getArticle()));
        documentItemRepository.save(documentItem);
    }

    public void bookDocument(Long id) throws CustomException {
        Document document = documentRepository.findOne(id);
        if (document == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }

        if(document.getIsBooked()){
            throw new CustomException(ExceptionMessage.DOCUMENT_CANT_BE_BOOKED);
        }
        //TODO: PITATI STA SE RADI SA DOKUMENTIMA KOJI NISU PROKNJIZENI KADA SE GODINA ZAVRSI
        if(document.getBusinessYear().getIsFinished()){
            throw new CustomException(ExceptionMessage.BOOK_NOT_ALLOWED);
        }

        List<DocumentItem> documentItems = documentItemRepository.findByDocument(id);

        if (documentItems.isEmpty()) {
            throw new CustomException(ExceptionMessage.DOCUMENT_ITEM_DOESNT_EXIST);
        }

        for (DocumentItem documentItem : documentItems) {
            addAnalytics(documentItem);
        }

        document.setIsBooked(true);
        documentRepository.save(document);
    }

    public void reverseDocument(Long id) throws CustomException {
        Document document = documentRepository.findOne(id);
        if (document == null) {
            throw new CustomException(ExceptionMessage.OBJECT_DOESNT_EXIST);
        }

        if(!document.getIsBooked()){
            throw new CustomException(ExceptionMessage.DOCUMENT_CANT_BE_REVERSE);
        }

        if(document.getIsReversed()){
            throw new CustomException(ExceptionMessage.DOCUMENT_CANT_BE_REVERSE);
        }


        //TODO: PITATI STA SE RADI SA DOKUMENTIMA KOJI NISU PROKNJIZENI KADA SE GODINA ZAVRSI
        if(document.getBusinessYear().getIsFinished()){
            throw new CustomException(ExceptionMessage.REVERSE_NOT_ALLOWED);
        }

        List<DocumentItem> documentItems = documentItemRepository.findByDocument(id);

        if (documentItems.isEmpty()) {
            throw new CustomException(ExceptionMessage.DOCUMENT_ITEM_DOESNT_EXIST);
        }

        for (DocumentItem documentItem : documentItems) {
            List<WarehouseCard> warehouseCards = warehouseCardRepository.findActiveByWarehouseAndArticle(documentItem.getDocument().getWarehouse().getId(), documentItem.getArticle().getId());

            if(warehouseCards.isEmpty()){
                throw new CustomException(ExceptionMessage.REVERSE_NOT_ALLOWED);
            }

            if(documentItem.getDocument().getType() == TypeDocument.BETWEEN){
                createAnalyticsWithSign(documentItem, warehouseCards.get(0), TypeAnalytics.OUTPUT,TypeDirection.INPUT, new BigDecimal(1),true);

                List<WarehouseCard> warehouseCardsOut = warehouseCardRepository.findActiveByWarehouseAndArticle(documentItem.getDocument().getWarehouseOut().get().getId(), documentItem.getArticle().getId());
                if(warehouseCardsOut.isEmpty()){
                    throw new CustomException(ExceptionMessage.REVERSE_NOT_ALLOWED_NOT_ACTIVE_CARD);
                }

                createAnalyticsWithSign(documentItem, warehouseCardsOut.get(0), TypeAnalytics.INPUT,TypeDirection.INPUT, new BigDecimal(-1),true);
            }else if(documentItem.getDocument().getType() == TypeDocument.INPUT){
                createAnalyticsWithSign(documentItem, warehouseCards.get(0), TypeAnalytics.INPUT,TypeDirection.INPUT, new BigDecimal(-1),true);
            }else if(documentItem.getDocument().getType() == TypeDocument.OUTPUT){
                createAnalyticsWithSign(documentItem, warehouseCards.get(0), TypeAnalytics.OUTPUT,TypeDirection.OUTPUT, new BigDecimal(-1),true);
            }
        }

        document.setIsReversed(true);
        documentRepository.save(document);
    }


    private void addAnalytics(DocumentItem documentItem) throws CustomException {
        List<WarehouseCard> warehouseCards = warehouseCardRepository.findActiveByWarehouseAndArticle(documentItem.getDocument().getWarehouse().getId(), documentItem.getArticle().getId());

        WarehouseCard warehouseCard = null;

        if (!warehouseCards.isEmpty()) {
            warehouseCard = warehouseCards.get(0);
        }else{
            warehouseCard = createWarehouseCard(documentItem, documentItem.getDocument().getWarehouse());

        }

        switch (documentItem.getType()) {
            case INPUT:
                createAnalyticsForPartner(documentItem, warehouseCard, TypeAnalytics.INPUT, TypeDirection.INPUT);
                break;
            case OUTPUT:
                createAnalyticsForPartner(documentItem, warehouseCard, TypeAnalytics.OUTPUT, TypeDirection.OUTPUT);
                break;
            case BETWEEN:
                createAnalyticsForWarehouse(documentItem, warehouseCard);
                break;
        }
    }

    private void createAnalyticsForPartner(DocumentItem documentItem, WarehouseCard warehouseCard, TypeAnalytics typeAnalytics, TypeDirection typeDirection) throws CustomException {
        Analytics analytics = new Analytics();
        analytics.setWarehouseCard(warehouseCard);
        analytics.setType(typeAnalytics);
        analytics.setDirection(typeDirection);
        analytics.setDocumentItem(Optional.ofNullable(documentItem));
        analytics.setPrice(documentItem.getPrice());
        analytics.setQuantity(documentItem.getQuantity());
        analytics.setDate(ZonedDateTime.now());
        analytics.setIsDeleted(false);

        if (documentItem.getType() == TypeDocument.INPUT) {
            analytics.setBalance(warehouseCard.getBalance().add(documentItem.getQuantity()));

            analytics.setValue(documentItem.getPrice().multiply(documentItem.getQuantity()));

            warehouseCard.setInput(warehouseCard.getInput().add(analytics.getQuantity()));

            BigDecimal leftPart = (warehouseCard.getTotalValue().add(documentItem.getQuantity().multiply(documentItem.getPrice())));
            warehouseCard.setAveragePrice(leftPart.divide(warehouseCard.getBalance().add(documentItem.getQuantity()),4, BigDecimal.ROUND_HALF_UP));

            warehouseCard.setBalance(warehouseCard.getStartState().add(warehouseCard.getInput()).subtract(warehouseCard.getOutput()));

            warehouseCard.setInValue(warehouseCard.getInValue().add(analytics.getValue()));

            warehouseCard.setTotalValue(warehouseCard.getStartState().add(warehouseCard.getInValue().subtract(warehouseCard.getOutValue())));

            analytics.setTotalValue(warehouseCard.getTotalValue());
        } else {  //OUTPUT
            if(documentItem.getQuantity().compareTo(warehouseCard.getBalance()) == 1){
                throw new CustomException(ExceptionMessage.MORE_THAN_HAVE);
            }

            analytics.setBalance(warehouseCard.getBalance().subtract(documentItem.getQuantity()));

            analytics.setValue(documentItem.getPrice().multiply(documentItem.getQuantity()));

            warehouseCard.setOutput(warehouseCard.getOutput().add(analytics.getQuantity()));

            warehouseCard.setBalance(warehouseCard.getStartState().add(warehouseCard.getInput()).subtract(warehouseCard.getOutput()));

            warehouseCard.setOutValue(warehouseCard.getOutValue().add(analytics.getValue()));

            warehouseCard.setTotalValue(warehouseCard.getStartState().add(warehouseCard.getInValue().subtract(warehouseCard.getOutValue())));

            analytics.setTotalValue(warehouseCard.getTotalValue());
        }

        warehouseCardRepository.save(warehouseCard);
        analyticsRepository.save(analytics);
    }

    private void createAnalyticsForWarehouse(DocumentItem documentItem, WarehouseCard warehouseCard) throws CustomException {

        if(documentItem.getQuantity().compareTo(warehouseCard.getBalance()) == 1){
            throw new CustomException(ExceptionMessage.MORE_THAN_HAVE);
        }
        createAnalyticsWithSign(documentItem, warehouseCard, TypeAnalytics.BETWEEN,TypeDirection.OUTPUT, new BigDecimal(-1),false);



        Warehouse warehouse = documentItem.getDocument().getWarehouseOut().get();

        List<WarehouseCard> warehouseCards = warehouseCardRepository.findActiveByWarehouseAndArticle(warehouse.getId(), documentItem.getArticle().getId());

        WarehouseCard outWarehouseCard = null;

        if (!warehouseCards.isEmpty()) {
            outWarehouseCard = warehouseCards.get(0);
        }else{
            outWarehouseCard = createWarehouseCard(documentItem, warehouse);
        }

        createAnalyticsWithSign(documentItem, outWarehouseCard, TypeAnalytics.BETWEEN,TypeDirection.INPUT, new BigDecimal(+1),false);
    }

    private void createAnalyticsWithSign(DocumentItem documentItem, WarehouseCard warehouseCard, TypeAnalytics typeAnalytics,TypeDirection typeDirection, BigDecimal sign, boolean isReverse) throws CustomException {
        Analytics analytics = new Analytics();
        analytics.setWarehouseCard(warehouseCard);
        analytics.setType(typeAnalytics);
        analytics.setDirection(typeDirection);
        analytics.setDocumentItem(Optional.ofNullable(documentItem));
        analytics.setPrice(documentItem.getPrice());
        analytics.setQuantity(documentItem.getQuantity().multiply(sign));
        if(isReverse){
            if(typeAnalytics==TypeAnalytics.OUTPUT)
                analytics.setBalance(warehouseCard.getBalance().add(documentItem.getQuantity()));
            else{

                analytics.setBalance(warehouseCard.getBalance().add(documentItem.getQuantity().multiply(sign)));
                if(analytics.getBalance().compareTo(BigDecimal.ZERO) == -1 ){
                    throw new CustomException(ExceptionMessage.NOT_ENOUGHT_QUANTITY_FOR_REVERSE);
                }
            }

        }else {
            analytics.setBalance(warehouseCard.getBalance().add(documentItem.getQuantity().multiply(sign)));
        }

        analytics.setDate(ZonedDateTime.now());
        analytics.setIsDeleted(false);
        analytics.setValue(analytics.getQuantity().multiply(analytics.getPrice()));
        if(isReverse){
            if(typeAnalytics==TypeAnalytics.OUTPUT){
                analytics.setTotalValue(warehouseCard.getTotalValue().add(analytics.getValue().multiply(sign)));
            }else {
                analytics.setTotalValue(warehouseCard.getTotalValue().add(analytics.getValue()));
            }
        }else{
            if(analytics.getType()==TypeAnalytics.BETWEEN){
                analytics.setTotalValue(warehouseCard.getTotalValue().add(analytics.getValue()));
            }else{
                analytics.setTotalValue(warehouseCard.getTotalValue().add(analytics.getValue().multiply(sign)));
            }

        }

        analyticsRepository.save(analytics);

        if(isReverse){
//            if(typeAnalytics ==TypeAnalytics.OUTPUT){
//                warehouseCard.setOutput(warehouseCard.getOutput().add(analytics.getQuantity()));
//                warehouseCard.setBalance(warehouseCard.getStartState().add(warehouseCard.getInput()).subtract(warehouseCard.getOutput()));
//                warehouseCard.setOutValue(warehouseCard.getOutValue().add(analytics.getValue()));
//                warehouseCard.setTotalValue(warehouseCard.getStartState().add(warehouseCard.getInValue().subtract(warehouseCard.getOutValue())));
//            }else{
                warehouseCard.setInput(warehouseCard.getInput().add(analytics.getQuantity()));
                warehouseCard.setBalance(warehouseCard.getStartState().add(warehouseCard.getInput()).subtract(warehouseCard.getOutput()));
                warehouseCard.setInValue(warehouseCard.getInValue().add(analytics.getValue()));
                warehouseCard.setTotalValue(warehouseCard.getStartState().add(warehouseCard.getInValue().subtract(warehouseCard.getOutValue())));
//            }
        }else{
            warehouseCard.setInput(warehouseCard.getInput().add(analytics.getQuantity()));
            warehouseCard.setBalance(warehouseCard.getStartState().add(warehouseCard.getInput()).subtract(warehouseCard.getOutput()));
            warehouseCard.setInValue(warehouseCard.getInValue().add(analytics.getValue()));
            warehouseCard.setTotalValue(warehouseCard.getStartState().add(warehouseCard.getInValue().subtract(warehouseCard.getOutValue())));
        }

        warehouseCardRepository.save(warehouseCard);
    }

    private WarehouseCard createWarehouseCard(DocumentItem documentItem, Warehouse warehouse) {
        WarehouseCard warehouseCard;
        warehouseCard = new WarehouseCard();
        warehouseCard.setArticle(documentItem.getArticle());
        warehouseCard.setIsDeleted(false);
        warehouseCard.setBalance(BigDecimal.ZERO);
        warehouseCard.setBusinessYear(documentItem.getDocument().getBusinessYear());
        warehouseCard.setInput(BigDecimal.ZERO);
        warehouseCard.setInValue(BigDecimal.ZERO);
        warehouseCard.setOutput(BigDecimal.ZERO);
        warehouseCard.setOutValue(BigDecimal.ZERO);
        warehouseCard.setTotalValue(BigDecimal.ZERO);
        warehouseCard.setWarehouse(warehouse);
        warehouseCard.setStartState(BigDecimal.ZERO);
        warehouseCard.setAveragePrice(BigDecimal.ZERO);
        warehouseCardRepository.save(warehouseCard);

        /*
            Start state analytic
         */
//        Analytics analytics = new Analytics();
//        analytics.setType(TypeAnalytics.START_STATE);
//        analytics.setDirection(TypeDirection.INPUT);
//        analytics.setPrice(BigDecimal.ZERO);
//        analytics.setQuantity(BigDecimal.ZERO);
//        analytics.setBalance(BigDecimal.ZERO);
//        analytics.setTotalValue(BigDecimal.ZERO);
//        analytics.setValue(BigDecimal.ZERO);
//        analytics.setWarehouseCard(warehouseCard);
//        analytics.setDate(ZonedDateTime.now());
//        analytics.setIsDeleted(false);
//        analyticsRepository.save(analytics);

        return warehouseCard;
    }
}
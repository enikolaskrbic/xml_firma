package com.poslovna.backend.service;

import com.poslovna.backend.model.Firm;
import com.poslovna.backend.model.User;
import com.poslovna.backend.model.enumeration.ExceptionMessage;
import com.poslovna.backend.model.enumeration.Role;
import com.poslovna.backend.repository.FirmRepository;
import com.poslovna.backend.repository.UserRepository;
import com.poslovna.backend.web.rest.dto.FirmDTO;
import com.poslovna.backend.web.rest.dto.UserDTO;
import com.poslovna.backend.web.rest.exception.CustomException;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by Skrbic on 3/16/2017.
 */
@Service
public class AdminService {

    @Inject
    FirmRepository firmRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    private PasswordEncoder passwordEncoder;

    public void addFirm(FirmDTO firmDTO) throws CustomException {
        Firm firm = new Firm();
        if(firmDTO.getPib().length()!=8)
            throw new CustomException(ExceptionMessage.PIB_INCORECT);
        List<Firm> firms = firmRepository.findByPib(firmDTO.getPib());
        if(firms.size()>0){
            throw new CustomException(ExceptionMessage.PIB_ALREADY_EXISTS);
        }
        firm.setPib(firmDTO.getPib());
        firm.setName(firmDTO.getName());
        firm.setAddress(firmDTO.getAddress());
        firm.setCity(firmDTO.getCity());
        firm.setCountry(firmDTO.getCountry());

        firm.setEmail(Optional.ofNullable(firmDTO.getEmail()));
        firm.setPhone(Optional.ofNullable(firmDTO.getPhone()));
        firm.setWeb(Optional.ofNullable(firmDTO.getWeb()));

        firm.setIsDeleted(false);

        firmRepository.save(firm);
    }

    public void addUser(UserDTO userDTO) throws CustomException {
        final Optional<User> optionalUser = userRepository.findByEmail(userDTO.getEmail());
        if (optionalUser.isPresent()) {
            throw new CustomException(ExceptionMessage.USER_ALREADY_EXISTS);
        }
        Firm firm = firmRepository.findOne(userDTO.getIdFirm());
        User user = new User();

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setIsDeleted(false);
        user.setFirm(Optional.ofNullable(firm));
        user.setTimetamp(ZonedDateTime.now());
        user.setEmail(userDTO.getEmail());
        user.setRole(Role.ROLE_MANAGER);
        user.setPasswordHash(passwordEncoder.encode("Poslovna123"));
        user.setEmailVerificationCode(Optional.of(RandomStringUtils.randomAlphanumeric(64)));
        user.setEmailVerificationCodeTimestamp(Optional.of(ZonedDateTime.now(ZoneId.of("UTC")).plusDays(1)));
        user.setEmailVerified(false);

        userRepository.save(user);

    }
}

package com.poslovna.backend.service;

import java.time.*;
import java.util.Locale;

import com.poslovna.backend.database.DatabaseQuery;
import com.poslovna.backend.xml_model.korisnik.Korisnik;
import org.apache.commons.lang.RandomStringUtils;

import java.util.Optional;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.poslovna.backend.config.CustomProperties;
import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.*;
import com.poslovna.backend.web.rest.dto.*;
import com.poslovna.backend.web.rest.exception.*;
import com.poslovna.backend.security.JWTUtils;


@Service
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private CustomProperties customProperties;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private MailService mailService;

    @Inject
    private FirmRepository firmRepository;

    @Inject
    private UserRepository userRepository;

    public User changePassword(Long userId, String oldPassword, String newPassword) {

        log.debug("changePassword(userId: {})", userId);

        final User user = userRepository.findOne(userId);
        if (user == null) {
            throw new AuthenticationError("credentials.invalid", "Credentials are invalid!");
        }
        if (!passwordEncoder.matches(oldPassword, user.getPasswordHash())) {
            throw new AuthenticationError("credentials.invalid", "Credentials are invalid!");
        }
        user.setPasswordHash(passwordEncoder.encode(newPassword));
        userRepository.save(user);
        return user;
    }

    public User emailSignUp(String firstName, String lastName, Boolean isDeleted, ZonedDateTime timetamp, Long firmId, String email, String password) {

        log.debug("emailSignUp(firstName: {}, lastName: {}, isDeleted: {}, timetamp: {}, firmId: {}, email: {})", firstName, lastName, isDeleted, timetamp, firmId, email);

        final Optional<User> optionalUser = userRepository.findByEmail(email);
        if (optionalUser.isPresent()) {
            throw new UserWithEmailAlreadyExists("User with email: " + email + " already exists.");
        }

        final User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setIsDeleted(isDeleted);
        user.setTimetamp(ZonedDateTime.now(ZoneId.of("UTC")).plusDays(1));
        if (firmId == null) {
            user.setFirm(Optional.empty());
        } else {
            final Firm firm = firmRepository.findOne(firmId);
            user.setFirm(Optional.of(firm));
        }
        user.setEmail(email);
        user.setRole(Role.ROLE_MANAGER);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setEmailVerificationCode(Optional.of(RandomStringUtils.randomAlphanumeric(64)));
        user.setEmailVerificationCodeTimestamp(Optional.of(ZonedDateTime.now(ZoneId.of("UTC")).plusDays(1)));
        user.setEmailVerified(false);
        userRepository.save(user);

        //mailService.sendVerificationEmail(email, user.getEmailVerificationCode().get(), Locale.ENGLISH);

        return user;
    }

    public SignInResponse emailSignIn(String email, String password) {

        log.debug("emailSignIn(email: {})", email);

        final User user = userRepository.findByEmail(email).orElseThrow(() -> new AuthenticationError("credentials.invalid", "Credentials are invalid!"));
        if (!passwordEncoder.matches(password, user.getPasswordHash()))
            throw new AuthenticationError("credentials.invalid", "Credentials are invalid!");

        final SignInResponse response = new SignInResponse();
        //final String accessToken = JWTUtils.createToken(user.getId(), user.getRole(), customProperties.getSecretKey());
        //response.setAccessToken(accessToken);
        response.setId(user.getId());
        response.setFirstName(user.getFirstName());
        response.setLastName(user.getLastName());
        response.setIsDeleted(user.getIsDeleted());
        response.setTimetamp(user.getTimetamp());
        response.setFirmId(user.getFirm().map(Firm::getId).orElse(null));
        response.setRole(user.getRole());
        response.setEmail(user.getEmail());
        return response;
    }


    public SignInResponse signInXml(String email, String password){
        Korisnik korisnik= DatabaseQuery.searchKorisnik(email,password);
        if(korisnik==null){
            throw new AuthenticationError("credentials.invalid", "Credentials are invalid!");
        }

        final SignInResponse response = new SignInResponse();
        final String accessToken = JWTUtils.createToken(korisnik.getKorisnickoIme(), korisnik.getRola(), customProperties.getSecretKey());
        response.setAccessToken(accessToken);
        response.setKorisnickoIme(korisnik.getKorisnickoIme());
        response.setRola(korisnik.getRola());
        response.setPibFirme(korisnik.getPibFirme());
        response.setApiFirme(korisnik.getApiFirme());
        //response.setId(user.getId());
        //response.setFirstName(user.getFirstName());
        //response.setLastName(user.getLastName());
        //response.setIsDeleted(user.getIsDeleted());
        //response.setTimetamp(user.getTimetamp());
        //response.setFirmId();
        //response.setRole(user.getRole());
        //response.setEmail(user.getEmail());
        return response;


    }

    public User verifyEmail(String emailVerificationCode) {

        log.debug("verifyEmail(emailVerificationCode: {})", emailVerificationCode);

        final Optional<User> optionalUser = userRepository.findByEmailVerificationCodeMandatory(emailVerificationCode);
        if (!optionalUser.isPresent()) {
            throw new AuthenticationError("credentials.invalid", "Invalid email verification code!");
        }

        final User user = optionalUser.get();
        if (user.getEmailVerificationCodeTimestamp().get().isBefore(ZonedDateTime.now(ZoneId.of("UTC")))) {
            throw new AuthenticationError("emailVerificationCode.expired", "Email verification code expired!");
        }

        user.setEmailVerified(true);
        userRepository.save(user);
        return user;
    }

    public void forgotPassword(String email) {

        log.debug("forgotPassword(email: {})", email);

        final Optional<User> optionalUser = userRepository.findByEmail(email).filter(User::getEmailVerified);
        if (!optionalUser.isPresent()) {
            throw new BadRequestError("invalid.email", "Email: " + email + " does not exist or is not registered.");
        }

        final User user = optionalUser.get();
        final String resetPasswordCode = RandomStringUtils.randomAlphabetic(64);
        user.setResetPasswordCode(Optional.of(resetPasswordCode));
        user.setResetPasswordCodeTimestamp(Optional.of(ZonedDateTime.now(ZoneOffset.UTC).plusHours(1)));
        userRepository.save(user);

        mailService.sendResetPasswordEmail(user.getEmail(), resetPasswordCode, Locale.ENGLISH);
    }

    public void resetPassword(String resetPasswordCode, String newPassword) {

        log.debug("resetPassword()");

        final Optional<User> optionalUser = userRepository.findByResetPasswordCodeMandatory(resetPasswordCode);
        if (!optionalUser.isPresent()) {
            throw new BadRequestError("resetPasswordCode.invalid", "Invalid reset password code!");
        }

        final User user = optionalUser.get();
        if (user.getResetPasswordCodeTimestamp().get().isBefore(ZonedDateTime.now(ZoneOffset.UTC))) {
            throw new AuthenticationError("resetPasswordCode.expired", "Reset password code expired!");
        }

        user.setPasswordHash(passwordEncoder.encode(newPassword));
        user.setResetPasswordCodeTimestamp(Optional.empty());
        userRepository.save(user);
    }

}

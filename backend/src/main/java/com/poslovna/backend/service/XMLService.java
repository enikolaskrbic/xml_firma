package com.poslovna.backend.service;

import com.poslovna.backend.model.enumeration.ExceptionMessage;
import com.poslovna.backend.util.MyValidationEventHandler;
import com.poslovna.backend.web.rest.exception.CustomException;
import com.poslovna.backend.xml_model.faktura.Faktura;
import com.poslovna.backend.xml_model.faktura.StavkaFakture;
import com.poslovna.backend.xml_model.izvod.IzvodRequest;
import com.poslovna.backend.xml_model.nalogzaplacanje.NalogZaPlacanjeRequest;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;

/**
 * Created by ivansavic on 4/30/17.
 */
@Service
public class XMLService {

    public String validateSchemaMarshall(String nazivPaketa, String putanjaSeme, Object objekat, String nazivKlase) throws CustomException {
        Faktura faktura = null;
        StavkaFakture stavkaFakture = null;
        String xml = null;
        try{
            JAXBContext context = JAXBContext.newInstance(nazivPaketa);

            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.xml");
            File file = new File("test.xml");

            PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.close();

            marshaller.marshal(objekat, file);

            //aaaaaaaa
//            InputStream is = new FileInputStream("test.xml");
//            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
//            String line2 = buf.readLine(); StringBuilder sb = new StringBuilder();
//            while(line2 != null){
//                sb.append(line2).append("\n");
//                line2 = buf.readLine();
//            }
//            String fileAsString = sb.toString();



            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File(putanjaSeme));

            Unmarshaller unmarshaller = context.createUnmarshaller();

            // Podešavanje unmarshaller-a za XML schema validaciju
            unmarshaller.setSchema(schema);
            unmarshaller.setEventHandler(new MyValidationEventHandler());

            if(nazivKlase.equals("faktura")){
                faktura = (Faktura) unmarshaller.unmarshal(file);
            }else if(nazivKlase.equals("stavkaFakture")){
                stavkaFakture = (StavkaFakture) unmarshaller.unmarshal(file);
            }else if(nazivKlase.equals("izvodRequest")){
                IzvodRequest izvodRequest= (IzvodRequest) unmarshaller.unmarshal(file);
            }else if(nazivKlase.equals("NalogZaPlacanjeRequest")){
                NalogZaPlacanjeRequest nalogZaPlacanjeRequest = (NalogZaPlacanjeRequest)unmarshaller.unmarshal(file);
            }

            BufferedReader br = new BufferedReader(new FileReader("test.xml"));
            String line = null;
            xml = "";
            boolean first = true;
            while ((line = br.readLine()) != null) {
                if(first){
                    first = false;
                }else{
                    xml += line;
                }
            }
        }catch (JAXBException e1){
            e1.printStackTrace();
            throw new CustomException(e1.getLinkedException().getMessage());
        }catch (SAXException e2){
            e2.printStackTrace();
            throw new CustomException(e2.getMessage());
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            throw new CustomException(e3.getMessage());
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
            throw new CustomException(e4.getMessage());
        } catch (IOException e5) {
            e5.printStackTrace();
            throw new CustomException(e5.getMessage());
        }

        return xml;
    }
}

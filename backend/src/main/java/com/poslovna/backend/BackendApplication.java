package com.poslovna.backend;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import com.poslovna.backend.config.CustomProperties;
import org.springframework.util.SocketUtils;


@ComponentScan("com.poslovna.backend")
@EnableAutoConfiguration
@EnableConfigurationProperties({LiquibaseProperties.class, CustomProperties.class})
public class BackendApplication {

    /* HTTPS - HOW TO LINKS */
    /* BASIC INFO: https://www.instantssl.com/ssl-certificate-products/https.html */
    /* HOW TO CREATE KS AND CERT AND HOT TO INSTALL IT: https://www.digicert.com/csr-creation-java.htm */
    /* MAKE YOU CERT LEGAL FOR FREE: https://secure.instantssl.com/products/SSLIdASignup1a */
    /* SPRING CONFIG: http://docs.spring.io/spring-boot/docs/current/reference/html/howto-embedded-servlet-containers.html */
    /* SPRING INFO ABOUT SERVER: https://spring.io/blog/2014/03/07/deploying-spring-boot-applications */
    /* stackoverflow: http://stackoverflow.com/questions/7811128/how-to-develop-https-site-with-spring-3-x */
    /* tomcat http://tomcat.apache.org/tomcat-5.5-doc/ssl-howto.html#Prepare_the_Certificate_Keystore */

//    @Bean
//    public Integer port() {
//        return SocketUtils.findAvailableTcpPort();
//    }
//
//    @Bean
//    public EmbeddedServletContainerFactory servletContainer() {
//        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
//        tomcat.addAdditionalTomcatConnectors(createStandardConnector());
//        return tomcat;
//    }
//
//    private Connector createStandardConnector() {
//        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
//        connector.setPort(port());
//        return connector;
//    }

    /*
        HTTPS setup:
        https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs#generating-ssl-certificates
        
     */
    public static void main(String[] args) {
        final SpringApplication app = new SpringApplication(BackendApplication.class);
        final SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);
        if (!source.containsProperty("spring.profiles.active") && !System.getenv().containsKey("SPRING_PROFILES_ACTIVE")) {
            app.setAdditionalProfiles("dev");
        }
        app.run(args).getEnvironment();
    }

}

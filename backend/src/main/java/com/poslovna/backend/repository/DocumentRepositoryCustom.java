package com.poslovna.backend.repository;

import java.math.BigDecimal;
import java.time.*;
import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.tuple.*;


public interface DocumentRepositoryCustom {

    List<Document> findByType(TypeDocument type);

    List<Document> findByDateOfDelivery(Optional<ZonedDateTime> dateOfDelivery);

    List<Document> findByDateOfDeliveryMandatory(ZonedDateTime dateOfDelivery);

    List<Document> findByDateOfValue(Optional<ZonedDateTime> dateOfValue);

    List<Document> findByDateOfValueMandatory(ZonedDateTime dateOfValue);

    List<Document> findByDateOfStatement(Optional<ZonedDateTime> dateOfStatement);

    List<Document> findByDateOfStatementMandatory(ZonedDateTime dateOfStatement);

    List<Document> findByAmount(Optional<BigDecimal> amount);

    List<Document> findByAmountMandatory(BigDecimal amount);

    List<Document> findByBusinessYear(Long businessYearId);

    List<Document> findByBusinessPartner(Optional<Long> businessPartnerId);

    List<Document> findByBusinessPartnerMandatory(Long businessPartnerId);

    List<Document> findByWarehouse(Long warehouseId);

    List<Document> findByWarehouseOut(Optional<Long> warehouseOutId);

    List<Document> findByWarehouseOutMandatory(Long warehouseOutId);

    List<Document> findByIsReversed(Boolean isReversed);

    List<Document> findByIsBooked(Boolean isBooked);

    List<Document> findByIsDeleted(Boolean isDeleted);

    List<DocumentBusinessPartnerWarehouseBusinessYearTuple> documentsSearch(Optional<Long> documentId, Optional<ZonedDateTime> documentDateOfValue, Optional<ZonedDateTime> documentDateOfDelivery,
            Optional<Long> warehuseId, Optional<String> warehouseName, Optional<Long> businessPartnerId, Optional<String> bussinesPartnerName, Optional<Long> businessYearId,
            Optional<String> businessYearYear);

    Optional<Document> deleteDocumentHard(Long id);

}

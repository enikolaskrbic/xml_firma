
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class AnalyticsWarehouseCardArticleTuple {

    private final Analytics analytics;
    private final WarehouseCard warehouseCard;
    private final Article article;

    public AnalyticsWarehouseCardArticleTuple(Analytics analytics, WarehouseCard warehouseCard, Article article) {
        this.analytics = analytics;
        this.warehouseCard = warehouseCard;
        this.article = article;
    }

    public Analytics getAnalytics() {
        return analytics;
    }

    public WarehouseCard getWarehouseCard() {
        return warehouseCard;
    }

    public Article getArticle() {
        return article;
    }

}
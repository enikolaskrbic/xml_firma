package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.UnitOfMeasureRepositoryCustom;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class UnitOfMeasureRepositoryImpl implements UnitOfMeasureRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(UnitOfMeasureRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<UnitOfMeasure> findByName(String name) {
        log.trace(".findByName(name: {})", name);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        return factory.select(unitOfMeasure).from(unitOfMeasure).where(unitOfMeasure.name.eq(name)).fetch();
    }

    @Override
    public List<UnitOfMeasure> findByShortName(Optional<String> shortName) {
        log.trace(".findByShortName(shortName: {})", shortName);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        return factory.select(unitOfMeasure).from(unitOfMeasure).where(shortName.isPresent() ? unitOfMeasure.shortName.eq(shortName.get()) : null).fetch();
    }

    @Override
    public List<UnitOfMeasure> findByShortNameMandatory(String shortName) {
        log.trace(".findByShortNameMandatory(shortName: {})", shortName);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        return factory.select(unitOfMeasure).from(unitOfMeasure).where(unitOfMeasure.shortName.eq(shortName)).fetch();
    }

    @Override
    public List<UnitOfMeasure> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        return factory.select(unitOfMeasure).from(unitOfMeasure).where(unitOfMeasure.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<UnitOfMeasure> unitOfMeasures() {
        log.trace(".unitOfMeasures()");
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        return factory.select(unitOfMeasure).from(unitOfMeasure).fetch();
    }

    @Override
    public List<UnitOfMeasure> unitOfMeasuresSearch(Optional<Long> unitOfMeasureId, Optional<String> unitOfMeasureName, Optional<String> unitOfMeasureShortName) {
        log.trace(".unitOfMeasuresSearch(unitOfMeasureId: {}, unitOfMeasureName: {}, unitOfMeasureShortName: {})", unitOfMeasureId, unitOfMeasureName, unitOfMeasureShortName);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        return factory.select(unitOfMeasure).from(unitOfMeasure)
                .where(new BooleanBuilder().and(unitOfMeasureId.isPresent() ? unitOfMeasure.id.eq(unitOfMeasureId.get()) : null)
                        .or(unitOfMeasureName.isPresent() ? unitOfMeasure.name.like("%" + unitOfMeasureName.get() + "%") : null)
                        .or(unitOfMeasureShortName.isPresent() ? unitOfMeasure.shortName.like("%" + unitOfMeasureShortName.get() + "%") : null))
                .fetch();
    }

    @Override
    public Optional<UnitOfMeasure> deleteUnitOfMeasureHard(Long id) {
        log.trace(".deleteUnitOfMeasureHard(id: {})", id);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        return Optional.ofNullable(factory.select(unitOfMeasure).from(unitOfMeasure).where(unitOfMeasure.id.eq(id)).fetchOne());
    }

}

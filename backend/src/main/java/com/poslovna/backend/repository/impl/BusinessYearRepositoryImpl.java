package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.BusinessYearRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class BusinessYearRepositoryImpl implements BusinessYearRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(BusinessYearRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<BusinessYear> findByYear(String year) {
        log.trace(".findByYear(year: {})", year);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        return factory.select(businessYear).from(businessYear).where(businessYear.year.eq(year)).fetch();
    }

    @Override
    public List<BusinessYear> findByIsFinished(Boolean isFinished) {
        log.trace(".findByIsFinished(isFinished: {})", isFinished);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        return factory.select(businessYear).from(businessYear).where(businessYear.isFinished.eq(isFinished)).fetch();
    }

    @Override
    public List<BusinessYear> findByFirmAndIsFinished(Long firmId, Boolean isFinished) {
        log.trace(".findByIsFinished(isFinished: {})", isFinished);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        return factory.select(businessYear).from(businessYear).where(businessYear.isFinished.eq(isFinished).and(businessYear.firm.id.eq(firmId))).fetch();
    }

    @Override
    public List<BusinessYear> findByFirmAndIsDeleted(Long firmId, Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        return factory.select(businessYear).from(businessYear).where(businessYear.isDeleted.eq(isDeleted).and(businessYear.firm.id.eq(firmId))).fetch();
    }

    @Override
    public List<BusinessYear> findByFirm(Long firmId) {
        log.trace(".findByFirm(firmId: {})", firmId);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        return factory.select(businessYear).from(businessYear).where(businessYear.firm.id.eq(firmId)).fetch();
    }

    @Override
    public List<BusinessYear> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        return factory.select(businessYear).from(businessYear).where(businessYear.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<BusinessYearFirmTuple> businessYears(Long firmId) {
        log.trace(".businessYears()");
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QFirm firm = QFirm.firm;
        return factory.select(businessYear, firm).from(businessYear).innerJoin(businessYear.firm, firm).where(businessYear.firm.id.eq(firmId)).fetch().stream().map(t -> new BusinessYearFirmTuple(t.get(businessYear), t.get(firm)))
                .collect(Collectors.toList());
    }

    @Override
    public List<BusinessYearFirmTuple> activeBusinessYear() {
        log.trace(".activeBusinessYear()");
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QFirm firm = QFirm.firm;
        return factory.select(businessYear, firm).from(businessYear).innerJoin(businessYear.firm, firm).where(businessYear.isFinished.eq(false)).fetch().stream()
                .map(t -> new BusinessYearFirmTuple(t.get(businessYear), t.get(firm))).collect(Collectors.toList());
    }

    @Override
    public List<BusinessYearFirmTuple> activeBusinessYearByFirm(Long firmId) {
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QFirm firm = QFirm.firm;
        return factory.select(businessYear, firm).from(businessYear).innerJoin(businessYear.firm, firm).where(businessYear.isFinished.eq(false).and(businessYear.firm.id.eq(firmId))).fetch().stream()
                .map(t -> new BusinessYearFirmTuple(t.get(businessYear), t.get(firm))).collect(Collectors.toList());
    }

    @Override
    public List<BusinessYearFirmTuple> businessYearsSearch(Optional<Long> businessYearId, Optional<String> businessYearYear, Optional<Boolean> businessYearIsFinished) {
        log.trace(".businessYearsSearch(businessYearId: {}, businessYearYear: {}, businessYearIsFinished: {})", businessYearId, businessYearYear, businessYearIsFinished);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QFirm firm = QFirm.firm;
        return factory.select(businessYear, firm).from(businessYear).innerJoin(businessYear.firm, firm)
                .where(new BooleanBuilder().and(businessYearId.isPresent() ? businessYear.id.eq(businessYearId.get()) : null)
                        .or(businessYearYear.isPresent() ? businessYear.year.eq(businessYearYear.get()) : null)
                        .or(businessYearIsFinished.isPresent() ? businessYear.isFinished.eq(businessYearIsFinished.get()) : null))
                .fetch().stream().map(t -> new BusinessYearFirmTuple(t.get(businessYear), t.get(firm))).collect(Collectors.toList());
    }

    @Override
    public Optional<BusinessYear> deleteBusinessYearHard(Long id) {
        log.trace(".deleteBusinessYearHard(id: {})", id);
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        return Optional.ofNullable(factory.select(businessYear).from(businessYear).where(businessYear.id.eq(id)).fetchOne());
    }

}

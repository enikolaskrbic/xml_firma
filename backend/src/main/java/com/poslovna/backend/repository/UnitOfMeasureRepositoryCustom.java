package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;


public interface UnitOfMeasureRepositoryCustom {

    List<UnitOfMeasure> findByName(String name);

    List<UnitOfMeasure> findByShortName(Optional<String> shortName);

    List<UnitOfMeasure> findByShortNameMandatory(String shortName);

    List<UnitOfMeasure> findByIsDeleted(Boolean isDeleted);

    List<UnitOfMeasure> unitOfMeasures();

    List<UnitOfMeasure> unitOfMeasuresSearch(Optional<Long> unitOfMeasureId, Optional<String> unitOfMeasureName, Optional<String> unitOfMeasureShortName);

    Optional<UnitOfMeasure> deleteUnitOfMeasureHard(Long id);

}

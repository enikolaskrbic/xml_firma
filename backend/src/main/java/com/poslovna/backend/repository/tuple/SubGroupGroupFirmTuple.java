
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class SubGroupGroupFirmTuple {

    private final SubGroup subGroup;
    private final GroupFirm groupFirm;

    public SubGroupGroupFirmTuple(SubGroup subGroup, GroupFirm groupFirm) {
        this.subGroup = subGroup;
        this.groupFirm = groupFirm;
    }

    public SubGroup getSubGroup() {
        return subGroup;
    }

    public GroupFirm getGroupFirm() {
        return groupFirm;
    }

}
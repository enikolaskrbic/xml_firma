package com.poslovna.backend.repository.impl;

import java.time.*;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.UserRepositoryCustom;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class UserRepositoryImpl implements UserRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(UserRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<User> findByFirstName(String firstName) {
        log.trace(".findByFirstName(firstName: {})", firstName);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.firstName.eq(firstName)).fetch();
    }

    @Override
    public List<User> findByLastName(String lastName) {
        log.trace(".findByLastName(lastName: {})", lastName);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.lastName.eq(lastName)).fetch();
    }

    @Override
    public List<User> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<User> findByTimetamp(ZonedDateTime timetamp) {
        log.trace(".findByTimetamp(timetamp: {})", timetamp);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.timetamp.eq(timetamp)).fetch();
    }

    @Override
    public List<User> findByFirm(Optional<Long> firmId) {
        log.trace(".findByFirm(firmId: {})", firmId);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(firmId.isPresent() ? user.firm.id.eq(firmId.get()) : null).fetch();
    }

    @Override
    public List<User> findByFirmMandatory(Long firmId) {
        log.trace(".findByFirmMandatory(firmId: {})", firmId);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.firm.id.eq(firmId)).fetch();
    }

    @Override
    public List<User> findByRole(Role role) {
        log.trace(".findByRole(role: {})", role);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.role.eq(role)).fetch();
    }

    @Override
    public Optional<User> findByEmail(String email) {
        log.trace(".findByEmail(email: {})", email);
        final QUser user = QUser.user;
        return Optional.ofNullable(factory.select(user).from(user).where(user.email.eq(email)).fetchOne());
    }

    @Override
    public List<User> findByPasswordHash(String passwordHash) {
        log.trace(".findByPasswordHash(passwordHash)");
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.passwordHash.eq(passwordHash)).fetch();
    }

    @Override
    public List<User> findByEmailVerificationCode(Optional<String> emailVerificationCode) {
        log.trace(".findByEmailVerificationCode(emailVerificationCode)");
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(emailVerificationCode.isPresent() ? user.emailVerificationCode.eq(emailVerificationCode.get()) : null).fetch();
    }

    @Override
    public Optional<User> findByEmailVerificationCodeMandatory(String emailVerificationCode) {
        log.trace(".findByEmailVerificationCodeMandatory(emailVerificationCode)");
        final QUser user = QUser.user;
        return Optional.ofNullable(factory.select(user).from(user).where(user.emailVerificationCode.eq(emailVerificationCode)).fetchOne());
    }

    @Override
    public List<User> findByEmailVerificationCodeTimestamp(Optional<ZonedDateTime> emailVerificationCodeTimestamp) {
        log.trace(".findByEmailVerificationCodeTimestamp(emailVerificationCodeTimestamp: {})", emailVerificationCodeTimestamp);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(emailVerificationCodeTimestamp.isPresent() ? user.emailVerificationCodeTimestamp.eq(emailVerificationCodeTimestamp.get()) : null).fetch();
    }

    @Override
    public List<User> findByEmailVerificationCodeTimestampMandatory(ZonedDateTime emailVerificationCodeTimestamp) {
        log.trace(".findByEmailVerificationCodeTimestampMandatory(emailVerificationCodeTimestamp: {})", emailVerificationCodeTimestamp);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.emailVerificationCodeTimestamp.eq(emailVerificationCodeTimestamp)).fetch();
    }

    @Override
    public List<User> findByEmailVerified(Boolean emailVerified) {
        log.trace(".findByEmailVerified(emailVerified: {})", emailVerified);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.emailVerified.eq(emailVerified)).fetch();
    }

    @Override
    public List<User> findByResetPasswordCode(Optional<String> resetPasswordCode) {
        log.trace(".findByResetPasswordCode(resetPasswordCode)");
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(resetPasswordCode.isPresent() ? user.resetPasswordCode.eq(resetPasswordCode.get()) : null).fetch();
    }

    @Override
    public Optional<User> findByResetPasswordCodeMandatory(String resetPasswordCode) {
        log.trace(".findByResetPasswordCodeMandatory(resetPasswordCode)");
        final QUser user = QUser.user;
        return Optional.ofNullable(factory.select(user).from(user).where(user.resetPasswordCode.eq(resetPasswordCode)).fetchOne());
    }

    @Override
    public List<User> findByResetPasswordCodeTimestamp(Optional<ZonedDateTime> resetPasswordCodeTimestamp) {
        log.trace(".findByResetPasswordCodeTimestamp(resetPasswordCodeTimestamp: {})", resetPasswordCodeTimestamp);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(resetPasswordCodeTimestamp.isPresent() ? user.resetPasswordCodeTimestamp.eq(resetPasswordCodeTimestamp.get()) : null).fetch();
    }

    @Override
    public List<User> findByResetPasswordCodeTimestampMandatory(ZonedDateTime resetPasswordCodeTimestamp) {
        log.trace(".findByResetPasswordCodeTimestampMandatory(resetPasswordCodeTimestamp: {})", resetPasswordCodeTimestamp);
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(user.resetPasswordCodeTimestamp.eq(resetPasswordCodeTimestamp)).fetch();
    }

    @Override
    public List<User> users() {
        log.trace(".users()");
        final QUser user = QUser.user;
        return factory.select(user).from(user).where(new BooleanBuilder().and(user.role.eq(Role.ROLE_MANAGER)).and(user.isDeleted.eq(false))).fetch();
    }

}

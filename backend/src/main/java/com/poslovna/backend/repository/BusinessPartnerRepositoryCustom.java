package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.tuple.*;


public interface BusinessPartnerRepositoryCustom {

    List<BusinessPartner> findByName(String name);

    List<BusinessPartner> findByAddress(String address);

    List<BusinessPartner> findByCity(String city);

    List<BusinessPartner> findByCountry(String country);

    List<BusinessPartner> findByEmail(Optional<String> email);

    List<BusinessPartner> findByEmailMandatory(String email);

    List<BusinessPartner> findByPhone(Optional<String> phone);

    List<BusinessPartner> findByPhoneMandatory(String phone);

    List<BusinessPartner> findByWeb(Optional<String> web);

    List<BusinessPartner> findByWebMandatory(String web);

    List<BusinessPartner> findByStatus(RoleBusinessPartner status);

    List<BusinessPartner> findByFirm(Long firmId);

    List<BusinessPartnerFirmTuple> businessPartners(Long firmId);

    List<BusinessPartner> findByIsDeleted(Boolean isDeleted);

    List<BusinessPartnerFirmTuple> businessPartnersSearch(Optional<Long> businessPartnerId, Optional<String> businessPartnerName, Optional<String> businessPartnerAddress,
            Optional<String> businessPartnerCity, Optional<String> businessPartnerCountry, Optional<String> businessPartnerWeb, Optional<String> businessPartnerEmail,
            Optional<String> businessPartnerPhone, Optional<RoleBusinessPartner> businessPartnerStatus);

    Optional<BusinessPartner> deleteBusinessPartnerHard(Long id);

}

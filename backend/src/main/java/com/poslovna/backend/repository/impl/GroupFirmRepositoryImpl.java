package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.GroupFirmRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class GroupFirmRepositoryImpl implements GroupFirmRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(GroupFirmRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<GroupFirm> findByName(String name) {
        log.trace(".findByName(name: {})", name);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        return factory.select(groupFirm).from(groupFirm).where(groupFirm.name.eq(name)).fetch();
    }

    @Override
    public List<GroupFirm> findByFirm(Long firmId) {
        log.trace(".findByFirm(firmId: {})", firmId);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        return factory.select(groupFirm).from(groupFirm).where(groupFirm.firm.id.eq(firmId)).fetch();
    }

    @Override
    public List<GroupFirm> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        return factory.select(groupFirm).from(groupFirm).where(groupFirm.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<GroupFirmFirmTuple> groups(Long groupFirmId) {
        log.trace(".groups()");
        final QFirm firm = QFirm.firm;
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        return factory.select(firm, groupFirm).from(groupFirm).innerJoin(groupFirm.firm, firm).where(groupFirm.firm.id.eq(groupFirmId)).fetch().stream().map(t -> new GroupFirmFirmTuple(t.get(groupFirm), t.get(firm)))
                .collect(Collectors.toList());
    }

    @Override
    public List<GroupFirmFirmTuple> searchGroups(Optional<Long> groupFirmId, Optional<String> groupFirmName) {
        log.trace(".searchGroups(groupFirmId: {}, groupFirmName: {})", groupFirmId, groupFirmName);
        final QFirm firm = QFirm.firm;
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        return factory.select(firm, groupFirm).from(groupFirm).innerJoin(groupFirm.firm, firm)
                .where(new BooleanBuilder().and(groupFirmId.isPresent() ? groupFirm.id.eq(groupFirmId.get()) : null).or(groupFirmName.isPresent() ? groupFirm.name.like("%" + groupFirmName.get() + "%") : null))
                .fetch().stream().map(t -> new GroupFirmFirmTuple(t.get(groupFirm), t.get(firm))).collect(Collectors.toList());
    }

    @Override
    public Optional<GroupFirm> deleteGroupHard(Long groupId) {
        log.trace(".deleteGroupHard(groupId: {})", groupId);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        return Optional.ofNullable(factory.select(groupFirm).from(groupFirm).where(groupFirm.id.eq(groupId)).fetchOne());
    }

}

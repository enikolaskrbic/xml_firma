package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.tuple.*;


public interface BusinessYearRepositoryCustom {

    List<BusinessYear> findByYear(String year);

    List<BusinessYear> findByIsFinished(Boolean isFinished);

    List<BusinessYear> findByFirmAndIsFinished(Long firmId,Boolean isFinished);

    List<BusinessYear> findByFirmAndIsDeleted(Long firmId,Boolean isDeleted);

    List<BusinessYear> findByFirm(Long firmId);

    List<BusinessYearFirmTuple> businessYears(Long firmId);

    List<BusinessYear> findByIsDeleted(Boolean isDeleted);

    List<BusinessYearFirmTuple> activeBusinessYear();

    List<BusinessYearFirmTuple> activeBusinessYearByFirm(Long firmId);

    List<BusinessYearFirmTuple> businessYearsSearch(Optional<Long> businessYearId, Optional<String> businessYearYear, Optional<Boolean> businessYearIsFinished);

    Optional<BusinessYear> deleteBusinessYearHard(Long id);

}

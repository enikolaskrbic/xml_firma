package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.Document;


public interface DocumentRepository extends JpaRepository<Document, Long>, DocumentRepositoryCustom {

}

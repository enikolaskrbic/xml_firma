package com.poslovna.backend.repository;

import java.math.BigDecimal;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.tuple.*;


public interface DocumentItemRepositoryCustom {

    List<DocumentItem> findByDocument(Long documentId);

    List<DocumentItem> findByQuantity(BigDecimal quantity);

    List<DocumentItem> findByType(TypeDocument type);

    List<DocumentItem> findByPrice(BigDecimal price);

    List<DocumentItem> findByArticle(Long articleId);

    List<DocumentItem> findByArticleAndDocument(Long articleId,Long documentId);

    List<DocumentItem> findByIsDeleted(Boolean isDeleted);

    List<DocumentItemArticleDocumentUnitOfMeasureTuple> documentItems(Long documentId);

    List<DocumentItemArticleDocumentUnitOfMeasureTuple> documentItemsSearch(Optional<Long> documentItemId, Optional<TypeDocument> documentItemType, Optional<Long> documentId,
            Optional<String> articleName, Optional<Long> articleUnitOfMeasureId);

    List<DocumentItemArticleDocumentTuple> documentDocumentItem(Long documentId);

    List<DocumentItemArticleDocumentTuple> findDocumentDocumentItem(Long documentId, Integer drop, Integer take);

    Long countDocumentDocumentItem(Long documentId);

    List<DocumentItemArticleDocumentTuple> articleDocumentItem(Long articleId);

    List<DocumentItemArticleDocumentTuple> findArticleDocumentItem(Long articleId, Integer drop, Integer take);

    Long countArticleDocumentItem(Long articleId);

    Optional<DocumentItem> deleteDocumentItemHard(Long id);

}

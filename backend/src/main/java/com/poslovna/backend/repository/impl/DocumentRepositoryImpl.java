package com.poslovna.backend.repository.impl;

import java.math.BigDecimal;
import java.time.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.DocumentRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class DocumentRepositoryImpl implements DocumentRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(DocumentRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<Document> findByType(TypeDocument type) {
        log.trace(".findByType(type: {})", type);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.type.eq(type)).fetch();
    }

    @Override
    public List<Document> findByDateOfDelivery(Optional<ZonedDateTime> dateOfDelivery) {
        log.trace(".findByDateOfDelivery(dateOfDelivery: {})", dateOfDelivery);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(dateOfDelivery.isPresent() ? document.dateOfDelivery.eq(dateOfDelivery.get()) : null).fetch();
    }

    @Override
    public List<Document> findByDateOfDeliveryMandatory(ZonedDateTime dateOfDelivery) {
        log.trace(".findByDateOfDeliveryMandatory(dateOfDelivery: {})", dateOfDelivery);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.dateOfDelivery.eq(dateOfDelivery)).fetch();
    }

    @Override
    public List<Document> findByDateOfValue(Optional<ZonedDateTime> dateOfValue) {
        log.trace(".findByDateOfValue(dateOfValue: {})", dateOfValue);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(dateOfValue.isPresent() ? document.dateOfValue.eq(dateOfValue.get()) : null).fetch();
    }

    @Override
    public List<Document> findByDateOfValueMandatory(ZonedDateTime dateOfValue) {
        log.trace(".findByDateOfValueMandatory(dateOfValue: {})", dateOfValue);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.dateOfValue.eq(dateOfValue)).fetch();
    }

    @Override
    public List<Document> findByDateOfStatement(Optional<ZonedDateTime> dateOfStatement) {
        log.trace(".findByDateOfStatement(dateOfStatement: {})", dateOfStatement);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(dateOfStatement.isPresent() ? document.dateOfStatement.eq(dateOfStatement.get()) : null).fetch();
    }

    @Override
    public List<Document> findByDateOfStatementMandatory(ZonedDateTime dateOfStatement) {
        log.trace(".findByDateOfStatementMandatory(dateOfStatement: {})", dateOfStatement);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.dateOfStatement.eq(dateOfStatement)).fetch();
    }

    @Override
    public List<Document> findByAmount(Optional<BigDecimal> amount) {
        log.trace(".findByAmount(amount: {})", amount);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(amount.isPresent() ? document.amount.eq(amount.get()) : null).fetch();
    }

    @Override
    public List<Document> findByAmountMandatory(BigDecimal amount) {
        log.trace(".findByAmountMandatory(amount: {})", amount);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.amount.eq(amount)).fetch();
    }

    @Override
    public List<Document> findByBusinessYear(Long businessYearId) {
        log.trace(".findByBusinessYear(businessYearId: {})", businessYearId);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.businessYear.id.eq(businessYearId)).fetch();
    }

    @Override
    public List<Document> findByBusinessPartner(Optional<Long> businessPartnerId) {
        log.trace(".findByBusinessPartner(businessPartnerId: {})", businessPartnerId);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(businessPartnerId.isPresent() ? document.businessPartner.id.eq(businessPartnerId.get()) : null).fetch();
    }

    @Override
    public List<Document> findByBusinessPartnerMandatory(Long businessPartnerId) {
        log.trace(".findByBusinessPartnerMandatory(businessPartnerId: {})", businessPartnerId);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.businessPartner.id.eq(businessPartnerId)).fetch();
    }

    @Override
    public List<Document> findByWarehouse(Long warehouseId) {
        log.trace(".findByWarehouse(warehouseId: {})", warehouseId);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.warehouse.id.eq(warehouseId)).fetch();
    }

    @Override
    public List<Document> findByWarehouseOut(Optional<Long> warehouseOutId) {
        log.trace(".findByWarehouseOut(warehouseOutId: {})", warehouseOutId);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(warehouseOutId.isPresent() ? document.warehouseOut.id.eq(warehouseOutId.get()) : null).fetch();
    }

    @Override
    public List<Document> findByWarehouseOutMandatory(Long warehouseOutId) {
        log.trace(".findByWarehouseOutMandatory(warehouseOutId: {})", warehouseOutId);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.warehouseOut.id.eq(warehouseOutId)).fetch();
    }

    @Override
    public List<Document> findByIsReversed(Boolean isReversed) {
        log.trace(".findByIsReversed(isReversed: {})", isReversed);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.isReversed.eq(isReversed)).fetch();
    }

    @Override
    public List<Document> findByIsBooked(Boolean isBooked) {
        log.trace(".findByIsBooked(isBooked: {})", isBooked);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.isBooked.eq(isBooked)).fetch();
    }

    @Override
    public List<Document> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QDocument document = QDocument.document;
        return factory.select(document).from(document).where(document.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<DocumentBusinessPartnerWarehouseBusinessYearTuple> documentsSearch(Optional<Long> documentId, Optional<ZonedDateTime> documentDateOfValue,
            Optional<ZonedDateTime> documentDateOfDelivery, Optional<Long> warehuseId, Optional<String> warehouseName, Optional<Long> businessPartnerId, Optional<String> bussinesPartnerName,
            Optional<Long> businessYearId, Optional<String> businessYearYear) {
        log.trace(
                ".documentsSearch(documentId: {}, documentDateOfValue: {}, documentDateOfDelivery: {}, warehuseId: {}, warehouseName: {}, businessPartnerId: {}, bussinesPartnerName: {}, businessYearId: {}, businessYearYear: {})",
                documentId, documentDateOfValue, documentDateOfDelivery, warehuseId, warehouseName, businessPartnerId, bussinesPartnerName, businessYearId, businessYearYear);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QDocument document = QDocument.document;
        final QWarehouse warehouseOut = QWarehouse.warehouse;
        return factory.select(businessPartner, warehouse, businessYear, document).from(document).innerJoin(document.businessPartner, businessPartner).innerJoin(document.warehouse, warehouse)
                .innerJoin(document.businessYear, businessYear)
                .where(new BooleanBuilder().and(documentId.isPresent() ? document.id.eq(documentId.get()) : null)
                        .or(documentDateOfValue.isPresent() ? document.dateOfValue.after(documentDateOfValue.get()) : null)
                        .or(documentDateOfDelivery.isPresent() ? document.dateOfValue.before(documentDateOfDelivery.get()) : null)
                        .or(warehuseId.isPresent() ? warehouse.id.eq(warehuseId.get()) : null)
                        .or(warehouseName.isPresent() ? warehouse.name.like("%" + warehouseName.get() + "%") : null)
                        .or(businessPartnerId.isPresent() ? businessPartner.id.eq(businessPartnerId.get()) : null)
                        .or(bussinesPartnerName.isPresent() ? businessPartner.name.like("%" + bussinesPartnerName.get() + "%") : null)
                        .or(businessYearId.isPresent() ? businessYear.id.eq(businessYearId.get()) : null)
                        .or(businessYearYear.isPresent() ? businessYear.year.eq(businessYearYear.get()) : null))
                .fetch().stream().map(t -> new DocumentBusinessPartnerWarehouseBusinessYearTuple(t.get(document), t.get(businessPartner), t.get(warehouse), t.get(businessYear)))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Document> deleteDocumentHard(Long id) {
        log.trace(".deleteDocumentHard(id: {})", id);
        final QDocument document = QDocument.document;
        return Optional.ofNullable(factory.select(document).from(document).where(document.id.eq(id)).fetchOne());
    }

}

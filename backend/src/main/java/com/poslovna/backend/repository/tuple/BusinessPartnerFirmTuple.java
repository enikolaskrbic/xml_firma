
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class BusinessPartnerFirmTuple {

    private final BusinessPartner businessPartner;
    private final Firm firm;

    public BusinessPartnerFirmTuple(BusinessPartner businessPartner, Firm firm) {
        this.businessPartner = businessPartner;
        this.firm = firm;
    }

    public BusinessPartner getBusinessPartner() {
        return businessPartner;
    }

    public Firm getFirm() {
        return firm;
    }

}
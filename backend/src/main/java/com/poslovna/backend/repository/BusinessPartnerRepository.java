package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.BusinessPartner;


public interface BusinessPartnerRepository extends JpaRepository<BusinessPartner, Long>, BusinessPartnerRepositoryCustom {

}

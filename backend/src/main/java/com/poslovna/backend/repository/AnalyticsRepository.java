package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.Analytics;


public interface AnalyticsRepository extends JpaRepository<Analytics, Long>, AnalyticsRepositoryCustom {

}

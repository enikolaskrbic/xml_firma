package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.BusinessYear;


public interface BusinessYearRepository extends JpaRepository<BusinessYear, Long>, BusinessYearRepositoryCustom {

}

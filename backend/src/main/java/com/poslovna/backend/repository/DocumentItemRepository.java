package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.DocumentItem;


public interface DocumentItemRepository extends JpaRepository<DocumentItem, Long>, DocumentItemRepositoryCustom {

}

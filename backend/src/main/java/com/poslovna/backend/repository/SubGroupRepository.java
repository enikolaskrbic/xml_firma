package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.SubGroup;


public interface SubGroupRepository extends JpaRepository<SubGroup, Long>, SubGroupRepositoryCustom {

}

package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.ArticleRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class ArticleRepositoryImpl implements ArticleRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(ArticleRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<Article> findByName(String name) {
        log.trace(".findByName(name: {})", name);
        final QArticle article = QArticle.article;
        return factory.select(article).from(article).where(article.name.eq(name)).fetch();
    }

    @Override
    public List<Article> findByCode(String code) {
        log.trace(".findByCode(code: {})", code);
        final QArticle article = QArticle.article;
        return factory.select(article).from(article).where(article.code.eq(code)).fetch();
    }

    @Override
    public List<Article> findByStatus(RoleArticle status) {
        log.trace(".findByStatus(status: {})", status);
        final QArticle article = QArticle.article;
        return factory.select(article).from(article).where(article.status.eq(status)).fetch();
    }

    @Override
    public List<Article> findBySubGroup(Long subGroupId) {
        log.trace(".findBySubGroup(subGroupId: {})", subGroupId);
        final QArticle article = QArticle.article;
        return factory.select(article).from(article).where(article.subGroup.id.eq(subGroupId)).fetch();
    }

    @Override
    public List<Article> findByUnitOfMeasure(Long unitOfMeasureId) {
        log.trace(".findByUnitOfMeasure(unitOfMeasureId: {})", unitOfMeasureId);
        final QArticle article = QArticle.article;
        return factory.select(article).from(article).where(article.unitOfMeasure.id.eq(unitOfMeasureId)).fetch();
    }

    @Override
    public List<Article> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QArticle article = QArticle.article;
        return factory.select(article).from(article).where(article.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<ArticleUnitOfMeasureSubGroupTuple> articles(Long firmId) {
        log.trace(".articles()");
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        final QArticle article = QArticle.article;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(unitOfMeasure, article, subGroup).from(article).innerJoin(article.unitOfMeasure, unitOfMeasure).innerJoin(article.subGroup, subGroup).where(subGroup.group.firm.id.eq(firmId)).fetch().stream()
                .map(t -> new ArticleUnitOfMeasureSubGroupTuple(t.get(article), t.get(unitOfMeasure), t.get(subGroup))).collect(Collectors.toList());
    }

    @Override
    public List<ArticleUnitOfMeasureSubGroupTuple> searchArticles(Optional<Long> articleId, Optional<String> ArticleName, Optional<String> articleCode, Optional<RoleArticle> articleStatus,
            Optional<Long> subGroupId, Optional<String> subGroupName, Optional<Long> unitOfMeasureId, Optional<String> unitOfMeasureName, Optional<String> unitOfMeasureShortName) {
        log.trace(
                ".searchArticles(articleId: {}, ArticleName: {}, articleCode: {}, articleStatus: {}, subGroupId: {}, subGroupName: {}, unitOfMeasureId: {}, unitOfMeasureName: {}, unitOfMeasureShortName: {})",
                articleId, ArticleName, articleCode, articleStatus, subGroupId, subGroupName, unitOfMeasureId, unitOfMeasureName, unitOfMeasureShortName);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        final QArticle article = QArticle.article;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(unitOfMeasure, article, subGroup).from(article).innerJoin(article.unitOfMeasure, unitOfMeasure).innerJoin(article.subGroup, subGroup)
                .where(new BooleanBuilder().and(articleId.isPresent() ? article.id.eq(articleId.get()) : null).or(ArticleName.isPresent() ? article.name.like("%" + ArticleName.get() + "%") : null)
                        .or(articleCode.isPresent() ? article.code.like("%" + articleCode.get() + "%") : null).or(articleStatus.isPresent() ? article.status.eq(articleStatus.get()) : null)
                        .or(subGroupId.isPresent() ? subGroup.id.eq(subGroupId.get()) : null).or(subGroupName.isPresent() ? subGroup.name.like("%" + subGroupName.get() + "%") : null)
                        .or(unitOfMeasureId.isPresent() ? unitOfMeasure.id.eq(unitOfMeasureId.get()) : null).or(unitOfMeasureName.isPresent() ? unitOfMeasure.name.like("%" + unitOfMeasureName.get() + "%") : null)
                        .or(unitOfMeasureShortName.isPresent() ? unitOfMeasure.shortName.like("%" + unitOfMeasureShortName.get() + "%" ) : null))
                .fetch().stream().map(t -> new ArticleUnitOfMeasureSubGroupTuple(t.get(article), t.get(unitOfMeasure), t.get(subGroup))).collect(Collectors.toList());
    }

    @Override
    public List<ArticleUnitOfMeasureSubGroupTuple> subGroupArticle(Long subGroupId) {
        log.trace(".subGroupArticle(subGroupId: {})", subGroupId);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        final QArticle article = QArticle.article;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(unitOfMeasure, article, subGroup).from(article).innerJoin(article.unitOfMeasure, unitOfMeasure).innerJoin(article.subGroup, subGroup).where(subGroup.id.eq(subGroupId))
                .fetch().stream().map(t -> new ArticleUnitOfMeasureSubGroupTuple(t.get(article), t.get(unitOfMeasure), t.get(subGroup))).collect(Collectors.toList());
    }

    @Override
    public List<ArticleUnitOfMeasureSubGroupTuple> findSubGroupArticle(Long subGroupId, Integer drop, Integer take) {
        log.trace(".findSubGroupArticle(subGroupId: {}, drop: {}, take: {})", subGroupId, drop, take);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        final QArticle article = QArticle.article;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(unitOfMeasure, article, subGroup).from(article).innerJoin(article.unitOfMeasure, unitOfMeasure).innerJoin(article.subGroup, subGroup).where(subGroup.id.eq(subGroupId))
                .offset(drop).limit(take).fetch().stream().map(t -> new ArticleUnitOfMeasureSubGroupTuple(t.get(article), t.get(unitOfMeasure), t.get(subGroup))).collect(Collectors.toList());
    }

    @Override
    public Long countSubGroupArticle(Long subGroupId) {
        log.trace(".countSubGroupArticle(subGroupId: {})", subGroupId);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        final QArticle article = QArticle.article;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(unitOfMeasure, article, subGroup).from(article).innerJoin(article.unitOfMeasure, unitOfMeasure).innerJoin(article.subGroup, subGroup).where(subGroup.id.eq(subGroupId))
                .fetchCount();
    }

    @Override
    public Optional<Article> deleteArticleHard(Long id) {
        log.trace(".deleteArticleHard(id: {})", id);
        final QArticle article = QArticle.article;
        return Optional.ofNullable(factory.select(article).from(article).where(article.id.eq(id)).fetchOne());
    }

}

package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.Article;


public interface ArticleRepository extends JpaRepository<Article, Long>, ArticleRepositoryCustom {

}

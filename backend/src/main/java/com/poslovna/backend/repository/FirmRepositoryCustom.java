package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;


public interface FirmRepositoryCustom {

    List<Firm> findByName(String name);

    List<Firm> findByAddress(String address);

    List<Firm> findByCity(String city);

    List<Firm> findByCountry(String country);

    List<Firm> findByEmail(Optional<String> email);

    List<Firm> findByEmailMandatory(String email);

    List<Firm> findByPhone(Optional<String> phone);

    List<Firm> findByPhoneMandatory(String phone);

    List<Firm> findByWeb(Optional<String> web);

    List<Firm> findByWebMandatory(String web);

    List<Firm> findByPib(String pib);

    List<Firm> findByIsDeleted(Boolean isDeleted);

    List<Firm> firms();

}

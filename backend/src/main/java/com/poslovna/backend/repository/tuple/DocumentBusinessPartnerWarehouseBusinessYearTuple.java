
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class DocumentBusinessPartnerWarehouseBusinessYearTuple {

    private final Document document;
    private final BusinessPartner businessPartner;
    private final Warehouse warehouse;
    private final BusinessYear businessYear;

    public DocumentBusinessPartnerWarehouseBusinessYearTuple(Document document, BusinessPartner businessPartner, Warehouse warehouse, BusinessYear businessYear) {
        this.document = document;
        this.businessPartner = businessPartner;
        this.warehouse = warehouse;
        this.businessYear = businessYear;
    }

    public Document getDocument() {
        return document;
    }

    public BusinessPartner getBusinessPartner() {
        return businessPartner;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public BusinessYear getBusinessYear() {
        return businessYear;
    }

}
package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.User;


public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

}

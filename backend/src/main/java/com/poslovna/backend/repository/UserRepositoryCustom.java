package com.poslovna.backend.repository;

import java.time.*;
import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;


public interface UserRepositoryCustom {

    List<User> findByFirstName(String firstName);

    List<User> findByLastName(String lastName);

    List<User> findByIsDeleted(Boolean isDeleted);

    List<User> findByTimetamp(ZonedDateTime timetamp);

    List<User> findByFirm(Optional<Long> firmId);

    List<User> findByFirmMandatory(Long firmId);

    List<User> findByRole(Role role);

    Optional<User> findByEmail(String email);

    List<User> findByPasswordHash(String passwordHash);

    List<User> findByEmailVerificationCode(Optional<String> emailVerificationCode);

    Optional<User> findByEmailVerificationCodeMandatory(String emailVerificationCode);

    List<User> findByEmailVerificationCodeTimestamp(Optional<ZonedDateTime> emailVerificationCodeTimestamp);

    List<User> findByEmailVerificationCodeTimestampMandatory(ZonedDateTime emailVerificationCodeTimestamp);

    List<User> findByEmailVerified(Boolean emailVerified);

    List<User> findByResetPasswordCode(Optional<String> resetPasswordCode);

    Optional<User> findByResetPasswordCodeMandatory(String resetPasswordCode);

    List<User> findByResetPasswordCodeTimestamp(Optional<ZonedDateTime> resetPasswordCodeTimestamp);

    List<User> findByResetPasswordCodeTimestampMandatory(ZonedDateTime resetPasswordCodeTimestamp);

    List<User> users();

}

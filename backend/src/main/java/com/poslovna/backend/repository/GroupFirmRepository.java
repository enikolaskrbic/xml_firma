package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.GroupFirm;


public interface GroupFirmRepository extends JpaRepository<GroupFirm, Long>, GroupFirmRepositoryCustom {

}

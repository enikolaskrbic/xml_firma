package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.Firm;


public interface FirmRepository extends JpaRepository<Firm, Long>, FirmRepositoryCustom {

}

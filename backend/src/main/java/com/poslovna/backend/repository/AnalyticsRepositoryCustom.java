package com.poslovna.backend.repository;

import java.math.BigDecimal;
import java.time.*;
import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.tuple.*;


public interface AnalyticsRepositoryCustom {

    List<Analytics> findByDate(ZonedDateTime date);

    List<Analytics> findByType(TypeAnalytics type);

    List<Analytics> findByDirection(TypeDirection direction);

    List<Analytics> findByQuantity(BigDecimal quantity);

    List<Analytics> findByBalance(BigDecimal balance);

    List<Analytics> findByPrice(BigDecimal price);

    List<Analytics> findByValue(BigDecimal value);

    List<Analytics> findByTotalValue(BigDecimal totalValue);

    List<Analytics> findByDocumentItem(Optional<Long> documentItemId);

    List<Analytics> findByDocumentItemMandatory(Long documentItemId);

    List<Analytics> findByWarehouseCard(Long warehouseCardId);

    List<Analytics> findByIsDeleted(Boolean isDeleted);

    List<AnalyticsWarehouseCardArticleTuple> analytics(Long warehouseCardId);

    List<AnalyticsWarehouseCardArticleTuple> analyticsSearch(Long warehouseCardId, Optional<Long> analyticsId, Optional<TypeAnalytics> analyticsType, Optional<BigDecimal> analyticsQuantity,
            Optional<BigDecimal> analyticsBalance, Optional<BigDecimal> analyticsPrice, Optional<BigDecimal> analyticsValue, Optional<BigDecimal> analyticsTotalValue);

}

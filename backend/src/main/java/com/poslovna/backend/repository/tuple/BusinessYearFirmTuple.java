
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class BusinessYearFirmTuple {

    private final BusinessYear businessYear;
    private final Firm firm;

    public BusinessYearFirmTuple(BusinessYear businessYear, Firm firm) {
        this.businessYear = businessYear;
        this.firm = firm;
    }

    public BusinessYear getBusinessYear() {
        return businessYear;
    }

    public Firm getFirm() {
        return firm;
    }

}
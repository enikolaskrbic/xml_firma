
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class ArticleUnitOfMeasureSubGroupTuple {

    private final Article article;
    private final UnitOfMeasure unitOfMeasure;
    private final SubGroup subGroup;

    public ArticleUnitOfMeasureSubGroupTuple(Article article, UnitOfMeasure unitOfMeasure, SubGroup subGroup) {
        this.article = article;
        this.unitOfMeasure = unitOfMeasure;
        this.subGroup = subGroup;
    }

    public Article getArticle() {
        return article;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public SubGroup getSubGroup() {
        return subGroup;
    }

}
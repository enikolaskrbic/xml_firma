package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.Warehouse;


public interface WarehouseRepository extends JpaRepository<Warehouse, Long>, WarehouseRepositoryCustom {

}

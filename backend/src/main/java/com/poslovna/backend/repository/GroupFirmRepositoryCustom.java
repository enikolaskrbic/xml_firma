package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.tuple.*;


public interface GroupFirmRepositoryCustom {

    List<GroupFirm> findByName(String name);

    List<GroupFirm> findByFirm(Long firmId);

    List<GroupFirmFirmTuple> groups(Long groupFirmId);

    List<GroupFirm> findByIsDeleted(Boolean isDeleted);

    List<GroupFirmFirmTuple> searchGroups(Optional<Long> groupFirmId, Optional<String> groupFirmName);

    Optional<GroupFirm> deleteGroupHard(Long groupId);

}

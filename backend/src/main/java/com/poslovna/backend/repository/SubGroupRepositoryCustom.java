package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.tuple.*;


public interface SubGroupRepositoryCustom {

    List<SubGroup> findByName(String name);

    List<SubGroup> findByGroup(Long groupId);

    List<SubGroupGroupFirmTuple> subgroups(Long subGroupId);

    List<SubGroup> findByIsDeleted(Boolean isDeleted);

    List<SubGroupGroupFirmTuple> searchSubGroups(Optional<Long> subGroupId, Optional<Long> subGroupGroupId, Optional<String> subGroupName, Optional<String> groupFirmName);

    Optional<SubGroup> deleteSubGroupHard(Long id);

    List<SubGroupGroupFirmTuple> groupSubGroup(Long groupFirmId);

    List<SubGroupGroupFirmTuple> findGroupSubGroup(Long groupFirmId, Integer drop, Integer take);

    Long countGroupSubGroup(Long groupFirmId);

}

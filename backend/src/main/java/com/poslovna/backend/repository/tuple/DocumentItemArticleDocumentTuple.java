
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class DocumentItemArticleDocumentTuple {

    private final DocumentItem documentItem;
    private final Article article;
    private final Document document;

    public DocumentItemArticleDocumentTuple(DocumentItem documentItem, Article article, Document document) {
        this.documentItem = documentItem;
        this.article = article;
        this.document = document;
    }

    public DocumentItem getDocumentItem() {
        return documentItem;
    }

    public Article getArticle() {
        return article;
    }

    public Document getDocument() {
        return document;
    }

}
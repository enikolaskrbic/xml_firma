
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class WarehouseFirmTuple {

    private final Warehouse warehouse;
    private final Firm firm;

    public WarehouseFirmTuple(Warehouse warehouse, Firm firm) {
        this.warehouse = warehouse;
        this.firm = firm;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public Firm getFirm() {
        return firm;
    }

}
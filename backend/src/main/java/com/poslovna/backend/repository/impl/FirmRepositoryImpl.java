package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.FirmRepositoryCustom;

import com.querydsl.jpa.JPQLQueryFactory;


public class FirmRepositoryImpl implements FirmRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(FirmRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<Firm> findByName(String name) {
        log.trace(".findByName(name: {})", name);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.name.eq(name)).fetch();
    }

    @Override
    public List<Firm> findByAddress(String address) {
        log.trace(".findByAddress(address: {})", address);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.address.eq(address)).fetch();
    }

    @Override
    public List<Firm> findByCity(String city) {
        log.trace(".findByCity(city: {})", city);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.city.eq(city)).fetch();
    }

    @Override
    public List<Firm> findByCountry(String country) {
        log.trace(".findByCountry(country: {})", country);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.country.eq(country)).fetch();
    }

    @Override
    public List<Firm> findByEmail(Optional<String> email) {
        log.trace(".findByEmail(email: {})", email);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(email.isPresent() ? firm.email.eq(email.get()) : null).fetch();
    }

    @Override
    public List<Firm> findByEmailMandatory(String email) {
        log.trace(".findByEmailMandatory(email: {})", email);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.email.eq(email)).fetch();
    }

    @Override
    public List<Firm> findByPhone(Optional<String> phone) {
        log.trace(".findByPhone(phone: {})", phone);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(phone.isPresent() ? firm.phone.eq(phone.get()) : null).fetch();
    }

    @Override
    public List<Firm> findByPhoneMandatory(String phone) {
        log.trace(".findByPhoneMandatory(phone: {})", phone);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.phone.eq(phone)).fetch();
    }

    @Override
    public List<Firm> findByWeb(Optional<String> web) {
        log.trace(".findByWeb(web: {})", web);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(web.isPresent() ? firm.web.eq(web.get()) : null).fetch();
    }

    @Override
    public List<Firm> findByWebMandatory(String web) {
        log.trace(".findByWebMandatory(web: {})", web);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.web.eq(web)).fetch();
    }

    @Override
    public List<Firm> findByPib(String pib) {
        log.trace(".findByPib(pib: {})", pib);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.pib.eq(pib)).fetch();
    }

    @Override
    public List<Firm> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).where(firm.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<Firm> firms() {
        log.trace(".firms()");
        final QFirm firm = QFirm.firm;
        return factory.select(firm).from(firm).fetch();
    }

}

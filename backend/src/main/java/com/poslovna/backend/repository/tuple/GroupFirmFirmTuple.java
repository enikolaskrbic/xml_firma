
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class GroupFirmFirmTuple {

    private final GroupFirm groupFirm;
    private final Firm firm;

    public GroupFirmFirmTuple(GroupFirm groupFirm, Firm firm) {
        this.groupFirm = groupFirm;
        this.firm = firm;
    }

    public GroupFirm getGroupFirm() {
        return groupFirm;
    }

    public Firm getFirm() {
        return firm;
    }

}
package com.poslovna.backend.repository.impl;

import java.math.BigDecimal;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.WarehouseCardRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class WarehouseCardRepositoryImpl implements WarehouseCardRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(WarehouseCardRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<WarehouseCard> findByStartState(BigDecimal startState) {
        log.trace(".findByStartState(startState: {})", startState);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.startState.eq(startState)).fetch();
    }

    @Override
    public List<WarehouseCard> findByInput(BigDecimal input) {
        log.trace(".findByInput(input: {})", input);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.input.eq(input)).fetch();
    }

    @Override
    public List<WarehouseCard> findByOutput(BigDecimal output) {
        log.trace(".findByOutput(output: {})", output);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.output.eq(output)).fetch();
    }

    @Override
    public List<WarehouseCard> findByBalance(BigDecimal balance) {
        log.trace(".findByBalance(balance: {})", balance);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.balance.eq(balance)).fetch();
    }

    @Override
    public List<WarehouseCard> findByInValue(BigDecimal inValue) {
        log.trace(".findByInValue(inValue: {})", inValue);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.inValue.eq(inValue)).fetch();
    }

    @Override
    public List<WarehouseCard> findByOutValue(BigDecimal outValue) {
        log.trace(".findByOutValue(outValue: {})", outValue);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.outValue.eq(outValue)).fetch();
    }

    @Override
    public List<WarehouseCard> findByTotalValue(BigDecimal totalValue) {
        log.trace(".findByTotalValue(totalValue: {})", totalValue);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.totalValue.eq(totalValue)).fetch();
    }

    @Override
    public List<WarehouseCard> findByWarehouse(Long warehouseId) {
        log.trace(".findByWarehouse(warehouseId: {})", warehouseId);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.warehouse.id.eq(warehouseId)).fetch();
    }

    @Override
    public List<WarehouseCard> findByWarehouseAndBusinessYear(Long warehouseId) {
        log.trace(".findByWarehouse(warehouseId: {})", warehouseId);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.warehouse.id.eq(warehouseId).and(warehouseCard.businessYear.isFinished.eq(false))).fetch();
    }

    @Override
    public List<WarehouseCard> findActiveByWarehouseAndArticle(Long warehouseId, Long articleId){
        log.trace(".findctiveByWarehouse(warehouseId: {})", warehouseId);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.warehouse.id.eq(warehouseId).and(warehouseCard.article.id.eq(articleId)).and(warehouseCard.isDeleted.eq(false))).fetch();
    }

    @Override
    public List<WarehouseCard> findByBusinessYear(Long businessYearId) {
        log.trace(".findByBusinessYear(businessYearId: {})", businessYearId);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.businessYear.id.eq(businessYearId)).fetch();
    }

    @Override
    public List<WarehouseCard> findByArticle(Long articleId) {
        log.trace(".findByArticle(articleId: {})", articleId);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.article.id.eq(articleId)).fetch();
    }

    @Override
    public List<WarehouseCard> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<WarehouseCard> findByAveragePrice(BigDecimal averagePrice) {
        log.trace(".findByAveragePrice(averagePrice: {})", averagePrice);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.averagePrice.eq(averagePrice)).fetch();
    }

    @Override
    public List<WarehouseCard> findByLastPrice(Optional<BigDecimal> lastPrice) {
        log.trace(".findByLastPrice(lastPrice: {})", lastPrice);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(lastPrice.isPresent() ? warehouseCard.lastPrice.eq(lastPrice.get()) : null).fetch();
    }

    @Override
    public List<WarehouseCard> findByLastPriceMandatory(BigDecimal lastPrice) {
        log.trace(".findByLastPriceMandatory(lastPrice: {})", lastPrice);
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouseCard).from(warehouseCard).where(warehouseCard.lastPrice.eq(lastPrice)).fetch();
    }

    @Override
    public List<WarehouseCardArticleWarehouseBusinessYearTuple> warehousesCard() {
        return null;
    }

    @Override
    public List<WarehouseCardArticleWarehouseBusinessYearTuple> warehousesCard(Long firmId) {
        log.trace(".warehousesCard()");
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear).where(warehouse.firm.id.eq(firmId)).orderBy(warehouseCard.businessYear.year.asc()).fetch().stream()
                .map(t -> new WarehouseCardArticleWarehouseBusinessYearTuple(t.get(warehouseCard), t.get(article), t.get(warehouse), t.get(businessYear))).collect(Collectors.toList());

    }

    @Override
    public List<WarehouseCardArticleWarehouseBusinessYearTuple> warehousesCardSearch(Optional<Long> warehouseCardId, Optional<Long> warehouseCardBusinessYearId, Optional<String> businessYearYear,
            Optional<Long> articleId, Optional<String> articleName, Optional<String> articleCode, Optional<BigDecimal> warehouseCardInput, Optional<BigDecimal> warehouseCardOutput,
            Optional<BigDecimal> warehouseCardBalance, Optional<BigDecimal> warehouseCardInValue, Optional<BigDecimal> warehouseCardOutValue, Optional<BigDecimal> warehouseCardTotalValue,
            Optional<Long> warehouseId, Optional<String> warehouseName) {
        log.trace(
                ".warehousesCardSearch(warehouseCardId: {}, warehouseCardBusinessYearId: {}, businessYearYear: {}, articleId: {}, articleName: {}, articleCode: {}, warehouseCardInput: {}, warehouseCardOutput: {}, warehouseCardBalance: {}, warehouseCardInValue: {}, warehouseCardOutValue: {}, warehouseCardTotalValue: {}, warehouseId: {}, warehouseName: {})",
                warehouseCardId, warehouseCardBusinessYearId, businessYearYear, articleId, articleName, articleCode, warehouseCardInput, warehouseCardOutput, warehouseCardBalance,
                warehouseCardInValue, warehouseCardOutValue, warehouseCardTotalValue, warehouseId, warehouseName);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear)
                .where(new BooleanBuilder().and(warehouseCardId.isPresent() ? warehouseCard.id.eq(warehouseCardId.get()) : null)
                        .or(warehouseCardBusinessYearId.isPresent() ? warehouseCard.businessYear.id.eq(warehouseCardBusinessYearId.get()) : null)
                        .or(businessYearYear.isPresent() ? businessYear.year.like("%" + businessYearYear.get() + "%" ) : null)
                        .or(articleId.isPresent() ? article.id.eq(articleId.get()) : null)
                        .or(articleName.isPresent() ? article.name.like("%" + articleName.get() + "%")   : null)
                        .or(articleCode.isPresent() ? article.code.like("%" + articleCode.get() + "%") : null)
                        .or(warehouseCardInput.isPresent() ? warehouseCard.input.eq(warehouseCardInput.get()) : null)
                        .or(warehouseCardOutput.isPresent() ? warehouseCard.output.eq(warehouseCardOutput.get()) : null)
                        .or(warehouseCardBalance.isPresent() ? warehouseCard.balance.eq(warehouseCardBalance.get()) : null)
                        .or(warehouseCardInValue.isPresent() ? warehouseCard.inValue.eq(warehouseCardInValue.get()) : null)
                        .or(warehouseCardOutValue.isPresent() ? warehouseCard.outValue.eq(warehouseCardOutValue.get()) : null)
                        .or(warehouseCardTotalValue.isPresent() ? warehouseCard.totalValue.eq(warehouseCardTotalValue.get()) : null)
                        .or(warehouseId.isPresent() ? warehouse.id.eq(warehouseId.get()) : null).or(warehouseName.isPresent() ? warehouse.name.like("%" + warehouseName.get()+ "%") : null))
                .fetch().stream().map(t -> new WarehouseCardArticleWarehouseBusinessYearTuple(t.get(warehouseCard), t.get(article), t.get(warehouse), t.get(businessYear)))
                .collect(Collectors.toList());
    }

    @Override
    public List<WarehouseCardArticleWarehouseBusinessYearTuple> warehouseWarehouseCard(Long warehouseId) {
        log.trace(".warehouseWarehouseCard(warehouseId: {})", warehouseId);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear).where(warehouse.id.eq(warehouseId)).fetch().stream()
                .map(t -> new WarehouseCardArticleWarehouseBusinessYearTuple(t.get(warehouseCard), t.get(article), t.get(warehouse), t.get(businessYear))).collect(Collectors.toList());
    }

    @Override
    public List<WarehouseCardArticleWarehouseBusinessYearTuple> findWarehouseWarehouseCard(Long warehouseId, Integer drop, Integer take) {
        log.trace(".findWarehouseWarehouseCard(warehouseId: {}, drop: {}, take: {})", warehouseId, drop, take);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear).where(warehouse.id.eq(warehouseId)).offset(drop).limit(take).fetch().stream()
                .map(t -> new WarehouseCardArticleWarehouseBusinessYearTuple(t.get(warehouseCard), t.get(article), t.get(warehouse), t.get(businessYear))).collect(Collectors.toList());
    }

    @Override
    public Long countWarehouseWarehouseCard(Long warehouseId) {
        log.trace(".countWarehouseWarehouseCard(warehouseId: {})", warehouseId);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear).where(warehouse.id.eq(warehouseId)).fetchCount();
    }

    @Override
    public List<WarehouseCardArticleWarehouseBusinessYearTuple> articleWarehouseCard(Long articleId) {
        log.trace(".articleWarehouseCard(articleId: {})", articleId);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear).where(article.id.eq(articleId)).fetch().stream()
                .map(t -> new WarehouseCardArticleWarehouseBusinessYearTuple(t.get(warehouseCard), t.get(article), t.get(warehouse), t.get(businessYear))).collect(Collectors.toList());
    }

    @Override
    public List<WarehouseCardArticleWarehouseBusinessYearTuple> findArticleWarehouseCard(Long articleId, Integer drop, Integer take) {
        log.trace(".findArticleWarehouseCard(articleId: {}, drop: {}, take: {})", articleId, drop, take);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear).where(article.id.eq(articleId)).offset(drop).limit(take).fetch().stream()
                .map(t -> new WarehouseCardArticleWarehouseBusinessYearTuple(t.get(warehouseCard), t.get(article), t.get(warehouse), t.get(businessYear))).collect(Collectors.toList());
    }

    @Override
    public Long countArticleWarehouseCard(Long articleId) {
        log.trace(".countArticleWarehouseCard(articleId: {})", articleId);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QBusinessYear businessYear = QBusinessYear.businessYear;
        final QArticle article = QArticle.article;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(warehouse, businessYear, article, warehouseCard).from(warehouseCard).innerJoin(warehouseCard.article, article).innerJoin(warehouseCard.warehouse, warehouse)
                .innerJoin(warehouseCard.businessYear, businessYear).where(article.id.eq(articleId)).fetchCount();
    }

}

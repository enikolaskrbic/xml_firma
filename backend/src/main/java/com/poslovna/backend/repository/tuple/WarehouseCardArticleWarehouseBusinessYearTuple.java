
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class WarehouseCardArticleWarehouseBusinessYearTuple {

    private final WarehouseCard warehouseCard;
    private final Article article;
    private final Warehouse warehouse;
    private final BusinessYear businessYear;

    public WarehouseCardArticleWarehouseBusinessYearTuple(WarehouseCard warehouseCard, Article article, Warehouse warehouse, BusinessYear businessYear) {
        this.warehouseCard = warehouseCard;
        this.article = article;
        this.warehouse = warehouse;
        this.businessYear = businessYear;
    }

    public WarehouseCard getWarehouseCard() {
        return warehouseCard;
    }

    public Article getArticle() {
        return article;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public BusinessYear getBusinessYear() {
        return businessYear;
    }

}
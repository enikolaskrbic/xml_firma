package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.BusinessPartnerRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class BusinessPartnerRepositoryImpl implements BusinessPartnerRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(BusinessPartnerRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<BusinessPartner> findByName(String name) {
        log.trace(".findByName(name: {})", name);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.name.eq(name)).fetch();
    }

    @Override
    public List<BusinessPartner> findByAddress(String address) {
        log.trace(".findByAddress(address: {})", address);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.address.eq(address)).fetch();
    }

    @Override
    public List<BusinessPartner> findByCity(String city) {
        log.trace(".findByCity(city: {})", city);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.city.eq(city)).fetch();
    }

    @Override
    public List<BusinessPartner> findByCountry(String country) {
        log.trace(".findByCountry(country: {})", country);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.country.eq(country)).fetch();
    }

    @Override
    public List<BusinessPartner> findByEmail(Optional<String> email) {
        log.trace(".findByEmail(email: {})", email);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(email.isPresent() ? businessPartner.email.eq(email.get()) : null).fetch();
    }

    @Override
    public List<BusinessPartner> findByEmailMandatory(String email) {
        log.trace(".findByEmailMandatory(email: {})", email);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.email.eq(email)).fetch();
    }

    @Override
    public List<BusinessPartner> findByPhone(Optional<String> phone) {
        log.trace(".findByPhone(phone: {})", phone);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(phone.isPresent() ? businessPartner.phone.eq(phone.get()) : null).fetch();
    }

    @Override
    public List<BusinessPartner> findByPhoneMandatory(String phone) {
        log.trace(".findByPhoneMandatory(phone: {})", phone);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.phone.eq(phone)).fetch();
    }

    @Override
    public List<BusinessPartner> findByWeb(Optional<String> web) {
        log.trace(".findByWeb(web: {})", web);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(web.isPresent() ? businessPartner.web.eq(web.get()) : null).fetch();
    }

    @Override
    public List<BusinessPartner> findByWebMandatory(String web) {
        log.trace(".findByWebMandatory(web: {})", web);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.web.eq(web)).fetch();
    }

    @Override
    public List<BusinessPartner> findByStatus(RoleBusinessPartner status) {
        log.trace(".findByStatus(status: {})", status);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.status.eq(status)).fetch();
    }

    @Override
    public List<BusinessPartner> findByFirm(Long firmId) {
        log.trace(".findByFirm(firmId: {})", firmId);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.firm.id.eq(firmId)).fetch();
    }

    @Override
    public List<BusinessPartner> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return factory.select(businessPartner).from(businessPartner).where(businessPartner.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<BusinessPartnerFirmTuple> businessPartners(Long firmId) {
        log.trace(".businessPartners()");
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        final QFirm firm = QFirm.firm;
        return factory.select(businessPartner, firm).from(businessPartner).innerJoin(businessPartner.firm, firm).where(businessPartner.firm.id.eq(firmId)).fetch().stream()
                .map(t -> new BusinessPartnerFirmTuple(t.get(businessPartner), t.get(firm))).collect(Collectors.toList());
    }

    @Override
    public List<BusinessPartnerFirmTuple> businessPartnersSearch(Optional<Long> businessPartnerId, Optional<String> businessPartnerName, Optional<String> businessPartnerAddress,
            Optional<String> businessPartnerCity, Optional<String> businessPartnerCountry, Optional<String> businessPartnerWeb, Optional<String> businessPartnerEmail,
            Optional<String> businessPartnerPhone, Optional<RoleBusinessPartner> businessPartnerStatus) {
        log.trace(
                ".businessPartnersSearch(businessPartnerId: {}, businessPartnerName: {}, businessPartnerAddress: {}, businessPartnerCity: {}, businessPartnerCountry: {}, businessPartnerWeb: {}, businessPartnerEmail: {}, businessPartnerPhone: {}, businessPartnerStatus: {})",
                businessPartnerId, businessPartnerName, businessPartnerAddress, businessPartnerCity, businessPartnerCountry, businessPartnerWeb, businessPartnerEmail, businessPartnerPhone,
                businessPartnerStatus);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        final QFirm firm = QFirm.firm;
        return factory.select(businessPartner, firm).from(businessPartner).innerJoin(businessPartner.firm, firm)
                .where(new BooleanBuilder().and(businessPartnerId.isPresent() ? businessPartner.id.eq(businessPartnerId.get()) : null)
                        .or(businessPartnerName.isPresent() ? businessPartner.name.like("%" + businessPartnerName.get() + "%") : null)
                        .or(businessPartnerAddress.isPresent() ? businessPartner.address.like("%" + businessPartnerAddress.get() + "%") : null)
                        .or(businessPartnerCity.isPresent() ? businessPartner.city.like("%" + businessPartnerCity.get() +"%") : null)
                        .or(businessPartnerCountry.isPresent() ? businessPartner.country.like("%" + businessPartnerCountry.get() + "%") : null)
                        .or(businessPartnerWeb.isPresent() ? businessPartner.web.like("%" + businessPartnerWeb.get()+ "%") : null)
                        .or(businessPartnerEmail.isPresent() ? businessPartner.email.like("%" + businessPartnerEmail.get() + "%") : null)
                        .or(businessPartnerPhone.isPresent() ? businessPartner.phone.like("%" + businessPartnerPhone.get()+ "%") : null)
                        .or(businessPartnerStatus.isPresent() ? businessPartner.status.eq(businessPartnerStatus.get()) : null))
                .fetch().stream().map(t -> new BusinessPartnerFirmTuple(t.get(businessPartner), t.get(firm))).collect(Collectors.toList());
    }

    @Override
    public Optional<BusinessPartner> deleteBusinessPartnerHard(Long id) {
        log.trace(".deleteBusinessPartnerHard(id: {})", id);
        final QBusinessPartner businessPartner = QBusinessPartner.businessPartner;
        return Optional.ofNullable(factory.select(businessPartner).from(businessPartner).where(businessPartner.id.eq(id)).fetchOne());
    }

}

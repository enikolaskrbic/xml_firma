package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.WarehouseRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class WarehouseRepositoryImpl implements WarehouseRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(WarehouseRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<Warehouse> findByFirm(Long firmId) {
        log.trace(".findByFirm(firmId: {})", firmId);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return factory.select(warehouse).from(warehouse).where(warehouse.firm.id.eq(firmId)).fetch();
    }

    @Override
    public List<Warehouse> findByCode(String code) {
        log.trace(".findByCode(code: {})", code);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return factory.select(warehouse).from(warehouse).where(warehouse.code.eq(code)).fetch();
    }

    @Override
    public List<Warehouse> findByName(String name) {
        log.trace(".findByName(name: {})", name);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return factory.select(warehouse).from(warehouse).where(warehouse.name.eq(name)).fetch();
    }

    @Override
    public List<Warehouse> findByAddress(String address) {
        log.trace(".findByAddress(address: {})", address);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return factory.select(warehouse).from(warehouse).where(warehouse.address.eq(address)).fetch();
    }

    @Override
    public List<Warehouse> findByCity(String city) {
        log.trace(".findByCity(city: {})", city);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return factory.select(warehouse).from(warehouse).where(warehouse.city.eq(city)).fetch();
    }

    @Override
    public List<Warehouse> findByCountry(String country) {
        log.trace(".findByCountry(country: {})", country);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return factory.select(warehouse).from(warehouse).where(warehouse.country.eq(country)).fetch();
    }

    @Override
    public List<Warehouse> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return factory.select(warehouse).from(warehouse).where(warehouse.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<WarehouseFirmTuple> warehouses(Long firmId) {
        log.trace(".warehouses()");
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QFirm firm = QFirm.firm;
        return factory.select(warehouse, firm).from(warehouse).innerJoin(warehouse.firm, firm).where(warehouse.firm.id.eq(firmId)).fetch().stream().map(t -> new WarehouseFirmTuple(t.get(warehouse), t.get(firm)))
                .collect(Collectors.toList());
    }

    @Override
    public List<WarehouseFirmTuple> warehousesSearch(Optional<Long> warehouseId, Optional<String> warehouseCode, Optional<String> warehouseName, Optional<String> warehouseAddress,
            Optional<String> warehouseCity, Optional<String> warehouseCountry) {
        log.trace(".warehousesSearch(warehouseId: {}, warehouseCode: {}, warehouseName: {}, warehouseAddress: {}, warehouseCity: {}, warehouseCountry: {})", warehouseId, warehouseCode, warehouseName,
                warehouseAddress, warehouseCity, warehouseCountry);
        final QWarehouse warehouse = QWarehouse.warehouse;
        final QFirm firm = QFirm.firm;
        return factory.select(warehouse, firm).from(warehouse).innerJoin(warehouse.firm, firm)
                .where(new BooleanBuilder().and(warehouseId.isPresent() ? warehouse.id.eq(warehouseId.get()) : null).or(warehouseCode.isPresent() ? warehouse.code.like("%" + warehouseCode.get()+ "%") : null)
                        .or(warehouseName.isPresent() ? warehouse.name.like("%" + warehouseName.get() + "%") : null).or(warehouseAddress.isPresent() ? warehouse.address.like("%" + warehouseAddress.get() + "%") : null)
                        .or(warehouseCity.isPresent() ? warehouse.city.like("%" + warehouseCity.get() + "%") : null).or(warehouseCountry.isPresent() ? warehouse.country.like("%" + warehouseCountry.get() + "%") : null))
                .fetch().stream().map(t -> new WarehouseFirmTuple(t.get(warehouse), t.get(firm))).collect(Collectors.toList());
    }

    @Override
    public Optional<Warehouse> deleteWarehouseHard(Long id) {
        log.trace(".deleteWarehouseHard(id: {})", id);
        final QWarehouse warehouse = QWarehouse.warehouse;
        return Optional.ofNullable(factory.select(warehouse).from(warehouse).where(warehouse.id.eq(id)).fetchOne());
    }

}

package com.poslovna.backend.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.SubGroupRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class SubGroupRepositoryImpl implements SubGroupRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(SubGroupRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<SubGroup> findByName(String name) {
        log.trace(".findByName(name: {})", name);
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(subGroup).from(subGroup).where(subGroup.name.eq(name)).fetch();
    }

    @Override
    public List<SubGroup> findByGroup(Long groupId) {
        log.trace(".findByGroup(groupId: {})", groupId);
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(subGroup).from(subGroup).where(subGroup.group.id.eq(groupId)).fetch();
    }

    @Override
    public List<SubGroup> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(subGroup).from(subGroup).where(subGroup.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<SubGroupGroupFirmTuple> subgroups(Long subGroupId) {
        log.trace(".subgroups()");
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(groupFirm, subGroup).from(subGroup).innerJoin(subGroup.group, groupFirm).where(groupFirm.firm.id.eq(subGroupId)).fetch().stream().map(t -> new SubGroupGroupFirmTuple(t.get(subGroup), t.get(groupFirm)))
                .collect(Collectors.toList());
    }

    @Override
    public List<SubGroupGroupFirmTuple> searchSubGroups(Optional<Long> subGroupId, Optional<Long> subGroupGroupId, Optional<String> subGroupName, Optional<String> groupFirmName) {
        log.trace(".searchSubGroups(subGroupId: {}, subGroupGroupId: {}, subGroupName: {}, groupFirmName: {})", subGroupId, subGroupGroupId, subGroupName, groupFirmName);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(groupFirm, subGroup).from(subGroup).innerJoin(subGroup.group, groupFirm)
                .where(new BooleanBuilder().and(subGroupId.isPresent() ? subGroup.id.eq(subGroupId.get()) : null).or(subGroupGroupId.isPresent() ? subGroup.group.id.eq(subGroupGroupId.get()) : null)
                        .or(subGroupName.isPresent() ? subGroup.name.like("%" + subGroupName.get() + "%") : null).or(groupFirmName.isPresent() ? groupFirm.name.like("%" + groupFirmName.get() + "%") : null))
                .fetch().stream().map(t -> new SubGroupGroupFirmTuple(t.get(subGroup), t.get(groupFirm))).collect(Collectors.toList());
    }

    @Override
    public Optional<SubGroup> deleteSubGroupHard(Long id) {
        log.trace(".deleteSubGroupHard(id: {})", id);
        final QSubGroup subGroup = QSubGroup.subGroup;
        return Optional.ofNullable(factory.select(subGroup).from(subGroup).where(subGroup.id.eq(id)).fetchOne());
    }

    @Override
    public List<SubGroupGroupFirmTuple> groupSubGroup(Long groupFirmId) {
        log.trace(".groupSubGroup(groupFirmId: {})", groupFirmId);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(groupFirm, subGroup).from(subGroup).innerJoin(subGroup.group, groupFirm).where(groupFirm.id.eq(groupFirmId)).fetch().stream()
                .map(t -> new SubGroupGroupFirmTuple(t.get(subGroup), t.get(groupFirm))).collect(Collectors.toList());
    }

    @Override
    public List<SubGroupGroupFirmTuple> findGroupSubGroup(Long groupFirmId, Integer drop, Integer take) {
        log.trace(".findGroupSubGroup(groupFirmId: {}, drop: {}, take: {})", groupFirmId, drop, take);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(groupFirm, subGroup).from(subGroup).innerJoin(subGroup.group, groupFirm).where(groupFirm.id.eq(groupFirmId)).offset(drop).limit(take).fetch().stream()
                .map(t -> new SubGroupGroupFirmTuple(t.get(subGroup), t.get(groupFirm))).collect(Collectors.toList());
    }

    @Override
    public Long countGroupSubGroup(Long groupFirmId) {
        log.trace(".countGroupSubGroup(groupFirmId: {})", groupFirmId);
        final QGroupFirm groupFirm = QGroupFirm.groupFirm;
        final QSubGroup subGroup = QSubGroup.subGroup;
        return factory.select(groupFirm, subGroup).from(subGroup).innerJoin(subGroup.group, groupFirm).where(groupFirm.id.eq(groupFirmId)).fetchCount();
    }

}

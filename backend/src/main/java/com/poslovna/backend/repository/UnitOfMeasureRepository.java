package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.UnitOfMeasure;


public interface UnitOfMeasureRepository extends JpaRepository<UnitOfMeasure, Long>, UnitOfMeasureRepositoryCustom {

}

package com.poslovna.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovna.backend.model.WarehouseCard;


public interface WarehouseCardRepository extends JpaRepository<WarehouseCard, Long>, WarehouseCardRepositoryCustom {

}

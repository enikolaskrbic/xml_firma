package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.tuple.*;


public interface ArticleRepositoryCustom {

    List<Article> findByName(String name);

    List<Article> findByCode(String code);

    List<Article> findByStatus(RoleArticle status);

    List<Article> findBySubGroup(Long subGroupId);

    List<Article> findByUnitOfMeasure(Long unitOfMeasureId);

    List<ArticleUnitOfMeasureSubGroupTuple> articles(Long firmId);

    List<Article> findByIsDeleted(Boolean isDeleted);

    List<ArticleUnitOfMeasureSubGroupTuple> searchArticles(Optional<Long> articleId, Optional<String> ArticleName, Optional<String> articleCode, Optional<RoleArticle> articleStatus,
            Optional<Long> subGroupId, Optional<String> subGroupName, Optional<Long> unitOfMeasureId, Optional<String> unitOfMeasureName, Optional<String> unitOfMeasureShortName);

    List<ArticleUnitOfMeasureSubGroupTuple> subGroupArticle(Long subGroupId);

    List<ArticleUnitOfMeasureSubGroupTuple> findSubGroupArticle(Long subGroupId, Integer drop, Integer take);

    Long countSubGroupArticle(Long subGroupId);

    Optional<Article> deleteArticleHard(Long id);

}

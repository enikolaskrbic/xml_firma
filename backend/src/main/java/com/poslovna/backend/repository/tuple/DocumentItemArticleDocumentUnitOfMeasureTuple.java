
package com.poslovna.backend.repository.tuple;

import com.poslovna.backend.model.*;


public class DocumentItemArticleDocumentUnitOfMeasureTuple {

    private final DocumentItem documentItem;
    private final Article article;
    private final Document document;
    private final UnitOfMeasure unitOfMeasure;

    public DocumentItemArticleDocumentUnitOfMeasureTuple(DocumentItem documentItem, Article article, Document document, UnitOfMeasure unitOfMeasure) {
        this.documentItem = documentItem;
        this.article = article;
        this.document = document;
        this.unitOfMeasure = unitOfMeasure;
    }

    public DocumentItem getDocumentItem() {
        return documentItem;
    }

    public Article getArticle() {
        return article;
    }

    public Document getDocument() {
        return document;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

}
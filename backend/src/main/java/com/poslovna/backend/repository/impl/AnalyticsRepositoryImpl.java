package com.poslovna.backend.repository.impl;

import java.math.BigDecimal;
import java.time.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.AnalyticsRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class AnalyticsRepositoryImpl implements AnalyticsRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(AnalyticsRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<Analytics> findByDate(ZonedDateTime date) {
        log.trace(".findByDate(date: {})", date);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.date.eq(date)).fetch();
    }

    @Override
    public List<Analytics> findByType(TypeAnalytics type) {
        log.trace(".findByType(type: {})", type);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.type.eq(type)).fetch();
    }

    @Override
    public List<Analytics> findByDirection(TypeDirection direction) {
        log.trace(".findByDirection(direction: {})", direction);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.direction.eq(direction)).fetch();
    }

    @Override
    public List<Analytics> findByQuantity(BigDecimal quantity) {
        log.trace(".findByQuantity(quantity: {})", quantity);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.quantity.eq(quantity)).fetch();
    }

    @Override
    public List<Analytics> findByBalance(BigDecimal balance) {
        log.trace(".findByBalance(balance: {})", balance);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.balance.eq(balance)).fetch();
    }

    @Override
    public List<Analytics> findByPrice(BigDecimal price) {
        log.trace(".findByPrice(price: {})", price);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.price.eq(price)).fetch();
    }

    @Override
    public List<Analytics> findByValue(BigDecimal value) {
        log.trace(".findByValue(value: {})", value);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.value.eq(value)).fetch();
    }

    @Override
    public List<Analytics> findByTotalValue(BigDecimal totalValue) {
        log.trace(".findByTotalValue(totalValue: {})", totalValue);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.totalValue.eq(totalValue)).fetch();
    }

    @Override
    public List<Analytics> findByDocumentItem(Optional<Long> documentItemId) {
        log.trace(".findByDocumentItem(documentItemId: {})", documentItemId);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(documentItemId.isPresent() ? analytics.documentItem.id.eq(documentItemId.get()) : null).fetch();
    }

    @Override
    public List<Analytics> findByDocumentItemMandatory(Long documentItemId) {
        log.trace(".findByDocumentItemMandatory(documentItemId: {})", documentItemId);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.documentItem.id.eq(documentItemId)).fetch();
    }

    @Override
    public List<Analytics> findByWarehouseCard(Long warehouseCardId) {
        log.trace(".findByWarehouseCard(warehouseCardId: {})", warehouseCardId);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.warehouseCard.id.eq(warehouseCardId)).fetch();
    }

    @Override
    public List<Analytics> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QAnalytics analytics = QAnalytics.analytics;
        return factory.select(analytics).from(analytics).where(analytics.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<AnalyticsWarehouseCardArticleTuple> analytics(Long warehouseCardId) {
        log.trace(".analytics(warehouseCardId: {})", warehouseCardId);
        final QArticle article = QArticle.article;
        final QAnalytics analytics = QAnalytics.analytics;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(article, analytics, warehouseCard).from(analytics).innerJoin(analytics.warehouseCard, warehouseCard).innerJoin(warehouseCard.article, article)
                .where(warehouseCard.id.eq(warehouseCardId)).fetch().stream().map(t -> new AnalyticsWarehouseCardArticleTuple(t.get(analytics), t.get(warehouseCard), t.get(article)))
                .collect(Collectors.toList());
    }

    @Override
    public List<AnalyticsWarehouseCardArticleTuple> analyticsSearch(Long warehouseCardId, Optional<Long> analyticsId, Optional<TypeAnalytics> analyticsType, Optional<BigDecimal> analyticsQuantity,
            Optional<BigDecimal> analyticsBalance, Optional<BigDecimal> analyticsPrice, Optional<BigDecimal> analyticsValue, Optional<BigDecimal> analyticsTotalValue) {
        log.trace(
                ".analyticsSearch(warehouseCardId: {}, analyticsId: {}, analyticsType: {}, analyticsQuantity: {}, analyticsBalance: {}, analyticsPrice: {}, analyticsValue: {}, analyticsTotalValue: {})",
                warehouseCardId, analyticsId, analyticsType, analyticsQuantity, analyticsBalance, analyticsPrice, analyticsValue, analyticsTotalValue);
        final QArticle article = QArticle.article;
        final QAnalytics analytics = QAnalytics.analytics;
        final QWarehouseCard warehouseCard = QWarehouseCard.warehouseCard;
        return factory.select(article, analytics, warehouseCard).from(analytics).innerJoin(analytics.warehouseCard, warehouseCard).innerJoin(warehouseCard.article, article)
                .where(new BooleanBuilder().and(warehouseCard.id.eq(warehouseCardId)).or(analyticsId.isPresent() ? analytics.id.eq(analyticsId.get()) : null)
                        .or(analyticsType.isPresent() ? analytics.type.eq(analyticsType.get()) : null).or(analyticsQuantity.isPresent() ? analytics.quantity.eq(analyticsQuantity.get()) : null)
                        .or(analyticsBalance.isPresent() ? analytics.balance.eq(analyticsBalance.get()) : null).or(analyticsPrice.isPresent() ? analytics.price.eq(analyticsPrice.get()) : null)
                        .or(analyticsValue.isPresent() ? analytics.value.eq(analyticsValue.get()) : null)
                        .or(analyticsTotalValue.isPresent() ? analytics.totalValue.eq(analyticsTotalValue.get()) : null))
                .fetch().stream().map(t -> new AnalyticsWarehouseCardArticleTuple(t.get(analytics), t.get(warehouseCard), t.get(article))).collect(Collectors.toList());
    }

}

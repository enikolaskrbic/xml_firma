package com.poslovna.backend.repository;

import java.math.BigDecimal;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.tuple.*;


public interface WarehouseCardRepositoryCustom {

    List<WarehouseCard> findByStartState(BigDecimal startState);

    List<WarehouseCard> findByInput(BigDecimal input);

    List<WarehouseCard> findByOutput(BigDecimal output);

    List<WarehouseCard> findByBalance(BigDecimal balance);

    List<WarehouseCard> findByInValue(BigDecimal inValue);

    List<WarehouseCard> findByOutValue(BigDecimal outValue);

    List<WarehouseCard> findByTotalValue(BigDecimal totalValue);

    List<WarehouseCard> findByWarehouse(Long warehouseId);

    List<WarehouseCard> findByWarehouseAndBusinessYear(Long warehouseId);

    List<WarehouseCard> findActiveByWarehouseAndArticle(Long warehouseId, Long articleId);

    List<WarehouseCard> findByBusinessYear(Long businessYearId);

    List<WarehouseCard> findByArticle(Long articleId);

    List<WarehouseCardArticleWarehouseBusinessYearTuple> warehousesCard(Long firmId);

    List<WarehouseCard> findByIsDeleted(Boolean isDeleted);

    List<WarehouseCard> findByAveragePrice(BigDecimal averagePrice);

    List<WarehouseCard> findByLastPrice(Optional<BigDecimal> lastPrice);

    List<WarehouseCard> findByLastPriceMandatory(BigDecimal lastPrice);

    List<WarehouseCardArticleWarehouseBusinessYearTuple> warehousesCard();

    List<WarehouseCardArticleWarehouseBusinessYearTuple> warehousesCardSearch(Optional<Long> warehouseCardId, Optional<Long> warehouseCardBusinessYearId, Optional<String> businessYearYear,
            Optional<Long> articleId, Optional<String> articleName, Optional<String> articleCode, Optional<BigDecimal> warehouseCardInput, Optional<BigDecimal> warehouseCardOutput,
            Optional<BigDecimal> warehouseCardBalance, Optional<BigDecimal> warehouseCardInValue, Optional<BigDecimal> warehouseCardOutValue, Optional<BigDecimal> warehouseCardTotalValue,
            Optional<Long> warehouseId, Optional<String> warehouseName);

    List<WarehouseCardArticleWarehouseBusinessYearTuple> warehouseWarehouseCard(Long warehouseId);

    List<WarehouseCardArticleWarehouseBusinessYearTuple> findWarehouseWarehouseCard(Long warehouseId, Integer drop, Integer take);

    Long countWarehouseWarehouseCard(Long warehouseId);

    List<WarehouseCardArticleWarehouseBusinessYearTuple> articleWarehouseCard(Long articleId);

    List<WarehouseCardArticleWarehouseBusinessYearTuple> findArticleWarehouseCard(Long articleId, Integer drop, Integer take);

    Long countArticleWarehouseCard(Long articleId);

}

package com.poslovna.backend.repository;

import java.util.List;
import java.util.Optional;

import com.poslovna.backend.model.*;

import com.poslovna.backend.repository.tuple.*;


public interface WarehouseRepositoryCustom {

    List<Warehouse> findByFirm(Long firmId);

    List<Warehouse> findByCode(String code);

    List<Warehouse> findByName(String name);

    List<Warehouse> findByAddress(String address);

    List<Warehouse> findByCity(String city);

    List<Warehouse> findByCountry(String country);

    List<Warehouse> findByIsDeleted(Boolean isDeleted);

    public List<WarehouseFirmTuple> warehouses(Long firmId);

    List<WarehouseFirmTuple> warehousesSearch(Optional<Long> warehouseId, Optional<String> warehouseCode, Optional<String> warehouseName, Optional<String> warehouseAddress,
            Optional<String> warehouseCity, Optional<String> warehouseCountry);

    Optional<Warehouse> deleteWarehouseHard(Long id);

}

package com.poslovna.backend.repository.impl;

import java.math.BigDecimal;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import com.poslovna.backend.model.*;
import com.poslovna.backend.model.enumeration.*;
import com.poslovna.backend.repository.DocumentItemRepositoryCustom;
import com.poslovna.backend.repository.tuple.*;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQueryFactory;


public class DocumentItemRepositoryImpl implements DocumentItemRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(DocumentItemRepositoryImpl.class);

    @Inject
    private JPQLQueryFactory factory;

    @Override
    public List<DocumentItem> findByDocument(Long documentId) {
        log.trace(".findByDocument(documentId: {})", documentId);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return factory.select(documentItem).from(documentItem).where(documentItem.document.id.eq(documentId)).fetch();
    }

    @Override
    public List<DocumentItem> findByQuantity(BigDecimal quantity) {
        log.trace(".findByQuantity(quantity: {})", quantity);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return factory.select(documentItem).from(documentItem).where(documentItem.quantity.eq(quantity)).fetch();
    }

    @Override
    public List<DocumentItem> findByType(TypeDocument type) {
        log.trace(".findByType(type: {})", type);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return factory.select(documentItem).from(documentItem).where(documentItem.type.eq(type)).fetch();
    }

    @Override
    public List<DocumentItem> findByPrice(BigDecimal price) {
        log.trace(".findByPrice(price: {})", price);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return factory.select(documentItem).from(documentItem).where(documentItem.price.eq(price)).fetch();
    }

    @Override
    public List<DocumentItem> findByArticle(Long articleId) {
        log.trace(".findByArticle(articleId: {})", articleId);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return factory.select(documentItem).from(documentItem).where(documentItem.article.id.eq(articleId)).fetch();
    }

    @Override
    public List<DocumentItem> findByIsDeleted(Boolean isDeleted) {
        log.trace(".findByIsDeleted(isDeleted: {})", isDeleted);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return factory.select(documentItem).from(documentItem).where(documentItem.isDeleted.eq(isDeleted)).fetch();
    }

    @Override
    public List<DocumentItemArticleDocumentUnitOfMeasureTuple> documentItems(Long documentId) {
        log.trace(".documentItems(documentId: {})", documentId);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(unitOfMeasure, article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document)
                .innerJoin(article.unitOfMeasure, unitOfMeasure).where(document.id.eq(documentId)).fetch().stream()
                .map(t -> new DocumentItemArticleDocumentUnitOfMeasureTuple(t.get(documentItem), t.get(article), t.get(document), t.get(unitOfMeasure))).collect(Collectors.toList());
    }

    @Override
    public List<DocumentItemArticleDocumentUnitOfMeasureTuple> documentItemsSearch(Optional<Long> documentItemId, Optional<TypeDocument> documentItemType, Optional<Long> documentId,
            Optional<String> articleName, Optional<Long> articleUnitOfMeasureId) {
        log.trace(".documentItemsSearch(documentItemId: {}, documentItemType: {}, documentId: {}, articleName: {}, articleUnitOfMeasureId: {})", documentItemId, documentItemType, documentId,
                articleName, articleUnitOfMeasureId);
        final QUnitOfMeasure unitOfMeasure = QUnitOfMeasure.unitOfMeasure;
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(unitOfMeasure, article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document)
                .innerJoin(article.unitOfMeasure, unitOfMeasure)
                .where(new BooleanBuilder().and(documentItemId.isPresent() ? documentItem.id.eq(documentItemId.get()) : null)
                        .or(documentItemType.isPresent() ? documentItem.type.eq(documentItemType.get()) : null).or(documentId.isPresent() ? document.id.eq(documentId.get()) : null)
                        .or(articleName.isPresent() ? article.name.like("%" + articleName.get() + "%") : null)
                        .or(articleUnitOfMeasureId.isPresent() ? article.unitOfMeasure.id.eq(articleUnitOfMeasureId.get()) : null))
                .fetch().stream().map(t -> new DocumentItemArticleDocumentUnitOfMeasureTuple(t.get(documentItem), t.get(article), t.get(document), t.get(unitOfMeasure))).collect(Collectors.toList());
    }

    @Override
    public List<DocumentItemArticleDocumentTuple> documentDocumentItem(Long documentId) {
        log.trace(".documentDocumentItem(documentId: {})", documentId);
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document).where(document.id.eq(documentId))
                .fetch().stream().map(t -> new DocumentItemArticleDocumentTuple(t.get(documentItem), t.get(article), t.get(document))).collect(Collectors.toList());
    }

    @Override
    public List<DocumentItemArticleDocumentTuple> findDocumentDocumentItem(Long documentId, Integer drop, Integer take) {
        log.trace(".findDocumentDocumentItem(documentId: {}, drop: {}, take: {})", documentId, drop, take);
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document).where(document.id.eq(documentId))
                .offset(drop).limit(take).fetch().stream().map(t -> new DocumentItemArticleDocumentTuple(t.get(documentItem), t.get(article), t.get(document))).collect(Collectors.toList());
    }

    @Override
    public Long countDocumentDocumentItem(Long documentId) {
        log.trace(".countDocumentDocumentItem(documentId: {})", documentId);
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document).where(document.id.eq(documentId))
                .fetchCount();
    }

    @Override
    public List<DocumentItemArticleDocumentTuple> articleDocumentItem(Long articleId) {
        log.trace(".articleDocumentItem(articleId: {})", articleId);
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document).where(article.id.eq(articleId))
                .fetch().stream().map(t -> new DocumentItemArticleDocumentTuple(t.get(documentItem), t.get(article), t.get(document))).collect(Collectors.toList());
    }

    @Override
    public List<DocumentItemArticleDocumentTuple> findArticleDocumentItem(Long articleId, Integer drop, Integer take) {
        log.trace(".findArticleDocumentItem(articleId: {}, drop: {}, take: {})", articleId, drop, take);
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document).where(article.id.eq(articleId))
                .offset(drop).limit(take).fetch().stream().map(t -> new DocumentItemArticleDocumentTuple(t.get(documentItem), t.get(article), t.get(document))).collect(Collectors.toList());
    }

    @Override
    public Long countArticleDocumentItem(Long articleId) {
        log.trace(".countArticleDocumentItem(articleId: {})", articleId);
        final QArticle article = QArticle.article;
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        final QDocument document = QDocument.document;
        return factory.select(article, documentItem, document).from(documentItem).innerJoin(documentItem.article, article).innerJoin(documentItem.document, document).where(article.id.eq(articleId))
                .fetchCount();
    }

    @Override
    public List<DocumentItem> findByArticleAndDocument(Long articleId, Long documentId) {

        log.trace(".findByArticleAndDocument(articleId: {})", articleId);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return factory.select(documentItem).from(documentItem).where(documentItem.article.id.eq(articleId).and(documentItem.document.id.eq(documentId))).fetch();

    }

    public Optional<DocumentItem> deleteDocumentItemHard(Long id) {
        log.trace(".deleteDocumentItemHard(id: {})", id);
        final QDocumentItem documentItem = QDocumentItem.documentItem;
        return Optional.ofNullable(factory.select(documentItem).from(documentItem).where(documentItem.id.eq(id)).fetchOne());

    }

}

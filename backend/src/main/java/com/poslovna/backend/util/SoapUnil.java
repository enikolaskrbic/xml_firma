package com.poslovna.backend.util;

import com.poslovna.backend.xml_model.izvod.IzvodRequest;
import com.poslovna.backend.xml_model.nalogzaplacanje.NalogZaPlacanjeRequest;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ivansavic on 5/11/17.
 */
public class SoapUnil {

    public static String createIzvodSoapMessge(IzvodRequest izvodRequest) throws SOAPException, IOException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        String serverURI = "http://xml.web.services.security/izvod";

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("izv", serverURI);

        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("izvodRequest", "izv");

        //add element to body
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("brojRacuna", "izv");
        soapBodyElem2.addTextNode(izvodRequest.getBrojRacuna());
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("datum", "izv");
        soapBodyElem3.addTextNode(izvodRequest.getDatum().toString());
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("redniBrojPreseka", "izv");
        soapBodyElem4.addTextNode(String.valueOf(izvodRequest.getRedniBrojPreseka()));

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String strMsg = new String(out.toByteArray());
        return strMsg;
    }

    public static String createNalogMessage(NalogZaPlacanjeRequest nalogZaPlacanjeRequest) throws SOAPException, IOException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        String serverURI = "http://xml.web.services.security/nalogZaPlacanje";

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("nal", serverURI);

        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("nalogZaPlacanjeRequest", "nal");
        Name datumNaloga = envelope.createName("datumNaloga");
        soapBodyElem.addAttribute(datumNaloga,nalogZaPlacanjeRequest.getDatumNaloga().toString());
        Name datumRacuna = envelope.createName("datumRacuna");
        soapBodyElem.addAttribute(datumRacuna,nalogZaPlacanjeRequest.getDatumRacuna().toString());
        Name hitno = envelope.createName("hitno");
        if(nalogZaPlacanjeRequest.isHitno()){
            soapBodyElem.addAttribute(hitno,"true");
        }else{
            soapBodyElem.addAttribute(hitno,"false");

        }
        //add element to body
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("svrhaPlacanja", "nal");
        soapBodyElem1.addTextNode(nalogZaPlacanjeRequest.getSvrhaPlacanja());
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("iznos", "nal");
        soapBodyElem2.addTextNode(nalogZaPlacanjeRequest.getIznos().toString());
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("oznakaValute", "nal");
        soapBodyElem3.addTextNode(nalogZaPlacanjeRequest.getOznakaValute());
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("podaciDuznika", "nal");

        SOAPElement soapBodyElem41 = soapBodyElem4.addChildElement("naziv", "nal");
        soapBodyElem41.addTextNode(nalogZaPlacanjeRequest.getPodaciDuznika().getNaziv());
        SOAPElement soapBodyElem42 = soapBodyElem4.addChildElement("racun", "nal");
        soapBodyElem42.addTextNode(nalogZaPlacanjeRequest.getPodaciDuznika().getRacun());
        SOAPElement soapBodyElem43 = soapBodyElem4.addChildElement("model", "nal");
        soapBodyElem43.addTextNode(String.valueOf(nalogZaPlacanjeRequest.getPodaciDuznika().getModel()));
        SOAPElement soapBodyElem44 = soapBodyElem4.addChildElement("poziv", "nal");
        soapBodyElem44.addTextNode(nalogZaPlacanjeRequest.getPodaciDuznika().getPoziv());

        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("podaciPoverioca", "nal");

        SOAPElement soapBodyElem51 = soapBodyElem5.addChildElement("naziv", "nal");
        soapBodyElem51.addTextNode(nalogZaPlacanjeRequest.getPodaciPoverioca().getNaziv());
        SOAPElement soapBodyElem52 = soapBodyElem5.addChildElement("racun", "nal");
        soapBodyElem52.addTextNode(nalogZaPlacanjeRequest.getPodaciPoverioca().getRacun());
        SOAPElement soapBodyElem53 = soapBodyElem5.addChildElement("model", "nal");
        soapBodyElem53.addTextNode(String.valueOf(nalogZaPlacanjeRequest.getPodaciPoverioca().getModel()));
        SOAPElement soapBodyElem54 = soapBodyElem5.addChildElement("poziv", "nal");
        soapBodyElem54.addTextNode(nalogZaPlacanjeRequest.getPodaciPoverioca().getPoziv());


        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String strMsg = new String(out.toByteArray());


        return strMsg;

    }

    public static String callSoapService(String xmlRequest, String apiBanke) throws IOException {
        URL url = new URL(apiBanke);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "text/xml");
        // Send the request
        java.io.OutputStreamWriter wr = new java.io.OutputStreamWriter(conn.getOutputStream());
        wr.write(xmlRequest);
        wr.flush();

        //Read the response
        java.io.BufferedReader rd = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream()));
        String line;
        String retVal="";
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
            retVal+=line;
        }
        conn.disconnect();

        return retVal;
    }
}

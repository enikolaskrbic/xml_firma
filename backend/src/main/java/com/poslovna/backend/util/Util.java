package com.poslovna.backend.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.marklogic.client.DatabaseClientFactory.Authentication;

public class Util {
	/**
	 * ExampleProperties represents the configuration for the examples.
	 */
	static public class ConnectionProperties {

		public String host;
		public int port = -1;
		public String user;
		public String password;
		public String database;
		public Authentication authType;

		public ConnectionProperties(Properties props) {
			super();
			host = props.getProperty("example.host").trim();
			port = Integer.parseInt(props.getProperty("example.port"));
			user = props.getProperty("example.admin_user").trim();
			password = props.getProperty("example.admin_password").trim();
			database = props.getProperty("example.database").trim();
			authType = Authentication.valueOf(props.getProperty("example.authentication_type").toUpperCase().trim());
		}
	}

	/**
	 * Read the configuration properties for the example.
	 * 
	 * @return the configuration object
	 */
	public static ConnectionProperties loadProperties() throws IOException {
		String propsName = "Config.properties";

		InputStream propsStream = openStream(propsName);
		if (propsStream == null)
			throw new IOException("Could not read properties " + propsName);

		Properties props = new Properties();
		props.load(propsStream);

		return new ConnectionProperties(props);
	}

	/**
	 * Read a resource for an example.
	 * 
	 * @param fileName
	 *            the name of the resource
	 * @return an input stream for the resource
	 * @throws IOException
	 */
	public static InputStream openStream(String fileName) throws IOException {
		return Util.class.getClassLoader().getResourceAsStream(fileName);
	}
}

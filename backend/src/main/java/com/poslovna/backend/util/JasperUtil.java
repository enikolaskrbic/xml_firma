package com.poslovna.backend.util;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Map;

/**
 * Created by ivansavic on 4/19/17.
 */
@Service
public class JasperUtil {
    @Inject
    DataSourceProperties dataSourceProperties;

    public byte[] createReport(String fileName,Map<String,Object> params){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // Generate PDF using Jasper
        try {
            Connection connection = (Connection) DriverManager.getConnection(dataSourceProperties.getUrl() , dataSourceProperties.getUsername(), dataSourceProperties.getPassword());
            InputStream jasperStream = this.getClass().getClassLoader().getResourceAsStream(fileName);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);
            JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
            connection.close();
            return baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

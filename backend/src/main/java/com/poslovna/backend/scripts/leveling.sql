DELIMITER $$

CREATE PROCEDURE `levelingWarehouse` (  IN warehouseId bigint(20))
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE warehouseCardId bigint(20);
  DECLARE averagePrice decimal(10,4);
  DECLARE balance decimal(10,4);
  DECLARE totalValue decimal(10,4);
  DECLARE leveling decimal(10,4) DEFAUlT 0;
  DECLARE _rollback BOOL DEFAULT 0;
  DECLARE warehouseCards CURSOR FOR SELECT WarehouseCard.id, WarehouseCard.averagePrice, WarehouseCard.balance, WarehouseCard.totalValue
                        FROM WarehouseCard join BusinessYear on WarehouseCard.businessYearId = BusinessYear.id
                        WHERE WarehouseCard.warehouseId = warehouseId and BusinessYear.isFinished = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;

	OPEN warehouseCards;
  listOfWarehouseCard : LOOP
		FETCH warehouseCards INTO warehouseCardId, averagePrice, balance, totalValue;
		IF done THEN
			LEAVE listOfWarehouseCard;
		END IF;

    SET leveling = averagePrice * balance - totalValue;

    START TRANSACTION;

    CALL add_analytics('LEVELING', 'INPUT', balance, averagePrice, totalValue+leveling, null, warehouseCardId, 0, leveling);
    UPDATE WarehouseCard SET WarehouseCard.totalValue = totalValue+leveling WHERE WarehouseCard.id = warehouseCardId;

    IF _rollback THEN
      ROLLBACK;
    ELSE
      COMMIT;
    END IF;

	END LOOP;
END$$






DELIMITER $$
CREATE PROCEDURE `add_analytics`(  IN typeAnalytics varchar(255),
									IN directionAnalytics varchar(255),
                                    IN balanceAnalytics decimal(10,4),
                                    IN priceAnalytics decimal(10,4),
                                    IN totalValueAnalytics decimal(10,4),
                                    IN documentItemIdAnalytics bigint(20),
                                    IN warehouseCardIdAnalytics bigint(20),
                                    IN quantityAnalytics decimal(10,4),
                                    IN valueAnalytics decimal(10,4) )
BEGIN
	INSERT INTO `poslovna`.`Analytics` (`date`, `type`, `direction`, `balance`, `price`, `totalValue`, `documentItemId`, `isDeleted`, `warehouseCardId`, `quantity`, `value`)
    VALUES (NOW(), typeAnalytics, directionAnalytics, balanceAnalytics, priceAnalytics, totalValueAnalytics, documentItemIdAnalytics, 0, warehouseCardIdAnalytics, quantityAnalytics, valueAnalytics );
END$$
xdmp:document-delete("example.xml")

declare namespace pp = "http://xml.web.services.security/faktura";
xdmp:node-insert-child(doc("instanceSpisakFaktura.xml")//pp:spisakFaktura//pp:listaUlaznihFaktura[@pibFirme=55000000], <a/>);

declare namespace pp = "http://xml.web.services.security/faktura";
xdmp:node-delete(doc("instanceSpisakFaktura.xml")//pp:spisakFaktura//pp:listaUlaznihFaktura//a);

fn:doc("instanceSpisakFaktura.xml")

*************************menjanje sadrzaja atributa**************************
declare namespace pp = "http://xml.web.services.security/firma";
declare variable $doc_name as xs:string external;
declare variable $userid as xs:string external;
let $users_doc := doc("instanceSpisakFirmi.xml")//pp:spisakFirmi

let $old as attribute() := $users_doc//pp:firma[@pib="11111111"]/@pib
let $new as attribute() := attribute pib {33333333}

return xdmp:node-replace($old, $new);
*****************************************************************************

****************************menjanje sadrzaja noda***************************
declare namespace pp = "http://xml.web.services.security/firma";
declare variable $doc_name as xs:string external;
declare variable $userid as xs:string external;
let $users_doc := doc("instanceSpisakFirmi.xml")//pp:spisakFirmi

let $old  := $users_doc//pp:firma[@pib="33333333"]//pp:naziv/text()
let $new  :=   text{"firma1"}

return xdmp:node-replace($old, $new);
******************************************************************************


declare namespace pp = "http://xml.web.services.security/firma";
doc("instanceSpisakFirmi.xml")//pp:spisakFirmi//pp:firma[@pib!="11111111"];
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('AboutFirmController', AboutFirmController);

    AboutFirmController.$inject = ['$scope', 'managerApi','modalWindows'];

    function AboutFirmController($scope,managerApi,modalWindows) {
        $scope.model = [];
        $scope.errorCode = null;

        load()

        function load() {

            var req ={
                pib: localStorage.getItem('pibFirme'),
                api: localStorage.getItem('apiFirme')
            }

            managerApi.getAboutFirm(req).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;

                localStorage.setItem('aboutFirm',JSON.stringify(response.data));


            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }
        


        $scope.submit = function () {

            var req ={
                api:localStorage.getItem('apiFirme'),
                apiBanke: $scope.selectedModel.racunDuznika.nazivBanke.value,
                brojRacuna : $scope.selectedModel.racunDuznika.brojRacuna,
                datum : $scope.selectedModel.datum,
                redniBrojPreseka : $scope.selectedModel.brojPreseka

            }

            managerApi.izvodRacuna(req).then(onSuccess, onError);

            function onSuccess(response) {
                localStorage.setItem('izvod',JSON.stringify(response.data));
                modalWindows.openModalIzvod({
                    reload: afterChoose
                });
            }

            function onError(response) {
                swal('Failure', response.data.message, 'error');
            }

        }

        function afterChoose() {

        }
    }
})();
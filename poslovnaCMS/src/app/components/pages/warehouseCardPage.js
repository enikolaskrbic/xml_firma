(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('WarehouseCardPageController', WarehouseCardPageController);

    WarehouseCardPageController.$inject = ['$scope', 'modalWindows', 'eventBus'];

    function WarehouseCardPageController($scope, modalWindows, eventBus) {

        $scope.onClickLoadYear = onClickLoadYear;
        $scope.onClickLoadArticle = onClickLoadArticle;
        $scope.onClickLoadWarehouse = onClickLoadWarehouse;

        function onClickLoadYear() {
            modalWindows.openModalBussinesYear();
        }

        function onClickLoadArticle() {
            modalWindows.openModalArticles();
        }

        function onClickLoadWarehouse() {
            modalWindows.openModalWarehouse();
        }

    }
})();
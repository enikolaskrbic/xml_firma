(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('SignUpPageController', SignUpPageController);

    SignUpPageController.$inject = ['$scope', '$state'];

    function SignUpPageController($scope, $state) {

        $scope.onClickSignIn = onClickSignIn;

        function onClickSignIn() {
            $state.go('signInPage');
        }

    }
})();
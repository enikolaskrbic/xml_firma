(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('SignInPageController', SignInPageController);

    SignInPageController.$inject = ['$scope', '$state'];

    function SignInPageController($scope, $state) {

        $scope.onClickForgotPassword = onClickForgotPassword;
        $scope.onClickSignUp = onClickSignUp;

        function onClickForgotPassword() {
            $state.go('forgotPasswordPage');
        }

        function onClickSignUp() {
            $state.go('signUpPage');
        }

    }
})();
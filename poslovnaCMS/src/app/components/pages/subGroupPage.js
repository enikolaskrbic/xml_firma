(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('SubGroupPageController', SubGroupPageController);

    SubGroupPageController.$inject = ['$scope', 'modalWindows', 'eventBus'];

    function SubGroupPageController($scope, modalWindows, eventBus) {

        $scope.onClickLoadGroup = onClickLoadGroup;

        function onClickLoadGroup() {
            modalWindows.openModalGroups();
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('ArticlePageController', ArticlePageController);

    ArticlePageController.$inject = ['$scope', 'modalWindows', 'eventBus'];

    function ArticlePageController($scope, modalWindows, eventBus) {

        $scope.onClickLoadSubGroup = onClickLoadSubGroup;
        $scope.onClickLoadUnit = onClickLoadUnit;

        function onClickLoadSubGroup() {
            modalWindows.openModalSubGroups();
        }

        function onClickLoadUnit() {
            modalWindows.openModalUnitOfMeasures();
        }

    }
})();
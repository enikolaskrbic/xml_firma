(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('DocumentsPageController', DocumentsPageController);

    DocumentsPageController.$inject = ['$scope', 'modalWindows', 'eventBus'];

    function DocumentsPageController($scope, modalWindows, eventBus) {

        $scope.onClickLoadBusinesPartner = onClickLoadBusinesPartner;

        function onClickLoadBusinesPartner() {
            modalWindows.openModalBussinesPartner();
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('ResetPasswordPageController', ResetPasswordPageController);

    ResetPasswordPageController.$inject = ['$scope', '$stateParams'];

    function ResetPasswordPageController($scope, $stateParams) {
        $scope.resetPasswordCode = $stateParams.resetPasswordCode;

    }
})();
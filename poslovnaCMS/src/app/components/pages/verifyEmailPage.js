(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .controller('VerifyEmailPageController', VerifyEmailPageController);

    VerifyEmailPageController.$inject = ['$scope', '$stateParams'];

    function VerifyEmailPageController($scope, $stateParams) {
        $scope.emailVerificationCode = $stateParams.emailVerificationCode;

    }
})();
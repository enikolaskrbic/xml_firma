(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('verifyEmailForm', function() {
            return {
                restrict: 'E',
                scope: {
                    emailVerificationCode: '='
                },
                templateUrl: 'src/app/components/forms/verifyEmailForm.html',
                controller: 'VerifyEmailFormController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('VerifyEmailFormController', VerifyEmailFormController);

    VerifyEmailFormController.$inject = ['$scope', '$state', 'authenticationApi'];

    function VerifyEmailFormController($scope, $state, authenticationApi) {

        $scope.model = {};
        $scope.errorCode = null;
        $scope.submit = submit;

        function submit(form) {
            if (form !== undefined && form.$submitted && form.$invalid) {
                return false;
            }
            authenticationApi.verifyEmail($scope.model).then(onSuccess, onError);

            function onSuccess(response) {
                $state.go('signInPage');
                $scope.errorCode = null;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

    }

})();
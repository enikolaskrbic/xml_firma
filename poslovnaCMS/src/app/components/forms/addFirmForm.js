(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('addFirmForm', function() {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'src/app/components/forms/addFirmForm.html',
                controller: 'AddFirmFormController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('AddFirmFormController', AddFirmFormController);

    AddFirmFormController.$inject = ['$scope', 'adminApi'];

    function AddFirmFormController($scope, adminApi) {

        $scope.model = {};
        $scope.errorCode = null;
        $scope.submit = submit;

        function submit(form) {
            if (form !== undefined && form.$submitted && form.$invalid) {
                return false;
            }
            adminApi.addFirm($scope.model).then(onSuccess, onError);

            function onSuccess(response) {
                swal('Success', 'You have successfully create new firm', 'success');
                localStorage.setItem('activeMenuItem', 'firms');
                $state.go("basePage.usersPage");
                $scope.errorCode = null;
            }

            function onError(response) {
                swal('Failure', response.data.message, 'error');
            }

        }

    }

})();
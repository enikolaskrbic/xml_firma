(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('resetPasswordForm', function() {
            return {
                restrict: 'E',
                scope: {
                    resetPasswordCode: '='
                },
                templateUrl: 'src/app/components/forms/resetPasswordForm.html',
                controller: 'ResetPasswordFormController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ResetPasswordFormController', ResetPasswordFormController);

    ResetPasswordFormController.$inject = ['$scope', '$state', 'authenticationApi'];

    function ResetPasswordFormController($scope, $state, authenticationApi) {

        $scope.model = {};
        $scope.errorCode = null;
        $scope.submit = submit;

        function submit(form) {
            if (form !== undefined && form.$submitted && form.$invalid) {
                return false;
            }
            authenticationApi.resetPassword($scope.model).then(onSuccess, onError);

            function onSuccess(response) {
                $state.go('signInPage');
                $scope.errorCode = null;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

    }

})();
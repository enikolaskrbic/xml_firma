(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('addUserForm', function() {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'src/app/components/forms/addUserForm.html',
                controller: 'AddUserFormController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('AddUserFormController', AddUserFormController);

    AddUserFormController.$inject = ['$scope', 'adminApi','$state'];

    function AddUserFormController($scope, adminApi,$state) {

        $scope.model = {};
        $scope.errorCode = null;
        $scope.submit = submit;
        $scope.loadFirms = loadFirms;

        loadFirms();


        function submit(form) {
            if (form !== undefined && form.$submitted && form.$invalid) {
                return false;
            }
            adminApi.addUser($scope.model).then(onSuccess, onError);

            function onSuccess(response) {
                swal('Success', 'You have successfully create new user', 'success');
                localStorage.setItem('activeMenuItem', 'users');
                $state.go("basePage.usersPage");
                $scope.errorCode = null;
            }

            function onError(response) {
                swal('Failure', response.data.message, 'error');
            }

        }

        function loadFirms() {

            adminApi.firms().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.listOfFirms = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

    }

})();
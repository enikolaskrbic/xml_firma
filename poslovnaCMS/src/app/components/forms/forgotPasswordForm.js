(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('forgotPasswordForm', function() {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'src/app/components/forms/forgotPasswordForm.html',
                controller: 'ForgotPasswordFormController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ForgotPasswordFormController', ForgotPasswordFormController);

    ForgotPasswordFormController.$inject = ['$scope', '$state', 'authenticationApi'];

    function ForgotPasswordFormController($scope, $state, authenticationApi) {

        $scope.model = {};
        $scope.errorCode = null;
        $scope.onClickSignIn = onClickSignIn;
        $scope.submit = submit;

        function submit(form) {
            if (form !== undefined && form.$submitted && form.$invalid) {
                return false;
            }
            authenticationApi.forgotPassword($scope.model).then(onSuccess, onError);

            function onSuccess(response) {
                $state.go('signInPage');
                $scope.errorCode = null;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function onClickSignIn() {
            $state.go('signInPage');
        }

    }

})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('signUpForm', function() {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'src/app/components/forms/signUpForm.html',
                controller: 'SignUpFormController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('SignUpFormController', SignUpFormController);

    SignUpFormController.$inject = ['$scope', '$state', 'authenticationApi'];

    function SignUpFormController($scope, $state, authenticationApi) {

        $scope.model = {};
        $scope.model.isDeleted = false;
        $scope.errorCode = null;
        $scope.onClickSignIn = onClickSignIn;
        $scope.submit = submit;

        function submit(form) {
            if (form !== undefined && form.$submitted && form.$invalid) {
                return false;
            }
            authenticationApi.signUp($scope.model).then(onSuccess, onError);

            function onSuccess(response) {
                $state.go('signInPage');
                $scope.errorCode = null;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function onClickSignIn() {
            $state.go('signInPage');
        }

    }

})();
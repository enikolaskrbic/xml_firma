(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('signInForm', function() {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'src/app/components/forms/signInForm.html',
                controller: 'SignInFormController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('SignInFormController', SignInFormController);

    SignInFormController.$inject = ['$scope', 'sessionService', 'eventBus', 'authenticationApi','$state'];

    function SignInFormController($scope, sessionService, eventBus, authenticationApi,$state) {

        $scope.model = {};
        $scope.errorCode = null;
        $scope.submit = submit;
        $scope.onClickForgotPassword = onClickForgotPassword;
        $scope.onClickSignUp = onClickSignUp;

        function submit(form) {
            if (form !== undefined && form.$submitted && form.$invalid) {
                return false;
            }
            authenticationApi.signIn($scope.model).then(onSuccess, onError);

            function onSuccess(response) {

                sessionService.save(response.data);
                localStorage.setItem('korisnickoIme',response.data.korisnickoIme);
                localStorage.setItem('rola',response.data.rola);
                localStorage.setItem('pibFirme',response.data.pibFirme);
                localStorage.setItem('apiFirme',response.data.apiFirme);
                eventBus.emitEvent('UserSignedIn', {
                    accessToken: response.data.accessToken,
                    id: response.data.id,
                    firstName: response.data.firstName,
                    lastName: response.data.lastName,
                    isDeleted: response.data.isDeleted,
                    timetamp: response.data.timetamp,
                    firmId: response.data.firmId,
                    role: response.data.role,
                    email: response.data.email
                });
                $scope.errorCode = null;
                if(response.data.role == 'ROLE_ADMIN'){
                    $state.go('basePage.companiesPage');
                    localStorage.setItem('activeMenuItem', 'firms');
                }else if(response.data.role == 'ROLE_MANAGER'){
                    $state.go('basePage.groupsPage');
                    localStorage.setItem('activeMenuItem', 'groups');
                }else if(response.data.rola = 'ROLE_FIRM'){
                    $state.go('basePage.aboutFirm');
                    localStorage.setItem('activeMenuItem', 'firms');
                }
            }

            function onError(response) {
                swal('Failure', response.data.message, 'error');
            }

        }

        function onClickForgotPassword() {
            $state.go('forgotPasswordPage');
        }


        function onClickSignUp() {
            $state.go('signUpPage');
        }

    }

})();
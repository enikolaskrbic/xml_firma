(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllSubGroups', function() {
            return {
                restrict: 'E',
                scope: {
                    group : '='
                },
                templateUrl: 'src/app/components/tables/listOfAllSubGroups.html',
                controller: 'ListOfAllSubGroupsController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllSubGroupsController', ListOfAllSubGroupsController);

    ListOfAllSubGroupsController.$inject = ['$scope', 'managerApi','$state','modalWindows'];

    function ListOfAllSubGroupsController($scope, managerApi,$state,modalWindows) {
        $scope.model = [];
        $scope.groupModels = [];
        $scope.errorCode = null;
        $scope.loadGroups = loadGroups;

        $scope.slectedGroup = localStorage.getItem('groupId');
        localStorage.removeItem('groupId');

        loadGroups();
        load($scope.slectedGroup);

        function load(data) {
            if(data=='' || data==null){
                managerApi.subgroups().then(onSuccess, onError);
            }else{
                var request = {
                    groupFirmId: $scope.slectedGroup,
                    drop: 0,
                    take: 25
                }
                managerApi.groupSubGroup(request).then(OnSuccessGroup, onError);
            }
            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }
            function OnSuccessGroup(response) {
                $scope.model = response.data.results;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function loadGroups() {
            managerApi.groups().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.modelGroups = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }
        }


        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.afterOk = afterOk;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];

        $scope.state ='Edit';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                //$scope.selectedModel.year = $scope.model[0].year;
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.id = $scope.model[selectedRow].id;
            $scope.selectedModel.group = $scope.model[selectedRow].groupId;
            $scope.selectedModel.name = $scope.model[selectedRow].name;
            $scope.selectedModel.groupName = $scope.model[selectedRow].groupFirmName;

        }
        function submit() {

            if($scope.state=='Add'){
                if(validate()==false){
                    return;
                }

                var data= {
                    id: $scope.selectedModel.group,
                    name: $scope.selectedModel.name
                }
                managerApi.addSubGroup(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal('Failure', 'Nothing to edit!', 'error');
                    return false;
                }
                var data= {
                    id: $scope.selectedModel.id,
                    group: $scope.selectedModel.group,
                    name: $scope.selectedModel.name
                }
                managerApi.editSubGroup(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Search'){
                var data= {
                    subGroupGroupId: $scope.selectedModel.group,
                    subGroupName: $scope.selectedModel.name
                }
                managerApi.searchSubGroups(data).then(onSuccessSearch, onErrorSearch);
            }

            function onSuccessSubmit(response) {
                $state.reload();
                swal('Success', 'You have successfully '+$scope.state.toLowerCase()+ ' subgroup.', 'success');
            }
            function onErrorSubmit(response) {
                swal("Failure",response.data.message,"error");
            }
            function onSuccessSearch(response) {
                $scope.model = response.data;
                initModel();
            }
            function onErrorSearch(response) {

            }
        }

        function validate() {
            if($scope.selectedModel.group=='' || $scope.selectedModel.group==null ||
                $scope.selectedModel.name=='' || $scope.selectedModel.name==null){
                swal('Failure', 'Group and name cant be empty!', 'error');
                return false;
            }
            return true;

        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Edit';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.nextState = function () {
            localStorage.setItem('subGroupId',$scope.selectedModel.id);
            $state.go("basePage.articlePage");
        }

        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteSubGroupHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }

        $scope.onClickLoadGroup = onClickLoadGroup;

        function onClickLoadGroup() {
            modalWindows.openModalGroups({
                reload: afterOk
            });

        }

        function afterOk(data) {

            $scope.selectedModel.group = data;
        }


    }
})();
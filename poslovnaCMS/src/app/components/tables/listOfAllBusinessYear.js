(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllBusinessYear', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllBusinessYear.html',
                controller: 'ListOfAllBusinessYearController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllBusinessYearController', ListOfAllBusinessYearController);

    ListOfAllBusinessYearController.$inject = ['$scope', 'managerApi','$state'];

    function ListOfAllBusinessYearController($scope, managerApi,$state) {
        $scope.model = [];
        $scope.errorCode = null;
        $scope.initModel = initModel;
        $scope.getStatus = getStatus;
        $scope.finishYear = finishYear;

        load();

        function load() {

            managerApi.businessYears().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }


        function getStatus(item) {
            if(item.isFinished){
                return 'Finished';
            }else{
                return 'Active';
            }
        }

        function finishYear(item) {
            if(item.id=='' || item.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            $scope.itemYearId = item.id;
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, finish);
        }

        $scope.finish = finish;
        function finish() {
            var data= {
                id: $scope.itemYearId,
            }

            managerApi.finishYear(data).then(onSuccess, onError);

            function onSuccess() {

                $state.reload();
                swal('Success', 'You have successfully finish business year', 'success');
            }
            function onError(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Edit';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.id = $scope.model[selectedRow].id;
            $scope.selectedModel.year = parseInt($scope.model[selectedRow].year,10);
            $scope.selectedModel.isFinished = $scope.model[selectedRow].isFinished;
        }
        function submit() {


            if($scope.state=='Add'){

                if(validate()==false){

                    return;
                }

                var data= {
                    year: $scope.selectedModel.year,
                    isFinished : false
                }

                managerApi.addBussinesYear(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal('Failure', 'Nothing to edit!', 'error');
                    return false;
                }

                var data= {
                    id : $scope.selectedModel.id,
                    year: $scope.selectedModel.year,
                    isFinished : $scope.selectedModel.isFinished
                }
                managerApi.editBussinesYear(data).then(onSuccessSubmit, onErrorSubmit);
            }else if($scope.state=='Search'){

                var data= {
                    businessYearYear: $scope.selectedModel.year,
                    businessYearIsFinished: $scope.selectedModel.isFinished
                }
                managerApi.businessYearsSearch(data).then(onSuccessSearch, onErrorSearch);
            }

            function onSuccessSearch(response) {
                $scope.model = response.data;
                initModel();
            }
            function onErrorSearch(response) {

            }

            function onSuccessSubmit() {

                $state.reload();
                swal('Success', 'You have successfully '+$scope.state.toLowerCase()+' business year', 'success');
            }
            function onErrorSubmit(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {
            if($scope.selectedModel.year=='' || $scope.selectedModel.year==null){
                swal('Failure', 'Year cant be empty!', 'error');
                return false;
            }
            if($scope.selectedModel.year.toString().length!=4 ){
                swal('Failure', 'Length must be 4!', 'error');
                return false;
            }
            var y = parseInt($scope.selectedModel.year,10);
            if(y<=0){
                swal('Failure', 'Wrong format', 'error');
                return false;
            }
            return true;

        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Edit';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }



        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteBusinessYearHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }


    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('modalPrelazFakture', function() {
            return {
                restrict: 'E',
                scope: {
                    closeModal : "=",
                    reload: '='
                },
                templateUrl: 'src/app/components/tables/modalPrelazFakture.html',
                controller: 'ModalPrelazFaktureController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ModalPrelazFaktureController', ModalPrelazFaktureController);

    ModalPrelazFaktureController.$inject = ['$scope', 'managerApi'];

    function ModalPrelazFaktureController($scope, managerApi) {
        $scope.model = [
            {
                broj : 1,
                naziv : 'Prelaz na stavke'
            },
            {
                broj : 2,
                naziv : 'Prelaz na nalog za placanje'
            }
        ];
        $scope.errorCode = null;


        $scope.updateSelectedModel = updateSelectedModel;
        $scope.ok = ok;

        function ok() {
            if($scope.selectedModel===null){
                swal('Warning','Selektujte stranicu','error');
                return;
            }
            $scope.closeModal();
            $scope.reload($scope.selectedModel);

        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel = $scope.model[selectedRow];

        }

        $scope.selectedModel = null;
        $scope.rowHighilited = function( row){
            if($scope.selectedModel===null)
                $scope.selectedModel=[];
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

    }
})();
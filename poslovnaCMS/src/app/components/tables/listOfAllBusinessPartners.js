(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllBusinessPartners', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllBusinessPartners.html',
                controller: 'ListOfAllBusinessPartnersController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllBusinessPartnersController', ListOfAllBusinessPartnersController);

    ListOfAllBusinessPartnersController.$inject = ['$scope', 'managerApi','$state'];

    function ListOfAllBusinessPartnersController($scope, managerApi,$state) {
        $scope.model = [];
        $scope.errorCode = null;
        $scope.initModel = initModel;

        load();

        function load() {

            managerApi.businessPartners().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Edit';
        $scope.selectedModel.type = 'BUYER';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.id = $scope.model[selectedRow].id;
            $scope.selectedModel.name = $scope.model[selectedRow].name;
            $scope.selectedModel.address = $scope.model[selectedRow].address;
            $scope.selectedModel.city = $scope.model[selectedRow].city;
            $scope.selectedModel.country = $scope.model[selectedRow].country;
            $scope.selectedModel.web = $scope.model[selectedRow].web;
            $scope.selectedModel.email = $scope.model[selectedRow].email;
            $scope.selectedModel.phone = $scope.model[selectedRow].phone;
            $scope.selectedModel.type = $scope.model[selectedRow].status;
        }
        function submit() {

            if($scope.state=='Add'){
                if(validate()==false){
                    return;
                }
                var data= {
                    name: $scope.selectedModel.name,
                    address: $scope.selectedModel.address,
                    city: $scope.selectedModel.city,
                    country: $scope.selectedModel.country,
                    email: $scope.selectedModel.email,
                    phone: $scope.selectedModel.phone,
                    web: $scope.selectedModel.web,
                    status: $scope.selectedModel.type
                }
                managerApi.addBussinesPartner(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal('Failure', 'Nothing to edit!', 'error');
                    return false;
                }
                var data= {
                    idPartner: $scope.selectedModel.id,
                    name: $scope.selectedModel.name,
                    address: $scope.selectedModel.address,
                    city: $scope.selectedModel.city,
                    country: $scope.selectedModel.country,
                    email: $scope.selectedModel.email,
                    phone: $scope.selectedModel.phone,
                    web: $scope.selectedModel.web,
                    status: $scope.selectedModel.type
                }
                managerApi.editBussinesPartner(data).then(onSuccessSubmit, onErrorSubmit);
            }else if($scope.state=='Search'){
                var data= {
                    businessPartnerName: $scope.selectedModel.name,
                    businessPartnerAddress: $scope.selectedModel.address,
                    businessPartnerCity: $scope.selectedModel.city,
                    businessPartnerCountry: $scope.selectedModel.country,
                    businessPartnerWeb: $scope.selectedModel.web,
                    businessPartnerEmail: $scope.selectedModel.email,
                    businessPartnerPhone: $scope.selectedModel.phone,
                    businessPartnerStatus: $scope.selectedModel.type
                }
                managerApi.businessPartnersSearch(data).then(onSuccessSearch, onErrorSearch);
            }

            function onSuccessSearch(response) {
                $scope.model = response.data;
                initModel();
            }
            function onErrorSearch(response) {

            }


            function onSuccessSubmit() {
                $state.reload();
                swal('Success', 'You have successfully '+$scope.state.toLowerCase()+' business year', 'success');
            }
            function onErrorSubmit(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {
            if($scope.selectedModel.name=='' || $scope.selectedModel.name==null ||
                $scope.selectedModel.address=='' || $scope.selectedModel.address==null ||
                $scope.selectedModel.city=='' || $scope.selectedModel.city==null ||
                $scope.selectedModel.country=='' || $scope.selectedModel.country==null ||
                $scope.selectedModel.type=='' || $scope.selectedModel.type==null){
                swal('Failure', 'Name, address, city and country cant be empty!', 'error');
                return false;
            }
            return true;
        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Edit';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }


        $scope.nextState = function () {
            localStorage.setItem('businessPartnerId',$scope.selectedModel.id);
            $state.go("basePage.documentsPage");
        }

        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteBusinessPartnerHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllCompanies', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllCompanies.html',
                controller: 'ListOfAllCompaniesController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllCompaniesController', ListOfAllCompaniesController);


    ListOfAllCompaniesController.$inject = ['$scope', 'adminApi'];

    function ListOfAllCompaniesController($scope, adminApi) {
        $scope.model = [];
        $scope.errorCode = null;
        load();


        function load() {

            adminApi.firms().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        //toolbar actions
        $scope.selectedRow =0;

        $scope.rowHighilited = function( row){
            $scope.selectedRow = row;
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1)
                $scope.selectedRow = $scope.selectedRow+1;
        }
        $scope.previousRow = function () {
            if($scope.selectedRow>0)
                $scope.selectedRow = $scope.selectedRow-1;
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
        }

    }
})();
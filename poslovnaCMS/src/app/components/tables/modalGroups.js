(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('modalGroups', function() {
            return {
                restrict: 'E',
                scope: {
                    closeModal : "=",
                    reload: '='
                },
                templateUrl: 'src/app/components/tables/modalGroups.html',
                controller: 'ModalGroupsController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ModalGroupsController', ModalGroupsController);

    ModalGroupsController.$inject = ['$scope', 'managerApi','modalWindows','eventBus'];

    function ModalGroupsController($scope, managerApi,modalWindows,eventBus) {
        $scope.model = [];
        $scope.errorCode = null;

        load();

        function load() {
            managerApi.groups().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        $scope.updateSelectedModel = updateSelectedModel;
        $scope.ok = ok;
        
        function ok() {
            if($scope.selectedModel===null){
                swal('Warning','Select a group!','error');
                return;
            }
            $scope.closeModal();
            $scope.reload($scope.selectedModel.id);

        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel = $scope.model[selectedRow];

        }

        $scope.selectedModel = null;
        $scope.rowHighilited = function( row){
            if($scope.selectedModel===null)
                $scope.selectedModel=[];
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllDocuments', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllDocuments.html',
                controller: 'ListOfAllDocumentsController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllDocumentsController', ListOfAllDocumentsController);

    ListOfAllDocumentsController.$inject = ['$scope', 'managerApi','modalWindows','$state','$filter'];

    function ListOfAllDocumentsController($scope, managerApi,modalWindows,$state,$filter) {
        $scope.model = [];
        $scope.errorCode = null;

        var retrievedData = localStorage.getItem("aboutFirm");
        var aboutFirm = JSON.parse(retrievedData);

        $scope.nazivDobavljaca = aboutFirm.naziv;
        $scope.adresaDobavljaca = aboutFirm.adresa;
        $scope.pibDobavljaca = aboutFirm.pib;



        loadOstaleFirme();
        loadAboutFirma()
        load();

        function load() {
            var req ={
                pib: localStorage.getItem('pibFirme'),
                api: localStorage.getItem('apiFirme')
            }
            managerApi.getIzlazneFakture(req).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }




        function loadAboutFirma() {

            var req ={
                pib: localStorage.getItem('pibFirme'),
                api: localStorage.getItem('apiFirme')
            }

            managerApi.getAboutFirm(req).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.listaRacuna = response.data.racun;

            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }



        function loadFirma(pibFirme) {

            var req ={
                pib: pibFirme,
                api: localStorage.getItem('apiFirme')
            }

            managerApi.getAboutFirm(req).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.modelFirma = response.data;

            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function loadOstaleFirme() {


            var req ={
                pib: localStorage.getItem('pibFirme'),
                api: localStorage.getItem('apiFirme')

            }
            managerApi.getOstaleFirme(req).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.modelOstaleFirme = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }



        $scope.kupacChange= function() {
            var newTemp = $filter("filter")($scope.modelOstaleFirme, { pib : $scope.selectedModel.pibKupca});
            $scope.selectedModel.nazivKupca=newTemp[0].naziv;
            $scope.selectedModel.adresaKupca=newTemp[0].adresa;
        }





        $scope.posaljiFakturu = posaljiFakturu;

        function posaljiFakturu(item) {
            $scope.item = item;
            swal({
                title: "Da li ste sigurni?",
                text: "Poslacete selektovanu fakturu.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            },posalji);
        }

        function posalji() {
            loadFirma($scope.model[$scope.selectedRow].zaglavljeFakture.pibKupca);

            var data= {
                pib: localStorage.getItem('pibFirme'),
                faktura: $scope.model[$scope.selectedRow],
                api: $scope.modelFirma.putanjaServera
            }
            managerApi.createUlaznaFaktura(data).then(onSuccess, onError);

            function onSuccess(response) {
                $state.reload();
                swal('Success', 'Uspesno poslata faktura', 'success');
            }

            function onError(response) {
                swal('Failure', response.data.message, 'error');
            }
        }



        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Add';
        $scope.selectedModel.type = 'BUYER';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {

            $scope.selectedModel.idPoruke = $scope.model[selectedRow].idPoruke;
            $scope.selectedModel.pibKupca = $scope.model[selectedRow].zaglavljeFakture.pibKupca;
            $scope.selectedModel.brojRacuna = $scope.model[selectedRow].zaglavljeFakture.brojRacuna;
            $scope.selectedModel.uplataNaRacun = $scope.model[selectedRow].zaglavljeFakture.uplataNaRacun;
            $scope.selectedModel.datumRacuna = $scope.model[selectedRow].zaglavljeFakture.datumRacuna;
            $scope.selectedModel.datumValute = $scope.model[selectedRow].zaglavljeFakture.datumValute;

        }
        function submit() {

            if($scope.state=='Add'){
                if(validate()==false){
                    return;
                }


                var data = {
                        pib: localStorage.getItem('pibFirme'),
                        api: localStorage.getItem('apiFirme'),
                        idPoruke: "cao buraz",
                        zaglavljeFakture : {
                            nazivDobavljaca: $scope.nazivDobavljaca,
                            adresaDobavljaca: $scope.adresaDobavljaca,
                            pibDobavljaca: $scope.pibDobavljaca.toString(),
                            nazivKupca: $scope.selectedModel.nazivKupca,
                            adresaKupca: $scope.selectedModel.adresaKupca,
                            pibKupca: $scope.selectedModel.pibKupca.toString(),
                            brojRacuna: $scope.selectedModel.brojRacuna,
                            vrednostRobe: 0,
                            vrednostUsluga: 0,
                            upunoRobeIUsluga: 0,
                            ukupanRabat: 0,
                            ukupanPorez: 0,
                            oznakaValute: $scope.selectedModel.oznakaValute,
                            iznosZaUplatu: 0,
                            uplataNaRacun: $scope.selectedModel.uplataNaRacun.brojRacuna,
                            datumRacuna: $scope.selectedModel.datumRacuna,
                            datumValute: $scope.selectedModel.datumValute
                    }

                }
                //console.log(data)
                managerApi.createIzlaznaFaktura(data).then(onSuccessSubmit, onErrorSubmit);



            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal('Failure', 'Nothing to edit!', 'error');
                    return false;
                }


            }

            function onSuccessSubmit() {
                $state.reload();
                swal('Success', 'Uspesno dodata faktura!', 'success');
            }
            function onErrorSubmit(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {



            return true;
        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Add';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.nextState = function () {
            if($scope.selectedModel.idPoruke!=null || $scope.selectedModel.idPoruke!=""){
                localStorage.setItem('idPoruke',$scope.selectedModel.idPoruke);
                $state.go("basePage.documentItemsPage");
            }

        }
        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }

        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteDocumentHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }

        $scope.onClickLoadBusinesPartner = onClickLoadBusinesPartner;
        $scope.onClickLoadWarehouse = onClickLoadWarehouse;
        $scope.onClickLoadWarehouseOut = onClickLoadWarehouseOut;
        $scope.onClickLoadYear = onClickLoadYear;

        function onClickLoadBusinesPartner() {
            modalWindows.openModalBussinesPartner({
                reload: afterBusinessPartner
            });
        }

        function onClickLoadWarehouse() {
            modalWindows.openModalWarehouse({
                reload: afterWarehouse
            });
        }
        function onClickLoadYear() {
            modalWindows.openModalBussinesYear({
                reload: afterYear
            });
        }

        function onClickLoadWarehouseOut() {
            modalWindows.openModalWarehouse({
                reload: afterWarehouseOut
            });
        }

        function afterYear(data) {
            $scope.selectedModel.businessYearId = data.id;
        }

        function afterWarehouse(data) {
            $scope.selectedModel.warehouseId = data.id;
        }

        function afterWarehouseOut(data) {
            $scope.selectedModel.warehouseOutId = data.id;
            warehouseOutChange();
        }

        function afterBusinessPartner(data) {
            $scope.selectedModel.businessPartnerId = data.id;
            $scope.selectedModel.businessPartnerStatus = data.status;
            partnerChange();
        }

        $scope.partnerChange = partnerChange
        $scope.warehouseOutChange = warehouseOutChange;

        function partnerChange() {
            var newTemp = $filter("filter")($scope.modelBusinessPartner, { id : $scope.selectedModel.businessPartnerId});
            if(newTemp[0].status=='BUYER'){
                $scope.selectedModel.type = 'OUTPUT'
            }else if(newTemp[0].status=='SUPPLIER'){
                $scope.selectedModel.type = 'INPUT'
            }
            $scope.selectedModel.warehouseOutId = null;
        }

        function warehouseOutChange() {
            $scope.selectedModel.type = 'BETWEEN'
            $scope.selectedModel.businessPartnerId = null;
        }


    }
})();
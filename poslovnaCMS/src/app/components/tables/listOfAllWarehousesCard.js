(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllWarehousesCard', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllWarehousesCard.html',
                controller: 'ListOfAllWarehousesCardController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllWarehousesCardController', ListOfAllWarehousesCardController);

    ListOfAllWarehousesCardController.$inject = ['$scope', 'managerApi','modalWindows','$state'];

    function ListOfAllWarehousesCardController($scope, managerApi,modalWindows,$state) {
        $scope.model = [];
        $scope.errorCode = null;
        $scope.initModel = initModel;
        $scope.warehouseCardPdf =warehouseCardPdf;

        $scope.slectedWarehouse = localStorage.getItem('warehouseId');
        localStorage.removeItem('warehouseId');

        load($scope.slectedWarehouse);

        function load(data) {

            if(data=='' || data==null){
                managerApi.warehousesCard().then(onSuccess, onError);
            }else{
                var request = {
                    warehouseId: $scope.slectedWarehouse,
                    drop: 0,
                    take: 25
                }
                managerApi.warehouseWarehouseCard(request).then(OnSuccessWarehouse, onError);
            }


            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function OnSuccessWarehouse(response) {
                $scope.model = response.data.results;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function warehouseCardPdf(item) {
            var data ={
                warehouseCardId : item.id
            }

            managerApi.getWarehouseCardReport(data).then(onSuccess, onError);

            function onSuccess(response) {

                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL =  window.URL.createObjectURL(file);
                //$scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                window.open(fileURL);
            }
            function onError(response) {
                swal("Failure",'PDF is empty.',"error");            }
        }

        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Search';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.id = $scope.model[selectedRow].id;
            $scope.selectedModel.businessYear = $scope.model[selectedRow].businessYearYear;
            $scope.selectedModel.articleName = $scope.model[selectedRow].articleName;
            $scope.selectedModel.articleCode = $scope.model[selectedRow].articleCode
            $scope.selectedModel.warehouseName = $scope.model[selectedRow].id;
            $scope.selectedModel.input = $scope.model[selectedRow].input;
            $scope.selectedModel.output = $scope.model[selectedRow].output;
            $scope.selectedModel.balance = $scope.model[selectedRow].balance;
            $scope.selectedModel.inValue = $scope.model[selectedRow].inValue;
            $scope.selectedModel.outValue = $scope.model[selectedRow].outValue;
            $scope.selectedModel.totalValue = $scope.model[selectedRow].totalValue;

        }
        function submit() {

            if($scope.state=='Search'){
                var data = {
                    businessYearYear: $scope.selectedModel.year,
                    articleName: $scope.selectedModel.name,
                    articleCode: $scope.selectedModel.code,
                    warehouseCardInput: $scope.selectedModel.input,
                    warehouseCardOutput: $scope.selectedModel.output,
                    warehouseCardBalance: $scope.selectedModel.balance,
                    warehouseCardInValue: $scope.selectedModel.inValue,
                    warehouseCardOutValue: $scope.selectedModel.outValue,
                    warehouseCardTotalValue: $scope.selectedModel.totalValue,
                    warehouseName: $scope.selectedModel.warehouseName
                }

                managerApi.warehousesCardSearch(data).then(onSuccessSearch, onErrorSearch);
            }

            function onSuccessSearch(response) {
                $scope.model = response.data;
                initModel();
            }
            function onErrorSearch(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {
            if($scope.selectedModel.businessYearId=='' || $scope.selectedModel.businessYearId==null ||
                $scope.selectedModel.articleId=='' || $scope.selectedModel.articleId==null ||
                $scope.selectedModel.warehouseId=='' || $scope.selectedModel.warehouseId==null){
                swal('Failure', 'Name, code, address, city and country cant be empty!', 'error');
                return false;
            }
            return true;
        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Edit';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Search';
            // $scope.selectedModel = [];
            // $scope.selectedRow = -1;
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.nextState = function () {
            if($scope.selectedModel.id!=null){
                localStorage.setItem('warehouseCardId',$scope.selectedModel.id);
                $state.go("basePage.analyticsPage");
            }

        }

        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }

        $scope.onClickLoadYear = onClickLoadYear;
        $scope.onClickLoadArticle = onClickLoadArticle;
        $scope.onClickLoadWarehouse = onClickLoadWarehouse;
        $scope.afterYear = afterYear;
        $scope.afterArticle = afterArticle;
        $scope.afterWarehouse = afterWarehouse;



        function onClickLoadYear() {
            modalWindows.openModalBussinesYear({
                reload: afterYear
            });
        }

        function onClickLoadArticle() {
            modalWindows.openModalArticles({
                reload: afterArticle
            });
        }

        function onClickLoadWarehouse() {
            modalWindows.openModalWarehouse({
                reload: afterWarehouse
            });
        }

        function afterYear(data) {
            $scope.selectedModel.businessYear = data.year;
        }

        function afterArticle(data) {
            $scope.selectedModel.articleName = data.name;
            $scope.selectedModel.articleCode = data.code;
        }

        function afterWarehouse(data) {
            $scope.selectedModel.warehouseName= data.name;
        }

    }
})();
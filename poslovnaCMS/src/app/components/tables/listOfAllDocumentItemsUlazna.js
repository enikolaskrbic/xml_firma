(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllDocumentItemsUlazna', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllDocumentItemsUlazna.html',
                controller: 'ListOfAllDocumentItemsUlaznaController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllDocumentItemsUlaznaController', ListOfAllDocumentItemsUlaznaController);

    ListOfAllDocumentItemsUlaznaController.$inject = ['$scope', 'managerApi','$state','modalWindows'];

    function ListOfAllDocumentItemsUlaznaController($scope, managerApi,$state,modalWindows) {
        $scope.model = [];
        $scope.errorCode = null;
        $scope.changeArticle =changeArticle;

        $scope.idPoruke = localStorage.getItem('idPoruke');


        //loadArticles();

        if ($scope.idPoruke) load($scope.idPoruke);


        function load(idPoruke) {
            var request = {
                pib: localStorage.getItem('pibFirme'),
                api: localStorage.getItem('apiFirme'),
                idPoruke: idPoruke
            };
            managerApi.getStavkeUlazneFakture(request).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }



        function changeArticle() {
            if($scope.typeDocument=='INPUT')
                return;
            var request = {
                articleId: $scope.selectedModel.articleId,
                documentId : $scope.documentId
            };
            managerApi.getArticlePrice(request).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.selectedModel.price =response.data;
            }

            function onError(response) {
                $scope.selectedModel.price = null;
                $scope.selectedModel.articleId = null;
                $scope.selectedModel.quantity = null;
                swal('Failure', response.data.message, 'error');
            }
        }

        $scope.isEdit=function(){
            if($scope.state=='Edit'){
                return true;
            }
            return false;
        }

        $scope.isDisabledPrice=function(){
            if($scope.typeDocument=='INPUT'){
                return false;
            }
            return true;
        }



        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Add';
        $scope.selectedModel.type = 'BUYER';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                updateSelectedModel(0);
            }
        }


        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.redniBroj = $scope.model[selectedRow].redniBroj;
            $scope.selectedModel.nazivRobeIliUsluge = $scope.model[selectedRow].nazivRobeIliUsluge;
            $scope.selectedModel.kolicina = $scope.model[selectedRow].kolicina;
            $scope.selectedModel.jedinicnaCena = $scope.model[selectedRow].jedinicnaCena;
            $scope.selectedModel.jedinicaMere = $scope.model[selectedRow].jedinicaMere;
            $scope.selectedModel.procenatRabata = $scope.model[selectedRow].procenatRabata;
            $scope.selectedModel.iznosRabata = $scope.model[selectedRow].iznosRabata;
            $scope.selectedModel.umanjenoZaRabat = $scope.model[selectedRow].umanjenoZaRabat;
            $scope.selectedModel.ukupanPorez = $scope.model[selectedRow].ukupanPorez;
            $scope.selectedModel.vrednost = $scope.model[selectedRow].vrednost;

        }
        function submit() {

            if($scope.state=='Add'){
                if(validate()==false){
                    return;
                }

                var data = {
                    pib: localStorage.getItem('pibFirme'),
                    api: localStorage.getItem('apiFirme'),
                    idPoruke: $scope.idPoruke,
                    iznosRabata: 0,
                    jedinicaMere: $scope.selectedModel.jedinicaMere,
                    jedinicnaCena: $scope.selectedModel.jedinicnaCena,
                    kolicina: $scope.selectedModel.kolicina,
                    nazivRobeIliUsluge: $scope.selectedModel.nazivRobeIliUsluge,
                    procenatRabata: $scope.selectedModel.procenatRabata,
                    redniBroj: ($scope.model.length+1),
                    ukupanPorez: 0,
                    umanjenoZaRabat: 0,
                    vrednost: 0
                }
                managerApi.createStavkaIzlazneFakture(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal('Failure', 'Nothing to edit!', 'error');
                    return false;
                }


            }else if($scope.state=='Search'){
                var data= {
                    //todo izmeniti search
                    documentId: $scope.documentId
                }

            }

            function onSuccessSubmit() {
                $state.reload();
                swal('Success', 'Uspesno krirana stavka.', 'success');
            }
            function onErrorSubmit(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {


            return true;
        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Add';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.nextState = function () {
            if($scope.selectedModel.id!=null){
                modalWindows.openModalPrelazFakture({
                    reload: afterChoose
                });

            }

        }

        function afterChoose(data) {
            if(data.broj==1){
                $state.go("basePage.analyticsPage");
            }

        }
        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteDocumentItemHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }

        $scope.onClickLoadArticle = onClickLoadArticle;

        function onClickLoadArticle() {
            modalWindows.openModalArticles({
                reload: afterArticle
            });
        }

        function afterArticle(data) {
            $scope.selectedModel.articleId = data.id;
            changeArticle();
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllWarehouses', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllWarehouses.html',
                controller: 'ListOfAllWarehousesController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllWarehousesController', ListOfAllWarehousesController);

    ListOfAllWarehousesController.$inject = ['$scope', 'managerApi','$state','$sce'];

    function ListOfAllWarehousesController($scope, managerApi,$state,$sce) {
        $scope.model = [];
        $scope.errorCode = null;
        $scope.initModel = initModel;
        $scope.levelingWarehouse = levelingWarehouse;

        load();

        function load() {

            managerApi.warehouses().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }


        function levelingWarehouse(item) {
            if(item.id=='' || item.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }

            $scope.levelItemId = item.id;
            swal({
                title: "Are you sure?",
                text: "You will leveling selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, leveling);
        }

        $scope.leveling= leveling;

        function leveling() {

            var request={
                warehouseId:$scope.levelItemId
            }

            managerApi.levelingWarehouse(request).then(onSuccessLeveling, onErrorLeveling);



        }
        function onSuccessLeveling(response) {
            swal('Success', 'Leveling successful!', 'success');
        }

        function onErrorLeveling(response) {
            swal('Failure', response.data.message, 'error');
        }


        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Edit';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.id = $scope.model[selectedRow].id;
            $scope.selectedModel.name = $scope.model[selectedRow].name;
            $scope.selectedModel.address = $scope.model[selectedRow].address;
            $scope.selectedModel.city = $scope.model[selectedRow].city;
            $scope.selectedModel.country = $scope.model[selectedRow].country;
            $scope.selectedModel.code = $scope.model[selectedRow].code;
        }
        function submit() {

            if($scope.state=='Add'){
                if(validate()==false){
                    return;
                }
                var data= {
                    name:$scope.selectedModel.name,
                    code: $scope.selectedModel.code,
                    address: $scope.selectedModel.address,
                    city: $scope.selectedModel.city,
                    country: $scope.selectedModel.country,

                }
                managerApi.addWarehouse(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal('Failure', 'Nothing to edit!', 'error');
                    return false;
                }
                var data= {
                    id: $scope.selectedModel.id,
                    name:$scope.selectedModel.name,
                    code: $scope.selectedModel.code,
                    address: $scope.selectedModel.address,
                    city: $scope.selectedModel.city,
                    country: $scope.selectedModel.country
                }
                managerApi.editWarehouse(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Search'){
                var data = {
                    warehouseCode: $scope.selectedModel.code,
                    warehouseName: $scope.selectedModel.name,
                    warehouseAddress: $scope.selectedModel.address,
                    warehouseCity: $scope.selectedModel.city,
                    warehouseCountry: $scope.selectedModel.country
                }
                managerApi.warehousesSearch(data).then(onSuccessSearch, onErrorSearch);
            }


            function onSuccessSubmit() {
                $state.reload();
                swal('Success', 'You have successfully '  +$scope.state.toLowerCase()+' business year', 'success');
            }
            function onErrorSubmit(response) {
                swal("Failure",response.data.message,"error");
            }

            function onSuccessSearch(response) {
                $scope.model = response.data;
                initModel();
            }
            function onErrorSearch(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {
            if($scope.selectedModel.name=='' || $scope.selectedModel.name==null ||
                $scope.selectedModel.address=='' || $scope.selectedModel.address==null ||
                $scope.selectedModel.city=='' || $scope.selectedModel.city==null ||
                $scope.selectedModel.country=='' || $scope.selectedModel.country==null ||
                $scope.selectedModel.code=='' || $scope.selectedModel.code==null){
                swal('Failure', 'Name, code, address, city and country cant be empty!', 'error');
                return false;
            }
            return true;
        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Edit';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.nextState = function () {
            localStorage.setItem('warehouseId',$scope.selectedModel.id);
            $state.go("basePage.warehouseCardPage");
        }

        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }

        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteWarehouseHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }

        $scope.lagerListReport = lagerListReport;

        function lagerListReport(item) {
            var data ={
                warehouseId : item.id
            }

            managerApi.getLagerListReport(data).then(onSuccess, onError);

            function onSuccess(response) {

                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL =  window.URL.createObjectURL(file);
                //$scope.pdfContent = $sce.trustAsResourceUrl(fileURL);


                window.open(fileURL);
            }
            function onError(response) {
                swal("Failure",'PDF is empty.',"error");

            }
        }

    }
})();
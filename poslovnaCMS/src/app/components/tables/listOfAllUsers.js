(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllUsers', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllUsers.html',
                controller: 'ListOfAllUsersController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllUsersController', ListOfAllUsersController);

    ListOfAllUsersController.$inject = ['$scope', 'adminApi'];

    function ListOfAllUsersController($scope, adminApi) {
        $scope.model = [];
        $scope.errorCode = null;

        load();

        function load() {

            adminApi.users().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

    }
})();
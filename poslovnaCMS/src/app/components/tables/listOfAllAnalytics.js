(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllAnalytics', function() {
            return {
                restrict: 'E',
                scope: {
                    warehouseCardId: '='
                },
                templateUrl: 'src/app/components/tables/listOfAllAnalytics.html',
                controller: 'ListOfAllAnalyticsController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllAnalyticsController', ListOfAllAnalyticsController);

    ListOfAllAnalyticsController.$inject = ['$scope', 'managerApi'];

    function ListOfAllAnalyticsController($scope, managerApi) {
        $scope.model = [];
        $scope.errorCode = null;
        $scope.hitno=false;

        loadFirma();

        var retrievedData = localStorage.getItem("aboutNalog");
        $scope.selectedModel = JSON.parse(retrievedData);
        localStorage.removeItem("aboutNalog");

        if ($scope.warehouseCardId) load($scope.warehouseCardId);

        function load(warehouseCardId) {
            var request = {
                warehouseCardId: warehouseCardId
            };
            //managerApi.analytics(request).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function loadFirma() {

            var req ={
                pib: localStorage.getItem('pibFirme'),
                api: localStorage.getItem('apiFirme')
            }

            managerApi.getAboutFirm(req).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.modelFirma = response.data;

            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        $scope.submit = submit;
        function submit() {

            var req={
                api:localStorage.getItem('apiFirme'),
                apiBanke:$scope.selectedModel.racunDuznika.nazivBanke.value,
                nalog:{
                    datumNaloga: $scope.selectedModel.datumValute,
                    datumRacuna: $scope.selectedModel.datumRacuna,
                    hitno: $scope.hitno,
                    iznos: $scope.selectedModel.iznosZaUplatu,
                    oznakaValute:$scope.selectedModel.oznakaValute,
                    podaciDuznika: {
                        model: $scope.selectedModel.modelZaduzenja,
                        naziv: $scope.selectedModel.nazivDobavljaca,
                        poziv: $scope.selectedModel.pozivZaduzenja,
                        racun: $scope.selectedModel.racunDuznika.brojRacuna
                    },
                    podaciPoverioca: {
                        model: $scope.selectedModel.modelOdobrenja,
                        naziv: $scope.selectedModel.nazivKupca,
                        poziv: $scope.selectedModel.pozivOdobrenja,
                        racun: $scope.selectedModel.uplataNaRacun
                    },
                    svrhaPlacanja: $scope.selectedModel.svrhaPlacanja
                }
            }

            managerApi.platiNalog(req).then(onSuccess, onError);

            function onSuccess(response) {
                swal('Success', "Uspesno!", 'success');

            }

            function onError(response) {
                swal('Failure', response.data.message, 'error');
            }


        }



    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllUnitOfMeasures', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllUnitOfMeasures.html',
                controller: 'ListOfAllUnitOfMeasuresController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllUnitOfMeasuresController', ListOfAllUnitOfMeasuresController);

    ListOfAllUnitOfMeasuresController.$inject = ['$scope', 'managerApi','$state'];

    function ListOfAllUnitOfMeasuresController($scope, managerApi,$state) {
        $scope.model = [];
        $scope.errorCode = null;

        load();

        function load() {

            managerApi.unitOfMeasures().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Edit';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                //$scope.selectedModel.year = $scope.model[0].year;
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.id = $scope.model[selectedRow].id;
            $scope.selectedModel.name = $scope.model[selectedRow].name;
            $scope.selectedModel.shortName = $scope.model[selectedRow].shortName;
        }
        function submit() {

            if($scope.state=='Add'){
                if(validate()==false){
                    return;
                }
                var data= {
                    name: $scope.selectedModel.name,
                    shortName : $scope.selectedModel.shortName
                }
                managerApi.addUnitOfMeasure(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal('Failure', 'Nothing to edit!', 'error');
                    return false;
                }
                var data= {
                    id: $scope.selectedModel.id,
                    name: $scope.selectedModel.name,
                    shortName : $scope.selectedModel.shortName
                }
                managerApi.editUnitOfMeasure(data).then(onSuccessSubmit, onErrorSubmit);
            }else if($scope.state=='Search'){
                var data= {
                    unitOfMeasureName: $scope.selectedModel.name,
                    unitOfMeasureShortName : $scope.selectedModel.shortName
                }
                managerApi.unitOfMeasuresSearch(data).then(onSuccessSearch, onErrorSearch);
            }


            function onSuccessSubmit() {
                $state.reload();
                swal('Success', 'You have successfully '+$scope.state.toLowerCase()+' group.', 'success');
            }
            function onErrorSubmit(response) {
                swal("Success",response.data.message,"Success");
            }
            function onSuccessSearch(response) {
                $scope.model = response.data;
                initModel();
            }
            function onErrorSearch(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {
            if($scope.selectedModel.name=='' || $scope.selectedModel.name==null ){
                swal('Failure', 'Name cant be empty!', 'error');
                return false;
            }
            return true;

        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Edit';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }
        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.nextState = function () {
            if($scope.selectedModel.id!=null){
                localStorage.setItem('unitOfMeasureId',$scope.selectedModel.id);
                $state.go("basePage.articlePage");
            }
        }

        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }

        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteUnitOfMeasureHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }

    }
})();
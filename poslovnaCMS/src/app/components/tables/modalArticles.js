(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('modalArticles', function() {
            return {
                restrict: 'E',
                scope: {
                    closeModal : "=",
                    reload: '='
                },
                templateUrl: 'src/app/components/tables/modalArticles.html',
                controller: 'ModalArticlesController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ModalArticlesController', ModalArticlesController);

    ModalArticlesController.$inject = ['$scope', 'managerApi'];

    function ModalArticlesController($scope, managerApi) {
        $scope.model = [];
        $scope.errorCode = null;

        load();

        function load() {

            managerApi.articles().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        $scope.updateSelectedModel = updateSelectedModel;
        $scope.ok = ok;

        function ok() {
            if($scope.selectedModel===null){
                swal('Warning','Select a aricle!','error');
                return;
            }
            $scope.closeModal();
            $scope.reload($scope.selectedModel);

        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel = $scope.model[selectedRow];

        }

        $scope.selectedModel = null;
        $scope.rowHighilited = function( row){
            if($scope.selectedModel===null)
                $scope.selectedModel=[];
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

    }
})();
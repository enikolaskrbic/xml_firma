(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('modalUnitOfMeasures', function() {
            return {
                restrict: 'E',
                scope: {
                    closeModal : "=",
                    reload: '='
                },
                templateUrl: 'src/app/components/tables/modalUnitOfMeasures.html',
                controller: 'ModalUnitOfMeasuresController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ModalUnitOfMeasuresController', ModalUnitOfMeasuresController);

    ModalUnitOfMeasuresController.$inject = ['$scope', 'managerApi'];

    function ModalUnitOfMeasuresController($scope, managerApi) {
        $scope.model = [];
        $scope.errorCode = null;

        load();

        function load() {

            managerApi.unitOfMeasures().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        $scope.updateSelectedModel = updateSelectedModel;
        $scope.ok = ok;

        function ok() {
            if($scope.selectedModel===null){
                swal('Warning','Select a unit of measure!','error');
                return;
            }
            $scope.closeModal();
            $scope.reload($scope.selectedModel.id);

        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel = $scope.model[selectedRow];

        }

        $scope.selectedModel = null;
        $scope.rowHighilited = function( row){
            if($scope.selectedModel===null)
                $scope.selectedModel=[];
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('listOfAllArticles', function() {
            return {
                restrict: 'E',
                scope: {

                },
                templateUrl: 'src/app/components/tables/listOfAllArticles.html',
                controller: 'ListOfAllArticlesController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('ListOfAllArticlesController', ListOfAllArticlesController);

    ListOfAllArticlesController.$inject = ['$scope', 'managerApi','modalWindows','$state'];

    function ListOfAllArticlesController($scope, managerApi,modalWindows,$state) {
        $scope.model = [];
        $scope.errorCode = null;
        $scope.initModel = initModel;
        $scope.loadSubGroups = loadSubGroups;
        $scope.loadUnitOfMeasure = loadUnitOfMeasure;
        $scope.loadPdfArticle =loadPdfArticle;

        loadUnitOfMeasure();
        loadSubGroups();

        $scope.slectedSubGroup = localStorage.getItem('subGroupId');
        $scope.unitOfMeasureId = localStorage.getItem('unitOfMeasureId');
        localStorage.removeItem('subGroupId');
        localStorage.removeItem('unitOfMeasureId');

        if($scope.unitOfMeasureId == null || $scope.unitOfMeasureId==''){
            load($scope.slectedSubGroup);
        }else{
            loadUnitOfMeasurePage($scope.unitOfMeasureId);
        }

        function load(data) {
            if(data=='' || data==null){
                managerApi.articles().then(onSuccess, onError);
            }else{
                var request = {
                    subGroupId: $scope.slectedSubGroup,
                    drop: 0,
                    take: 25
                }
                managerApi.subGroupArticle(request).then(OnSuccessSubGroup, onError);
            }



            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function OnSuccessSubGroup(response) {
                $scope.model = response.data.results;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function loadUnitOfMeasurePage(unitOfMeasureId) {
            var request = {
                unitOfMeasureId: unitOfMeasureId,

            }
            managerApi.searchArticles(request).then(onSuccess, onError);

            function onSuccess(response) {
                $scope.model = response.data;
                initModel();
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }

        }

        function loadSubGroups() {
            managerApi.subgroups().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.modelSubGroups = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }
        }

        function loadUnitOfMeasure() {
            managerApi.unitOfMeasures().then(onSuccess, onError);

            function onSuccess(response) {
                $scope.modelUnitOfMeasure = response.data;
            }

            function onError(response) {
                if (response.status && response.statusText) {
                    $scope.errorCode = response.statusText;
                } else {
                    $scope.errorCode = 'Unknown error';
                }
            }
        }

        function loadPdfArticle() {


            managerApi.getArticleReport().then(onSuccess, onError);

            function onSuccess(response) {

                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL =  window.URL.createObjectURL(file);
                //$scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                window.open(fileURL);
            }
            function onError(response) {
                swal("Failure",'PDF is empty.',"error");            }
        }

        //toolbar actions
        $scope.updateSelectedModel = updateSelectedModel;
        $scope.submit = submit;
        $scope.validate = validate;
        $scope.selectedModel = [];
        $scope.state ='Edit';
        function initModel() {
            $scope.selectedRow =0;
            if($scope.model.length>0){
                updateSelectedModel(0);
            }
        }

        function updateSelectedModel(selectedRow) {
            $scope.selectedModel.id = $scope.model[selectedRow].id;
            $scope.selectedModel.name = $scope.model[selectedRow].name;
            $scope.selectedModel.code = $scope.model[selectedRow].code;
            $scope.selectedModel.status = $scope.model[selectedRow].status;
            $scope.selectedModel.subGroup = $scope.model[selectedRow].subGroupId;
            $scope.selectedModel.unitOfMeasure = $scope.model[selectedRow].unitOfMeasureId;
        }
        function submit() {

            if($scope.state=='Add'){
                if(validate()==false){
                    return;
                }
                var data= {
                    code: $scope.selectedModel.code,
                    name: $scope.selectedModel.name,
                    status: $scope.selectedModel.status,
                    subGroup: $scope.selectedModel.subGroup,
                    unitOfMeasure: $scope.selectedModel.unitOfMeasure

                }
                managerApi.addArticle(data).then(onSuccessSubmit, onErrorSubmit);

            }else if($scope.state=='Edit'){
                if(validate()==false){
                    return;
                }
                if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                    swal("Failure",'Nothing to edit.',"error");
                    return;
                }
                data= {
                    id: $scope.selectedModel.id,
                    code: $scope.selectedModel.code,
                    name: $scope.selectedModel.name,
                    status: $scope.selectedModel.status,
                    subGroup: $scope.selectedModel.subGroup,
                    unitOfMeasure: $scope.selectedModel.unitOfMeasure
                }
                managerApi.editArticle(data).then(onSuccessSubmit, onErrorSubmit);
            }else if($scope.state=='Search'){
                var data= {
                    ArticleName: $scope.selectedModel.name,
                    articleCode: $scope.selectedModel.code,
                    articleStatus: $scope.selectedModel.status,
                    subGroupId: $scope.selectedModel.subGroup,
                    unitOfMeasureId: $scope.selectedModel.unitOfMeasure
                }
                managerApi.searchArticles(data).then(onSuccessSearch, onErrorSearch);
            }


            function onSuccessSubmit() {
                $state.reload();
                swal('Success', 'You have successfully '+$scope.state.toLowerCase()+' article', 'success');
            }
            function onErrorSubmit(response) {
                swal("Failure",response.data.message,"error");
            }
            function onSuccessSearch(response) {
                $scope.model = response.data;
                initModel();
            }
            function onErrorSearch(response) {
                swal("Failure",response.data.message,"error");
            }
        }

        function validate() {
            if($scope.selectedModel.name=='' || $scope.selectedModel.name==null ||
                $scope.selectedModel.code=='' || $scope.selectedModel.code==null ||
                $scope.selectedModel.status=='' || $scope.selectedModel.status==null ||
                $scope.selectedModel.subGroup=='' || $scope.selectedModel.subGroup==null ||
                $scope.selectedModel.unitOfMeasure=='' || $scope.selectedModel.unitOfMeasure==null){
                swal('Failure', 'Name, code, status, subgroup and unit of measure cant be empty!', 'error');
                return false;
            }
            return true;
        }

        $scope.rowHighilited = function( row){
            $scope.state = 'Edit';
            $scope.selectedRow = row;
            updateSelectedModel($scope.selectedRow);
        };

        $scope.nextRow = function () {
            if($scope.selectedRow<$scope.model.length-1){
                $scope.selectedRow = $scope.selectedRow+1;
                updateSelectedModel($scope.selectedRow);
                //updateSelectedModel();
            }

        }

        $scope.previousRow = function () {
            if($scope.selectedRow>0){
                $scope.selectedRow = $scope.selectedRow-1;
                updateSelectedModel($scope.selectedRow);
            }
        }

        $scope.firstRow = function () {
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }

        $scope.lastRow = function () {
            $scope.selectedRow = $scope.model.length-1;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.addState = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.searchState = function () {
            $scope.state = 'Search';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }
        $scope.refresh = function () {
            // $scope.state = 'Edit';
            // $scope.selectedRow = 0;
            // //$scope.selectedModel = $scope.model[$scope.selectedRow];
            // updateSelectedModel($scope.selectedRow);
            load(null);
        }

        $scope.editState = function () {
            $scope.state = 'Edit';
            $scope.selectedRow = 0;
            //$scope.selectedModel = $scope.model[$scope.selectedRow];
            updateSelectedModel($scope.selectedRow);
        }
        $scope.rollBack = function () {
            $scope.state = 'Add';
            $scope.selectedModel = [];
            $scope.selectedRow = -1;
        }

        $scope.deleteSelectedItem = deleteSelectedItem;
        function deleteSelectedItem() {
            if($scope.selectedModel.id=='' || $scope.selectedModel.id==null){
                swal('Failure', 'Nothing select.', 'error');
                return;
            }
            swal({
                title: "Are you sure?",
                text: "You will be delete selected item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, deleteItem);
        }
        $scope.deleteItem = deleteItem;
        function deleteItem() {
            var data = {
                id : $scope.selectedModel.id
            }
            managerApi.deleteArticleHard(data).then(onSuccessDelete, onErrorDelete);

        }
        function onSuccessDelete(response) {
            swal('Success', 'You have successfully delete item.', 'success');
            $state.reload();
        }

        function onErrorDelete(response) {
            swal('Failure', 'This object can not be deleted.', 'error');
        }


        $scope.onClickLoadSubGroup = onClickLoadSubGroup;
        $scope.onClickLoadUnit = onClickLoadUnit;

        function onClickLoadSubGroup() {
            modalWindows.openModalSubGroups({
                reload: afterLoadGroup
            });
        }

        function onClickLoadUnit() {
            modalWindows.openModalUnitOfMeasures({
                reload: afterLoadUnit
            });
        }

        function afterLoadGroup(data) {
            $scope.selectedModel.subGroup = data;
        }
        function afterLoadUnit(data) {
            $scope.selectedModel.unitOfMeasure = data;
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('navigation', function() {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'src/app/components/containers/navigation.html',
                controller: 'NavigationController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('NavigationController', NavigationController);

    NavigationController.$inject = ['$scope', '$state', 'sessionService'];

    function NavigationController($scope, $state, sessionService) {
        $scope.menuCollapse = "inactive";
        $scope.isCollapsed = false;
        $scope.activeMenuItem = localStorage.getItem('activeMenuItem');
        $scope.collapseMenu = collapseMenu;
        $scope.logOut = logOut;

        var sessionData = sessionService.getSessionData();

        $scope.firstName = sessionData.firstName;
        $scope.role = sessionData.role;


        function collapseMenu() {
            if ($scope.menuCollapse === "inactive") {
                $scope.isCollapsed = true;
                $scope.menuCollapse = "active";
            } else {
                $scope.menuCollapse = "inactive";
                $scope.isCollapsed = false;
            }
        }

        function logOut() {
            swal({
                title: "Are you sure?",
                text: "You will be logged out",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, signOut);
        }


        function signOut() {
            localStorage.clear();
            $state.go('signInPage');
        }

        function navigationRedirect(navItem) {
            $scope.activeMenuItem = navItem.type;
            localStorage.setItem('activeMenuItem', navItem.type);
            $state.go(navItem.state);
        }

        //------------------------- HELPER FUNCTIONS -------------------------


        $scope.navigationMenuSwitch = function (navItem) {
            var currentState = $state.current.name;
            navigationRedirect(navItem);
        };


        switch (localStorage.getItem('rola')) {
            case 'ROLE_ADMIN':
                $scope.navItems = [
                    {
                        "name": 'Firms',
                        "imgSrcActive": 'app/img/nav/firms.png',
                        "imgSrc": 'app/img/nav/firms.png',
                        "type": "firms",
                        "state": "basePage.companiesPage"
                    },
                    {
                        "name": 'Add firm',
                        "imgSrcActive": 'app/img/nav/firms.png',
                        "imgSrc": 'app/img/nav/firms.png',
                        "type": "addFirm",
                        "state": "basePage.addFirmPage"
                    },
                    {
                        "name": 'Users',
                        "imgSrcActive": 'app/img/nav/profile.png',
                        "imgSrc": 'app/img/nav/profile.png',
                        "type": "users",
                        "state": "basePage.usersPage"
                    },
                    {
                        "name": 'Add user',
                        "imgSrcActive": 'app/img/nav/profile.png',
                        "imgSrc": 'app/img/nav/profile.png',
                        "type": "addUser",
                        "state": "basePage.addUserPage"
                    }
                ];
                break;
            case 'ROLE_FIRM':
                $scope.navItems = [
                    {
                        "name": 'O firmi',
                        "imgSrcActive": 'app/img/nav/manager/document.png',
                        "imgSrc": 'app/img/nav/manager/document.png',
                        "type": "firms",
                        "state": "basePage.aboutFirm"
                    },
                    {
                        "name": 'Izlazne fakture',
                        "imgSrcActive": 'app/img/nav/manager/document.png',
                        "imgSrc": 'app/img/nav/manager/document.png',
                        "type": "izlazne",
                        "state": "basePage.documentsPage"
                    },
                    {
                        "name": 'Ulazne fakture',
                        "imgSrcActive": 'app/img/nav/manager/document.png',
                        "imgSrc": 'app/img/nav/manager/document.png',
                        "type": "ulazne",
                        "state": "basePage.ulazneFakturePage"
                    },
                    {
                        "name": 'Nalozi za placanje',
                        "imgSrcActive": 'app/img/nav/manager/document.png',
                        "imgSrc": 'app/img/nav/manager/document.png',
                        "type": "nalozi",
                        "state": "basePage.analyticsPage"
                    }


                ];
                break;

        }


    }
})();
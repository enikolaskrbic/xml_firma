(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .constant('roleArticle', [
            'WARES',
            'SERVICE'
        ])
        .constant('roleBusinessPartner', [
            'BUYER',
            'SUPPLIER',
            'BUYER_AND_SUPPLIER'
        ])
        .constant('typeAnalytics', [
            'INPUT',
            'OUTPUT',
            'LEVELING',
            'CORRELATION',
            'BETWEEN',
            'START_STATE'
        ]);
})();
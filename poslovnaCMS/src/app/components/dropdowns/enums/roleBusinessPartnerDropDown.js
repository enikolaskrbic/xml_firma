(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('roleBusinessPartnerDropDown', function() {
            return {
                restrict: 'E',
                scope: {
                    htmlName: '@',
                    htmlRequired: '@',
                    htmlId: '@',
                    selectedId: '='
                },
                templateUrl: 'src/app/components/dropdowns/enums/roleBusinessPartnerDropDown.html',
                controller: 'RoleBusinessPartnerDropDownController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('RoleBusinessPartnerDropDownController', RoleBusinessPartnerDropDownController);

    RoleBusinessPartnerDropDownController.$inject = ['$scope', 'roleBusinessPartner'];

    function RoleBusinessPartnerDropDownController($scope, roleBusinessPartner) {

        $scope.model = roleBusinessPartner;

    }
})();
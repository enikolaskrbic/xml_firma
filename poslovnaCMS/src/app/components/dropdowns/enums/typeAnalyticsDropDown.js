(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('typeAnalyticsDropDown', function() {
            return {
                restrict: 'E',
                scope: {
                    htmlName: '@',
                    htmlRequired: '@',
                    htmlId: '@',
                    selectedId: '='
                },
                templateUrl: 'src/app/components/dropdowns/enums/typeAnalyticsDropDown.html',
                controller: 'TypeAnalyticsDropDownController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('TypeAnalyticsDropDownController', TypeAnalyticsDropDownController);

    TypeAnalyticsDropDownController.$inject = ['$scope', 'typeAnalytics'];

    function TypeAnalyticsDropDownController($scope, typeAnalytics) {

        $scope.model = typeAnalytics;

    }
})();
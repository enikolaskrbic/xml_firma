(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .directive('roleArticleDropDown', function() {
            return {
                restrict: 'E',
                scope: {
                    htmlName: '@',
                    htmlRequired: '@',
                    htmlId: '@',
                    selectedId: '='
                },
                templateUrl: 'src/app/components/dropdowns/enums/roleArticleDropDown.html',
                controller: 'RoleArticleDropDownController'
            };
        });

    angular
        .module('poslovnaCMS')
        .controller('RoleArticleDropDownController', RoleArticleDropDownController);

    RoleArticleDropDownController.$inject = ['$scope', 'roleArticle'];

    function RoleArticleDropDownController($scope, roleArticle) {

        $scope.model = roleArticle;

    }
})();
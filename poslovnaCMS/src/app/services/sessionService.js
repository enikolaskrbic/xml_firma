(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .service('sessionService', sessionService);

    function sessionService() {

        /* jshint ignore:start */
        var publicStates = [];
        publicStates['basePage'] = true;


        publicStates['basePage.companiesPage'] = true;
        publicStates['basePage.usersPage'] = true;
        publicStates['basePage.addFirmPage'] = true;
        publicStates['basePage.addUserPage'] = true;
        publicStates['basePage.groupsPage'] = true;
        publicStates['basePage.businessYearsPage'] = true;
        publicStates['basePage.subGroupPage'] = true;
        publicStates['basePage.articlePage'] = true;
        publicStates['basePage.warehousePage'] = true;
        publicStates['basePage.warehouseCardPage'] = true;
        publicStates['basePage.businessPartnersPage'] = true;
        publicStates['basePage.unitOfMeasuresPage'] = true;
        publicStates['basePage.documentsPage'] = true;
        publicStates['basePage.documentItemsPage'] = true;
        publicStates['basePage.analyticsPage'] = true;

        publicStates['signInPage'] = true;
        publicStates['signUpPage'] = true;
        publicStates['verifyEmailPage'] = true;
        publicStates['forgotPasswordPage'] = true;
        publicStates['resetPasswordPage'] = true;
        publicStates['basePage.aboutFirm'] = true;
        publicStates['basePage.ulazneFakturePage'] = true;
        publicStates['basePage.documentItemsUlaznePage'] = true;


        /* jshint ignore:end */

        return {
            save: save,
            clear: clear,
            getSessionData: getSessionData,
            isLoggedIn: isLoggedIn,
            canUserAccessState: canUserAccessState
        };

        function save(sessionData) {
            localStorage.setItem('accessToken', sessionData.accessToken);
            localStorage.setItem('id', sessionData.id);
            localStorage.setItem('firstName', sessionData.firstName);
            localStorage.setItem('lastName', sessionData.lastName);
            localStorage.setItem('isDeleted', sessionData.isDeleted);
            localStorage.setItem('timetamp', sessionData.timetamp);
            localStorage.setItem('firmId', sessionData.firmId);
            localStorage.setItem('role', sessionData.role);
            localStorage.setItem('email', sessionData.email);
        }

        function clear() {
            localStorage.removeItem('accessToken');
            localStorage.removeItem('id');
            localStorage.removeItem('firstName');
            localStorage.removeItem('lastName');
            localStorage.removeItem('isDeleted');
            localStorage.removeItem('timetamp');
            localStorage.removeItem('firmId');
            localStorage.removeItem('role');
            localStorage.removeItem('email');
        }

        function getSessionData() {
            return {
                accessToken: localStorage.getItem('accessToken'),
                id: localStorage.getItem('id'),
                firstName: localStorage.getItem('firstName'),
                lastName: localStorage.getItem('lastName'),
                isDeleted: localStorage.getItem('isDeleted'),
                timetamp: localStorage.getItem('timetamp'),
                firmId: localStorage.getItem('firmId'),
                role: localStorage.getItem('role'),
                email: localStorage.getItem('email')
            };
        }

        function isLoggedIn() {
            return localStorage.getItem("accessToken") !== null;
        }

        function canUserAccessState(stateName) {
            if (publicStates[stateName]) {
                return true;
            }

            var user = getSessionData();
            if (!user) {
                return false;
            }

            return stateAccessRights[stateName][user.role];
        }

    }

})();
(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .service('authenticationApiService', authenticationApiService);

    authenticationApiService.$inject = ['$http', 'sessionService'];

    function authenticationApiService($http, sessionService) {

        var backendApiUrl = '';

        return {
            init: init,
            signUp: signUp,
            signIn: signIn,
            forgotPassword: forgotPassword,
            resetPassword: resetPassword,
            verifyEmail: verifyEmail,
            changePassword: changePassword
        };

        function init(backendUrl) {
            backendApiUrl = backendUrl;
        }

        /** signUp 
         * request - SignUpRequest {
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   email: String
         *   password: String
         * }
         *
         * response - SignUpResponse {
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function signUp(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/sign-up',
                data: {
                    firstName: model.firstName,
                    lastName: model.lastName,
                    isDeleted: model.isDeleted,
                    timetamp: model.timetamp,
                    firmId: model.firmId,
                    email: model.email,
                    password: model.password
                }
            }).then(convertSignUpResponse);
        }

        function convertSignUpResponse(response) {
            response.data.timetamp = new Date(response.data.timetamp);
            return response;
        }

        /** signIn 
         * request - SignInRequest {
         *   email: String
         *   password: String
         * }
         *
         * response - SignInResponse {
         *   accessToken: String
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function signIn(model) {
            return $http({
                method: 'POST',
                url: 'http://localhost:8080' + '/api/sign-in',
                data: {
                    email: model.email,
                    password: model.password
                }
            }).then(convertSignInResponse);
        }

        function convertSignInResponse(response) {
            response.data.timetamp = new Date(response.data.timetamp);
            return response;
        }

        /** forgotPassword 
         * request - ForgotPasswordRequest {
         *   email: String
         * }
         *
         * response - Unit
         *
         */
        function forgotPassword(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/forgot-password',
                data: {
                    email: model.email
                }
            });
        }

        /** resetPassword 
         * request - ResetPasswordRequest {
         *   resetPasswordCode: String
         *   newPassword: String
         * }
         *
         * response - Unit
         *
         */
        function resetPassword(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/reset-password',
                data: {
                    resetPasswordCode: model.resetPasswordCode,
                    newPassword: model.newPassword
                }
            });
        }

        /** verifyEmail 
         * request - VerifyEmailRequest {
         *   emailVerificationCode: String
         * }
         *
         * response - VerifyEmailResponse {
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function verifyEmail(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/verify-email',
                data: {
                    emailVerificationCode: model.emailVerificationCode
                }
            }).then(convertVerifyEmailResponse);
        }

        function convertVerifyEmailResponse(response) {
            response.data.timetamp = new Date(response.data.timetamp);
            return response;
        }

        /** changePassword (secured)
         * request - ChangePasswordRequest {
         *   oldPassword: String
         *   newPassword: String
         * }
         *
         * response - ChangePasswordResponse {
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function changePassword(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/change-password',
                data: {
                    oldPassword: model.oldPassword,
                    newPassword: model.newPassword
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            }).then(convertChangePasswordResponse);
        }

        function convertChangePasswordResponse(response) {
            response.data.timetamp = new Date(response.data.timetamp);
            return response;
        }
    }
})();
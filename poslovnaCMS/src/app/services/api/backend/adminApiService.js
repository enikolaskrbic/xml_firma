(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .service('adminApiService', adminApiService);

    adminApiService.$inject = ['$http', 'sessionService'];

    function adminApiService($http, sessionService) {

        var backendApiUrl = '';

        return {
            init: init,
            firms: firms,
            addFirm: addFirm,
            users: users,
            addUser: addUser
        };

        function init(backendUrl) {
            backendApiUrl = backendUrl;
        }

        /** firms (secured)
         * request - Unit
         *
         * response - List [
         *   FirmsResponse {
         *     id: Int
         *     name: String
         *     pib: String
         *     address: String
         *     city: String
         *     country: String
         *     phone: Optional[String]
         *     email: Optional[String]
         *     web: Optional[String]
         *   }
         * ]
         *
         */
        function firms() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/admin/firms',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addFirm (secured)
         * request - FirmDTO {
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   email: String
         *   phone: String
         *   web: String
         *   pib: String
         * }
         *
         * response - Unit
         *
         */
        function addFirm(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/admin/add-firm',
                data: {
                    name: model.name,
                    address: model.address,
                    city: model.city,
                    country: model.country,
                    email: model.email,
                    phone: model.phone,
                    web: model.web,
                    pib: model.pib
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** users (secured)
         * request - Unit
         *
         * response - List [
         *   UsersResponse {
         *     firstName: String
         *     lastName: String
         *     firmId: Optional[Int]
         *   }
         * ]
         *
         */
        function users() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/admin/users',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addUser (secured)
         * request - UserDTO {
         *   firstName: String
         *   lastName: String
         *   email: String
         *   idFirm: Int
         * }
         *
         * response - Unit
         *
         */
        function addUser(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/admin/add-user',
                data: {
                    firstName: model.firstName,
                    lastName: model.lastName,
                    email: model.email,
                    idFirm: model.idFirm
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

    }
})();
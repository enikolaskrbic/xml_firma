(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .service('managerApiService', managerApiService);

    managerApiService.$inject = ['$http', 'sessionService', '$soap'];

    function managerApiService($http, sessionService, $soap) {

        var backendApiUrl = '';

        return {
            init: init,
            addGroup: addGroup,
            editGroup: editGroup,
            deleteGroup: deleteGroup,
            groups: groups,
            searchGroups: searchGroups,
            deleteGroupHard: deleteGroupHard,
            addSubGroup: addSubGroup,
            editSubGroup: editSubGroup,
            deleteSubGroup: deleteSubGroup,
            subgroups: subgroups,
            searchSubGroups: searchSubGroups,
            deleteSubGroupHard: deleteSubGroupHard,
            addArticle: addArticle,
            editArticle: editArticle,
            articles: articles,
            searchArticles: searchArticles,
            addBussinesYear: addBussinesYear,
            editBussinesYear: editBussinesYear,
            businessYears: businessYears,
            activeBusinessYear: activeBusinessYear,
            businessYearsSearch: businessYearsSearch,
            finishYear: finishYear,
            addWarehouse: addWarehouse,
            editWarehouse: editWarehouse,
            warehouses: warehouses,
            warehousesSearch: warehousesSearch,
            addWarehouseCard: addWarehouseCard,
            editWarehouseCard: editWarehouseCard,
            warehousesCard: warehousesCard,
            warehousesCardSearch: warehousesCardSearch,
            addBussinesPartner: addBussinesPartner,
            editBussinesPartner: editBussinesPartner,
            businessPartners: businessPartners,
            businessPartnersSearch: businessPartnersSearch,
            addUnitOfMeasure: addUnitOfMeasure,
            editUnitOfMeasure: editUnitOfMeasure,
            unitOfMeasures: unitOfMeasures,
            unitOfMeasuresSearch: unitOfMeasuresSearch,
            addDocument: addDocument,
            editDocument: editDocument,
            bookDocument: bookDocument,
            reverseDocument: reverseDocument,
            documents: documents,
            documentsSearch: documentsSearch,
            addDocumentItem: addDocumentItem,
            editDocumentItem: editDocumentItem,
            documentItems: documentItems,
            documentItemsSearch: documentItemsSearch,
            analytics: analytics,
            analyticsSearch: analyticsSearch,
            groupSubGroup: groupSubGroup,
            subGroupArticle: subGroupArticle,
            warehouseWarehouseCard: warehouseWarehouseCard,
            documentDocumentItem: documentDocumentItem,
            articleDocumentItem: articleDocumentItem,
            articleWarehouseCard: articleWarehouseCard,
            getLagerListReport : getLagerListReport,
            getArticleReport :getArticleReport,
            getWarehouseCardReport :getWarehouseCardReport,
            deleteArticleHard: deleteArticleHard,
            deleteBusinessYearHard: deleteBusinessYearHard,
            deleteWarehouseHard: deleteWarehouseHard,
            deleteBusinessPartnerHard: deleteBusinessPartnerHard,
            deleteDocumentHard: deleteDocumentHard,
            deleteDocumentItemHard: deleteDocumentItemHard,
            deleteUnitOfMeasureHard: deleteUnitOfMeasureHard,
            levelingWarehouse: levelingWarehouse,
            getArticlePrice: getArticlePrice,
            getAboutFirm: getAboutFirm,
            getUlazneFakture:getUlazneFakture,
            getOstaleFirme: getOstaleFirme,
            createIzlaznaFaktura:createIzlaznaFaktura,
            getIzlazneFakture: getIzlazneFakture,
            getStavkeIzlazneFakture: getStavkeIzlazneFakture,
            createStavkaIzlazneFakture: createStavkaIzlazneFakture,
            getStavkeUlazneFakture: getStavkeUlazneFakture,
            createUlaznaFaktura: createUlaznaFaktura,
            getIzvod : getIzvod,
            izvodRacuna: izvodRacuna,
            platiNalog:platiNalog
        };

        function init(backendUrl) {
            backendApiUrl = backendUrl;
        }

        /** addGroup (secured)
         * request - GroupDto {
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function addGroup(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-group',
                data: {
                    name: model.name
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editGroup (secured)
         * request - GroupIdDto {
         *   id: Int
         *   group: Optional[String]
         * }
         *
         * response - Unit
         *
         */
        function editGroup(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-group',
                data: {
                    id: model.id,
                    group: model.group
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteGroup (secured)
         * request - GroupIdDto {
         *   id: Int
         *   group: Optional[String]
         * }
         *
         * response - Unit
         *
         */
        function deleteGroup(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/delete-group',
                data: {
                    id: model.id,
                    group: model.group
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** groups (secured)
         * request - Unit
         *
         * response - List [
         *   GroupsResponse {
         *     id: Int
         *     name: String
         *   }
         * ]
         *
         */
        function groups() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/groups',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** searchGroups (secured)
         * request - Unit
         *
         * response - List [
         *   SearchGroupsResponse {
         *     id: Int
         *     name: String
         *     firmId: Int
         *     isDeleted: Boolean
         *     firmName: String
         *     firmAddress: String
         *     firmCity: String
         *     firmCountry: String
         *     firmEmail: Optional[String]
         *     firmPhone: Optional[String]
         *     firmWeb: Optional[String]
         *     firmPib: String
         *     firmIsDeleted: Boolean
         *   }
         * ]
         *
         */
        function searchGroups(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/groups/search',
                params: {
                    groupFirmId: model.groupFirmId,
                    groupFirmName: model.groupFirmName
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteGroupHard (secured)
         * request - DeleteGroupHardRequest {
         *   groupId: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteGroupHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/group/' + model.groupId + '',
                data: {
                    groupId: model.groupId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addSubGroup (secured)
         * request - SubGroupDto {
         *   id: Int
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function addSubGroup(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-subgroup',
                data: {
                    id: model.id,
                    name: model.name
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editSubGroup (secured)
         * request - SubGroupEditDto {
         *   id: Int
         *   group: Int
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function editSubGroup(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-subgroup',
                data: {
                    id: model.id,
                    group: model.group,
                    name: model.name
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteSubGroup (secured)
         * request - SubGroupDto {
         *   id: Int
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function deleteSubGroup(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/delete-subgroup',
                data: {
                    id: model.id,
                    name: model.name
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** subgroups (secured)
         * request - Unit
         *
         * response - List [
         *   SubgroupsResponse {
         *     id: Int
         *     groupId: Int
         *     name: String
         *     groupFirmName: String
         *   }
         * ]
         *
         */
        function subgroups() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/subgroups',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** searchSubGroups (secured)
         * request - Unit
         *
         * response - List [
         *   SearchSubGroupsResponse {
         *     id: Int
         *     groupId: Int
         *     name: String
         *     groupFirmName: String
         *   }
         * ]
         *
         */
        function searchSubGroups(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/subgroups/search',
                params: {
                    subGroupId: model.subGroupId,
                    subGroupGroupId: model.subGroupGroupId,
                    subGroupName: model.subGroupName,
                    groupFirmName: model.groupFirmName
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteSubGroupHard (secured)
         * request - DeleteSubGroupHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteSubGroupHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/subgroup/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addArticle (secured)
         * request - ArticleDto {
         *   code: String
         *   name: String
         *   status: RoleArticle
         *   subGroup: Int
         *   unitOfMeasure: Int
         * }
         *
         * response - Unit
         *
         */
        function addArticle(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-article',
                data: {
                    code: model.code,
                    name: model.name,
                    status: model.status,
                    subGroup: model.subGroup,
                    unitOfMeasure: model.unitOfMeasure
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editArticle (secured)
         * request - ArticleEditDto {
         *   id: Int
         *   code: String
         *   name: String
         *   status: RoleArticle
         *   subGroup: Int
         *   unitOfMeasure: Int
         * }
         *
         * response - Unit
         *
         */
        function editArticle(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-article',
                data: {
                    id: model.id,
                    code: model.code,
                    name: model.name,
                    status: model.status,
                    subGroup: model.subGroup,
                    unitOfMeasure: model.unitOfMeasure
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** articles (secured)
         * request - Unit
         *
         * response - List [
         *   ArticlesResponse {
         *     id: Int
         *     name: String
         *     code: String
         *     status: RoleArticle
         *     subGroupId: Int
         *     subGroupName: String
         *     unitOfMeasureId: Int
         *     unitOfMeasureName: String
         *     unitOfMeasureShortName: Optional[String]
         *   }
         * ]
         *
         */
        function articles() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/articles',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** searchArticles (secured)
         * request - Unit
         *
         * response - List [
         *   SearchArticlesResponse {
         *     id: Int
         *     name: String
         *     code: String
         *     status: RoleArticle
         *     subGroupId: Int
         *     subGroupName: String
         *     unitOfMeasureId: Int
         *     unitOfMeasureName: String
         *     unitOfMeasureShortName: Optional[String]
         *   }
         * ]
         *
         */
        function searchArticles(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/articles/search',
                params: {
                    articleId: model.articleId,
                    ArticleName: model.ArticleName,
                    articleCode: model.articleCode,
                    articleStatus: model.articleStatus,
                    subGroupId: model.subGroupId,
                    subGroupName: model.subGroupName,
                    unitOfMeasureId: model.unitOfMeasureId,
                    unitOfMeasureName: model.unitOfMeasureName,
                    unitOfMeasureShortName: model.unitOfMeasureShortName
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addBussinesYear (secured)
         * request - BusinessYearDto {
         *   year: String
         *   isFinished: Boolean
         * }
         *
         * response - Unit
         *
         */
        function addBussinesYear(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-BussinesYear',
                data: {
                    year: model.year,
                    isFinished: model.isFinished
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editBussinesYear (secured)
         * request - BusinessYearIdDto {
         *   id: Int
         *   year: String
         *   isFinished: Boolean
         * }
         *
         * response - Unit
         *
         */
        function editBussinesYear(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-BussinesYear',
                data: {
                    id: model.id,
                    year: model.year,
                    isFinished: model.isFinished
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** businessYears (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessYearsResponse {
         *     id: Int
         *     year: String
         *     isFinished: Boolean
         *   }
         * ]
         *
         */
        function businessYears() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/businessYears',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** activeBusinessYear (secured)
         * request - Unit
         *
         * response - List [
         *   ActiveBusinessYearResponse {
         *     id: Int
         *     year: String
         *     isFinished: Boolean
         *   }
         * ]
         *
         */
        function activeBusinessYear() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/businessYear/active',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** businessYearsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessYearsSearchResponse {
         *     id: Int
         *     year: String
         *     isFinished: Boolean
         *   }
         * ]
         *
         */
        function businessYearsSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/businessYears/search',
                params: {
                    businessYearId: model.businessYearId,
                    businessYearYear: model.businessYearYear,
                    businessYearIsFinished: model.businessYearIsFinished
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** finishYear (secured)
         * request - YearIdDTO {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function finishYear(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/finish-BussinesYear',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addWarehouse (secured)
         * request - WarehouseDto {
         *   name: String
         *   code: String
         *   address: String
         *   city: String
         *   country: String
         * }
         *
         * response - Unit
         *
         */
        function addWarehouse(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-warehouse',
                data: {
                    name: model.name,
                    code: model.code,
                    address: model.address,
                    city: model.city,
                    country: model.country
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editWarehouse (secured)
         * request - WarehouseEditDto {
         *   id: Int
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   code: String
         * }
         *
         * response - Unit
         *
         */
        function editWarehouse(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-warehouse',
                data: {
                    id: model.id,
                    name: model.name,
                    address: model.address,
                    city: model.city,
                    country: model.country,
                    code: model.code
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** warehouses (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesResponse {
         *     id: Int
         *     code: String
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *   }
         * ]
         *
         */
        function warehouses() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/warehouses',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** warehousesSearch (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesSearchResponse {
         *     id: Int
         *     code: String
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *   }
         * ]
         *
         */
        function warehousesSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/warehouses/search',
                params: {
                    warehouseId: model.warehouseId,
                    warehouseCode: model.warehouseCode,
                    warehouseName: model.warehouseName,
                    warehouseAddress: model.warehouseAddress,
                    warehouseCity: model.warehouseCity,
                    warehouseCountry: model.warehouseCountry
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addWarehouseCard (secured)
         * request - WarehouseCardDto {
         *   balance: Optional[Decimal(10, 4)]
         *   warehouse: Int
         *   businessYear: Int
         *   article: Int
         * }
         *
         * response - Unit
         *
         */
        function addWarehouseCard(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-warehouse-card',
                data: {
                    balance: model.balance,
                    warehouse: model.warehouse,
                    businessYear: model.businessYear,
                    article: model.article
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editWarehouseCard (secured)
         * request - WarehouseCardEditDto {
         *   id: Int
         *   balance: Optional[Decimal(10, 4)]
         * }
         *
         * response - Unit
         *
         */
        function editWarehouseCard(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-warehouse-card',
                data: {
                    id: model.id,
                    balance: model.balance
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** warehousesCard (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesCardResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     averagePrice: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function warehousesCard() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/warehousecards',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** warehousesCardSearch (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesCardSearchResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     averagePrice: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function warehousesCardSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/warehousecards/search',
                params: {
                    warehouseCardId: model.warehouseCardId,
                    warehouseCardBusinessYearId: model.warehouseCardBusinessYearId,
                    businessYearYear: model.businessYearYear,
                    articleId: model.articleId,
                    articleName: model.articleName,
                    articleCode: model.articleCode,
                    warehouseCardInput: model.warehouseCardInput,
                    warehouseCardOutput: model.warehouseCardOutput,
                    warehouseCardBalance: model.warehouseCardBalance,
                    warehouseCardInValue: model.warehouseCardInValue,
                    warehouseCardOutValue: model.warehouseCardOutValue,
                    warehouseCardTotalValue: model.warehouseCardTotalValue,
                    warehouseId: model.warehouseId,
                    warehouseName: model.warehouseName
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addBussinesPartner (secured)
         * request - BussinesPartnerDto {
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   email: Optional[String]
         *   phone: Optional[String]
         *   web: Optional[String]
         *   status: RoleBusinessPartner
         * }
         *
         * response - Unit
         *
         */
        function addBussinesPartner(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-bussines-partner',
                data: {
                    name: model.name,
                    address: model.address,
                    city: model.city,
                    country: model.country,
                    email: model.email,
                    phone: model.phone,
                    web: model.web,
                    status: model.status
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editBussinesPartner (secured)
         * request - BussinesPartnerEditDto {
         *   idPartner: Int
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   email: Optional[String]
         *   phone: Optional[String]
         *   web: Optional[String]
         *   status: RoleBusinessPartner
         * }
         *
         * response - Unit
         *
         */
        function editBussinesPartner(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-bussines-partner',
                data: {
                    idPartner: model.idPartner,
                    name: model.name,
                    address: model.address,
                    city: model.city,
                    country: model.country,
                    email: model.email,
                    phone: model.phone,
                    web: model.web,
                    status: model.status
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** businessPartners (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessPartnersResponse {
         *     id: Int
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *     web: Optional[String]
         *     email: Optional[String]
         *     phone: Optional[String]
         *     status: RoleBusinessPartner
         *   }
         * ]
         *
         */
        function businessPartners() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/businessPartners',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** businessPartnersSearch (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessPartnersSearchResponse {
         *     id: Int
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *     web: Optional[String]
         *     email: Optional[String]
         *     phone: Optional[String]
         *     status: RoleBusinessPartner
         *   }
         * ]
         *
         */
        function businessPartnersSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/businessPartners/search',
                params: {
                    businessPartnerId: model.businessPartnerId,
                    businessPartnerName: model.businessPartnerName,
                    businessPartnerAddress: model.businessPartnerAddress,
                    businessPartnerCity: model.businessPartnerCity,
                    businessPartnerCountry: model.businessPartnerCountry,
                    businessPartnerWeb: model.businessPartnerWeb,
                    businessPartnerEmail: model.businessPartnerEmail,
                    businessPartnerPhone: model.businessPartnerPhone,
                    businessPartnerStatus: model.businessPartnerStatus
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addUnitOfMeasure (secured)
         * request - UnitOfMeasureDto {
         *   name: String
         *   shortName: String
         * }
         *
         * response - Unit
         *
         */
        function addUnitOfMeasure(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-unitOfMeasure',
                data: {
                    name: model.name,
                    shortName: model.shortName
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editUnitOfMeasure (secured)
         * request - UnitOfMeasureEditDto {
         *   id: Int
         *   name: String
         *   shortName: String
         * }
         *
         * response - Unit
         *
         */
        function editUnitOfMeasure(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-unitOfMeasure',
                data: {
                    id: model.id,
                    name: model.name,
                    shortName: model.shortName
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** unitOfMeasures (secured)
         * request - Unit
         *
         * response - List [
         *   UnitOfMeasuresResponse {
         *     id: Int
         *     name: String
         *     shortName: Optional[String]
         *   }
         * ]
         *
         */
        function unitOfMeasures() {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/unitOfMeasures',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** unitOfMeasuresSearch (secured)
         * request - Unit
         *
         * response - List [
         *   UnitOfMeasuresSearchResponse {
         *     id: Int
         *     name: String
         *     shortName: Optional[String]
         *   }
         * ]
         *
         */
        function unitOfMeasuresSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/unitOfMeasures/search',
                params: {
                    unitOfMeasureId: model.unitOfMeasureId,
                    unitOfMeasureName: model.unitOfMeasureName,
                    unitOfMeasureShortName: model.unitOfMeasureShortName
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** addDocument (secured)
         * request - DocumentDto {
         *   type: TypeDocument
         *   dateOfDelivery: Optional[DateTime]
         *   dateOfValue: Optional[DateTime]
         *   dateOfStatement: Optional[DateTime]
         *   businessYear: Int
         *   businessPartner: Optional[Int]
         *   warehouse: Int
         *   warehouseOut: Optional[Int]
         * }
         *
         * response - Unit
         *
         */
        function addDocument(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-document',
                data: {
                    type: model.type,
                    dateOfDelivery: model.dateOfDelivery,
                    dateOfValue: model.dateOfValue,
                    dateOfStatement: model.dateOfStatement,
                    businessYear: model.businessYear,
                    businessPartner: model.businessPartner,
                    warehouse: model.warehouse,
                    warehouseOut: model.warehouseOut
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editDocument (secured)
         * request - DocumentEditDto {
         *   id: Int
         *   type: TypeDocument
         *   dateOfDelivery: Optional[DateTime]
         *   dateOfValue: Optional[DateTime]
         *   dateOfStatement: Optional[DateTime]
         *   paymentOfAccount: Optional[String]
         *   referenceNumber: Optional[String]
         *   businessYear: Int
         *   businessPartner: Int
         *   warehouse: Int
         * }
         *
         * response - Unit
         *
         */
        function editDocument(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-document',
                data: {
                    id: model.id,
                    type: model.type,
                    dateOfDelivery: model.dateOfDelivery,
                    dateOfValue: model.dateOfValue,
                    dateOfStatement: model.dateOfStatement,
                    paymentOfAccount: model.paymentOfAccount,
                    referenceNumber: model.referenceNumber,
                    businessYear: model.businessYear,
                    businessPartner: model.businessPartner,
                    warehouse: model.warehouse
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** bookDocument (secured)
         * request - DocumentIdDto {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function bookDocument(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/book-document',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** reverseDocument (secured)
         * request - DocumentIdDto {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function reverseDocument(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/reverse-document',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** documents (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentResponseDto {
         *     id: Int
         *     dateOfValue: Optional[DateTime]
         *     dateOfDelivery: Optional[DateTime]
         *     dateOfStatement: Optional[DateTime]
         *     warehouseId: Int
         *     warehouseName: String
         *     businessPartnerId: Int
         *     name: String
         *     status: RoleBusinessPartner
         *     businessYearId: Int
         *     year: String
         *     warehouseIdOut: Int
         *     warehouseNameOut: String
         *   }
         * ]
         *
         */
        function documents(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/document',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            }).then(convertDocumentResponseDto);
        }

        function convertDocumentResponseDto(response) {
            response.data.forEach(function(item) {
                item.dateOfValue = item.dateOfValue ? new Date(item.dateOfValue) : null;
                item.dateOfDelivery = item.dateOfDelivery ? new Date(item.dateOfDelivery) : null;
                item.dateOfStatement = item.dateOfStatement ? new Date(item.dateOfStatement) : null;
            });
            return response;
        }

        /** documentsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentsSearchResponse {
         *     id: Int
         *     dateOfValue: Optional[DateTime]
         *     dateOfDelivery: Optional[DateTime]
         *     warehouseId: Int
         *     warehouseName: String
         *     businessPartnerId: Int
         *     businessPartnerName: String
         *     businessYearId: Int
         *     businessYearYear: String
         *   }
         * ]
         *
         */
        function documentsSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/document/search',
                params: {
                    documentId: model.documentId,
                    documentDateOfValue: model.documentDateOfValue,
                    documentDateOfDelivery: model.documentDateOfDelivery,
                    warehuseId: model.warehuseId,
                    warehouseName: model.warehouseName,
                    businessPartnerId: model.businessPartnerId,
                    bussinesPartnerName: model.bussinesPartnerName,
                    businessYearId: model.businessYearId,
                    businessYearYear: model.businessYearYear
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            }).then(convertDocumentsSearchResponse);
        }

        function convertDocumentsSearchResponse(response) {
            response.data.forEach(function(item) {
                item.dateOfValue = item.dateOfValue ? new Date(item.dateOfValue) : null;
                item.dateOfDelivery = item.dateOfDelivery ? new Date(item.dateOfDelivery) : null;
            });
            return response;
        }

        /** addDocumentItem (secured)
         * request - DocumentItemDto {
         *   document: Int
         *   quantity: Decimal(10, 4)
         *   price: Decimal(10, 4)
         *   article: Int
         * }
         *
         * response - Unit
         *
         */
        function addDocumentItem(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/add-document-item',
                data: {
                    document: model.document,
                    quantity: model.quantity,
                    price: model.price,
                    article: model.article
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** editDocumentItem (secured)
         * request - DocumentItemEditDto {
         *   id: Int
         *   document: Int
         *   quantity: Decimal(10, 4)
         *   price: Decimal(10, 4)
         *   article: Int
         * }
         *
         * response - Unit
         *
         */
        function editDocumentItem(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/edit-document-item',
                data: {
                    id: model.id,
                    document: model.document,
                    quantity: model.quantity,
                    price: model.price,
                    article: model.article
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** documentItems (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentItemsResponse {
         *     id: Int
         *     quantity: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     documentId: Int
         *     articleId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *     unitOfMeasureName: String
         *   }
         * ]
         *
         */
        function documentItems(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/documentItems',
                params: {
                    documentId: model.documentId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** documentItemsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentItemsSearchResponse {
         *     id: Int
         *     quantity: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     documentId: Int
         *     articleId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *     unitOfMeasureName: String
         *   }
         * ]
         *
         */
        function documentItemsSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/documentItems/search',
                params: {
                    documentItemId: model.documentItemId,
                    documentItemType: model.documentItemType,
                    documentId: model.documentId,
                    articleName: model.articleName,
                    articleUnitOfMeasureId: model.articleUnitOfMeasureId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** analytics (secured)
         * request - Unit
         *
         * response - List [
         *   AnalyticsResponse {
         *     id: Int
         *     type: TypeAnalytics
         *     quantity: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     value: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *   }
         * ]
         *
         */
        function analytics(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/analytics',
                params: {
                    warehouseCardId: model.warehouseCardId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** analyticsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   AnalyticsSearchResponse {
         *     id: Int
         *     type: TypeAnalytics
         *     quantity: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     value: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *   }
         * ]
         *
         */
        function analyticsSearch(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/firm/analytics/search',
                params: {
                    warehouseCardId: model.warehouseCardId,
                    analyticsId: model.analyticsId,
                    analyticsType: model.analyticsType,
                    analyticsQuantity: model.analyticsQuantity,
                    analyticsBalance: model.analyticsBalance,
                    analyticsPrice: model.analyticsPrice,
                    analyticsValue: model.analyticsValue,
                    analyticsTotalValue: model.analyticsTotalValue
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** groupSubGroup (secured)
         * request - Unit
         *
         * response - List [
         *   GroupSubGroupResponse {
         *     id: Int
         *     groupId: Int
         *     name: String
         *     groupFirmName: String
         *   }
         * ]
         *
         */
        function groupSubGroup(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/group/subgroup',
                params: {
                    groupFirmId: model.groupFirmId,
                    drop: model.drop,
                    take: model.take
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** subGroupArticle (secured)
         * request - Unit
         *
         * response - List [
         *   SubGroupArticleResponse {
         *     id: Int
         *     name: String
         *     code: String
         *     status: RoleArticle
         *     subGroupId: Int
         *     subGroupName: String
         *     unitOfMeasureId: Int
         *     unitOfMeasureName: String
         *     unitOfMeasureShortName: Optional[String]
         *   }
         * ]
         *
         */
        function subGroupArticle(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/subgroup/article',
                params: {
                    subGroupId: model.subGroupId,
                    drop: model.drop,
                    take: model.take
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** warehouseWarehouseCard (secured)
         * request - Unit
         *
         * response - List [
         *   WarehouseWarehouseCardResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function warehouseWarehouseCard(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/warehouse/warehousecard',
                params: {
                    warehouseId: model.warehouseId,
                    drop: model.drop,
                    take: model.take
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** documentDocumentItem (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentDocumentItemResponse {
         *     id: Int
         *     quantity: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     documentId: Int
         *     articleId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *   }
         * ]
         *
         */
        function documentDocumentItem(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/document/documentItems',
                params: {
                    documentId: model.documentId,
                    drop: model.drop,
                    take: model.take
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** articleDocumentItem (secured)
         * request - Unit
         *
         * response - List [
         *   ArticleDocumentItemResponse {
         *     id: Int
         *     type: TypeDocument
         *     documentId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *   }
         * ]
         *
         */
        function articleDocumentItem(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/article/documentItems',
                params: {
                    articleId: model.articleId,
                    drop: model.drop,
                    take: model.take
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** articleWarehouseCard (secured)
         * request - Unit
         *
         * response - List [
         *   ArticleWarehouseCardResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function articleWarehouseCard(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/article/warehousecard',
                params: {
                    articleId: model.articleId,
                    drop: model.drop,
                    take: model.take
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }


        function getLagerListReport(model) {
            return $http({
                method: 'GET',
                responseType : 'arraybuffer',
                url: backendApiUrl + '/api/manager/reports/lager-list',
                params: {
                    warehouseId: model.warehouseId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getArticleReport() {
            return $http({
                method: 'GET',
                responseType : 'arraybuffer',
                url: backendApiUrl + '/api/manager/reports/article-code',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getWarehouseCardReport(model) {
            return $http({
                method: 'GET',
                responseType : 'arraybuffer',
                url: backendApiUrl + '/api/manager/reports/analytics',
                params: {
                    warehouseCardId: model.warehouseCardId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteArticleHard (secured)
         * request - DeleteArticleHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteArticleHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/article/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteBusinessYearHard (secured)
         * request - DeleteBusinessYearHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteBusinessYearHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/businessYear/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteWarehouseHard (secured)
         * request - DeleteWarehouseHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteWarehouseHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/warehouse/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteBusinessPartnerHard (secured)
         * request - DeleteBusinessPartnerHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteBusinessPartnerHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/businessPartner/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteDocumentHard (secured)
         * request - DeleteDocumentHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteDocumentHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/document/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteDocumentItemHard (secured)
         * request - DeleteDocumentItemHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteDocumentItemHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/documentItem/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        /** deleteUnitOfMeasureHard (secured)
         * request - DeleteUnitOfMeasureHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteUnitOfMeasureHard(model) {
            return $http({
                method: 'DELETE',
                url: backendApiUrl + '/api/manager/unitOfMeasure/' + model.id + '',
                data: {
                    id: model.id
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getArticlePrice(model) {
            return $http({
                method: 'GET',
                url: backendApiUrl + '/api/manager/article/price',
                params: {
                    articleId: model.articleId,
                    documentId: model.documentId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function levelingWarehouse(model) {
            return $http({
                method: 'POST',
                url: backendApiUrl + '/api/manager/warehouse/leveling',
                params: {
                    warehouseId: model.warehouseId
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }


        //XML API

        function getAboutFirm(model) {
            return $http({
                method: 'GET',
                url: model.api + '/api/xml/firma/' + model.pib + '',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getUlazneFakture(model) {
            return $http({
                method: 'GET',
                url: model.api + '/api/xml/firma/'+ model.pib +'/ulazne-fakture',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getIzlazneFakture(model) {
            return $http({
                method: 'GET',
                url: model.api + '/api/xml/firma/'+ model.pib +'/izlazne-fakture',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getOstaleFirme(model) {
            return $http({
                method: 'GET',
                url: model.api + '/api/xml/firme/bez/'+model.pib+'',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function createIzlaznaFaktura(model) {
            return $http({
                method: 'POST',
                url: model.api + '/api/xml/firma/'+model.pib+'/faktura/output',
                data: {
                    idPoruke: model.idPoruke,
                    zaglavljeFakture : {
                        nazivDobavljaca: model.zaglavljeFakture.nazivDobavljaca,
                        adresaDobavljaca: model.zaglavljeFakture.adresaDobavljaca,
                        pibDobavljaca: model.zaglavljeFakture.pibDobavljaca,
                        nazivKupca: model.zaglavljeFakture.nazivKupca,
                        adresaKupca: model.zaglavljeFakture.adresaKupca,
                        pibKupca: model.zaglavljeFakture.pibKupca,
                        brojRacuna: model.zaglavljeFakture.brojRacuna,
                        vrednostRobe: model.zaglavljeFakture.vrednostRobe,
                        vrednostUsluga: model.zaglavljeFakture.vrednostUsluga,
                        upunoRobeIUsluga: model.zaglavljeFakture.upunoRobeIUsluga,
                        ukupanRabat: model.zaglavljeFakture.ukupanRabat,
                        ukupanPorez: model.zaglavljeFakture.ukupanPorez,
                        oznakaValute: model.zaglavljeFakture.oznakaValute,
                        iznosZaUplatu: model.zaglavljeFakture.iznosZaUplatu,
                        uplataNaRacun: model.zaglavljeFakture.uplataNaRacun,
                        datumRacuna: model.zaglavljeFakture.datumRacuna,
                        datumValute: model.zaglavljeFakture.datumValute
                    }
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function izvodRacuna(model) {
            return $http({
                method: 'POST',
                url: model.api + '/api/xml/firme/izvod/racun',
                params:{
                    apiBanke:model.apiBanke
                },
                data: {
                    brojRacuna : model.brojRacuna,
                    datum : model.datum,
                    redniBrojPreseka: model.redniBrojPreseka
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function platiNalog(model) {
            return $http({
                method: 'POST',
                url: model.api + '/api/xml/firme/nalog',
                params:{
                    apiBanke : model.apiBanke
                },
                data: model.nalog,
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }




        function getStavkeIzlazneFakture(model) {
            return $http({
                method: 'GET',
                url: model.api + '/api/xml/firma/'+model.pib+'/faktura-izlazna/'+model.idPoruke+'/stavka',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getStavkeUlazneFakture(model) {
            return $http({
                method: 'GET',
                url: model.api + '/api/xml/firma/'+model.pib+'/faktura-ulazna/'+model.idPoruke+'/stavka',
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function createStavkaIzlazneFakture(model) {
            return $http({
                method: 'POST',
                url: model.api + '/api/xml/firma/'+model.pib+'/faktura/'+model.idPoruke+'/stavka/output',
                data: {
                    iznosRabata: model.iznosRabata,
                    jedinicaMere: model.jedinicaMere,
                    jedinicnaCena: model.jedinicnaCena,
                    kolicina: model.kolicina,
                    nazivRobeIliUsluge: model.nazivRobeIliUsluge,
                    procenatRabata: model.procenatRabata,
                    redniBroj: model.redniBroj,
                    ukupanPorez: model.ukupanPorez,
                    umanjenoZaRabat: model.umanjenoZaRabat,
                    vrednost: model.vrednost
                },
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function createUlaznaFaktura(model) {
            return $http({
                method: 'POST',
                url: model.api + '/api/xml/firma/'+model.pib+'/faktura/input',
                data: model.faktura,
                headers: {
                    'Authorization': "Bearer " + sessionService.getSessionData().accessToken
                }
            });
        }

        function getIzvod(model) {
            // return $http({
            //     method: 'POST',
            //     url: model.api,
            //     data: model.xmlRequest,
            //     headers: {
            //         "Content-Type" : "text/xml",
            //         'Access-Control-Allow-Origin' : '*',
            //         'Access-Control-Allow-Methods' : 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
            //         'Access-Control-Allow-Headers' : 'Origin, Content-Type, X-Auth-Token'
            //     }
            // });

            var sr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:izv=\"http://xml.web.services.security/izvod\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <izv:izvodRequest>\n" +
                "         <izv:brojRacuna>"+model.brojRacuna+"</izv:brojRacuna>\n" +
                "         <izv:datum>"+model.datum+"/izv:datum>\n" +
                "         <izv:redniBrojPreseka>"+model.redniBrojPreseka+"</izv:redniBrojPreseka>\n" +
                "      </izv:izvodRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";

            var xmlHttp = SOAPClient._getXmlHttp();

            xmlHttp.open("POST", model.api, true);
            xmlHttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
            xmlHttp.setRequestHeader("Access-Control-Allow-Origin", model.api+"");
            xmlHttp.setRequestHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS");
            xmlHttp.setRequestHeader("Access-Control-Allow-Headers","X-PINGOTHER, Content-Type");
            xmlHttp.setRequestHeader("Access-Control-Max-Age","86400");
            xmlHttp.send(sr);

            return xmlHttp;

        }




    }
})();
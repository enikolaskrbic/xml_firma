(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .service('managerApiMockService', managerApiMockService);

    managerApiMockService.$inject = ['$timeout'];

    function managerApiMockService($timeout) {

        return {
            addGroup: addGroup,
            editGroup: editGroup,
            deleteGroup: deleteGroup,
            groups: groups,
            searchGroups: searchGroups,
            deleteGroupHard: deleteGroupHard,
            addSubGroup: addSubGroup,
            editSubGroup: editSubGroup,
            deleteSubGroup: deleteSubGroup,
            subgroups: subgroups,
            searchSubGroups: searchSubGroups,
            deleteSubGroupHard: deleteSubGroupHard,
            addArticle: addArticle,
            editArticle: editArticle,
            articles: articles,
            searchArticles: searchArticles,
            addBussinesYear: addBussinesYear,
            editBussinesYear: editBussinesYear,
            businessYears: businessYears,
            activeBusinessYear: activeBusinessYear,
            businessYearsSearch: businessYearsSearch,
            finishYear: finishYear,
            addWarehouse: addWarehouse,
            editWarehouse: editWarehouse,
            warehouses: warehouses,
            warehousesSearch: warehousesSearch,
            addWarehouseCard: addWarehouseCard,
            editWarehouseCard: editWarehouseCard,
            warehousesCard: warehousesCard,
            warehousesCardSearch: warehousesCardSearch,
            addBussinesPartner: addBussinesPartner,
            editBussinesPartner: editBussinesPartner,
            businessPartners: businessPartners,
            businessPartnersSearch: businessPartnersSearch,
            addUnitOfMeasure: addUnitOfMeasure,
            editUnitOfMeasure: editUnitOfMeasure,
            unitOfMeasures: unitOfMeasures,
            unitOfMeasuresSearch: unitOfMeasuresSearch,
            addDocument: addDocument,
            editDocument: editDocument,
            bookDocument: bookDocument,
            reverseDocument: reverseDocument,
            documents: documents,
            documentsSearch: documentsSearch,
            addDocumentItem: addDocumentItem,
            editDocumentItem: editDocumentItem,
            documentItems: documentItems,
            documentItemsSearch: documentItemsSearch,
            analytics: analytics,
            analyticsSearch: analyticsSearch,
            groupSubGroup: groupSubGroup,
            subGroupArticle: subGroupArticle,
            warehouseWarehouseCard: warehouseWarehouseCard,
            documentDocumentItem: documentDocumentItem,
            articleDocumentItem: articleDocumentItem,
            articleWarehouseCard: articleWarehouseCard,
            deleteArticleHard: deleteArticleHard,
            deleteBusinessYearHard: deleteBusinessYearHard,
            deleteWarehouseHard: deleteWarehouseHard,
            deleteBusinessPartnerHard: deleteBusinessPartnerHard,
            deleteDocumentHard: deleteDocumentHard,
            deleteDocumentItemHard: deleteDocumentItemHard,
            deleteUnitOfMeasureHard: deleteUnitOfMeasureHard
        };

        /** addGroup (secured)
         * request - GroupDto {
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function addGroup(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editGroup (secured)
         * request - GroupIdDto {
         *   id: Int
         *   group: Optional[String]
         * }
         *
         * response - Unit
         *
         */
        function editGroup(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteGroup (secured)
         * request - GroupIdDto {
         *   id: Int
         *   group: Optional[String]
         * }
         *
         * response - Unit
         *
         */
        function deleteGroup(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** groups (secured)
         * request - Unit
         *
         * response - List [
         *   GroupsResponse {
         *     id: Int
         *     name: String
         *   }
         * ]
         *
         */
        function groups() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** searchGroups (secured)
         * request - Unit
         *
         * response - List [
         *   SearchGroupsResponse {
         *     id: Int
         *     name: String
         *     firmId: Int
         *     isDeleted: Boolean
         *     firmName: String
         *     firmAddress: String
         *     firmCity: String
         *     firmCountry: String
         *     firmEmail: Optional[String]
         *     firmPhone: Optional[String]
         *     firmWeb: Optional[String]
         *     firmPib: String
         *     firmIsDeleted: Boolean
         *   }
         * ]
         *
         */
        function searchGroups(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteGroupHard (secured)
         * request - DeleteGroupHardRequest {
         *   groupId: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteGroupHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addSubGroup (secured)
         * request - SubGroupDto {
         *   id: Int
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function addSubGroup(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editSubGroup (secured)
         * request - SubGroupEditDto {
         *   id: Int
         *   group: Int
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function editSubGroup(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteSubGroup (secured)
         * request - SubGroupDto {
         *   id: Int
         *   name: String
         * }
         *
         * response - Unit
         *
         */
        function deleteSubGroup(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** subgroups (secured)
         * request - Unit
         *
         * response - List [
         *   SubgroupsResponse {
         *     id: Int
         *     groupId: Int
         *     name: String
         *     groupFirmName: String
         *   }
         * ]
         *
         */
        function subgroups() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** searchSubGroups (secured)
         * request - Unit
         *
         * response - List [
         *   SearchSubGroupsResponse {
         *     id: Int
         *     groupId: Int
         *     name: String
         *     groupFirmName: String
         *   }
         * ]
         *
         */
        function searchSubGroups(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteSubGroupHard (secured)
         * request - DeleteSubGroupHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteSubGroupHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addArticle (secured)
         * request - ArticleDto {
         *   code: String
         *   name: String
         *   status: RoleArticle
         *   subGroup: Int
         *   unitOfMeasure: Int
         * }
         *
         * response - Unit
         *
         */
        function addArticle(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editArticle (secured)
         * request - ArticleEditDto {
         *   id: Int
         *   code: String
         *   name: String
         *   status: RoleArticle
         *   subGroup: Int
         *   unitOfMeasure: Int
         * }
         *
         * response - Unit
         *
         */
        function editArticle(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** articles (secured)
         * request - Unit
         *
         * response - List [
         *   ArticlesResponse {
         *     id: Int
         *     name: String
         *     code: String
         *     status: RoleArticle
         *     subGroupId: Int
         *     subGroupName: String
         *     unitOfMeasureId: Int
         *     unitOfMeasureName: String
         *     unitOfMeasureShortName: Optional[String]
         *   }
         * ]
         *
         */
        function articles() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** searchArticles (secured)
         * request - Unit
         *
         * response - List [
         *   SearchArticlesResponse {
         *     id: Int
         *     name: String
         *     code: String
         *     status: RoleArticle
         *     subGroupId: Int
         *     subGroupName: String
         *     unitOfMeasureId: Int
         *     unitOfMeasureName: String
         *     unitOfMeasureShortName: Optional[String]
         *   }
         * ]
         *
         */
        function searchArticles(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addBussinesYear (secured)
         * request - BusinessYearDto {
         *   year: String
         *   isFinished: Boolean
         * }
         *
         * response - Unit
         *
         */
        function addBussinesYear(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editBussinesYear (secured)
         * request - BusinessYearIdDto {
         *   id: Int
         *   year: String
         *   isFinished: Boolean
         * }
         *
         * response - Unit
         *
         */
        function editBussinesYear(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** businessYears (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessYearsResponse {
         *     id: Int
         *     year: String
         *     isFinished: Boolean
         *   }
         * ]
         *
         */
        function businessYears() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** activeBusinessYear (secured)
         * request - Unit
         *
         * response - List [
         *   ActiveBusinessYearResponse {
         *     id: Int
         *     year: String
         *     isFinished: Boolean
         *   }
         * ]
         *
         */
        function activeBusinessYear() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** businessYearsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessYearsSearchResponse {
         *     id: Int
         *     year: String
         *     isFinished: Boolean
         *   }
         * ]
         *
         */
        function businessYearsSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** finishYear (secured)
         * request - YearIdDTO {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function finishYear(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addWarehouse (secured)
         * request - WarehouseDto {
         *   name: String
         *   code: String
         *   address: String
         *   city: String
         *   country: String
         * }
         *
         * response - Unit
         *
         */
        function addWarehouse(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editWarehouse (secured)
         * request - WarehouseEditDto {
         *   id: Int
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   code: String
         * }
         *
         * response - Unit
         *
         */
        function editWarehouse(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** warehouses (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesResponse {
         *     id: Int
         *     code: String
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *   }
         * ]
         *
         */
        function warehouses() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** warehousesSearch (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesSearchResponse {
         *     id: Int
         *     code: String
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *   }
         * ]
         *
         */
        function warehousesSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addWarehouseCard (secured)
         * request - WarehouseCardDto {
         *   balance: Optional[Decimal(10, 4)]
         *   warehouse: Int
         *   businessYear: Int
         *   article: Int
         * }
         *
         * response - Unit
         *
         */
        function addWarehouseCard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editWarehouseCard (secured)
         * request - WarehouseCardEditDto {
         *   id: Int
         *   balance: Optional[Decimal(10, 4)]
         * }
         *
         * response - Unit
         *
         */
        function editWarehouseCard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** warehousesCard (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesCardResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     averagePrice: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function warehousesCard() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** warehousesCardSearch (secured)
         * request - Unit
         *
         * response - List [
         *   WarehousesCardSearchResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     averagePrice: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function warehousesCardSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addBussinesPartner (secured)
         * request - BussinesPartnerDto {
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   email: Optional[String]
         *   phone: Optional[String]
         *   web: Optional[String]
         *   status: RoleBusinessPartner
         * }
         *
         * response - Unit
         *
         */
        function addBussinesPartner(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editBussinesPartner (secured)
         * request - BussinesPartnerEditDto {
         *   idPartner: Int
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   email: Optional[String]
         *   phone: Optional[String]
         *   web: Optional[String]
         *   status: RoleBusinessPartner
         * }
         *
         * response - Unit
         *
         */
        function editBussinesPartner(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** businessPartners (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessPartnersResponse {
         *     id: Int
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *     web: Optional[String]
         *     email: Optional[String]
         *     phone: Optional[String]
         *     status: RoleBusinessPartner
         *   }
         * ]
         *
         */
        function businessPartners() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** businessPartnersSearch (secured)
         * request - Unit
         *
         * response - List [
         *   BusinessPartnersSearchResponse {
         *     id: Int
         *     name: String
         *     address: String
         *     city: String
         *     country: String
         *     web: Optional[String]
         *     email: Optional[String]
         *     phone: Optional[String]
         *     status: RoleBusinessPartner
         *   }
         * ]
         *
         */
        function businessPartnersSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addUnitOfMeasure (secured)
         * request - UnitOfMeasureDto {
         *   name: String
         *   shortName: String
         * }
         *
         * response - Unit
         *
         */
        function addUnitOfMeasure(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editUnitOfMeasure (secured)
         * request - UnitOfMeasureEditDto {
         *   id: Int
         *   name: String
         *   shortName: String
         * }
         *
         * response - Unit
         *
         */
        function editUnitOfMeasure(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** unitOfMeasures (secured)
         * request - Unit
         *
         * response - List [
         *   UnitOfMeasuresResponse {
         *     id: Int
         *     name: String
         *     shortName: Optional[String]
         *   }
         * ]
         *
         */
        function unitOfMeasures() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** unitOfMeasuresSearch (secured)
         * request - Unit
         *
         * response - List [
         *   UnitOfMeasuresSearchResponse {
         *     id: Int
         *     name: String
         *     shortName: Optional[String]
         *   }
         * ]
         *
         */
        function unitOfMeasuresSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addDocument (secured)
         * request - DocumentDto {
         *   type: TypeDocument
         *   dateOfDelivery: Optional[DateTime]
         *   dateOfValue: Optional[DateTime]
         *   dateOfStatement: Optional[DateTime]
         *   businessYear: Int
         *   businessPartner: Optional[Int]
         *   warehouse: Int
         *   warehouseOut: Optional[Int]
         * }
         *
         * response - Unit
         *
         */
        function addDocument(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editDocument (secured)
         * request - DocumentEditDto {
         *   id: Int
         *   type: TypeDocument
         *   dateOfDelivery: Optional[DateTime]
         *   dateOfValue: Optional[DateTime]
         *   dateOfStatement: Optional[DateTime]
         *   paymentOfAccount: Optional[String]
         *   referenceNumber: Optional[String]
         *   businessYear: Int
         *   businessPartner: Int
         *   warehouse: Int
         * }
         *
         * response - Unit
         *
         */
        function editDocument(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** bookDocument (secured)
         * request - DocumentIdDto {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function bookDocument(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** reverseDocument (secured)
         * request - DocumentIdDto {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function reverseDocument(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** documents (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentResponseDto {
         *     id: Int
         *     dateOfValue: Optional[DateTime]
         *     dateOfDelivery: Optional[DateTime]
         *     dateOfStatement: Optional[DateTime]
         *     warehouseId: Int
         *     warehouseName: String
         *     businessPartnerId: Int
         *     name: String
         *     status: RoleBusinessPartner
         *     businessYearId: Int
         *     year: String
         *     warehouseIdOut: Int
         *     warehouseNameOut: String
         *   }
         * ]
         *
         */
        function documents(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** documentsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentsSearchResponse {
         *     id: Int
         *     dateOfValue: Optional[DateTime]
         *     dateOfDelivery: Optional[DateTime]
         *     warehouseId: Int
         *     warehouseName: String
         *     businessPartnerId: Int
         *     businessPartnerName: String
         *     businessYearId: Int
         *     businessYearYear: String
         *   }
         * ]
         *
         */
        function documentsSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addDocumentItem (secured)
         * request - DocumentItemDto {
         *   document: Int
         *   quantity: Decimal(10, 4)
         *   price: Decimal(10, 4)
         *   article: Int
         * }
         *
         * response - Unit
         *
         */
        function addDocumentItem(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** editDocumentItem (secured)
         * request - DocumentItemEditDto {
         *   id: Int
         *   document: Int
         *   quantity: Decimal(10, 4)
         *   price: Decimal(10, 4)
         *   article: Int
         * }
         *
         * response - Unit
         *
         */
        function editDocumentItem(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** documentItems (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentItemsResponse {
         *     id: Int
         *     quantity: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     documentId: Int
         *     articleId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *     unitOfMeasureName: String
         *   }
         * ]
         *
         */
        function documentItems(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** documentItemsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentItemsSearchResponse {
         *     id: Int
         *     quantity: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     documentId: Int
         *     articleId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *     unitOfMeasureName: String
         *   }
         * ]
         *
         */
        function documentItemsSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** analytics (secured)
         * request - Unit
         *
         * response - List [
         *   AnalyticsResponse {
         *     id: Int
         *     type: TypeAnalytics
         *     quantity: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     value: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *   }
         * ]
         *
         */
        function analytics(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** analyticsSearch (secured)
         * request - Unit
         *
         * response - List [
         *   AnalyticsSearchResponse {
         *     id: Int
         *     type: TypeAnalytics
         *     quantity: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     value: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *   }
         * ]
         *
         */
        function analyticsSearch(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** groupSubGroup (secured)
         * request - Unit
         *
         * response - List [
         *   GroupSubGroupResponse {
         *     id: Int
         *     groupId: Int
         *     name: String
         *     groupFirmName: String
         *   }
         * ]
         *
         */
        function groupSubGroup(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** subGroupArticle (secured)
         * request - Unit
         *
         * response - List [
         *   SubGroupArticleResponse {
         *     id: Int
         *     name: String
         *     code: String
         *     status: RoleArticle
         *     subGroupId: Int
         *     subGroupName: String
         *     unitOfMeasureId: Int
         *     unitOfMeasureName: String
         *     unitOfMeasureShortName: Optional[String]
         *   }
         * ]
         *
         */
        function subGroupArticle(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** warehouseWarehouseCard (secured)
         * request - Unit
         *
         * response - List [
         *   WarehouseWarehouseCardResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function warehouseWarehouseCard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** documentDocumentItem (secured)
         * request - Unit
         *
         * response - List [
         *   DocumentDocumentItemResponse {
         *     id: Int
         *     quantity: Decimal(10, 4)
         *     price: Decimal(10, 4)
         *     documentId: Int
         *     articleId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *   }
         * ]
         *
         */
        function documentDocumentItem(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** articleDocumentItem (secured)
         * request - Unit
         *
         * response - List [
         *   ArticleDocumentItemResponse {
         *     id: Int
         *     type: TypeDocument
         *     documentId: Int
         *     articleName: String
         *     articleUnitOfMeasureId: Int
         *   }
         * ]
         *
         */
        function articleDocumentItem(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** articleWarehouseCard (secured)
         * request - Unit
         *
         * response - List [
         *   ArticleWarehouseCardResponse {
         *     id: Int
         *     businessYearId: Int
         *     businessYearYear: String
         *     articleId: Int
         *     articleName: String
         *     articleCode: String
         *     input: Decimal(10, 4)
         *     output: Decimal(10, 4)
         *     balance: Decimal(10, 4)
         *     inValue: Decimal(10, 4)
         *     outValue: Decimal(10, 4)
         *     totalValue: Decimal(10, 4)
         *     warehouseId: Int
         *     warehouseName: String
         *   }
         * ]
         *
         */
        function articleWarehouseCard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteArticleHard (secured)
         * request - DeleteArticleHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteArticleHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteBusinessYearHard (secured)
         * request - DeleteBusinessYearHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteBusinessYearHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteWarehouseHard (secured)
         * request - DeleteWarehouseHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteWarehouseHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteBusinessPartnerHard (secured)
         * request - DeleteBusinessPartnerHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteBusinessPartnerHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteDocumentHard (secured)
         * request - DeleteDocumentHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteDocumentHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteDocumentItemHard (secured)
         * request - DeleteDocumentItemHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteDocumentItemHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** deleteUnitOfMeasureHard (secured)
         * request - DeleteUnitOfMeasureHardRequest {
         *   id: Int
         * }
         *
         * response - Unit
         *
         */
        function deleteUnitOfMeasureHard(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }
    }
})();
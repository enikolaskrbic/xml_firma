(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .service('adminApiMockService', adminApiMockService);

    adminApiMockService.$inject = ['$timeout'];

    function adminApiMockService($timeout) {

        return {
            firms: firms,
            addFirm: addFirm,
            users: users,
            addUser: addUser
        };

        /** firms (secured)
         * request - Unit
         *
         * response - List [
         *   FirmsResponse {
         *     id: Int
         *     name: String
         *     pib: String
         *     address: String
         *     city: String
         *     country: String
         *     phone: Optional[String]
         *     email: Optional[String]
         *     web: Optional[String]
         *   }
         * ]
         *
         */
        function firms() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addFirm (secured)
         * request - FirmDTO {
         *   name: String
         *   address: String
         *   city: String
         *   country: String
         *   email: String
         *   phone: String
         *   web: String
         *   pib: String
         * }
         *
         * response - Unit
         *
         */
        function addFirm(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** users (secured)
         * request - Unit
         *
         * response - List [
         *   UsersResponse {
         *     firstName: String
         *     lastName: String
         *     firmId: Optional[Int]
         *   }
         * ]
         *
         */
        function users() {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** addUser (secured)
         * request - UserDTO {
         *   firstName: String
         *   lastName: String
         *   email: String
         *   idFirm: Int
         * }
         *
         * response - Unit
         *
         */
        function addUser(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }
    }
})();
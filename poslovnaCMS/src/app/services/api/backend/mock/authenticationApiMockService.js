(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .service('authenticationApiMockService', authenticationApiMockService);

    authenticationApiMockService.$inject = ['$timeout'];

    function authenticationApiMockService($timeout) {

        return {
            signUp: signUp,
            signIn: signIn,
            forgotPassword: forgotPassword,
            resetPassword: resetPassword,
            verifyEmail: verifyEmail,
            changePassword: changePassword
        };

        /** signUp 
         * request - SignUpRequest {
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   email: String
         *   password: String
         * }
         *
         * response - SignUpResponse {
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function signUp(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** signIn 
         * request - SignInRequest {
         *   email: String
         *   password: String
         * }
         *
         * response - SignInResponse {
         *   accessToken: String
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function signIn(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** forgotPassword 
         * request - ForgotPasswordRequest {
         *   email: String
         * }
         *
         * response - Unit
         *
         */
        function forgotPassword(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** resetPassword 
         * request - ResetPasswordRequest {
         *   resetPasswordCode: String
         *   newPassword: String
         * }
         *
         * response - Unit
         *
         */
        function resetPassword(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** verifyEmail 
         * request - VerifyEmailRequest {
         *   emailVerificationCode: String
         * }
         *
         * response - VerifyEmailResponse {
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function verifyEmail(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }

        /** changePassword (secured)
         * request - ChangePasswordRequest {
         *   oldPassword: String
         *   newPassword: String
         * }
         *
         * response - ChangePasswordResponse {
         *   id: Int
         *   firstName: String
         *   lastName: String
         *   isDeleted: Boolean
         *   timetamp: DateTime
         *   firmId: Optional[Int]
         *   role: Role
         *   email: String
         * }
         *
         */
        function changePassword(model) {
            $timeout(function() {
                successCallback({
                    //TODO fill up mocked data values
                }, 500);
            });
        }
    }
})();
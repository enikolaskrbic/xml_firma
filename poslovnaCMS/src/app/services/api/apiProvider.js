(function() {
    'use strict';

    angular
        .module('poslovnaCMS')
        .provider('authenticationApi', authenticationApi)
        .config(authenticationApiProvider);

    function authenticationApi() {
        var isMocked = false;

        var $get = ['authenticationApiService', 'authenticationApiMockService', 'clientConfigurationValues', function(authenticationApiService, authenticationApiMockService, clientConfigurationValues) {
            if (this.isMocked) {
                return authenticationApiMockService;
            } else {
                if (clientConfigurationValues.remoteBackendUrl) {
                    authenticationApiService.init(clientConfigurationValues.remoteBackendUrl);
                }
                return authenticationApiService;
            }
        }];

        return {
            isMocked: isMocked,
            $get: $get
        };
    }

    function authenticationApiProvider(clientConfigurationValues, authenticationApiProvider) {
        authenticationApiProvider.isMocked = clientConfigurationValues.useServerMock;
    }

    angular
        .module('poslovnaCMS')
        .provider('adminApi', adminApi)
        .config(adminApiProvider);

    function adminApi() {
        var isMocked = false;

        var $get = ['adminApiService', 'adminApiMockService', 'clientConfigurationValues', function(adminApiService, adminApiMockService, clientConfigurationValues) {
            if (this.isMocked) {
                return adminApiMockService;
            } else {
                if (clientConfigurationValues.remoteBackendUrl) {
                    adminApiService.init(clientConfigurationValues.remoteBackendUrl);
                }
                return adminApiService;
            }
        }];

        return {
            isMocked: isMocked,
            $get: $get
        };
    }

    function adminApiProvider(clientConfigurationValues, adminApiProvider) {
        adminApiProvider.isMocked = clientConfigurationValues.useServerMock;
    }

    angular
        .module('poslovnaCMS')
        .provider('managerApi', managerApi)
        .config(managerApiProvider);

    function managerApi() {
        var isMocked = false;

        var $get = ['managerApiService', 'managerApiMockService', 'clientConfigurationValues', function(managerApiService, managerApiMockService, clientConfigurationValues) {
            if (this.isMocked) {
                return managerApiMockService;
            } else {
                if (clientConfigurationValues.remoteBackendUrl) {
                    managerApiService.init(clientConfigurationValues.remoteBackendUrl);
                }
                return managerApiService;
            }
        }];

        return {
            isMocked: isMocked,
            $get: $get
        };
    }

    function managerApiProvider(clientConfigurationValues, managerApiProvider) {
        managerApiProvider.isMocked = clientConfigurationValues.useServerMock;
    }

})();
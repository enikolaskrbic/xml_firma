(function() {
    'use strict';

    angular.module('poslovnaCMS').factory('modalWindows', modalService);

    modalService.$inject = ['$uibModal'];

    function modalService($uibModal) {

        function openModal(input, template, size) {
            return $uibModal.open({
                template: template,
                controller: 'ModalController',
                bindToController: true,
                backdrop: "static",
                size: size,
                resolve: {
                    input: function() {
                        if (input) {
                            return input;
                        }
                        return {};
                    }
                }
            });
        }

        return {

            openModalGroups: function(input) {
                return openModal(input,

                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">Choose a group</h3>' +
                    '</div>' +
                    '<div style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-groups reload="input.reload" close-modal="close" class="my-button" ></modal-groups>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalSubGroups: function(input) {
                return openModal(input,
                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">{{"MODAL_SUB_GROUPS" | translate}}</h3>' +
                    '</div>' +
                    '<div  style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-sub-groups reload="input.reload" close-modal="close" class="my-button" ></modal-sub-groups>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalUnitOfMeasures: function(input) {
                return openModal(input,
                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">{{"MODAL_UNIT_OF_MEASURES" | translate}}</h3>' +
                    '</div>' +
                    '<div  style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-unit-of-measures reload="input.reload" close-modal="close" class="my-button" ></modal-unit-of-measures>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalBussinesYear: function(input) {
                return openModal(input,
                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">{{"MODAL_BUSSINES_YEAR" | translate}}</h3>' +
                    '</div>' +
                    '<div style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-bussines-year reload="input.reload" close-modal="close" class="my-button" ></modal-bussines-year>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalArticles: function(input) {
                return openModal(input,
                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">{{"MODAL_ARTICLES" | translate}}</h3>' +
                    '</div>' +
                    '<div style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-articles reload="input.reload" close-modal="close" class="my-button"  ></modal-articles>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalWarehouse: function(input) {
                return openModal(input,
                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">{{"MODAL_WAREHOUSE" | translate}}</h3>' +
                    '</div>' +
                    '<div  style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-warehouse reload="input.reload" close-modal="close" class="my-button" ></modal-warehouse>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalBussinesPartner: function(input) {
                return openModal(input,
                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">{{"MODAL_BUSSINES_PARTNER" | translate}}</h3>' +
                    '</div>' +
                    '<div style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-bussines-partner reload="input.reload" close-modal="close" class="my-button"  ></modal-bussines-partner>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalPrelazFakture: function(input) {
                return openModal(input,
                    '<div class="modal-header">' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">Izaberite stranicu</h3>' +
                    '</div>' +
                    '<div style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-prelaz-fakture reload="input.reload" close-modal="close" class="my-button"  ></modal-prelaz-fakture>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            },

            openModalIzvod: function(input) {
                return openModal(input,
                    '<div class="modal-header" >' +
                    '<button type="button" data-ng-click="close()" data-dismiss="modal" class="btn btn-link close" title="{{\'MODAL_WINDOW_CLOSE\' | translate}}">' +
                    '&times;' +
                    '</button>' +
                    '<h3 class="modal-title">Izvod</h3>' +
                    '</div>' +
                    '<div style="max-height: 400px; overflow-y: auto; background-color: #F2F2F2">' +
                    '<modal-izvod reload="input.reload" close-modal="close" class="my-button"  ></modal-izvod>' +
                    '</div>' +
                    '<div class="modal-footer"></div>'
                );
            }

        };
    }

    angular.module('poslovnaCMS').controller('ModalController', ModalController);
    ModalController.$inject = ['$scope', 'input', 'eventBus', '$uibModalInstance'];

    function ModalController($scope, input, eventBus, $uibModalInstance) {
        $scope.input = input;
        $scope.close = function() {
            $uibModalInstance.dismiss();
        };

        eventBus.onEvent('ModalClose', function() {
            $uibModalInstance.close();
        });
    }
})();
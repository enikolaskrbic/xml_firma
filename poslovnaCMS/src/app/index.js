(function() {
    'use strict';

    angular
        .module('poslovnaCMS', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router',
            'ui.bootstrap', 'ngMessages', 'pascalprecht.translate', 'app.config','ngMaterial', 'angularSoap'
        ])
        .config(appConfig)

    .run(run);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function appConfig($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('basePage', {
                url: '/poslovna/base-page',
                templateUrl: 'src/app/components/pages/basePage.html',
                controller: 'BasePageController'
            })
            .state('basePage.aboutFirm', {
                url: '/poslovna/about-firm',
                templateUrl: 'src/app/components/pages/aboutFirm.html',
                controller: 'AboutFirmController'
            })
            .state('basePage.ulazneFakturePage', {
                url: '/poslovna/ulazne-faktures',
                templateUrl: 'src/app/components/pages/ulazneFakturePage.html',
                controller: 'UlazneFakturePageController'
            })
            .state('basePage.documentItemsUlaznePage', {
                url: '/poslovna/ulazne-faktures',
                templateUrl: 'src/app/components/pages/documentItemsUlaznePage.html',
                controller: 'DocumentItemsUlaznePageController'
            })
            .state('basePage.companiesPage', {
                url: '/poslovna/companies',
                templateUrl: 'src/app/components/pages/companiesPage.html',
                controller: 'CompaniesPageController'
            })

            .state('basePage.usersPage', {
                url: '/poslovna/users',
                templateUrl: 'src/app/components/pages/usersPage.html',
                controller: 'UsersPageController'
            })
            .state('basePage.addFirmPage', {
                    url: '/poslovna/admin/add-firm',
                    templateUrl: 'src/app/components/pages/addFirmPage.html',
                    controller: 'AddFirmPageController'
                })
            .state('basePage.addUserPage', {
                url: '/poslovna/admin/add-user',
                templateUrl: 'src/app/components/pages/addUserPage.html',
                controller: 'AddUserPageController'
            })
            .state('basePage.groupsPage', {
                url: '/poslovna/manager/firm/groups',
                templateUrl: 'src/app/components/pages/groupsPage.html',
                controller: 'GroupsPageController'
            })
            .state('basePage.businessYearsPage', {
                url: '/poslovna/manager/firm/businessYears',
                templateUrl: 'src/app/components/pages/businessYearsPage.html',
                controller: 'BusinessYearsPageController'
            })
            .state('basePage.subGroupPage', {
                url: '/poslovna/manager/firm/subgroups',
                templateUrl: 'src/app/components/pages/subGroupPage.html',
                controller: 'SubGroupPageController'
            })
            .state('basePage.articlePage', {
                url: '/poslovna/manager/firm/articles',
                templateUrl: 'src/app/components/pages/articlePage.html',
                controller: 'ArticlePageController'
            })
            .state('basePage.warehousePage', {
                url: '/poslovna/manager/firm/warehouses',
                templateUrl: 'src/app/components/pages/warehousePage.html',
                controller: 'WarehousePageController'
            })
            .state('basePage.warehouseCardPage', {
                url: '/poslovna/manager/firm/warehousesCard',
                templateUrl: 'src/app/components/pages/warehouseCardPage.html',
                controller: 'WarehouseCardPageController'
            })
            .state('basePage.businessPartnersPage', {
                url: '/poslovna/manager/firm/business-partners',
                templateUrl: 'src/app/components/pages/businessPartnersPage.html',
                controller: 'BusinessPartnersPageController'
            })
            .state('basePage.unitOfMeasuresPage', {
                url: '/poslovna/manager/firm/unitOfMeasures',
                templateUrl: 'src/app/components/pages/unitOfMeasuresPage.html',
                controller: 'UnitOfMeasuresPageController'
            })
            .state('basePage.documentsPage', {
                url: '/poslovna/manager/firm/documents',
                templateUrl: 'src/app/components/pages/documentsPage.html',
                controller: 'DocumentsPageController'
            })
            .state('basePage.documentItemsPage', {
                url: '/poslovna/manager/firm/documentItems',
                templateUrl: 'src/app/components/pages/documentItemsPage.html',
                controller: 'DocumentItemsPageController'
            })
            .state('basePage.analyticsPage', {
                url: '/poslovna/manager/firm/analytics',
                templateUrl: 'src/app/components/pages/analyticsPage.html',
                controller: 'AnalyticsPageController'
            })
            .state('signInPage', {
                url: '/poslovna/sign-in',
                templateUrl: 'src/app/components/pages/signInPage.html',
                controller: 'SignInPageController'
            })
            .state('signUpPage', {
                url: '/poslovna/sign-up',
                templateUrl: 'src/app/components/pages/signUpPage.html',
                controller: 'SignUpPageController'
            })
            .state('verifyEmailPage', {
                url: '/poslovna/verify-email/{emailVerificationCode}',
                templateUrl: 'src/app/components/pages/verifyEmailPage.html',
                controller: 'VerifyEmailPageController'
            })
            .state('forgotPasswordPage', {
                url: '/poslovna/forgot-password',
                templateUrl: 'src/app/components/pages/forgotPasswordPage.html',
                controller: 'ForgotPasswordPageController'
            })
            .state('resetPasswordPage', {
                url: '/poslovna/reset-password/{resetPasswordCode}',
                templateUrl: 'src/app/components/pages/resetPasswordPage.html',
                controller: 'ResetPasswordPageController'
            });

        $urlRouterProvider.otherwise('/poslovna/sign-in');
        //
        // $httpProvider.defaults.headers.common = {};
        // $httpProvider.defaults.headers.post = {};
        // $httpProvider.defaults.headers.put = {};
        // $httpProvider.defaults.headers.patch = {};
    }

    run.$inject = ['$rootScope', '$state', 'sessionService', '$log'];

    function run($rootScope, $state, sessionService, $log) {
        $rootScope.$on('$stateChangeStart', function(ev, to) {
            if (to.name === 'signInPage') {
                sessionService.clear();
            } else if (!sessionService.canUserAccessState(to.name)) {
                $log.warn('Unauthorized access to secured page, redirecting to signIn.');
                ev.preventDefault();
                $state.go('signInPage');
            }
            $rootScope.pageTitle = to.title;
        });
    }

})();